// ID_LWEX.H
// Created Sun Feb 26 17:54:03 2012
// Author: LinuxWolf - Team RayCAST

#ifndef ID_LWEX_H
#define ID_LWEX_H

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#if !defined(_WIN32)
#	include <stdint.h>
#	include <string.h>
#	include <stdarg.h>
#endif
#include "wl_types.h"
#include "fixedptc.h"

#define X(a) (a).v[0]
#define Y(a) (a).v[1]
#define Z(a) (a).v[2]
#define W(a) (a).v[3]
#define C(a, i) (a).v[i]

#define Sq(a) ((a) * (a))

/* access numbered component of vector */
#define C(a, i) (a).v[i]

/* access (row,col) component of matrix */
#define CM(a, r, c) (a).v[(r) * 3 + (c)]

#define LWEX_Hash_Valid(hash) ((hash).priv != NULL)

#define TileIsSolid(x, y) LWEX_TileIsWall(vec2i(x, y))
#define TileIsDoor(x, y) LWEX_TileIsDoor(vec2i(x, y))
#define TileIsPushWall(x, y) LWEX_TileIsPushWall(vec2i(x, y))
#define TileIsPushable(x, y) LWEX_TileIsPushable(vec2i(x, y))
#define TileIsElevator(x, y) LWEX_TileIsElevator(vec2i(x, y))
#define DoorLock(x, y) LWEX_GetDoor(vec2i(x, y))->lock

typedef enum dir4_e
{
    dir4_east, dir4_north, dir4_west, dir4_south
} dir4_t;

typedef enum dir6_e
{
    dir6_east, dir6_north, dir6_west, dir6_south,
    dir6_down, dir6_up,
} dir6_t;

typedef struct vec2i_s
{
    int v[2];
} vec2i_t;

typedef struct mat3f_s
{
    float v[3 * 3];
} mat3f_t;

typedef struct vec3f_s
{ 
    float v[3];
} vec3f_t;

typedef struct vec3fixed_s
{
    fixed v[3];
} vec3fixed_t;

typedef struct mat3fixed_s
{
    fixed v[3 * 3];
} mat3fixed_t;

typedef struct LWEX_Hash_s
{
    void *priv;
} LWEX_Hash_t;

int LWEX_GetTile(vec2i_t pos);

byte LWEX_GetMinimapRevealTile(vec2i_t pos);

void LWEX_SetMinimapRevealTile(vec2i_t pos, byte reveal);

bool LWEX_TileIsWall(vec2i_t pos);

bool LWEX_TileIsDoor(vec2i_t pos);

bool LWEX_NextTileIsWall(vec2i_t pos, dir4_t dir);

bool LWEX_TileIsPushWall(vec2i_t pos);

bool LWEX_TileIsPushable(vec2i_t pos);

bool LWEX_DoorIsOpen(vec2i_t pos);

bool LWEX_DoorIsVertical(vec2i_t pos);

bool LWEX_TileIsElevator(vec2i_t pos);

static inline dir4_t LWEX_GetDoorDir(vec2i_t pos)
{
    return LWEX_DoorIsVertical(pos) ? dir4_east : dir4_north;
}

static inline bool LWEX_TilePosInRange(vec2i_t pos)
{
    return X(pos) >= 0 && X(pos) < 64 && 
        Y(pos) >= 0 && Y(pos) < 64;
}

static inline dir4_t dir4_next(dir4_t dir, int n)
{
    return (dir4_t)(((int)dir + 4 + n) % 4);
}

static inline vec2i_t vec2i(int x, int y)
{
    vec2i_t p;
    p.v[0] = x;
    p.v[1] = y;
    return p;
}

static inline vec2i_t vec2i_neigh(vec2i_t pos, dir4_t dir)
{
    C(pos, dir & 1) += 1 - 2 * (dir >> 1);
    return pos;
}

static inline vec2i_t vec2i_swap(vec2i_t pos)
{
    return vec2i(Y(pos), X(pos));
}

static inline bool vec2i_equal(vec2i_t a, vec2i_t b)
{
    return X(a) == X(b) && Y(a) == Y(b);
}

vec3f_t vec3f(float x, float y, float z);

mat3f_t mat3f_ident(void);

mat3f_t mat3f_mult(mat3f_t a, mat3f_t b);

vec3f_t mat3f_mult_point(mat3f_t a, vec3f_t b);

static inline vec3fixed_t vec3fixed(int32_t x, int32_t y, int32_t z)
{
    vec3fixed_t a;
    C(a, 0) = x;
    C(a, 1) = y;
    C(a, 2) = z;
    return a;
}

static inline vec3fixed_t vec3fixed_zero(void)
{
    vec3fixed_t a = { 0, 0, 0 };
    return a;
}

static inline vec3fixed_t vec3fixed_add(vec3fixed_t a, vec3fixed_t b)
{
    return vec3fixed(
        C(a, 0) + C(b, 0), 
        C(a, 1) + C(b, 1),
        C(a, 2) + C(b, 2)
        );
}

static inline vec3fixed_t vec3fixed_subtract(vec3fixed_t a, vec3fixed_t b)
{
    return vec3fixed(
        C(a, 0) - C(b, 0), 
        C(a, 1) - C(b, 1),
        C(a, 2) - C(b, 2)
        );
}

static inline vec3fixed_t vec3fixed_negate(vec3fixed_t a)
{
    return vec3fixed(-C(a, 0), -C(a, 1), -C(a, 2));
}

static inline fixed vec3fixed_dot(vec3fixed_t a, vec3fixed_t b)
{
    return fixedpt_mul(C(a, 0), C(b, 0)) + fixedpt_mul(C(a, 1), C(b, 1)) + 
        fixedpt_mul(C(a, 2), C(b, 2));
}

static inline vec2i_t vec3fixed_trunc(vec3fixed_t a)
{
    return vec2i(fixedpt_toint(X(a)), fixedpt_toint(Y(a)));
}

static inline vec3fixed_t vec3fixed_from2i(vec2i_t a)
{
    return vec3fixed(fixedpt_fromint(X(a)), fixedpt_fromint(Y(a)), 0);
}

static inline vec3fixed_t vec3fixed_scale(vec3fixed_t a, fixed b)
{
    return vec3fixed(fixedpt_mul(X(a), b), fixedpt_mul(Y(a), b), fixedpt_mul(Z(a), b));
}

static inline vec3fixed_t vec3fixed_div(vec3fixed_t a, fixed b)
{
    return vec3fixed(fixedpt_div(X(a), b), fixedpt_div(Y(a), b), fixedpt_div(Z(a), b));
}

vec3fixed_t vec3fixed_anglevec(int32_t angle);

static inline fixed vec3fixed_length(vec3fixed_t a)
{
    return fixedpt_sqrt(vec3fixed_dot(a, a));
}

static inline vec3fixed_t vec3fixed_normalize(vec3fixed_t a)
{
    vec3fixed_t b;
    fixed n;

    n = fixedpt_sqrt(vec3fixed_dot(a, a));
    return n ? vec3fixed(fixedpt_div(X(a), n), 
        fixedpt_div(Y(a), n), fixedpt_div(Z(a), n)) : 
        a;
}

static inline vec3fixed_t vec3fixed_unit(vec3fixed_t a, vec3fixed_t b)
{
    return vec3fixed_normalize(vec3fixed_subtract(b, a));
}

mat3fixed_t mat3fixed_zero(void);

mat3fixed_t mat3fixed_ident(void);

mat3fixed_t mat3fixed_diag(fixed a, fixed b, fixed c);

vec3fixed_t mat3fixed_mult_point(mat3fixed_t a, vec3fixed_t b);

static inline vec3fixed_t mat3fixed_mult_point2d(mat3fixed_t a, vec3fixed_t b)
{
    return vec3fixed(
        fixedpt_mul(CM(a, 0, 0), C(b, 0)) + fixedpt_mul(CM(a, 0, 1), C(b, 1)),
        fixedpt_mul(CM(a, 1, 0), C(b, 0)) + fixedpt_mul(CM(a, 1, 1), C(b, 1)),
        0);
}

mat3fixed_t mat3fixed_rot(fixed angle);

mat3fixed_t mat3fixed_transpose(mat3fixed_t a);

int LWEX_Log2(int x);

static inline fixed LWEX_FixedLerp(fixed a, fixed b, fixed t)
{
    return fixedpt_mul(fixedpt_fromint(1) - t, a) + fixedpt_mul(t, b);
}

LWEX_Hash_t LWEX_Hash_New(void);

void LWEX_Hash_Add(LWEX_Hash_t hash, int key, int val);

int LWEX_Hash_Find(LWEX_Hash_t hash, int key);

#endif
