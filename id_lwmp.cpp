// ID_LWMP.CPP
// Created Sun Feb 26 17:54:03 2012
// Author: LinuxWolf - Team RayCAST

#include <iostream>
#include <stdio.h>
#include <assert.h>
#include "id_lwmp.h"
#include "id_lwex.h"
#include "wl_def.h"
#include "wl_shade.h"
#include "vec.h"
#include "fixedptc.h"

#define LWMP_MAX_ROT_FRAMES 9

// frames 0-7 are rotation frames
// frame 8 is a replacement (no rotation) frame
#define LWMP_REPLACEMENT_FRAME (LWMP_MAX_ROT_FRAMES - 1)

#define LWMP_DEFAULT_DATADIR "lwmp"

#define LWMP_CHECK_ADDR(addr, origin, start, size) \
    ((uintptr_t)(addr) - (uintptr_t)(origin) >= (uintptr_t)(start) && \
        (uintptr_t)(addr) < (uintptr_t)(start) + (size))

#define LWMP_MATRIXSTACK_MAX 10

#define LWMP_DOORWALL (PMSpriteStart-8)

#define LWMP_MINIMAP_DOTSTROBE_TICS 50
#define LWMP_MINIMAP_DOTSTROBE_MINFACTOR 20
#define LWMP_MINIMAP_DOTSTROBE_MAXFACTOR 64
#define LWMP_MINIMAP_DOTCOLOR 0x63
#define LWMP_MINIMAP_SHADECOLOR 0x66

#define LWMP_MINIMAP_WIDTHRATIO 1 / 4
#define LWMP_MINIMAP_HEIGHTRATIO 7 / 20

#define LWMP_MINIMAP_FLOORPIX 0x19
#define LWMP_MINIMAP_DOTRADIUS 180

#define LWMP_ITERMINIMAPPIX(draw) ; LWMP_IterMinimapPixCond(draw); LWMP_IterMinimapPixNext(draw)

typedef enum LWMP_Layout_e
{
    LWMP_LAYOUT_1x1,       
    LWMP_LAYOUT_2x1,
    LWMP_LAYOUT_1x2,
    LWMP_LAYOUT_3x1,
    LWMP_LAYOUT_1x3,
    LWMP_LAYOUT_2x2,
    LWMP_LAYOUT_4x1,
    LWMP_LAYOUT_1x4,
    LWMP_LAYOUT_MAX,
} LWMP_Layout_t;

typedef struct LWMP_Sprite_s
{
    t_compshape *shape;
    int id;
    int shapenum;
} LWMP_Sprite_t;

typedef vec_t(LWMP_Sprite_t) LWMP_SpriteSeq_t;

typedef struct LWMP_ChunkPic_s
{
    byte *picdata;
    int width;
    int height;
    int chunknum;
    SDL_Surface *surf;
} LWMP_ChunkPic_t;

typedef vec_t(LWMP_ChunkPic_t) LWMP_ChunkPicSeq_t;

typedef struct LWMP_WallMipmap_s
{
    int wallpic;
    byte *data;
} LWMP_WallMipmap_t;

typedef vec_t(LWMP_WallMipmap_t) LWMP_WallMipmapSeq_t;

typedef struct LWMP_MinimapDrawTile_s
{
    word statindex;
    byte *source;
    byte col;
    bool revealed;
    bool isDoor;
    bool statVisible;
    bool isPushWall;
    fixed doorpos;
    int doorAxis;
    bool isWall;
    bool applyShade;
    bool pushWallBack;
    vec3fixed_t pushWallDir;
} LWMP_MinimapDrawTile_t;

typedef struct LWMP_MinimapDraw_s
{
    byte dotcolor;
    vec3fixed_t LWMP_DECL(playerpoint);
    vec3fixed_t LWMP_DECL(playeranglevec);
    LWMP_MinimapDrawTile_t tiles[MAPSIZE][MAPSIZE];
    vec3fixed_t ptu, ptv;
    vec3fixed_t stepu, stepv;
    byte *vbuf;
    byte *vbuf_scanline;
    int x, y;
    int scx, scy;
    int scw, sch;
    vec2i_t pos;
    uint8_t *shadetable;
    vec3fixed_t corner;
    mat3fixed_t rotmat;
    mat3fixed_t LWMP_DECL(arrowrotmat);
    fixed boxstep, boxw, boxh;
    vec3fixed_t boxdims, scdims;
} LWMP_MinimapDraw_t;

typedef struct LWMP_MinimapState_s
{
    bool rotated;
    fixed zoombox;
    bool LWMP_DECL(minimap_expand);
    int minimap_tics;
    int LWMP_DECL(minimap_xoff);
    int LWMP_DECL(minimap_yoff);
    int LWMP_DECL(minimap_width);
    int LWMP_DECL(minimap_height);
} LWMP_MinimapState_t;

typedef struct LWMP_Minimap_s
{
    LWMP_MinimapState_t state;
} LWMP_Minimap_t;

typedef struct LWMP_s
{
    int instanceNum;
    int numPlayers;
    int layout;
    int controlsInstance;
    LWMP_SpriteSeq_t sprites;
    LWEX_Hash_t spriteHash;
    LWMP_ChunkPicSeq_t chunkPics;
    LWEX_Hash_t chunkPicHash;
    char dataDir[128];
    int resWidth;
    int resHeight;
    int nonSplit;
    mat3f_t matCur;
    mat3f_t matStack[LWMP_MATRIXSTACK_MAX];
    int matStackTop;
    LWMP_WallMipmapSeq_t wallMipmaps;
    LWEX_Hash_t wallMipmapHash;
    LWMP_Minimap_t minimap;
} LWMP_t;

static LWMP_t lwmp =
{
    0,                   // instanceNum
    1,                   // numPlayers
    LWMP_LAYOUT_1x1,     // layout
    0,                   // controlsInstance
    { 0 },               // sprites
    { NULL },            // spriteHash
    { 0 },               // chunkPics
    { NULL },            // chunkPicHash
    "lwmp",              // dataDir
    640,                 // resWidth
    400,                 // resHeight
    0,                   // nonSplit
    { 
        { 
            1, 0, 0, 
            0, 1, 0, 
            0, 0, 1,
        }
    },                   // matCur
    { 0 },               // matStack
    0,                   // matStackTop
    { 0 },               // wallMipmaps
    { NULL },            // wallMipmapHash
    {
        {
            false,           // rotated
            FP(16),          // zoombox
            { 0 },           // minimap_expand
            0,               // minimap_tics
            { 0 },           // minimap_xoff
            { 0 },           // minimap_yoff
            { 0 },           // minimap_width
            { 0 },           // minimap_height
        },                   // state
    },                       // minimap
};

static const char *lwmpLayoutStrs[LWMP_LAYOUT_MAX] =
{
    "1x1",
    "2x1",
    "1x2",
    "3x1",
    "1x3",
    "2x2",
    "4x1",
    "1x4",
};

// 
// This table converts sprite enumeration to string and is valid only
// if it matches the table defined in wl_def.h and APOGEE_1_0 and
// APOGEE_1_1 are NOT defined.
//
static const char *LWMP_SpriteStrs[SPR_NUMSPRITES] =
{
    "SPR_DEMO",
    "SPR_DEATHCAM",
//
// static sprites
//
    "SPR_STAT_0","SPR_STAT_1","SPR_STAT_2","SPR_STAT_3",
    "SPR_STAT_4","SPR_STAT_5","SPR_STAT_6","SPR_STAT_7",

    "SPR_STAT_8","SPR_STAT_9","SPR_STAT_10","SPR_STAT_11",
    "SPR_STAT_12","SPR_STAT_13","SPR_STAT_14","SPR_STAT_15",

    "SPR_STAT_16","SPR_STAT_17","SPR_STAT_18","SPR_STAT_19",
    "SPR_STAT_20","SPR_STAT_21","SPR_STAT_22","SPR_STAT_23",

    "SPR_STAT_24","SPR_STAT_25","SPR_STAT_26","SPR_STAT_27",
    "SPR_STAT_28","SPR_STAT_29","SPR_STAT_30","SPR_STAT_31",

    "SPR_STAT_32","SPR_STAT_33","SPR_STAT_34","SPR_STAT_35",
    "SPR_STAT_36","SPR_STAT_37","SPR_STAT_38","SPR_STAT_39",

    "SPR_STAT_40","SPR_STAT_41","SPR_STAT_42","SPR_STAT_43",
    "SPR_STAT_44","SPR_STAT_45","SPR_STAT_46","SPR_STAT_47",

#ifdef SPEAR
    "SPR_STAT_48","SPR_STAT_49","SPR_STAT_50","SPR_STAT_51",
#endif

//
// guard
//
    "SPR_GRD_S_1","SPR_GRD_S_2","SPR_GRD_S_3","SPR_GRD_S_4",
    "SPR_GRD_S_5","SPR_GRD_S_6","SPR_GRD_S_7","SPR_GRD_S_8",

    "SPR_GRD_W1_1","SPR_GRD_W1_2","SPR_GRD_W1_3","SPR_GRD_W1_4",
    "SPR_GRD_W1_5","SPR_GRD_W1_6","SPR_GRD_W1_7","SPR_GRD_W1_8",

    "SPR_GRD_W2_1","SPR_GRD_W2_2","SPR_GRD_W2_3","SPR_GRD_W2_4",
    "SPR_GRD_W2_5","SPR_GRD_W2_6","SPR_GRD_W2_7","SPR_GRD_W2_8",

    "SPR_GRD_W3_1","SPR_GRD_W3_2","SPR_GRD_W3_3","SPR_GRD_W3_4",
    "SPR_GRD_W3_5","SPR_GRD_W3_6","SPR_GRD_W3_7","SPR_GRD_W3_8",

    "SPR_GRD_W4_1","SPR_GRD_W4_2","SPR_GRD_W4_3","SPR_GRD_W4_4",
    "SPR_GRD_W4_5","SPR_GRD_W4_6","SPR_GRD_W4_7","SPR_GRD_W4_8",

    "SPR_GRD_PAIN_1","SPR_GRD_DIE_1","SPR_GRD_DIE_2","SPR_GRD_DIE_3",
    "SPR_GRD_PAIN_2","SPR_GRD_DEAD",

    "SPR_GRD_SHOOT1","SPR_GRD_SHOOT2","SPR_GRD_SHOOT3",

//
// dogs
//
    "SPR_DOG_W1_1","SPR_DOG_W1_2","SPR_DOG_W1_3","SPR_DOG_W1_4",
    "SPR_DOG_W1_5","SPR_DOG_W1_6","SPR_DOG_W1_7","SPR_DOG_W1_8",

    "SPR_DOG_W2_1","SPR_DOG_W2_2","SPR_DOG_W2_3","SPR_DOG_W2_4",
    "SPR_DOG_W2_5","SPR_DOG_W2_6","SPR_DOG_W2_7","SPR_DOG_W2_8",

    "SPR_DOG_W3_1","SPR_DOG_W3_2","SPR_DOG_W3_3","SPR_DOG_W3_4",
    "SPR_DOG_W3_5","SPR_DOG_W3_6","SPR_DOG_W3_7","SPR_DOG_W3_8",

    "SPR_DOG_W4_1","SPR_DOG_W4_2","SPR_DOG_W4_3","SPR_DOG_W4_4",
    "SPR_DOG_W4_5","SPR_DOG_W4_6","SPR_DOG_W4_7","SPR_DOG_W4_8",

    "SPR_DOG_DIE_1","SPR_DOG_DIE_2","SPR_DOG_DIE_3","SPR_DOG_DEAD",
    "SPR_DOG_JUMP1","SPR_DOG_JUMP2","SPR_DOG_JUMP3",



//
// ss
//
    "SPR_SS_S_1","SPR_SS_S_2","SPR_SS_S_3","SPR_SS_S_4",
    "SPR_SS_S_5","SPR_SS_S_6","SPR_SS_S_7","SPR_SS_S_8",

    "SPR_SS_W1_1","SPR_SS_W1_2","SPR_SS_W1_3","SPR_SS_W1_4",
    "SPR_SS_W1_5","SPR_SS_W1_6","SPR_SS_W1_7","SPR_SS_W1_8",

    "SPR_SS_W2_1","SPR_SS_W2_2","SPR_SS_W2_3","SPR_SS_W2_4",
    "SPR_SS_W2_5","SPR_SS_W2_6","SPR_SS_W2_7","SPR_SS_W2_8",

    "SPR_SS_W3_1","SPR_SS_W3_2","SPR_SS_W3_3","SPR_SS_W3_4",
    "SPR_SS_W3_5","SPR_SS_W3_6","SPR_SS_W3_7","SPR_SS_W3_8",

    "SPR_SS_W4_1","SPR_SS_W4_2","SPR_SS_W4_3","SPR_SS_W4_4",
    "SPR_SS_W4_5","SPR_SS_W4_6","SPR_SS_W4_7","SPR_SS_W4_8",

    "SPR_SS_PAIN_1","SPR_SS_DIE_1","SPR_SS_DIE_2","SPR_SS_DIE_3",
    "SPR_SS_PAIN_2","SPR_SS_DEAD",

    "SPR_SS_SHOOT1","SPR_SS_SHOOT2","SPR_SS_SHOOT3",

//
// mutant
//
    "SPR_MUT_S_1","SPR_MUT_S_2","SPR_MUT_S_3","SPR_MUT_S_4",
    "SPR_MUT_S_5","SPR_MUT_S_6","SPR_MUT_S_7","SPR_MUT_S_8",

    "SPR_MUT_W1_1","SPR_MUT_W1_2","SPR_MUT_W1_3","SPR_MUT_W1_4",
    "SPR_MUT_W1_5","SPR_MUT_W1_6","SPR_MUT_W1_7","SPR_MUT_W1_8",

    "SPR_MUT_W2_1","SPR_MUT_W2_2","SPR_MUT_W2_3","SPR_MUT_W2_4",
    "SPR_MUT_W2_5","SPR_MUT_W2_6","SPR_MUT_W2_7","SPR_MUT_W2_8",

    "SPR_MUT_W3_1","SPR_MUT_W3_2","SPR_MUT_W3_3","SPR_MUT_W3_4",
    "SPR_MUT_W3_5","SPR_MUT_W3_6","SPR_MUT_W3_7","SPR_MUT_W3_8",

    "SPR_MUT_W4_1","SPR_MUT_W4_2","SPR_MUT_W4_3","SPR_MUT_W4_4",
    "SPR_MUT_W4_5","SPR_MUT_W4_6","SPR_MUT_W4_7","SPR_MUT_W4_8",

    "SPR_MUT_PAIN_1","SPR_MUT_DIE_1","SPR_MUT_DIE_2","SPR_MUT_DIE_3",
    "SPR_MUT_PAIN_2","SPR_MUT_DIE_4","SPR_MUT_DEAD",

    "SPR_MUT_SHOOT1","SPR_MUT_SHOOT2","SPR_MUT_SHOOT3","SPR_MUT_SHOOT4",

//
// officer
//
    "SPR_OFC_S_1","SPR_OFC_S_2","SPR_OFC_S_3","SPR_OFC_S_4",
    "SPR_OFC_S_5","SPR_OFC_S_6","SPR_OFC_S_7","SPR_OFC_S_8",

    "SPR_OFC_W1_1","SPR_OFC_W1_2","SPR_OFC_W1_3","SPR_OFC_W1_4",
    "SPR_OFC_W1_5","SPR_OFC_W1_6","SPR_OFC_W1_7","SPR_OFC_W1_8",

    "SPR_OFC_W2_1","SPR_OFC_W2_2","SPR_OFC_W2_3","SPR_OFC_W2_4",
    "SPR_OFC_W2_5","SPR_OFC_W2_6","SPR_OFC_W2_7","SPR_OFC_W2_8",

    "SPR_OFC_W3_1","SPR_OFC_W3_2","SPR_OFC_W3_3","SPR_OFC_W3_4",
    "SPR_OFC_W3_5","SPR_OFC_W3_6","SPR_OFC_W3_7","SPR_OFC_W3_8",

    "SPR_OFC_W4_1","SPR_OFC_W4_2","SPR_OFC_W4_3","SPR_OFC_W4_4",
    "SPR_OFC_W4_5","SPR_OFC_W4_6","SPR_OFC_W4_7","SPR_OFC_W4_8",

    "SPR_OFC_PAIN_1","SPR_OFC_DIE_1","SPR_OFC_DIE_2","SPR_OFC_DIE_3",
    "SPR_OFC_PAIN_2","SPR_OFC_DIE_4","SPR_OFC_DEAD",

    "SPR_OFC_SHOOT1","SPR_OFC_SHOOT2","SPR_OFC_SHOOT3",

#ifndef SPEAR
//
// ghosts
//
    "SPR_BLINKY_W1","SPR_BLINKY_W2","SPR_PINKY_W1","SPR_PINKY_W2",
    "SPR_CLYDE_W1","SPR_CLYDE_W2","SPR_INKY_W1","SPR_INKY_W2",

//
// hans
//
    "SPR_BOSS_W1","SPR_BOSS_W2","SPR_BOSS_W3","SPR_BOSS_W4",
    "SPR_BOSS_SHOOT1","SPR_BOSS_SHOOT2","SPR_BOSS_SHOOT3","SPR_BOSS_DEAD",

    "SPR_BOSS_DIE1","SPR_BOSS_DIE2","SPR_BOSS_DIE3",

//
// schabbs
//
    "SPR_SCHABB_W1","SPR_SCHABB_W2","SPR_SCHABB_W3","SPR_SCHABB_W4",
    "SPR_SCHABB_SHOOT1","SPR_SCHABB_SHOOT2",

    "SPR_SCHABB_DIE1","SPR_SCHABB_DIE2","SPR_SCHABB_DIE3","SPR_SCHABB_DEAD",
    "SPR_HYPO1","SPR_HYPO2","SPR_HYPO3","SPR_HYPO4",

//
// fake
//
    "SPR_FAKE_W1","SPR_FAKE_W2","SPR_FAKE_W3","SPR_FAKE_W4",
    "SPR_FAKE_SHOOT","SPR_FIRE1","SPR_FIRE2",

    "SPR_FAKE_DIE1","SPR_FAKE_DIE2","SPR_FAKE_DIE3","SPR_FAKE_DIE4",
    "SPR_FAKE_DIE5","SPR_FAKE_DEAD",

//
// hitler
//
    "SPR_MECHA_W1","SPR_MECHA_W2","SPR_MECHA_W3","SPR_MECHA_W4",
    "SPR_MECHA_SHOOT1","SPR_MECHA_SHOOT2","SPR_MECHA_SHOOT3","SPR_MECHA_DEAD",

    "SPR_MECHA_DIE1","SPR_MECHA_DIE2","SPR_MECHA_DIE3",

    "SPR_HITLER_W1","SPR_HITLER_W2","SPR_HITLER_W3","SPR_HITLER_W4",
    "SPR_HITLER_SHOOT1","SPR_HITLER_SHOOT2","SPR_HITLER_SHOOT3","SPR_HITLER_DEAD",

    "SPR_HITLER_DIE1","SPR_HITLER_DIE2","SPR_HITLER_DIE3","SPR_HITLER_DIE4",
    "SPR_HITLER_DIE5","SPR_HITLER_DIE6","SPR_HITLER_DIE7",

//
// giftmacher
//
    "SPR_GIFT_W1","SPR_GIFT_W2","SPR_GIFT_W3","SPR_GIFT_W4",
    "SPR_GIFT_SHOOT1","SPR_GIFT_SHOOT2",

    "SPR_GIFT_DIE1","SPR_GIFT_DIE2","SPR_GIFT_DIE3","SPR_GIFT_DEAD",
#endif
//
// "Rocket", smoke and small explosion
//
    "SPR_ROCKET_1","SPR_ROCKET_2","SPR_ROCKET_3","SPR_ROCKET_4",
    "SPR_ROCKET_5","SPR_ROCKET_6","SPR_ROCKET_7","SPR_ROCKET_8",

    "SPR_SMOKE_1","SPR_SMOKE_2","SPR_SMOKE_3","SPR_SMOKE_4",
    "SPR_BOOM_1","SPR_BOOM_2","SPR_BOOM_3",

//
// Angel of Death's DeathSparks(tm)
//
#ifdef SPEAR
    "SPR_HROCKET_1","SPR_HROCKET_2","SPR_HROCKET_3","SPR_HROCKET_4",
    "SPR_HROCKET_5","SPR_HROCKET_6","SPR_HROCKET_7","SPR_HROCKET_8",

    "SPR_HSMOKE_1","SPR_HSMOKE_2","SPR_HSMOKE_3","SPR_HSMOKE_4",
    "SPR_HBOOM_1","SPR_HBOOM_2","SPR_HBOOM_3",

    "SPR_SPARK1","SPR_SPARK2","SPR_SPARK3","SPR_SPARK4",
#endif

#ifndef SPEAR
//
// gretel
//
    "SPR_GRETEL_W1","SPR_GRETEL_W2","SPR_GRETEL_W3","SPR_GRETEL_W4",
    "SPR_GRETEL_SHOOT1","SPR_GRETEL_SHOOT2","SPR_GRETEL_SHOOT3","SPR_GRETEL_DEAD",

    "SPR_GRETEL_DIE1","SPR_GRETEL_DIE2","SPR_GRETEL_DIE3",

//
// fat face
//
    "SPR_FAT_W1","SPR_FAT_W2","SPR_FAT_W3","SPR_FAT_W4",
    "SPR_FAT_SHOOT1","SPR_FAT_SHOOT2","SPR_FAT_SHOOT3","SPR_FAT_SHOOT4",

    "SPR_FAT_DIE1","SPR_FAT_DIE2","SPR_FAT_DIE3","SPR_FAT_DEAD",

//
// bj
//
    "SPR_BJ_W1","SPR_BJ_W2","SPR_BJ_W3","SPR_BJ_W4",
    "SPR_BJ_JUMP1","SPR_BJ_JUMP2","SPR_BJ_JUMP3","SPR_BJ_JUMP4",
#else
//
// THESE ARE FOR 'SPEAR OF DESTINY'
//

//
// Trans Grosse
//
    "SPR_TRANS_W1","SPR_TRANS_W2","SPR_TRANS_W3","SPR_TRANS_W4",
    "SPR_TRANS_SHOOT1","SPR_TRANS_SHOOT2","SPR_TRANS_SHOOT3","SPR_TRANS_DEAD",

    "SPR_TRANS_DIE1","SPR_TRANS_DIE2","SPR_TRANS_DIE3",

//
// Wilhelm
//
    "SPR_WILL_W1","SPR_WILL_W2","SPR_WILL_W3","SPR_WILL_W4",
    "SPR_WILL_SHOOT1","SPR_WILL_SHOOT2","SPR_WILL_SHOOT3","SPR_WILL_SHOOT4",

    "SPR_WILL_DIE1","SPR_WILL_DIE2","SPR_WILL_DIE3","SPR_WILL_DEAD",

//
// UberMutant
//
    "SPR_UBER_W1","SPR_UBER_W2","SPR_UBER_W3","SPR_UBER_W4",
    "SPR_UBER_SHOOT1","SPR_UBER_SHOOT2","SPR_UBER_SHOOT3","SPR_UBER_SHOOT4",

    "SPR_UBER_DIE1","SPR_UBER_DIE2","SPR_UBER_DIE3","SPR_UBER_DIE4",
    "SPR_UBER_DEAD",

//
// Death Knight
//
    "SPR_DEATH_W1","SPR_DEATH_W2","SPR_DEATH_W3","SPR_DEATH_W4",
    "SPR_DEATH_SHOOT1","SPR_DEATH_SHOOT2","SPR_DEATH_SHOOT3","SPR_DEATH_SHOOT4",

    "SPR_DEATH_DIE1","SPR_DEATH_DIE2","SPR_DEATH_DIE3","SPR_DEATH_DIE4",
    "SPR_DEATH_DIE5","SPR_DEATH_DIE6","SPR_DEATH_DEAD",

//
// Ghost
//
    "SPR_SPECTRE_W1","SPR_SPECTRE_W2","SPR_SPECTRE_W3","SPR_SPECTRE_W4",
    "SPR_SPECTRE_F1","SPR_SPECTRE_F2","SPR_SPECTRE_F3","SPR_SPECTRE_F4",

//
// Angel of Death
//
    "SPR_ANGEL_W1","SPR_ANGEL_W2","SPR_ANGEL_W3","SPR_ANGEL_W4",
    "SPR_ANGEL_SHOOT1","SPR_ANGEL_SHOOT2","SPR_ANGEL_TIRED1","SPR_ANGEL_TIRED2",

    "SPR_ANGEL_DIE1","SPR_ANGEL_DIE2","SPR_ANGEL_DIE3","SPR_ANGEL_DIE4",
    "SPR_ANGEL_DIE5","SPR_ANGEL_DIE6","SPR_ANGEL_DIE7","SPR_ANGEL_DEAD",

#endif

//
// player attack frames
//
    "SPR_KNIFEREADY","SPR_KNIFEATK1","SPR_KNIFEATK2","SPR_KNIFEATK3",
    "SPR_KNIFEATK4",

    "SPR_PISTOLREADY","SPR_PISTOLATK1","SPR_PISTOLATK2","SPR_PISTOLATK3",
    "SPR_PISTOLATK4",

    "SPR_MACHINEGUNREADY","SPR_MACHINEGUNATK1","SPR_MACHINEGUNATK2","SPR_MACHINEGUNATK3",
    "SPR_MACHINEGUNATK4",

    "SPR_CHAINREADY","SPR_CHAINATK1","SPR_CHAINATK2","SPR_CHAINATK3",
    "SPR_CHAINATK4",

    "SPR_TANKGUNREADY","SPR_TANKGUNATK1","SPR_TANKGUNATK2","SPR_TANKGUNATK3",
    "SPR_TANKGUNATK4",

    "SPR_TANKWHEELREADY","SPR_TANKWHEELATK1","SPR_TANKWHEELATK2","SPR_TANKWHEELATK3",
    "SPR_TANKWHEELATK4",

    "SPR_MG42READY","SPR_MG42ATK1","SPR_MG42ATK2","SPR_MG42ATK3",
    "SPR_MG42ATK4",

    "SPR_FLAGREADY","SPR_FLAGATK1","SPR_FLAGATK2","SPR_FLAGATK3",
    "SPR_FLAGATK4",

    "SPR_FISTSREADY","SPR_FISTSATK1","SPR_FISTSATK2","SPR_FISTSATK3",
    "SPR_FISTSATK4",
    
    "SPR_BJ_KNIFEREADY", "SPR_BJ_KNIFEATK1", "SPR_BJ_KNIFEATK2", "SPR_BJ_KNIFEATK3",
    "SPR_BJ_KNIFEATK4",

    "SPR_BJ_PISTOLREADY", "SPR_BJ_PISTOLATK1", "SPR_BJ_PISTOLATK2", "SPR_BJ_PISTOLATK3",
    "SPR_BJ_PISTOLATK4",

    "SPR_BJ_MACHINEGUNREADY", "SPR_BJ_MACHINEGUNATK1", "SPR_BJ_MACHINEGUNATK2", "SPR_BJ_MACHINEGUNATK3",
    "SPR_BJ_MACHINEGUNATK4",

    "SPR_BJ_CHAINREADY", "SPR_BJ_CHAINATK1", "SPR_BJ_CHAINATK2", "SPR_BJ_CHAINATK3",
    "SPR_BJ_CHAINATK4",

    "SPR_BJ_TANKGUNREADY", "SPR_BJ_TANKGUNATK1", "SPR_BJ_TANKGUNATK2", "SPR_BJ_TANKGUNATK3",
    "SPR_BJ_TANKGUNATK4",

    "SPR_BJ_TANKWHEELREADY", "SPR_BJ_TANKWHEELATK1", "SPR_BJ_TANKWHEELATK2", "SPR_BJ_TANKWHEELATK3",
    "SPR_BJ_TANKWHEELATK4",

    "SPR_BJ_MG42READY", "SPR_BJ_MG42ATK1", "SPR_BJ_MG42ATK2", "SPR_BJ_MG42ATK3",
    "SPR_BJ_MG42ATK4",

    "SPR_BJ_FLAGREADY", "SPR_BJ_FLAGATK1", "SPR_BJ_FLAGATK2", "SPR_BJ_FLAGATK3",
    "SPR_BJ_FLAGATK4",

    "SPR_BJ_FISTSREADY", "SPR_BJ_FISTSATK1", "SPR_BJ_FISTSATK2", "SPR_BJ_FISTSATK3",
    "SPR_BJ_FISTSATK4",

    "SPR_BJ_STAND", "SPR_BJ_M1", "SPR_BJ_M2", "SPR_BJ_M3", "SPR_BJ_M4",
    "SPR_BJ_PAIN_1", "SPR_BJ_PAIN_2",

    "SPR_BJ_STAND_KNIFE", "SPR_BJ_M1_KNIFE", "SPR_BJ_M2_KNIFE", "SPR_BJ_M3_KNIFE", "SPR_BJ_M4_KNIFE",
    "SPR_BJ_PAIN_1_KNIFE", "SPR_BJ_PAIN_2_KNIFE",

    "SPR_BJ_STAND_PISTOL", "SPR_BJ_M1_PISTOL", "SPR_BJ_M2_PISTOL", "SPR_BJ_M3_PISTOL", "SPR_BJ_M4_PISTOL",
    "SPR_BJ_PAIN_1_PISTOL", "SPR_BJ_PAIN_2_PISTOL",

    "SPR_BJ_STAND_CHAIN", "SPR_BJ_M1_CHAIN", "SPR_BJ_M2_CHAIN", "SPR_BJ_M3_CHAIN", "SPR_BJ_M4_CHAIN",
    "SPR_BJ_PAIN_1_CHAIN", "SPR_BJ_PAIN_2_CHAIN",

    "SPR_BJ_STAND_TANKGUN", "SPR_BJ_M1_TANKGUN", "SPR_BJ_M2_TANKGUN", "SPR_BJ_M3_TANKGUN", "SPR_BJ_M4_TANKGUN",
    "SPR_BJ_PAIN_1_TANKGUN", "SPR_BJ_PAIN_2_TANKGUN",

    "SPR_BJ_STAND_TANKWHEEL", "SPR_BJ_M1_TANKWHEEL", "SPR_BJ_M2_TANKWHEEL", "SPR_BJ_M3_TANKWHEEL", "SPR_BJ_M4_TANKWHEEL",
    "SPR_BJ_PAIN_1_TANKWHEEL", "SPR_BJ_PAIN_2_TANKWHEEL",

    "SPR_BJ_STAND_MG42", "SPR_BJ_M1_MG42", "SPR_BJ_M2_MG42", "SPR_BJ_M3_MG42", "SPR_BJ_M4_MG42",
    "SPR_BJ_PAIN_1_MG42", "SPR_BJ_PAIN_2_MG42",

    "SPR_BJ_STAND_FLAG", "SPR_BJ_M1_FLAG", "SPR_BJ_M2_FLAG", "SPR_BJ_M3_FLAG", "SPR_BJ_M4_FLAG",
    "SPR_BJ_PAIN_1_FLAG", "SPR_BJ_PAIN_2_FLAG",

    "SPR_BJ_STAND_FISTS", "SPR_BJ_M1_FISTS", "SPR_BJ_M2_FISTS", "SPR_BJ_M3_FISTS", "SPR_BJ_M4_FISTS",
    "SPR_BJ_PAIN_1_FISTS", "SPR_BJ_PAIN_2_FISTS",

    "SPR_BJ_DIE_1", "SPR_BJ_DIE_2", "SPR_BJ_DIE_3", "SPR_BJ_DEAD",
    "SPR_BJ_BACKPACK","SPR_BJ_FALL",

    "SPR_RESPAWN_1", "SPR_RESPAWN_2", "SPR_RESPAWN_3", "SPR_RESPAWN_4",
    "SPR_RESPAWN_5", 

    "SPR_BJ_CGIB_DEAD", "SPR_BJ_CGIB_DIE_1", "SPR_BJ_CGIB_DIE_2", 
    "SPR_BJ_CGIB_DIE_3",

    "SPR_BJ_RGIB_DEAD", "SPR_BJ_RGIB_DIE_1", "SPR_BJ_RGIB_DIE_2", 
    "SPR_BJ_RGIB_DIE_3",

    "SPR_DOG_CGIB_DEAD", "SPR_DOG_CGIB_DIE_1", "SPR_DOG_CGIB_DIE_2", 
    "SPR_DOG_CGIB_DIE_3",

    "SPR_DOG_RGIB_DEAD", "SPR_DOG_RGIB_DIE_1", "SPR_DOG_RGIB_DIE_2", 
    "SPR_DOG_RGIB_DIE_3",

    "SPR_GRD_CGIB_DEAD", "SPR_GRD_CGIB_DIE_1", "SPR_GRD_CGIB_DIE_2", 
    "SPR_GRD_CGIB_DIE_3",

    "SPR_GRD_RGIB_DEAD", "SPR_GRD_RGIB_DIE_1", "SPR_GRD_RGIB_DIE_2", 
    "SPR_GRD_RGIB_DIE_3",

    "SPR_MUT_CGIB_DEAD", "SPR_MUT_CGIB_DIE_1", "SPR_MUT_CGIB_DIE_2", 
    "SPR_MUT_CGIB_DIE_3", "SPR_MUT_CGIB_DIE_4",

    "SPR_MUT_RGIB_DEAD", "SPR_MUT_RGIB_DIE_1", "SPR_MUT_RGIB_DIE_2", 
    "SPR_MUT_RGIB_DIE_3", "SPR_MUT_RGIB_DIE_4",

    "SPR_OFC_CGIB_DEAD", "SPR_OFC_CGIB_DIE_1", "SPR_OFC_CGIB_DIE_2", 
    "SPR_OFC_CGIB_DIE_3", "SPR_OFC_CGIB_DIE_4",

    "SPR_OFC_RGIB_DEAD", "SPR_OFC_RGIB_DIE_1", "SPR_OFC_RGIB_DIE_2",
    "SPR_OFC_RGIB_DIE_3", "SPR_OFC_RGIB_DIE_4",

    "SPR_SS_CGIB_DEAD", "SPR_SS_CGIB_DIE_1", "SPR_SS_CGIB_DIE_2", 
    "SPR_SS_CGIB_DIE_3",

    "SPR_SS_RGIB_DEAD", "SPR_SS_RGIB_DIE_1", "SPR_SS_RGIB_DIE_2", 
    "SPR_SS_RGIB_DIE_3",

    "SPR_WBARREL_STAND",
    "SPR_WBARREL_DIE1","SPR_WBARREL_DIE2","SPR_WBARREL_DIE3","SPR_WBARREL_DIE4",
    "SPR_WBARREL_DEAD",

    "SPR_TBARREL_STAND",
    "SPR_TBARREL_DIE1","SPR_TBARREL_DIE2","SPR_TBARREL_DIE3",
    "SPR_TBARREL_DEAD",

    "SPR_EBARREL_STAND",
    "SPR_EBARREL_DIE1","SPR_EBARREL_DIE2","SPR_EBARREL_DIE3",
    "SPR_EBARREL_DEAD",

    "SPR_TANK","SPR_TANK_NOGUN",

    "SPR_MG42",

    "SPR_DEFUSEBOMB_IDLE1","SPR_DEFUSEBOMB_IDLE2","SPR_DEFUSEBOMB_DEFUSED",

    "SPR_HEART1","SPR_HEART2",

    "SPR_FLAG_ANIM1","SPR_FLAG_ANIM2","SPR_FLAG_ANIM3","SPR_FLAG_ANIM4",
    "SPR_FLAG_ANIM5","SPR_FLAG_ANIM6",

    "SPR_HPILLAR_IDLE","SPR_HPILLAR_SKULL1","SPR_HPILLAR_SKULL2",

    "SPR_HSOUL1","SPR_HSOUL2","SPR_HSOUL3","SPR_HSOUL4","SPR_HSOUL5",

    "SPR_BLOODWELL","SPR_BLOODSINK","SPR_EMPTYSINK","SPR_BLOODKITCHEN",
    "SPR_EMPTYKITCHEN",

    "SPR_RAMPAGEAMMO",
};

// 
// This table converts pic enumeration to string and is valid only
// if it matches the table defined in gfxv_apo.h, gfxv_wl6.h or gfxv_sod.h and
// APOGEE_1_0, APOGEE_1_1 and APOGEE_1_2 are NOT defined.
//
#ifndef SPEAR
    #if defined(UPLOAD) || !defined(GOODTIMES) 
        // gfxv_apo.h
        static const char *LWMP_ChunkPicStrings[ENUMEND_EXTRA] =
        {
            "Dummy",                     // 0
            "Dummy",                     // 1
            "Dummy",                     // 2
            // Lump Start
            "H_BJPIC",                   // 3
            "H_CASTLEPIC",               // 4
            "H_KEYBOARDPIC",             // 5
            "H_JOYPIC",                  // 6
            "H_HEALPIC",                 // 7
            "H_TREASUREPIC",             // 8
            "H_GUNPIC",                  // 9
            "H_KEYPIC",                  // 10
            "H_BLAZEPIC",                // 11
            "H_WEAPON1234PIC",           // 12
            "H_WOLFLOGOPIC",             // 13
            "H_VISAPIC",                 // 14
            "H_MCPIC",                   // 15
            "H_IDLOGOPIC",               // 16
            "H_TOPWINDOWPIC",            // 17
            "H_LEFTWINDOWPIC",           // 18
            "H_RIGHTWINDOWPIC",          // 19
            "H_BOTTOMINFOPIC",           // 20
            "H_SPEARADPIC",              // 21
            // Lump Start
            "C_OPTIONSPIC",              // 22
            "C_CURSOR1PIC",              // 23
            "C_CURSOR2PIC",              // 24
            "C_NOTSELECTEDPIC",          // 25
            "C_SELECTEDPIC",             // 26
            "C_FXTITLEPIC",              // 27
            "C_DIGITITLEPIC",            // 28
            "C_MUSICTITLEPIC",           // 29
            "C_MOUSELBACKPIC",           // 30
            "C_BABYMODEPIC",             // 31
            "C_EASYPIC",                 // 32
            "C_NORMALPIC",               // 33
            "C_HARDPIC",                 // 34
            "C_LOADSAVEDISKPIC",         // 35
            "C_DISKLOADING1PIC",         // 36
            "C_DISKLOADING2PIC",         // 37
            "C_CONTROLPIC",              // 38
            "C_CUSTOMIZEPIC",            // 39
            "C_LOADGAMEPIC",             // 40
            "C_SAVEGAMEPIC",             // 41
            "C_EPISODE1PIC",             // 42
            "C_EPISODE2PIC",             // 43
            "C_EPISODE3PIC",             // 44
            "C_EPISODE4PIC",             // 45
            "C_EPISODE5PIC",             // 46
            "C_EPISODE6PIC",             // 47
            "C_CODEPIC",                 // 48
            "C_TIMECODEPIC",             // 49
            "C_LEVELPIC",                // 50
            "C_NAMEPIC",                 // 51
            "C_SCOREPIC",                // 52
            "C_JOY1PIC",                 // 53
            "C_JOY2PIC",                 // 54
            // Lump Start
            "L_GUYPIC",                  // 55
            "L_COLONPIC",                // 56
            "L_NUM0PIC",                 // 57
            "L_NUM1PIC",                 // 58
            "L_NUM2PIC",                 // 59
            "L_NUM3PIC",                 // 60
            "L_NUM4PIC",                 // 61
            "L_NUM5PIC",                 // 62
            "L_NUM6PIC",                 // 63
            "L_NUM7PIC",                 // 64
            "L_NUM8PIC",                 // 65
            "L_NUM9PIC",                 // 66
            "L_PERCENTPIC",              // 67
            "L_APIC",                    // 68
            "L_BPIC",                    // 69
            "L_CPIC",                    // 70
            "L_DPIC",                    // 71
            "L_EPIC",                    // 72
            "L_FPIC",                    // 73
            "L_GPIC",                    // 74
            "L_HPIC",                    // 75
            "L_IPIC",                    // 76
            "L_JPIC",                    // 77
            "L_KPIC",                    // 78
            "L_LPIC",                    // 79
            "L_MPIC",                    // 80
            "L_NPIC",                    // 81
            "L_OPIC",                    // 82
            "L_PPIC",                    // 83
            "L_QPIC",                    // 84
            "L_RPIC",                    // 85
            "L_SPIC",                    // 86
            "L_TPIC",                    // 87
            "L_UPIC",                    // 88
            "L_VPIC",                    // 89
            "L_WPIC",                    // 90
            "L_XPIC",                    // 91
            "L_YPIC",                    // 92
            "L_ZPIC",                    // 93
            "L_EXPOINTPIC",              // 94
            "L_APOSTROPHEPIC",           // 95
            "L_GUY2PIC",                 // 96
            "L_BJWINSPIC",               // 97
            "STATUSBARPIC",              // 98
            "TITLEPIC",                  // 99
            "PG13PIC",                   // 100
            "CREDITSPIC",                // 101
            "HIGHSCORESPIC",             // 102
            // Lump Start
            "KNIFEPIC",                  // 103
            "GUNPIC",                    // 104
            "MACHINEGUNPIC",             // 105
            "GATLINGGUNPIC",             // 106
            "NOKEYPIC",                  // 107
            "GOLDKEYPIC",                // 108
            "SILVERKEYPIC",              // 109
            "N_BLANKPIC",                // 110
            "N_0PIC",                    // 111
            "N_1PIC",                    // 112
            "N_2PIC",                    // 113
            "N_3PIC",                    // 114
            "N_4PIC",                    // 115
            "N_5PIC",                    // 116
            "N_6PIC",                    // 117
            "N_7PIC",                    // 118
            "N_8PIC",                    // 119
            "N_9PIC",                    // 120
            "FACE1APIC",                 // 121
            "FACE1BPIC",                 // 122
            "FACE1CPIC",                 // 123
            "FACE2APIC",                 // 124
            "FACE2BPIC",                 // 125
            "FACE2CPIC",                 // 126
            "FACE3APIC",                 // 127
            "FACE3BPIC",                 // 128
            "FACE3CPIC",                 // 129
            "FACE4APIC",                 // 130
            "FACE4BPIC",                 // 131
            "FACE4CPIC",                 // 132
            "FACE5APIC",                 // 133
            "FACE5BPIC",                 // 134
            "FACE5CPIC",                 // 135
            "FACE6APIC",                 // 136
            "FACE6BPIC",                 // 137
            "FACE6CPIC",                 // 138
            "FACE7APIC",                 // 139
            "FACE7BPIC",                 // 140
            "FACE7CPIC",                 // 141
            "FACE8APIC",                 // 142
            "GOTGATLINGPIC",             // 143
            "MUTANTBJPIC",               // 144
            "PAUSEDPIC",                 // 145
            "GETPSYCHEDPIC",             // 146

            "TILE8",                     // 147

            "ORDERSCREEN",               // 148
            "ERRORSCREEN",               // 149
            "T_HELPART",                 // 150
            "T_DEMO0",                   // 151
            "T_DEMO1",                   // 152
            "T_DEMO2",                   // 153
            "T_DEMO3",                   // 154
            "T_ENDART1",                 // 155
            "T_ENDART2",                 // 156
            "T_ENDART3",                 // 157
            "T_ENDART4",                 // 158
            "T_ENDART5",                 // 159
            "T_ENDART6",                 // 160
            "L_GUYPIC_KNIFE",            // 161
            "L_GUYPIC_KNIFE_HURT1",      // 162
            "L_GUYPIC_KNIFE_HURT2",      // 163
            "L_GUYPIC_KNIFE_HURT3",      // 164
            "L_GUYPIC_KNIFE_HURT4",      // 165
            "L_GUYPIC_KNIFE_HURT5",      // 166
            "L_GUYPIC_KNIFE_HURT6",      // 167
            "L_GUYPIC_KNIFE_HURT7",      // 168
            "L_GUYPIC_PISTOL",           // 169
            "L_GUYPIC_PISTOL_HURT1",     // 170
            "L_GUYPIC_PISTOL_HURT2",     // 171
            "L_GUYPIC_PISTOL_HURT3",     // 172
            "L_GUYPIC_PISTOL_HURT4",     // 173
            "L_GUYPIC_PISTOL_HURT5",     // 174
            "L_GUYPIC_PISTOL_HURT6",     // 175
            "L_GUYPIC_PISTOL_HURT7",     // 176
            "L_GUYPIC_MP40",             // 177
            "L_GUYPIC_MP40_HURT1",       // 178
            "L_GUYPIC_MP40_HURT2",       // 179
            "L_GUYPIC_MP40_HURT3",       // 180
            "L_GUYPIC_MP40_HURT4",       // 181
            "L_GUYPIC_MP40_HURT5",       // 182
            "L_GUYPIC_MP40_HURT6",       // 183
            "L_GUYPIC_MP40_HURT7",       // 184
            "L_GUYPIC_CHAIN",            // 185
            "L_GUYPIC_CHAIN_HURT1",      // 186
            "L_GUYPIC_CHAIN_HURT2",      // 187
            "L_GUYPIC_CHAIN_HURT3",      // 188
            "L_GUYPIC_CHAIN_HURT4",      // 189
            "L_GUYPIC_CHAIN_HURT5",      // 190
            "L_GUYPIC_CHAIN_HURT6",      // 191
            "L_GUYPIC_CHAIN_HURT7",      // 192
            "L_GUY2PIC_KNIFE",           // 193
            "L_GUY2PIC_KNIFE_HURT1",     // 194
            "L_GUY2PIC_KNIFE_HURT2",     // 195
            "L_GUY2PIC_KNIFE_HURT3",     // 196
            "L_GUY2PIC_KNIFE_HURT4",     // 197
            "L_GUY2PIC_KNIFE_HURT5",     // 198
            "L_GUY2PIC_KNIFE_HURT6",     // 199
            "L_GUY2PIC_KNIFE_HURT7",     // 200
            "L_GUY2PIC_PISTOL",          // 201
            "L_GUY2PIC_PISTOL_HURT1",    // 202
            "L_GUY2PIC_PISTOL_HURT2",    // 203
            "L_GUY2PIC_PISTOL_HURT3",    // 204
            "L_GUY2PIC_PISTOL_HURT4",    // 205
            "L_GUY2PIC_PISTOL_HURT5",    // 206
            "L_GUY2PIC_PISTOL_HURT6",    // 207
            "L_GUY2PIC_PISTOL_HURT7",    // 208
            "L_GUY2PIC_MP40",            // 209
            "L_GUY2PIC_MP40_HURT1",      // 210
            "L_GUY2PIC_MP40_HURT2",      // 211
            "L_GUY2PIC_MP40_HURT3",      // 212
            "L_GUY2PIC_MP40_HURT4",      // 213
            "L_GUY2PIC_MP40_HURT5",      // 214
            "L_GUY2PIC_MP40_HURT6",      // 215
            "L_GUY2PIC_MP40_HURT7",      // 216
            "L_GUY2PIC_CHAIN",           // 217
            "L_GUY2PIC_CHAIN_HURT1",     // 218
            "L_GUY2PIC_CHAIN_HURT2",     // 219
            "L_GUY2PIC_CHAIN_HURT3",     // 220
            "L_GUY2PIC_CHAIN_HURT4",     // 221
            "L_GUY2PIC_CHAIN_HURT5",     // 222
            "L_GUY2PIC_CHAIN_HURT6",     // 223
            "L_GUY2PIC_CHAIN_HURT7",     // 224
            "L_BJWINSPIC_KNIFE",         // 225
            "L_BJWINSPIC_KNIFE_HURT1",   // 226
            "L_BJWINSPIC_KNIFE_HURT2",   // 227
            "L_BJWINSPIC_KNIFE_HURT3",   // 228
            "L_BJWINSPIC_KNIFE_HURT4",   // 229
            "L_BJWINSPIC_KNIFE_HURT5",   // 230
            "L_BJWINSPIC_KNIFE_HURT6",   // 231
            "L_BJWINSPIC_PISTOL",        // 232
            "L_BJWINSPIC_PISTOL_HURT1",  // 233
            "L_BJWINSPIC_PISTOL_HURT2",  // 234
            "L_BJWINSPIC_PISTOL_HURT3",  // 235
            "L_BJWINSPIC_PISTOL_HURT4",  // 236
            "L_BJWINSPIC_PISTOL_HURT5",  // 237
            "L_BJWINSPIC_PISTOL_HURT6",  // 238
            "L_BJWINSPIC_MP40",          // 239
            "L_BJWINSPIC_MP40_HURT1",    // 240
            "L_BJWINSPIC_MP40_HURT2",    // 241
            "L_BJWINSPIC_MP40_HURT3",    // 242
            "L_BJWINSPIC_MP40_HURT4",    // 243
            "L_BJWINSPIC_MP40_HURT5",    // 244
            "L_BJWINSPIC_MP40_HURT6",    // 245
            "L_BJWINSPIC_CHAIN",         // 246
            "L_BJWINSPIC_CHAIN_HURT1",   // 247
            "L_BJWINSPIC_CHAIN_HURT2",   // 248
            "L_BJWINSPIC_CHAIN_HURT3",   // 249
            "L_BJWINSPIC_CHAIN_HURT4",   // 250
            "L_BJWINSPIC_CHAIN_HURT5",   // 251
            "L_BJWINSPIC_CHAIN_HURT6",   // 252
            "TANKGUNPIC",                // 253
            "TANKWHEELPIC",              // 254
            "N_INFL",                    // 255
            "N_INFR",                    // 256
            "MG42GUNPIC",                // 257
            "FACECPIC",                  // 258
            "FACEIPIC",                  // 259
            "FACEBPIC",                  // 260
            "FACEVPIC",                  // 261
            "FACEFPIC",                  // 262
            "FACEHPIC",                  // 263
            "FACEMPIC",                  // 264
            "FACERPIC",                  // 265
            "FACEZPIC",                  // 266
            "FACEZHPIC",                 // 267
            "FLAGPIC",                   // 268
            "FISTSPIC",                  // 269
            "SWTITLEPIC",                // 270
            "SWTITLE2PIC",               // 271
        };
    #else
        // gfxv_wl6.h
        static const char *LWMP_ChunkPicStrings[ENUMEND_EXTRA] =
        {
            "Dummy",                             // 0
            "Dummy",                             // 1
            "Dummy",                             // 2
            // Lump Start
            "H_BJPIC",                           // 3
            "H_CASTLEPIC",                       // 4
            "H_BLAZEPIC",                        // 5
            "H_TOPWINDOWPIC",                    // 6
            "H_LEFTWINDOWPIC",                   // 7
            "H_RIGHTWINDOWPIC",                  // 8
            "H_BOTTOMINFOPIC",                   // 9
            // Lump Start
            "C_OPTIONSPIC",                      // 10
            "C_CURSOR1PIC",                      // 11
            "C_CURSOR2PIC",                      // 12
            "C_NOTSELECTEDPIC",                  // 13
            "C_SELECTEDPIC",                     // 14
            "C_FXTITLEPIC",                      // 15
            "C_DIGITITLEPIC",                    // 16
            "C_MUSICTITLEPIC",                   // 17
            "C_MOUSELBACKPIC",                   // 18
            "C_BABYMODEPIC",                     // 19
            "C_EASYPIC",                         // 20
            "C_NORMALPIC",                       // 21
            "C_HARDPIC",                         // 22
            "C_LOADSAVEDISKPIC",                 // 23
            "C_DISKLOADING1PIC",                 // 24
            "C_DISKLOADING2PIC",                 // 25
            "C_CONTROLPIC",                      // 26
            "C_CUSTOMIZEPIC",                    // 27
            "C_LOADGAMEPIC",                     // 28
            "C_SAVEGAMEPIC",                     // 29
            "C_EPISODE1PIC",                     // 30
            "C_EPISODE2PIC",                     // 31
            "C_EPISODE3PIC",                     // 32
            "C_EPISODE4PIC",                     // 33
            "C_EPISODE5PIC",                     // 34
            "C_EPISODE6PIC",                     // 35
            "C_CODEPIC",                         // 36
            "C_TIMECODEPIC",                     // 37
            "C_LEVELPIC",                        // 38
            "C_NAMEPIC",                         // 39
            "C_SCOREPIC",                        // 40
            "C_JOY1PIC",                         // 41
            "C_JOY2PIC",                         // 42
            // Lump Start
            "L_GUYPIC",                          // 43
            "L_COLONPIC",                        // 44
            "L_NUM0PIC",                         // 45
            "L_NUM1PIC",                         // 46
            "L_NUM2PIC",                         // 47
            "L_NUM3PIC",                         // 48
            "L_NUM4PIC",                         // 49
            "L_NUM5PIC",                         // 50
            "L_NUM6PIC",                         // 51
            "L_NUM7PIC",                         // 52
            "L_NUM8PIC",                         // 53
            "L_NUM9PIC",                         // 54
            "L_PERCENTPIC",                      // 55
            "L_APIC",                            // 56
            "L_BPIC",                            // 57
            "L_CPIC",                            // 58
            "L_DPIC",                            // 59
            "L_EPIC",                            // 60
            "L_FPIC",                            // 61
            "L_GPIC",                            // 62
            "L_HPIC",                            // 63
            "L_IPIC",                            // 64
            "L_JPIC",                            // 65
            "L_KPIC",                            // 66
            "L_LPIC",                            // 67
            "L_MPIC",                            // 68
            "L_NPIC",                            // 69
            "L_OPIC",                            // 70
            "L_PPIC",                            // 71
            "L_QPIC",                            // 72
            "L_RPIC",                            // 73
            "L_SPIC",                            // 74
            "L_TPIC",                            // 75
            "L_UPIC",                            // 76
            "L_VPIC",                            // 77
            "L_WPIC",                            // 78
            "L_XPIC",                            // 79
            "L_YPIC",                            // 80
            "L_ZPIC",                            // 81
            "L_EXPOINTPIC",                      // 82
            "L_APOSTROPHEPIC",                   // 83
            "L_GUY2PIC",                         // 84
            "L_BJWINSPIC",                       // 85
            "STATUSBARPIC",                      // 86
            "TITLEPIC",                          // 87
            "PG13PIC",                           // 88
            "CREDITSPIC",                        // 89
            "HIGHSCORESPIC",                     // 90
            // Lump Start
            "KNIFEPIC",                          // 91
            "GUNPIC",                            // 92
            "MACHINEGUNPIC",                     // 93
            "GATLINGGUNPIC",                     // 94
            "NOKEYPIC",                          // 95
            "GOLDKEYPIC",                        // 96
            "SILVERKEYPIC",                      // 97
            "N_BLANKPIC",                        // 98
            "N_0PIC",                            // 99
            "N_1PIC",                            // 100
            "N_2PIC",                            // 101
            "N_3PIC",                            // 102
            "N_4PIC",                            // 103
            "N_5PIC",                            // 104
            "N_6PIC",                            // 105
            "N_7PIC",                            // 106
            "N_8PIC",                            // 107
            "N_9PIC",                            // 108
            "FACE1APIC",                         // 109
            "FACE1BPIC",                         // 110
            "FACE1CPIC",                         // 111
            "FACE2APIC",                         // 112
            "FACE2BPIC",                         // 113
            "FACE2CPIC",                         // 114
            "FACE3APIC",                         // 115
            "FACE3BPIC",                         // 116
            "FACE3CPIC",                         // 117
            "FACE4APIC",                         // 118
            "FACE4BPIC",                         // 119
            "FACE4CPIC",                         // 120
            "FACE5APIC",                         // 121
            "FACE5BPIC",                         // 122
            "FACE5CPIC",                         // 123
            "FACE6APIC",                         // 124
            "FACE6BPIC",                         // 125
            "FACE6CPIC",                         // 126
            "FACE7APIC",                         // 127
            "FACE7BPIC",                         // 128
            "FACE7CPIC",                         // 129
            "FACE8APIC",                         // 130
            "GOTGATLINGPIC",                     // 131
            "MUTANTBJPIC",                       // 132
            "PAUSEDPIC",                         // 133
            "GETPSYCHEDPIC",                     // 134
            "Dummy",                             // 135
            "ORDERSCREEN",                       // 136
            "ERRORSCREEN",                       // 137
            "T_HELPART",                         // 138
            "T_DEMO0",                           // 139
            "T_DEMO1",                           // 140
            "T_DEMO2",                           // 141
            "T_DEMO3",                           // 142
            "T_ENDART1",                         // 143
            "T_ENDART2",                         // 144
            "T_ENDART3",                         // 145
            "T_ENDART4",                         // 146
            "T_ENDART5",                         // 147
            "T_ENDART6",                         // 148
            "L_GUYPIC_KNIFE",                    // 149
            "L_GUYPIC_KNIFE_HURT1",              // 150
            "L_GUYPIC_KNIFE_HURT2",              // 151
            "L_GUYPIC_KNIFE_HURT3",              // 152
            "L_GUYPIC_KNIFE_HURT4",              // 153
            "L_GUYPIC_KNIFE_HURT5",              // 154
            "L_GUYPIC_KNIFE_HURT6",              // 155
            "L_GUYPIC_KNIFE_HURT7",              // 156
            "L_GUYPIC_PISTOL",                   // 157
            "L_GUYPIC_PISTOL_HURT1",             // 158
            "L_GUYPIC_PISTOL_HURT2",             // 159
            "L_GUYPIC_PISTOL_HURT3",             // 160
            "L_GUYPIC_PISTOL_HURT4",             // 161
            "L_GUYPIC_PISTOL_HURT5",             // 162
            "L_GUYPIC_PISTOL_HURT6",             // 163
            "L_GUYPIC_PISTOL_HURT7",             // 164
            "L_GUYPIC_MP40",                     // 165
            "L_GUYPIC_MP40_HURT1",               // 166
            "L_GUYPIC_MP40_HURT2",               // 167
            "L_GUYPIC_MP40_HURT3",               // 168
            "L_GUYPIC_MP40_HURT4",               // 169
            "L_GUYPIC_MP40_HURT5",               // 170
            "L_GUYPIC_MP40_HURT6",               // 171
            "L_GUYPIC_MP40_HURT7",               // 172
            "L_GUYPIC_CHAIN",                    // 173
            "L_GUYPIC_CHAIN_HURT1",              // 174
            "L_GUYPIC_CHAIN_HURT2",              // 175
            "L_GUYPIC_CHAIN_HURT3",              // 176
            "L_GUYPIC_CHAIN_HURT4",              // 177
            "L_GUYPIC_CHAIN_HURT5",              // 178
            "L_GUYPIC_CHAIN_HURT6",              // 179
            "L_GUYPIC_CHAIN_HURT7",              // 180
            "L_GUY2PIC_KNIFE",                   // 181
            "L_GUY2PIC_KNIFE_HURT1",             // 182
            "L_GUY2PIC_KNIFE_HURT2",             // 183
            "L_GUY2PIC_KNIFE_HURT3",             // 184
            "L_GUY2PIC_KNIFE_HURT4",             // 185
            "L_GUY2PIC_KNIFE_HURT5",             // 186
            "L_GUY2PIC_KNIFE_HURT6",             // 187
            "L_GUY2PIC_KNIFE_HURT7",             // 188
            "L_GUY2PIC_PISTOL",                  // 189
            "L_GUY2PIC_PISTOL_HURT1",            // 190
            "L_GUY2PIC_PISTOL_HURT2",            // 191
            "L_GUY2PIC_PISTOL_HURT3",            // 192
            "L_GUY2PIC_PISTOL_HURT4",            // 193
            "L_GUY2PIC_PISTOL_HURT5",            // 194
            "L_GUY2PIC_PISTOL_HURT6",            // 195
            "L_GUY2PIC_PISTOL_HURT7",            // 196
            "L_GUY2PIC_MP40",                    // 197
            "L_GUY2PIC_MP40_HURT1",              // 198
            "L_GUY2PIC_MP40_HURT2",              // 199
            "L_GUY2PIC_MP40_HURT3",              // 200
            "L_GUY2PIC_MP40_HURT4",              // 201
            "L_GUY2PIC_MP40_HURT5",              // 202
            "L_GUY2PIC_MP40_HURT6",              // 203
            "L_GUY2PIC_MP40_HURT7",              // 204
            "L_GUY2PIC_CHAIN",                   // 205
            "L_GUY2PIC_CHAIN_HURT1",             // 206
            "L_GUY2PIC_CHAIN_HURT2",             // 207
            "L_GUY2PIC_CHAIN_HURT3",             // 208
            "L_GUY2PIC_CHAIN_HURT4",             // 209
            "L_GUY2PIC_CHAIN_HURT5",             // 210
            "L_GUY2PIC_CHAIN_HURT6",             // 211
            "L_GUY2PIC_CHAIN_HURT7",             // 212
            "L_BJWINSPIC_KNIFE",                 // 213
            "L_BJWINSPIC_KNIFE_HURT1",           // 214
            "L_BJWINSPIC_KNIFE_HURT2",           // 215
            "L_BJWINSPIC_KNIFE_HURT3",           // 216
            "L_BJWINSPIC_KNIFE_HURT4",           // 217
            "L_BJWINSPIC_KNIFE_HURT5",           // 218
            "L_BJWINSPIC_KNIFE_HURT6",           // 219
            "L_BJWINSPIC_PISTOL",                // 220
            "L_BJWINSPIC_PISTOL_HURT1",          // 221
            "L_BJWINSPIC_PISTOL_HURT2",          // 222
            "L_BJWINSPIC_PISTOL_HURT3",          // 223
            "L_BJWINSPIC_PISTOL_HURT4",          // 224
            "L_BJWINSPIC_PISTOL_HURT5",          // 225
            "L_BJWINSPIC_PISTOL_HURT6",          // 226
            "L_BJWINSPIC_MP40",                  // 227
            "L_BJWINSPIC_MP40_HURT1",            // 228
            "L_BJWINSPIC_MP40_HURT2",            // 229
            "L_BJWINSPIC_MP40_HURT3",            // 230
            "L_BJWINSPIC_MP40_HURT4",            // 231
            "L_BJWINSPIC_MP40_HURT5",            // 232
            "L_BJWINSPIC_MP40_HURT6",            // 233
            "L_BJWINSPIC_CHAIN",                 // 234
            "L_BJWINSPIC_CHAIN_HURT1",           // 235
            "L_BJWINSPIC_CHAIN_HURT2",           // 236
            "L_BJWINSPIC_CHAIN_HURT3",           // 237
            "L_BJWINSPIC_CHAIN_HURT4",           // 238
            "L_BJWINSPIC_CHAIN_HURT5",           // 239
            "L_BJWINSPIC_CHAIN_HURT6",           // 240
            "TANKGUNPIC",                        // 241
            "TANKWHEELPIC",                      // 242
            "N_INFL",                            // 243
            "N_INFR",                            // 244
            "MG42GUNPIC",                        // 245
            "FACECPIC",                          // 246
            "FACEIPIC",                          // 247
            "FACEBPIC",                          // 248
            "FACEVPIC",                          // 249
            "FACEFPIC",                          // 250
            "FACEHPIC",                          // 251
            "FACEMPIC",                          // 252
            "FACERPIC",                          // 253
            "FACEZPIC",                          // 254
            "FACEZHPIC",                         // 255
            "FLAGPIC",                           // 256
            "FISTSPIC",                          // 257
            "SWTITLEPIC",                        // 258
            "SWTITLE2PIC",                       // 259
        };
    #endif
#else
    // gfxv_sod.h
    static const char *LWMP_ChunkPicStrings[ENUMEND_EXTRA] =
    {
        "Dummy",                             // 0
        "Dummy",                             // 1
        "Dummy",                             // 2
        // Lump Start
        "C_BACKDROPPIC",                     // 3
        "C_MOUSELBACKPIC",                   // 4
        "C_CURSOR1PIC",                      // 5
        "C_CURSOR2PIC",                      // 6
        "C_NOTSELECTEDPIC",                  // 7
        "C_SELECTEDPIC",                     // 8
        // Lump Start
        "C_CUSTOMIZEPIC",                    // 9
        "C_JOY1PIC",                         // 10
        "C_JOY2PIC",                         // 11
        "C_MOUSEPIC",                        // 12
        "C_JOYSTICKPIC",                     // 13
        "C_KEYBOARDPIC",                     // 14
        "C_CONTROLPIC",                      // 15
        // Lump Start
        "C_OPTIONSPIC",                      // 16
        // Lump Start
        "C_FXTITLEPIC",                      // 17
        "C_DIGITITLEPIC",                    // 18
        "C_MUSICTITLEPIC",                   // 19
        // Lump Start
        "C_HOWTOUGHPIC",                     // 20
        "C_BABYMODEPIC",                     // 21
        "C_EASYPIC",                         // 22
        "C_NORMALPIC",                       // 23
        "C_HARDPIC",                         // 24
        // Lump Start
        "C_DISKLOADING1PIC",                 // 25
        "C_DISKLOADING2PIC",                 // 26
        "C_LOADGAMEPIC",                     // 27
        "C_SAVEGAMEPIC",                     // 28
        // Lump Start
        "HIGHSCORESPIC",                     // 29
        "C_WONSPEARPIC",                     // 30
    #ifndef SPEARDEMO
        // Lump Start
        "BJCOLLAPSE1PIC",                    // 31
        "BJCOLLAPSE2PIC",                    // 32
        "BJCOLLAPSE3PIC",                    // 33
        "BJCOLLAPSE4PIC",                    // 34
        "ENDPICPIC",                         // 35
    #endif
        // Lump Start
        "L_GUYPIC",                          // 36
        "L_COLONPIC",                        // 37
        "L_NUM0PIC",                         // 38
        "L_NUM1PIC",                         // 39
        "L_NUM2PIC",                         // 40
        "L_NUM3PIC",                         // 41
        "L_NUM4PIC",                         // 42
        "L_NUM5PIC",                         // 43
        "L_NUM6PIC",                         // 44
        "L_NUM7PIC",                         // 45
        "L_NUM8PIC",                         // 46
        "L_NUM9PIC",                         // 47
        "L_PERCENTPIC",                      // 48
        "L_APIC",                            // 49
        "L_BPIC",                            // 50
        "L_CPIC",                            // 51
        "L_DPIC",                            // 52
        "L_EPIC",                            // 53
        "L_FPIC",                            // 54
        "L_GPIC",                            // 55
        "L_HPIC",                            // 56
        "L_IPIC",                            // 57
        "L_JPIC",                            // 58
        "L_KPIC",                            // 59
        "L_LPIC",                            // 60
        "L_MPIC",                            // 61
        "L_NPIC",                            // 62
        "L_OPIC",                            // 63
        "L_PPIC",                            // 64
        "L_QPIC",                            // 65
        "L_RPIC",                            // 66
        "L_SPIC",                            // 67
        "L_TPIC",                            // 68
        "L_UPIC",                            // 69
        "L_VPIC",                            // 70
        "L_WPIC",                            // 71
        "L_XPIC",                            // 72
        "L_YPIC",                            // 73
        "L_ZPIC",                            // 74
        "L_EXPOINTPIC",                      // 75
        "L_APOSTROPHEPIC",                   // 76
        "L_GUY2PIC",                         // 77
        "L_BJWINSPIC",                       // 78
        // Lump Start
        "TITLE1PIC",                         // 79
        "TITLE2PIC",                         // 80
    #ifndef SPEARDEMO
        // Lump Start
        "ENDSCREEN11PIC",                    // 81
        // Lump Start
        "ENDSCREEN12PIC",                    // 82
        "ENDSCREEN3PIC",                     // 83
        "ENDSCREEN4PIC",                     // 84
        "ENDSCREEN5PIC",                     // 85
        "ENDSCREEN6PIC",                     // 86
        "ENDSCREEN7PIC",                     // 87
        "ENDSCREEN8PIC",                     // 88
        "ENDSCREEN9PIC",                     // 89
    #endif
        "STATUSBARPIC",                      // 90
        "PG13PIC",                           // 91
        "CREDITSPIC",                        // 92
    #ifndef SPEARDEMO
        // Lump Start
        "IDGUYS1PIC",                        // 93
        "IDGUYS2PIC",                        // 94
        // Lump Start
        "COPYPROTTOPPIC",                    // 95
        "COPYPROTBOXPIC",                    // 96
        "BOSSPIC1PIC",                       // 97
        "BOSSPIC2PIC",                       // 98
        "BOSSPIC3PIC",                       // 99
        "BOSSPIC4PIC",                       // 100
    #endif
        // Lump Start
        "KNIFEPIC",                          // 101
        "GUNPIC",                            // 102
        "MACHINEGUNPIC",                     // 103
        "GATLINGGUNPIC",                     // 104
        "NOKEYPIC",                          // 105
        "GOLDKEYPIC",                        // 106
        "SILVERKEYPIC",                      // 107
        "N_BLANKPIC",                        // 108
        "N_0PIC",                            // 109
        "N_1PIC",                            // 110
        "N_2PIC",                            // 111
        "N_3PIC",                            // 112
        "N_4PIC",                            // 113
        "N_5PIC",                            // 114
        "N_6PIC",                            // 115
        "N_7PIC",                            // 116
        "N_8PIC",                            // 117
        "N_9PIC",                            // 118
        "FACE1APIC",                         // 119
        "FACE1BPIC",                         // 120
        "FACE1CPIC",                         // 121
        "FACE2APIC",                         // 122
        "FACE2BPIC",                         // 123
        "FACE2CPIC",                         // 124
        "FACE3APIC",                         // 125
        "FACE3BPIC",                         // 126
        "FACE3CPIC",                         // 127
        "FACE4APIC",                         // 128
        "FACE4BPIC",                         // 129
        "FACE4CPIC",                         // 130
        "FACE5APIC",                         // 131
        "FACE5BPIC",                         // 132
        "FACE5CPIC",                         // 133
        "FACE6APIC",                         // 134
        "FACE6BPIC",                         // 135
        "FACE6CPIC",                         // 136
        "FACE7APIC",                         // 137
        "FACE7BPIC",                         // 138
        "FACE7CPIC",                         // 139
        "FACE8APIC",                         // 140
        "GOTGATLINGPIC",                     // 141
        "GODMODEFACE1PIC",                   // 142
        "GODMODEFACE2PIC",                   // 143
        "GODMODEFACE3PIC",                   // 144
        "BJWAITING1PIC",                     // 145
        "BJWAITING2PIC",                     // 146
        "BJOUCHPIC",                         // 147
        "PAUSEDPIC",                         // 148
        "GETPSYCHEDPIC",                     // 149

        "TILE8",                             // 150

        "ORDERSCREEN",                       // 151
        "ERRORSCREEN",                       // 152
        "TITLEPALETTE",                      // 153
    #ifndef SPEARDEMO
        "END1PALETTE",                       // 154
        "END2PALETTE",                       // 155
        "END3PALETTE",                       // 156
        "END4PALETTE",                       // 157
        "END5PALETTE",                       // 158
        "END6PALETTE",                       // 159
        "END7PALETTE",                       // 160
        "END8PALETTE",                       // 161
        "END9PALETTE",                       // 162
        "IDGUYSPALETTE",                     // 163
    #endif
        "T_DEMO0",                           // 164
    #ifndef SPEARDEMO
        "T_DEMO1",                           // 165
        "T_DEMO2",                           // 166
        "T_DEMO3",                           // 167
        "T_ENDART1",                         // 168
    #endif
        "L_GUYPIC_KNIFE",                    // 169
        "L_GUYPIC_KNIFE_HURT1",              // 170
        "L_GUYPIC_KNIFE_HURT2",              // 171
        "L_GUYPIC_KNIFE_HURT3",              // 172
        "L_GUYPIC_KNIFE_HURT4",              // 173
        "L_GUYPIC_KNIFE_HURT5",              // 174
        "L_GUYPIC_KNIFE_HURT6",              // 175
        "L_GUYPIC_KNIFE_HURT7",              // 176
        "L_GUYPIC_PISTOL",                   // 177
        "L_GUYPIC_PISTOL_HURT1",             // 178
        "L_GUYPIC_PISTOL_HURT2",             // 179
        "L_GUYPIC_PISTOL_HURT3",             // 180
        "L_GUYPIC_PISTOL_HURT4",             // 181
        "L_GUYPIC_PISTOL_HURT5",             // 182
        "L_GUYPIC_PISTOL_HURT6",             // 183
        "L_GUYPIC_PISTOL_HURT7",             // 184
        "L_GUYPIC_MP40",                     // 185
        "L_GUYPIC_MP40_HURT1",               // 186
        "L_GUYPIC_MP40_HURT2",               // 187
        "L_GUYPIC_MP40_HURT3",               // 188
        "L_GUYPIC_MP40_HURT4",               // 189
        "L_GUYPIC_MP40_HURT5",               // 190
        "L_GUYPIC_MP40_HURT6",               // 191
        "L_GUYPIC_MP40_HURT7",               // 192
        "L_GUYPIC_CHAIN",                    // 193
        "L_GUYPIC_CHAIN_HURT1",              // 194
        "L_GUYPIC_CHAIN_HURT2",              // 195
        "L_GUYPIC_CHAIN_HURT3",              // 196
        "L_GUYPIC_CHAIN_HURT4",              // 197
        "L_GUYPIC_CHAIN_HURT5",              // 198
        "L_GUYPIC_CHAIN_HURT6",              // 199
        "L_GUYPIC_CHAIN_HURT7",              // 200
        "L_GUY2PIC_KNIFE",                   // 201
        "L_GUY2PIC_KNIFE_HURT1",             // 202
        "L_GUY2PIC_KNIFE_HURT2",             // 203
        "L_GUY2PIC_KNIFE_HURT3",             // 204
        "L_GUY2PIC_KNIFE_HURT4",             // 205
        "L_GUY2PIC_KNIFE_HURT5",             // 206
        "L_GUY2PIC_KNIFE_HURT6",             // 207
        "L_GUY2PIC_KNIFE_HURT7",             // 208
        "L_GUY2PIC_PISTOL",                  // 209
        "L_GUY2PIC_PISTOL_HURT1",            // 210
        "L_GUY2PIC_PISTOL_HURT2",            // 211
        "L_GUY2PIC_PISTOL_HURT3",            // 212
        "L_GUY2PIC_PISTOL_HURT4",            // 213
        "L_GUY2PIC_PISTOL_HURT5",            // 214
        "L_GUY2PIC_PISTOL_HURT6",            // 215
        "L_GUY2PIC_PISTOL_HURT7",            // 216
        "L_GUY2PIC_MP40",                    // 217
        "L_GUY2PIC_MP40_HURT1",              // 218
        "L_GUY2PIC_MP40_HURT2",              // 219
        "L_GUY2PIC_MP40_HURT3",              // 220
        "L_GUY2PIC_MP40_HURT4",              // 221
        "L_GUY2PIC_MP40_HURT5",              // 222
        "L_GUY2PIC_MP40_HURT6",              // 223
        "L_GUY2PIC_MP40_HURT7",              // 224
        "L_GUY2PIC_CHAIN",                   // 225
        "L_GUY2PIC_CHAIN_HURT1",             // 226
        "L_GUY2PIC_CHAIN_HURT2",             // 227
        "L_GUY2PIC_CHAIN_HURT3",             // 228
        "L_GUY2PIC_CHAIN_HURT4",             // 229
        "L_GUY2PIC_CHAIN_HURT5",             // 230
        "L_GUY2PIC_CHAIN_HURT6",             // 231
        "L_GUY2PIC_CHAIN_HURT7",             // 232
        "L_BJWINSPIC_KNIFE",                 // 233
        "L_BJWINSPIC_KNIFE_HURT1",           // 234
        "L_BJWINSPIC_KNIFE_HURT2",           // 235
        "L_BJWINSPIC_KNIFE_HURT3",           // 236
        "L_BJWINSPIC_KNIFE_HURT4",           // 237
        "L_BJWINSPIC_KNIFE_HURT5",           // 238
        "L_BJWINSPIC_KNIFE_HURT6",           // 239
        "L_BJWINSPIC_PISTOL",                // 240
        "L_BJWINSPIC_PISTOL_HURT1",          // 241
        "L_BJWINSPIC_PISTOL_HURT2",          // 242
        "L_BJWINSPIC_PISTOL_HURT3",          // 243
        "L_BJWINSPIC_PISTOL_HURT4",          // 244
        "L_BJWINSPIC_PISTOL_HURT5",          // 245
        "L_BJWINSPIC_PISTOL_HURT6",          // 246
        "L_BJWINSPIC_MP40",                  // 247
        "L_BJWINSPIC_MP40_HURT1",            // 248
        "L_BJWINSPIC_MP40_HURT2",            // 249
        "L_BJWINSPIC_MP40_HURT3",            // 250
        "L_BJWINSPIC_MP40_HURT4",            // 251
        "L_BJWINSPIC_MP40_HURT5",            // 252
        "L_BJWINSPIC_MP40_HURT6",            // 253
        "L_BJWINSPIC_CHAIN",                 // 254
        "L_BJWINSPIC_CHAIN_HURT1",           // 255
        "L_BJWINSPIC_CHAIN_HURT2",           // 256
        "L_BJWINSPIC_CHAIN_HURT3",           // 257
        "L_BJWINSPIC_CHAIN_HURT4",           // 258
        "L_BJWINSPIC_CHAIN_HURT5",           // 259
        "L_BJWINSPIC_CHAIN_HURT6",           // 260
        "TANKGUNPIC",                        // 261
        "TANKWHEELPIC",                      // 262
        "N_INFL",                            // 263
        "N_INFR",                            // 264
        "MG42GUNPIC",                        // 265
        "SFACECPIC",                         // 266
        "SFACEIPIC",                         // 267
        "SFACEBPIC",                         // 268
        "SFACEVPIC",                         // 269
        "SFACEFPIC",                         // 270
        "SFACEHPIC",                         // 271
        "SFACEMPIC",                         // 272
        "SFACERPIC",                         // 273
        "SFACEZPIC",                         // 274
        "SFACEZHPIC",                        // 275
        "FLAGPIC",                           // 276
        "FISTSPIC",                          // 277
        "SWTITLEPIC",                        // 278
        "SWTITLE2PIC",                       // 279
    };
#endif

#define GIB_SPRITE_REMAP_MAX 32

typedef struct LWMP_GibActor_s
{
    classtype obclass;
    enemy_t which;
    int spriteNums[GIB_METHOD_MAX][GIB_SPRITE_REMAP_MAX];
} LWMP_GibActor_t;

static LWMP_GibActor_t LWMP_GibActors[] =
{
    {
        playerobj, // obclass
        (enemy_t)-1, // which
        {
            {
                SPR_BJ_DIE_1, SPR_BJ_CGIB_DIE_1,
                SPR_BJ_DIE_2, SPR_BJ_CGIB_DIE_2,
                SPR_BJ_DIE_3, SPR_BJ_CGIB_DIE_3,
                SPR_BJ_DEAD, SPR_BJ_CGIB_DEAD,
                0,
            }, // GIB_METHOD_CHAINGUN
            {
                SPR_BJ_DIE_1, SPR_BJ_RGIB_DIE_1,
                SPR_BJ_DIE_2, SPR_BJ_RGIB_DIE_2,
                SPR_BJ_DIE_3, SPR_BJ_RGIB_DIE_3,
                SPR_BJ_DEAD, SPR_BJ_RGIB_DEAD,
                0,
            }, // GIB_METHOD_ROCKET
        }, // spriteNums
    },
    {
        bjmutantobj, // obclass
        en_bjmutant, // which
        {
            {
                SPR_BJ_DIE_1, SPR_BJ_CGIB_DIE_1,
                SPR_BJ_DIE_2, SPR_BJ_CGIB_DIE_2,
                SPR_BJ_DIE_3, SPR_BJ_CGIB_DIE_3,
                SPR_BJ_DEAD, SPR_BJ_CGIB_DEAD,
                0,
            }, // GIB_METHOD_CHAINGUN
            {
                SPR_BJ_DIE_1, SPR_BJ_RGIB_DIE_1,
                SPR_BJ_DIE_2, SPR_BJ_RGIB_DIE_2,
                SPR_BJ_DIE_3, SPR_BJ_RGIB_DIE_3,
                SPR_BJ_DEAD, SPR_BJ_RGIB_DEAD,
                0,
            }, // GIB_METHOD_ROCKET
        }, // spriteNums
    },
    {
        bjmutantdogobj, // obclass
        en_bjmutantdog, // which
        {
            {
                SPR_BJ_DIE_1, SPR_BJ_CGIB_DIE_1,
                SPR_BJ_DIE_2, SPR_BJ_CGIB_DIE_2,
                SPR_BJ_DIE_3, SPR_BJ_CGIB_DIE_3,
                SPR_BJ_DEAD, SPR_BJ_CGIB_DEAD,
                0,
            }, // GIB_METHOD_CHAINGUN
            {
                SPR_BJ_DIE_1, SPR_BJ_RGIB_DIE_1,
                SPR_BJ_DIE_2, SPR_BJ_RGIB_DIE_2,
                SPR_BJ_DIE_3, SPR_BJ_RGIB_DIE_3,
                SPR_BJ_DEAD, SPR_BJ_RGIB_DEAD,
                0,
            }, // GIB_METHOD_ROCKET
        }, // spriteNums
    },
    {
        dogobj, // obclass
        en_dog, // which
        {
            {
                SPR_DOG_DIE_1, SPR_DOG_CGIB_DIE_1,
                SPR_DOG_DIE_2, SPR_DOG_CGIB_DIE_2,
                SPR_DOG_DIE_3, SPR_DOG_CGIB_DIE_3,
                SPR_DOG_DEAD, SPR_DOG_CGIB_DEAD,
                0,
            }, // GIB_METHOD_CHAINGUN
            {
                SPR_DOG_DIE_1, SPR_DOG_RGIB_DIE_1,
                SPR_DOG_DIE_2, SPR_DOG_RGIB_DIE_2,
                SPR_DOG_DIE_3, SPR_DOG_RGIB_DIE_3,
                SPR_DOG_DEAD, SPR_DOG_RGIB_DEAD,
                0,
            }, // GIB_METHOD_ROCKET
        }, // spriteNums
    },
    {
        guardobj, // obclass
        en_guard, // which
        {
            {
                SPR_GRD_DIE_1, SPR_GRD_CGIB_DIE_1,
                SPR_GRD_DIE_2, SPR_GRD_CGIB_DIE_2, 
                SPR_GRD_DIE_3, SPR_GRD_CGIB_DIE_3,
                SPR_GRD_DEAD, SPR_GRD_CGIB_DEAD,
                0,
            }, // GIB_METHOD_CHAINGUN
            {
                SPR_GRD_DIE_1, SPR_GRD_RGIB_DIE_1,
                SPR_GRD_DIE_2, SPR_GRD_RGIB_DIE_2, 
                SPR_GRD_DIE_3, SPR_GRD_RGIB_DIE_3,
                SPR_GRD_DEAD, SPR_GRD_RGIB_DEAD,
                0,
            }, // GIB_METHOD_ROCKET
        }, // spriteNums
    },
    {
        mutantobj, // obclass
        en_mutant, // which
        {
            {
                SPR_MUT_DIE_1, SPR_MUT_CGIB_DIE_1,
                SPR_MUT_DIE_2, SPR_MUT_CGIB_DIE_2, 
                SPR_MUT_DIE_3, SPR_MUT_CGIB_DIE_3,
                SPR_MUT_DIE_4, SPR_MUT_CGIB_DIE_4,
                SPR_MUT_DEAD, SPR_MUT_CGIB_DEAD,
                0,
            }, // GIB_METHOD_CHAINGUN
            {
                SPR_MUT_DIE_1, SPR_MUT_RGIB_DIE_1,
                SPR_MUT_DIE_2, SPR_MUT_RGIB_DIE_2, 
                SPR_MUT_DIE_3, SPR_MUT_RGIB_DIE_3,
                SPR_MUT_DIE_4, SPR_MUT_RGIB_DIE_4,
                SPR_MUT_DEAD, SPR_MUT_RGIB_DEAD,
                0,
            }, // GIB_METHOD_ROCKET
        }, // spriteNums
    },
    {
        officerobj, // obclass
        en_officer, // which
        {
            {
                SPR_OFC_DIE_1, SPR_OFC_CGIB_DIE_1,
                SPR_OFC_DIE_2, SPR_OFC_CGIB_DIE_2,
                SPR_OFC_DIE_3, SPR_OFC_CGIB_DIE_3,
                SPR_OFC_DIE_4, SPR_OFC_CGIB_DIE_4,
                SPR_OFC_DEAD, SPR_OFC_CGIB_DEAD,
                0,
            }, // GIB_METHOD_CHAINGUN
            {
                SPR_OFC_DIE_1, SPR_OFC_RGIB_DIE_1,
                SPR_OFC_DIE_2, SPR_OFC_RGIB_DIE_2,
                SPR_OFC_DIE_3, SPR_OFC_RGIB_DIE_3,
                SPR_OFC_DIE_4, SPR_OFC_RGIB_DIE_4,
                SPR_OFC_DEAD, SPR_OFC_RGIB_DEAD,
                0,
            }, // GIB_METHOD_ROCKET
        }, // spriteNums
    },
    {
        ssobj, // obclass
        en_ss, // which
        {
            {
                SPR_SS_DIE_1, SPR_SS_CGIB_DIE_1,
                SPR_SS_DIE_2, SPR_SS_CGIB_DIE_2,
                SPR_SS_DIE_3, SPR_SS_CGIB_DIE_3,
                SPR_SS_DEAD, SPR_SS_CGIB_DEAD,
                0,
            }, // GIB_METHOD_CHAINGUN
            {
                SPR_SS_DIE_1, SPR_SS_RGIB_DIE_1,
                SPR_SS_DIE_2, SPR_SS_RGIB_DIE_2,
                SPR_SS_DIE_3, SPR_SS_RGIB_DIE_3,
                SPR_SS_DEAD, SPR_SS_RGIB_DEAD,
                0,
            }, // GIB_METHOD_ROCKET
        }, // spriteNums
    },
};

static bool LWMP_CheckLayout(void)
{
    bool ok = true;

    switch (lwmp.layout)
    {
    case LWMP_LAYOUT_1x1:
        ok = LWMP_NUM_PLAYERS == 1;
        break;
    case LWMP_LAYOUT_2x1:
        ok = LWMP_NUM_PLAYERS == 2;
        break;
    case LWMP_LAYOUT_1x2:
        ok = LWMP_NUM_PLAYERS == 2;
        break;
    case LWMP_LAYOUT_3x1:
        ok = LWMP_NUM_PLAYERS == 3;
        break;
    case LWMP_LAYOUT_1x3:
        ok = LWMP_NUM_PLAYERS == 3;
        break;
    case LWMP_LAYOUT_2x2:
        ok = LWMP_NUM_PLAYERS == 4;
        break;
    case LWMP_LAYOUT_4x1:
        ok = LWMP_NUM_PLAYERS == 4;
        break;
    case LWMP_LAYOUT_1x4:
        ok = LWMP_NUM_PLAYERS == 4;
        break;
    }

    return ok;
}

static int LWMP_CycleLimit(bool doMax)
{
    return (doMax ? LWMP_MAX_PLAYERS : LWMP_NUM_PLAYERS);
}

static int LWMP_GenSpriteId(int shapenum, int rot_frames)
{
    assert(rot_frames < LWMP_MAX_ROT_FRAMES);
    return (shapenum * LWMP_MAX_ROT_FRAMES) + rot_frames;
}

static void LWMP_SpritePath(int shapenum, int rot_frames, 
    char *path, int pathsize, bool lowerext)
{
    char dirstr[32];

    assert(rot_frames < LWMP_MAX_ROT_FRAMES);
    assert(shapenum < SPR_NUMSPRITES);

    memset(dirstr, 0, sizeof(dirstr));
    if (rot_frames < LWMP_REPLACEMENT_FRAME)
    {
        snprintf(dirstr, sizeof(dirstr) - 1, "dir%d/", 
            rot_frames + 1);
    }

    snprintf(path, pathsize - 1, "%s/sprites/%s%s.%s",
        lwmp.dataDir ? lwmp.dataDir : LWMP_DEFAULT_DATADIR, 
        dirstr, LWMP_SpriteStrs[shapenum], lowerext ? "bmp" : "BMP");
    path[pathsize - 1] = '\0';
}

static void LWMP_ChunkPicPath(int chunknum, char *path, int pathsize,
    bool lowerext)
{
    assert(chunknum < ENUMEND_EXTRA);

    snprintf(path, pathsize - 1, "%s/pics/%s.%s",
        lwmp.dataDir ? lwmp.dataDir : LWMP_DEFAULT_DATADIR, 
        LWMP_ChunkPicStrings[chunknum], lowerext ? "bmp" : "BMP");
    path[pathsize - 1] = '\0';
}

void LWMP_SoundPath(const char *filename, char *path, int pathsize,
    const char *ext)
{
    snprintf(path, pathsize - 1, "%s/sounds/%s.%s",
        lwmp.dataDir ? lwmp.dataDir : LWMP_DEFAULT_DATADIR, 
        filename, ext);
    path[pathsize - 1] = '\0';
}

static t_compshape *LWMP_BuildSprite(SDL_Surface *surf)
{
    byte pix;
    int x, y;
    int x1, x2;
    word leftpix, rightpix, spanpix, totalpix, prevpix;
    int total_linecmds;
    t_compshape *shape;
    short *linecmds;
    byte *data;
    int numbytes;
    word *cmdptr;
    byte *pixels;
    int starty, endy;
    byte *image;
    int pixels_offset;
    int linecmds_offset;

    assert(surf->w == TEXTURESIZE && surf->h == TEXTURESIZE);
    SDL_LockSurface(surf);
    image = (byte *)surf->pixels;

    leftpix = 0;
    for (x = 0; x < TEXTURESIZE; x++)
    {
        for (y = 0; y < TEXTURESIZE; y++)
        {
            pix = image[(y << TEXTURESHIFT) + x];
            if (pix != 0xff)
            {
                break;
            }
        }
        if (y != TEXTURESIZE)
        {
            leftpix = x;
            break;
        }
    }

    rightpix = -1;
    for (x = TEXTURESIZE - 1; x >= 0; x--)
    {
        for (y = 0; y < TEXTURESIZE; y++)
        {
            pix = image[(y << TEXTURESHIFT) + x];
            if (pix != 0xff)
            {
                break;
            }
        }
        if (y != TEXTURESIZE)
        {
            rightpix = x;
            break;
        }
    }

    totalpix = 0;
    total_linecmds = 0;
    for (x = leftpix; x <= rightpix; x++)
    {
        starty = endy = 0;
        prevpix = 0xff;
        for (y = 0; y < TEXTURESIZE; y++)
        {
            pix = image[(y << TEXTURESHIFT) + x];
            if (pix != 0xff)
            {
                totalpix++;
            }

            if (prevpix == 0xff && pix != 0xff)
            {
                starty = y;
            }
            else if (prevpix != 0xff && pix == 0xff)
            {
                endy = y;
            }

            if (y == TEXTURESIZE - 1 && pix != 0xff)
            {
                endy = TEXTURESIZE;
            }

            if (endy > starty)
            {
                total_linecmds++;
                endy = starty = 0;
            }

            prevpix = pix;
        }
        total_linecmds++;
    }

    spanpix = rightpix - leftpix + 1;
    linecmds_offset = 4 + (spanpix * 2);
    pixels_offset = linecmds_offset + (total_linecmds * 6);
    numbytes = pixels_offset + totalpix;
    data = (byte *)malloc(numbytes);
    CHECKMALLOCRESULT(data);
    memset(data, 0, numbytes);

    shape = (t_compshape *)data;
    shape->leftpix = leftpix;
    shape->rightpix = rightpix;

    if (rightpix < leftpix)
    {
        return shape;
    }

    cmdptr = shape->dataofs;
    linecmds = (short *)(data + linecmds_offset);
    pixels = data + pixels_offset;

    for (x = leftpix; x <= rightpix; x++)
    {
        *cmdptr++ = (word)((uintptr_t)linecmds - (uintptr_t)data);

        starty = endy = 0;
        prevpix = 0xff;
        for (y = 0; y < TEXTURESIZE; y++)
        {
            pix = image[(y << TEXTURESHIFT) + x];
            if (pix != 0xff)
            {
                *pixels++ = pix;
            }

            if (prevpix == 0xff && pix != 0xff)
            {
                starty = y;
            }
            else if (prevpix != 0xff && pix == 0xff)
            {
                endy = y;
            }

            if (y == TEXTURESIZE - 1 && pix != 0xff)
            {
                endy = TEXTURESIZE;
            }

            if (endy > starty)
            {
                linecmds[0] = 2 * endy;
                linecmds[1] = (word)((uintptr_t)pixels - 
                    (endy - starty) - (uintptr_t)data - starty);
                linecmds[2] = 2 * starty;
                linecmds += 3;

                endy = starty = 0;
            }

            prevpix = pix;
        }
        linecmds[0] = linecmds[1] = linecmds[2] = 0;
        linecmds += 3;
    }

    SDL_UnlockSurface(surf);
    return shape;
}

static byte *LWMP_BuildChunkPic(SDL_Surface *surf)
{
    int x, y;
    byte *image;
    byte *picdata;

    picdata = (byte *)malloc(surf->w * surf->h);
    CHECKMALLOCRESULT(picdata);

    SDL_LockSurface(surf);
    image = (byte *)surf->pixels;

    for(y = 0; y < surf->h; y++)
    {
        for(x = 0; x < surf->w; x++)
        {
            picdata[(y * (surf->w >> 2) + (x >> 2)) + (x & 3) * (surf->w >> 2) * surf->h] =
                image[y * surf->w + x];
        }
    }

    SDL_UnlockSurface(surf);
    return picdata;
}

static LWMP_Sprite_t *LWMP_GetSprite(int shapenum, int rot_frames)
{
    int i;
    int spriteId;
    LWMP_Sprite_t *sprite;
    SDL_Surface *surf;
    char path[256];

    spriteId = LWMP_GenSpriteId(shapenum, rot_frames);

    if (!LWEX_Hash_Valid(lwmp.spriteHash))
    {
        lwmp.spriteHash = LWEX_Hash_New();
    }

    i = LWEX_Hash_Find(lwmp.spriteHash, spriteId);
    if (i >= 0 && i < vec_size(lwmp.sprites))
    {
        sprite = &vec_at(lwmp.sprites, i);
        return sprite;
    }

    bool lowerext = true;

uppercase_repeat:
    LWMP_SpritePath(shapenum, rot_frames, path, sizeof(path), lowerext);

    surf = SDL_LoadBMP(path);
    if (surf != NULL)
    {
        LWEX_Hash_Add(lwmp.spriteHash, spriteId, vec_size(lwmp.sprites));

        sprite = vec_inc_top(lwmp.sprites, LWMP_Sprite_t);
        sprite->shape = LWMP_BuildSprite(surf);
        sprite->id = spriteId;
        sprite->shapenum = ChunksInFile + vec_size(lwmp.sprites) - 1;
        SDL_FreeSurface(surf);
    }
    else if (lowerext)
    {
        lowerext = false;
        goto uppercase_repeat;
    }

    return NULL;
}

static LWMP_ChunkPic_t *LWMP_GetChunkPic(int chunknum)
{
    int i;
    LWMP_ChunkPic_t *chunkPic = NULL;
    SDL_Surface *surf;
    char path[256];

    if (!LWEX_Hash_Valid(lwmp.chunkPicHash))
    {
        lwmp.chunkPicHash = LWEX_Hash_New();
    }

    i = LWEX_Hash_Find(lwmp.chunkPicHash, chunknum);
    if (i >= 0 && i < vec_size(lwmp.chunkPics))
    {
        chunkPic = &vec_at(lwmp.chunkPics, i);
        return chunkPic;
    }

    bool lowerext = true;

uppercase_repeat:
    LWMP_ChunkPicPath(chunknum, path, sizeof(path), lowerext);

    surf = SDL_LoadBMP(path);
    if (surf != NULL)
    {
        LWEX_Hash_Add(lwmp.chunkPicHash, chunknum, vec_size(lwmp.chunkPics));

        chunkPic = vec_inc_top(lwmp.chunkPics, LWMP_ChunkPic_t);
        chunkPic->picdata = LWMP_BuildChunkPic(surf);
        chunkPic->width = surf->w;
        chunkPic->height = surf->h;
        chunkPic->chunknum = chunknum;
        chunkPic->surf = surf;
    }
    else
    {
        if (lowerext)
        {
            lowerext = false;
            goto uppercase_repeat;
        }
    }

    return chunkPic;
}

/*
 * Return the pixel value at (x, y)
 * NOTE: The surface must be locked before calling this!
 */
static Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;

    case 2:
        return *(Uint16 *)p;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;

    case 4:
        return *(Uint32 *)p;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

/*
 * Set the pixel at (x, y) to the given value
 * NOTE: The surface must be locked before calling this!
 */
static void putpixel_addr(SDL_Surface *surface, Uint8 *pixel_addr, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;

    switch(bpp) {
    case 1:
        *pixel_addr = pixel;
        break;

    case 2:
        *(Uint16 *)pixel_addr = pixel;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            pixel_addr[0] = (pixel >> 16) & 0xff;
            pixel_addr[1] = (pixel >> 8) & 0xff;
            pixel_addr[2] = pixel & 0xff;
        } else {
            pixel_addr[0] = pixel & 0xff;
            pixel_addr[1] = (pixel >> 8) & 0xff;
            pixel_addr[2] = (pixel >> 16) & 0xff;
        }
        break;

    case 4:
        *(Uint32 *)pixel_addr = pixel;
        break;
    }
}

static void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to set */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
    putpixel_addr(surface, p, pixel);
}

static void LWMP_LayoutUpdate(void)
{
    int c, d;

    VH_Startup();

    for (LWMP_NONSPLIT_OVERRIDE(c))
    {
        for (LWMP_REPEAT(d))
        {
            NewViewSize (viewsize);
        }
    }
}

static byte LWMP_FindClosestColor(int r, int g, int b)
{
    int i;
    byte col = 0;
    int dist, mindist;

    mindist = 256 * 256 * 3;
    for (i = 0; i < 256; i++)
    {
        dist =
            Sq((int)gamepal[i].r - r) + 
            Sq((int)gamepal[i].g - g) + 
            Sq((int)gamepal[i].b - b);
        if (dist < mindist)
        {
            mindist = dist;
            col = (byte)i;
        }
    }

    return col;
}

static void LWMP_BuildWallMipmap(byte *source, byte *dest)
{
    int i, j, x, y;
    int size, texsize;
    byte col;
    int r, g, b;

    texsize = TEXTURESIZE;
    size = texsize * texsize;
    memcpy(dest, source, size);
    dest += size;

    for (i = 0; i < TEXTURESHIFT; i++)
    {
        for (x = 0; x < texsize; x += 2)
        {
            for (y = 0; y < texsize; y += 2)
            {
                r = g = b = 0;
                for (j = 0; j < 4; j++)
                {
                    col = source[(y + (j & 1)) + 
                        ((x + (j / 2)) << (TEXTURESHIFT - i))];
                    r += gamepal[col].r;
                    g += gamepal[col].g;
                    b += gamepal[col].b;
                }
                r >>= 2;
                g >>= 2;
                b >>= 2;

                col = LWMP_FindClosestColor(r, g, b);
                dest[(y >> 1) + ((x >> 1) << (TEXTURESHIFT - i - 1))] = col;
            }
        }

        source = dest;
        size >>= 2;
        texsize >>= 1;
        dest += size;
    }
}

static void LWMP_ShapeToImage(int shapenum, byte *image)
{
    int x, y, i;
    word *cmdptr;
    t_compshape *shape;
    byte *sprite;
    short *linecmds;

    memset(image, LWMP_MINIMAP_FLOORPIX, TEXTURESIZE * TEXTURESIZE);

    shape = (t_compshape *)LWMP_GetShape(shapenum);
    sprite = (byte *)shape;

    cmdptr = shape->dataofs;
    for (x = shape->leftpix; x <= shape->rightpix; x++)
    {
        linecmds = (short *)(sprite + *cmdptr++);
        for (; *linecmds; linecmds += 3)
        {
            i = linecmds[2] / 2 + linecmds[1];
            for (y = linecmds[2] / 2; y < linecmds[0] / 2; y++, i++)
            {
                image[x * 64 + y] = sprite[i];
            }
        }
    }
}

static byte *LWMP_GetWallMipmap(int wallpic, int mipmap)
{
    int i;
    byte *source;
    LWMP_WallMipmap_t *wallMipmap;
    int offset, size;
    byte shapeImage[TEXTURESIZE * TEXTURESIZE];

    if (!LWEX_Hash_Valid(lwmp.wallMipmapHash))
    {
        lwmp.wallMipmapHash = LWEX_Hash_New();
    }

    i = LWEX_Hash_Find(lwmp.wallMipmapHash, wallpic);

    if (i >= 0 && i < vec_size(lwmp.wallMipmaps))
    {
        wallMipmap = &vec_at(lwmp.wallMipmaps, i);
    }
    else
    {
        LWEX_Hash_Add(lwmp.wallMipmapHash, wallpic, vec_size(lwmp.wallMipmaps));

        wallMipmap = vec_inc_top(lwmp.wallMipmaps, LWMP_WallMipmap_t);
        wallMipmap->wallpic = wallpic;

        size = TEXTURESIZE * TEXTURESIZE * 2; // TODO: not efficient
        wallMipmap->data = (byte *)malloc(size);
        CHECKMALLOCRESULT(wallMipmap->data);
        memset(wallMipmap->data, 0, size);

        if (wallpic < PMSpriteStart)
        {
            source = PM_GetTexture(wallpic);
        }
        else
        {
            LWMP_ShapeToImage(wallpic - PMSpriteStart, shapeImage);
            source = shapeImage;
        }

        LWMP_BuildWallMipmap(source, wallMipmap->data);
    }

    offset = 0;
    size = TEXTURESIZE * TEXTURESIZE;
    while (mipmap > 0)
    {
        offset += size;
        size >>= 2;
        assert(size > 0);
        mipmap--;
    }

    return wallMipmap->data + offset;
}

static byte LWMP_GetMipMapPix(byte *source, vec3fixed_t point, int mipmap)
{
    int s, t;

    s = FP_REM_SCALE(X(point), TEXTURESIZE >> mipmap);
    t = FP_REM_SCALE(FP(1) - Y(point), TEXTURESIZE >> mipmap);

    return source[(s << (TEXTURESHIFT - mipmap)) + t];
}

static inline bool LWMP_IterMinimapPixCond(LWMP_MinimapDraw_t *draw)
{
    return draw->x < draw->scw && 
        fixedpt_toint(X(draw->ptu)) == X(draw->pos) &&
        fixedpt_toint(Y(draw->ptu)) == Y(draw->pos);
}

static void LWMP_IterMinimapPixNext(LWMP_MinimapDraw_t *draw)
{
    draw->x++;
    X(draw->ptu) += X(draw->stepu);
    Y(draw->ptu) += Y(draw->stepu);
    draw->vbuf_scanline++;
}

static inline void LWMP_PutMinimapPix(byte col, LWMP_MinimapDraw_t *draw)
{
    *draw->vbuf_scanline = draw->shadetable[col];
}

static inline void LWMP_PutMinimapPixSeries(byte col, LWMP_MinimapDraw_t *draw)
{
    for (LWMP_ITERMINIMAPPIX(draw))
    {
        LWMP_PutMinimapPix(col, draw);
    }
}

static void LWMP_CalcMinimapPix(int mipmap,
    LWMP_MinimapDraw_t *draw)
{
    int i, c;
    byte col = 0;
    doorobj_t *door;
    objtype tmpob;
    LWMP_MinimapDrawTile_t *dt;
    vec3fixed_t pointmoved;
    vec3fixed_t point;

    draw->shadetable = shadetable[0];

    draw->pos = vec3fixed_trunc(draw->ptu);
    if (!LWEX_TilePosInRange(draw->pos))
    {
        LWMP_PutMinimapPixSeries(0, draw);
        return;
    }

    dt = &draw->tiles[X(draw->pos)][Y(draw->pos)];
    if (!dt->revealed)
    {
        LWMP_PutMinimapPixSeries(0, draw);
        return;
    }

    if (dt->applyShade)
    {
        draw->shadetable = shadetable[SHADE_COUNT - 8];
    }

    if (dt->isDoor)
    {
        if (dt->source != NULL)
        {
            for (LWMP_ITERMINIMAPPIX(draw))
            {
                pointmoved = draw->ptu;
                C(pointmoved, dt->doorAxis) += dt->doorpos;

                if (FP_TRUNC(C(pointmoved, dt->doorAxis)) == 
                    FP_TRUNC(C(draw->ptu, dt->doorAxis)))
                {
                    col = LWMP_GetMipMapPix(dt->source, pointmoved, mipmap);
                    LWMP_PutMinimapPix(col, draw);
                }
                else
                {
                    LWMP_PutMinimapPix(LWMP_MINIMAP_FLOORPIX, draw);
                }
            }
        }
        else
        {
            LWMP_PutMinimapPixSeries(0, draw);
        }
    }
    else
    {
        if (dt->isWall)
        {
            if (dt->source != NULL)
            {
                for (LWMP_ITERMINIMAPPIX(draw))
                {
                    col = LWMP_GetMipMapPix(dt->source, draw->ptu, mipmap);
                    LWMP_PutMinimapPix(col, draw);
                }
            }
            else
            {
                LWMP_PutMinimapPixSeries(0, draw);
            }
        }
        else
        {
            if (dt->isPushWall)
            {
                if (dt->source != NULL)
                {
                    for (LWMP_ITERMINIMAPPIX(draw))
                    {
                        if (dt->pushWallBack ^ 
                            vec2i_equal(
                                vec3fixed_trunc(vec3fixed_add(draw->ptu, dt->pushWallDir)),
                                draw->pos
                                )
                            )
                        {
                            pointmoved = vec3fixed_add(draw->ptu, dt->pushWallDir);
                            col = LWMP_GetMipMapPix(dt->source, pointmoved, mipmap);
                            LWMP_PutMinimapPix(col, draw);
                        }
                        else
                        {
                            LWMP_PutMinimapPix(LWMP_MINIMAP_FLOORPIX, draw);
                        }
                    }
                }
                else
                {
                    LWMP_PutMinimapPixSeries(0, draw);
                }
            }
            else
            {
                if (dt->statVisible)
                {
                    for (LWMP_ITERMINIMAPPIX(draw))
                    {
                        col = LWMP_GetMipMapPix(dt->source, draw->ptu, mipmap);
                        LWMP_PutMinimapPix(col, draw);
                    }
                }
                else
                {
                    LWMP_PutMinimapPixSeries(LWMP_MINIMAP_FLOORPIX, draw);
                }
            }
        }
    }
}

static void LWMP_PrepareMinimapPix(LWMP_MinimapDraw_t *draw, int mipmap)
{
    int x, y;
    statobj_t *statptr;
    vec2i_t pos;
    fixed doorpos;
    LWMP_MinimapDrawTile_t *dt;
    int statindex;
    byte reveal;
    doorobj_t *door;
    int doornum;
    int tile, doortile, wallpic;
    int doorpage;
    dir4_t dir;

    for (statptr = &statobjlist[0]; statptr != laststatobj; statptr++)
    {
        if (statptr->shapenum != -1)
        {
            dt = &draw->tiles[statptr->tilex][63 - statptr->tiley];
            statindex = (statptr - &statobjlist[0]) + 1;
            dt->statindex = statindex;
        }
    }

    for (y = 0; y < MAPSIZE; y++)
    {
        for (x = 0; x < MAPSIZE; x++)
        {
            dt = &draw->tiles[x][y];
            dt->source = NULL;
            dt->col = 0;

            pos = vec2i(x, y);
            reveal = LWEX_GetMinimapRevealTile(pos);
            if ((reveal & minimap_rflg_area) != 0)
            {
                dt->revealed = true;

                tile = LWEX_GetTile(pos);
                if (tile & 0x80)
                {
                    dt->isDoor = true;

                    doornum = tile & 0x7f;
                    door = &doorobjlist[doornum];

                    switch(door->lock)
                    {
                        case dr_normal:
                            doorpage = LWMP_DOORWALL;
                            break;
                        case dr_lock1:
                        case dr_lock2:
                        case dr_lock3:
                        case dr_lock4:
                            doorpage = LWMP_DOORWALL + 6;
                            break;
                        case dr_elevator:
                            doorpage = LWMP_DOORWALL + 4;
                            break;
                    }

                    dt->source = LWMP_GetWallMipmap(doorpage, mipmap);

                    if (door->vertical)
                    {
                        dt->doorpos = doorposition[doornum];
                        dt->doorAxis = 1;
                    }
                    else
                    {
                        dt->doorpos = -doorposition[doornum];
                        dt->doorAxis = 0;
                    }
                }
                else
                {
                    if (tile != 0 && tile != 64 && tile < AREATILE)
                    {
                        dt->isWall = true;
                        if (tile & 0x40)
                        {
                            tile &= ~0x40;
                        }
                        wallpic = horizwall[tile];
                        dt->source = LWMP_GetWallMipmap(wallpic, mipmap);
                    }
                    else // tile == 0 || tile == 64 || tile >= AREATILE
                    {
                        if (tile == 64)
                        {
                            dt->isPushWall = true;
                            wallpic = (pwalltile - 1) * 2;
                            dt->source = LWMP_GetWallMipmap(wallpic, mipmap);

                            dir = (dir4_t)((pwalldir + 4 - 1) % 4);
                            if (dir == dir4_south || dir == dir4_north)
                            {
                                dir = dir4_next(dir, 2);
                            }

                            dt->pushWallDir = vec3fixed_scale(
                                vec3fixed_from2i(
                                    vec2i_neigh(vec2i(0, 0), dir)
                                    ),
                                -FP2(0, pwallpos * FP_UNIT / 64)
                                );

                            dt->pushWallBack = !vec2i_equal(pos, vec2i(pwallx, 63 - pwally));
                        }
                        else
                        {
                            statindex = dt->statindex;
                            if (statindex != 0 && (reveal & minimap_rflg_vis) != 0)
                            {
                                dt->statVisible = true;
                                statindex--;
                                statptr = &statobjlist[statindex];
                                dt->source = LWMP_GetWallMipmap(statptr->shapenum + 
                                    PMSpriteStart, mipmap);
                            }
                        }
                    }
                }

                if ((reveal & minimap_rflg_vis) == 0 && tile != 64)
                {
                    dt->applyShade = true;
                }
            }
        }
    }
}

static void LWMP_DrawMinimapPlayerSpots(LWMP_MinimapDraw_t *draw)
{
    int i, c;
    fixed span;
    vec3fixed_t curplayerpoint, p, b, n;
    dir4_t dir;
    mat3fixed_t boxscale;
    fixed spotx, spoty;
    int spotx_int, spoty_int;
    fixed running_dot_x, running_dot_y;
    fixed dotradius;
    fixed dotradius_sq;
    fixed dotradius_dist;
    int dot_strobe_tics;
    fixed t;
    vec2i_t pos;
    fixed cx, cy;
    fixed x, y, u, v;
    fixed arrow_w, arrow_h;
    LWMP_MinimapState_t *ms = &lwmp.minimap.state;

    dotradius_dist = FP(LWMP_MINIMAP_DOTRADIUS);
    dot_strobe_tics = LWMP_MINIMAP_DOTSTROBE_TICS;
    t = FP(ms->minimap_tics % dot_strobe_tics) / dot_strobe_tics;
    dotradius = LWEX_FixedLerp(
        dotradius_dist / LWMP_MINIMAP_DOTSTROBE_MINFACTOR,
        dotradius_dist / LWMP_MINIMAP_DOTSTROBE_MAXFACTOR,
        t);
    dotradius_sq = fixedpt_mul(dotradius, dotradius);
    arrow_w = fixedpt_mul(FP2(0, 0x3000), dotradius);
    arrow_h = fixedpt_mul(FP2(0, 0xa000), dotradius);

    for (LWMP_REPEAT(c))
    {
        if (c == 0)
        {
            curplayerpoint = draw->playerpoint_mp;
        }
        else
        {
            b = draw->playerpoint_mp;

            b = vec3fixed_subtract(b, curplayerpoint);
            b = mat3fixed_mult_point(mat3fixed_transpose(draw->rotmat), b);
            boxscale = mat3fixed_diag(draw->boxw / 2, draw->boxh / 2, FP(1));

            dir = dir4_east;
            for (i = 0; i < 4; i++)
            {
                pos = vec2i_neigh(vec2i(0, 0), dir);
                n = vec3fixed_from2i(pos);
                p = mat3fixed_mult_point(boxscale, n);

                t = vec3fixed_dot(vec3fixed_subtract(b, p), n);
                if (t > 0)
                {
                    t = fixedpt_div(vec3fixed_dot(p, n), vec3fixed_dot(b, n));
                    b = vec3fixed_scale(b, t);
                }

                dir = dir4_next(dir, 1);
            }

            b = mat3fixed_mult_point(draw->rotmat, b);
            b = vec3fixed_add(b, curplayerpoint);

            draw->playerpoint_mp = b;
        }
    }

    for (LWMP_REPEAT(c))
    {
        ColorRemap_SetShade(gamestate.player_shade_mp - 1);

        if (c == 0)
        {
            curplayerpoint = draw->playerpoint_mp;
        }

        b = draw->playerpoint_mp;
        b = vec3fixed_subtract(b, curplayerpoint);
        b = mat3fixed_mult_point(mat3fixed_transpose(draw->rotmat), b);

        for (i = 0; i < 2; i++)
        {
            C(b, i) += C(draw->boxdims, i) / 2;
            C(b, i) = fixedpt_mul(
                C(b, i),
                fixedpt_div(
                    C(draw->scdims, i), C(draw->boxdims, i)
                    )
                );
        }

        cx = X(b);
        cy = FP(draw->sch) - Y(b);

        running_dot_y = 0;
        for (y = 0; y < dotradius; y += FP(1))
        {
            running_dot_x = running_dot_y;
            for (x = 0; x < dotradius; x += FP(1))
            {
                if (running_dot_x > dotradius_sq)
                {
                    break;
                }

                for (i = 0; i < 4; i++)
                {
                    spotx = x;
                    if (i & 1)
                    {
                        spotx -= fixedpt_mul(x, FP(2));
                    }
                    spoty = y;
                    if (i & 2)
                    {
                        spoty -= fixedpt_mul(y, FP(2));
                    }

                    spotx_int = fixedpt_toint(spotx + cx);
                    spoty_int = fixedpt_toint(spoty + cy);

                    if (spotx_int >= 0 && spotx_int < draw->scw &&
                        spoty_int >= 0 && spoty_int < draw->sch)
                    {
                        p = vec3fixed(spotx, spoty, 0);
                        p = mat3fixed_mult_point2d(draw->arrowrotmat_mp, p);

                        if (X(p) >= arrow_w &&
                            X(p) + Y(p) - (arrow_w + arrow_h) <= 0 &&
                            X(p) - Y(p) - (arrow_w + arrow_h) <= 0)
                        {
                            // nothing
                        }
                        else
                        {
                            draw->vbuf[(draw->scy + spoty_int) * bufferPitch + 
                                draw->scx + spotx_int] = color_remap[draw->dotcolor];
                        }
                    }
                }

                running_dot_x += fixedpt_mul(x, FP(2)) + FP(1);
            }
            running_dot_y += fixedpt_mul(y, FP(2)) + FP(1);
        }

        ColorRemap_SetShade(COLORREMAP_SHADE_DEFAULT);
    }
}

void LWMP_SetResolution(int width, int height)
{
    lwmp.resWidth = width;
    lwmp.resHeight = height;
}

void LWMP_SetLayout(const char *layoutStr)
{
    int i;

    if (!layoutStr)
    {
        return;
    }

    for (i = 0; i < lengthof(lwmpLayoutStrs); i++)
    {
        if (strcmp(layoutStr, lwmpLayoutStrs[i]) == 0)
        {
            lwmp.layout = (LWMP_Layout_t)i;
            break;
        }
    }
}

const char *LWMP_GetLayoutString(void)
{
    return lwmpLayoutStrs[lwmp.layout];
}

void LWMP_SetInstance(int instanceNum)
{
    lwmp.instanceNum = instanceNum;
}

int LWMP_GetInstance(void)
{
    return lwmp.instanceNum;
}

bool LWMP_IsFirst(void)
{
    return lwmp.instanceNum == 0;
}

void LWMP_CycleInstance(bool doMax)
{
    LWMP_SetInstance((LWMP_GetInstance() + 1) % 
        LWMP_CycleLimit(doMax));
}

bool LWMP_Repeat(int cmd, int *count, bool doMax)
{
    bool ret = false;

    switch (cmd)
    {
    case 0:
        *count = 0;
        break;
    case 1:
        ret = (*count < LWMP_CycleLimit(doMax));
        break;
    case 2:
        (*count)++;
        LWMP_CycleInstance(doMax);
        break;
    }

    return ret;
}

bool LWMP_Repeat_Once(int cmd, int *count, int onceIndex)
{
    bool ret = false;

    switch (cmd)
    {
    case 0:
        *count = LWMP_GetInstance();
        LWMP_SetInstance(onceIndex); 
        break;
    case 1:
        ret = (*count >= 0);
        break;
    case 2:
        LWMP_SetInstance(*count);
        *count = -(*count + 1);
        break;
    }

    return ret;
}

bool LWMP_NonSplit(int cmd, int *count, bool override)
{
    bool ret = false;

    // LWMP_NONSPLIT will have no effect when screens are tiled

    // LWMP_NONSPLIT_OVERRIDE will undo the effect of LWMP_NONSPLIT for the
    // block it covers

    if (override)
    {
        switch (cmd)
        {
        case 0:
            *count = 0;
            lwmp.nonSplit = -lwmp.nonSplit;
            break;
        case 1:
            ret = (*count < 1);
            if (!ret)
            {
                lwmp.nonSplit = -lwmp.nonSplit;
            }
            break;
        case 2:
            (*count)++;
            break;
        }
    }
    else
    {
        switch (cmd)
        {
        case 0:
            *count = 0;
            lwmp.nonSplit++;
            break;
        case 1:
            assert(lwmp.nonSplit > 0);
            ret = (*count < 1);
            if (!ret)
            {
                lwmp.nonSplit--;
            }
            break;
        case 2:
            (*count)++;
            assert(lwmp.nonSplit > 0);
            break;
        }
    }

    return ret;
}

int LWMP_GetNumPlayers(void)
{
    return lwmp.numPlayers;
}

void LWMP_SetNumPlayers(int numPlayers)
{
    lwmp.numPlayers = numPlayers;
}

int LWMP_GetPhysicalScreenWidth(void)
{
    return lwmp.resWidth;
}

int LWMP_GetPhysicalScreenHeight(void)
{
    return lwmp.resHeight;
}

int LWMP_GetVirtualScreenX(void)
{
    int x1 = 0;

    if (lwmp.nonSplit > 0)
    {
        return x1;
    }

    switch (lwmp.layout)
    {
    case LWMP_LAYOUT_1x1:
        break;
    case LWMP_LAYOUT_2x1:
        x1 = (1 - LWMP_GetInstance()) * LWMP_GetVirtualScreenWidth();
        break;
    case LWMP_LAYOUT_1x2:
        break;
    case LWMP_LAYOUT_3x1:
        x1 = LWMP_GetInstance() * LWMP_GetVirtualScreenWidth();
        break;
    case LWMP_LAYOUT_1x3:
        break;
    case LWMP_LAYOUT_2x2:
        x1 = (LWMP_GetInstance() % 2) * LWMP_GetVirtualScreenWidth();
        break;
    case LWMP_LAYOUT_4x1:
        x1 = LWMP_GetInstance() * LWMP_GetVirtualScreenWidth();
        break;
    case LWMP_LAYOUT_1x4:
        break;
    }

    return x1;
}

int LWMP_GetVirtualScreenY(void)
{
    int y1 = 0;

    if (lwmp.nonSplit > 0)
    {
        return y1;
    }

    switch (lwmp.layout)
    {
    case LWMP_LAYOUT_1x1:
        break;
    case LWMP_LAYOUT_2x1:
        break;
    case LWMP_LAYOUT_1x2:
        y1 = LWMP_GetInstance() * LWMP_GetVirtualScreenHeight();
        break;
    case LWMP_LAYOUT_3x1:
        break;
    case LWMP_LAYOUT_1x3:
        y1 = LWMP_GetInstance() * LWMP_GetVirtualScreenHeight();
        break;
    case LWMP_LAYOUT_2x2:
        y1 = (LWMP_GetInstance() / 2) * LWMP_GetVirtualScreenHeight();
        break;
    case LWMP_LAYOUT_4x1:
        break;
    case LWMP_LAYOUT_1x4:
        y1 = LWMP_GetInstance() * LWMP_GetVirtualScreenHeight();
        break;
    }

    return y1;
}

int LWMP_GetVirtualScreenWidth(void)
{
    int width;

    width = lwmp.resWidth;
    if (lwmp.nonSplit > 0)
    {
        return width;
    }

    switch (lwmp.layout)
    {
    case LWMP_LAYOUT_1x1:
        break;
    case LWMP_LAYOUT_2x1:
        width /= 2;
        break;
    case LWMP_LAYOUT_1x2:
        break;
    case LWMP_LAYOUT_3x1:
        width /= 3;
        break;
    case LWMP_LAYOUT_1x3:
        break;
    case LWMP_LAYOUT_2x2:
        width /= 2;
        break;
    case LWMP_LAYOUT_4x1:
        width /= 4;
        break;
    case LWMP_LAYOUT_1x4:
        break;
    }

    return width;
}

int LWMP_GetVirtualScreenHeight(void)
{
    int height;

    height = lwmp.resHeight;
    if (lwmp.nonSplit > 0)
    {
        return height;
    }

    switch (lwmp.layout)
    {
    case LWMP_LAYOUT_1x1:
        break;
    case LWMP_LAYOUT_2x1:
        break;
    case LWMP_LAYOUT_1x2:
        height /= 2;
        break;
    case LWMP_LAYOUT_3x1:
        break;
    case LWMP_LAYOUT_1x3:
        height /= 3;
        break;
    case LWMP_LAYOUT_2x2:
        height /= 2;
        break;
    case LWMP_LAYOUT_4x1:
        break;
    case LWMP_LAYOUT_1x4:
        height /= 4;
        break;
    }

    return height;
}

int LWMP_GetControlsInstance(void)
{
    return lwmp.controlsInstance;
}

void LWMP_SetControlsInstance(int controlsInstance)
{
    lwmp.controlsInstance = controlsInstance;
}

void LWMP_CycleControlsInstance(int dir)
{
    if (dir)
    {
        LWMP_SetControlsInstance((LWMP_GetControlsInstance() + 1) % LWMP_NUM_PLAYERS);
    }
    else
    {
        LWMP_SetControlsInstance((LWMP_GetControlsInstance() + LWMP_NUM_PLAYERS - 1) % LWMP_NUM_PLAYERS);
    }
}

void LWMP_ThreeDRefresh(void)
{
    int c;

    for (LWMP_REPEAT(c))
    {
        ThreeDRefresh ();
    }

    for (LWMP_NONSPLIT(c))
    {
        if (DefuseMode::enabled() && DefuseMode::dispTimer())
        {
            DefuseMode::drawTimeLeft();
        }
        if (VampireMode::enabled())
        {
            VampireMode::drawBloodBar();
        }
        if (CtfMode::enabled())
        {
            CtfMode::drawTimeLeft();
        }
        if (HarvesterMode::enabled())
        {
            HarvesterMode::drawTimeLeft();
        }
        if (ZombieHarvesterMode::enabled())
        {
            ZombieHarvesterMode::drawTimeLeft();
        }
    }

    //
    // show screen and time last cycle
    //
    if (fizzlein)
    {
        FizzleFade(0, 0, MaxX, MaxY, 20, false);
        fizzlein = false;

        lasttimecount = GetTimeCount();          // don't make a big tic count
    }
    else
    {
#ifndef REMDEBUG
        if (fpscounter)
        {
            fontnumber = 0;
            SETFONTCOLOR(7, 127);
            PrintX = 4; PrintY = 1;
            VWB_Bar(0, 0, 50, 10, bordercol);
            US_PrintSigned(fps);
            US_Print(" fps");
        }
#endif
    }

#ifndef REMDEBUG
    if (fpscounter)
    {
        fps_frames++;
        fps_time += tics;

        if(fps_time > 35)
        {
            fps_time -= 35;
            fps = fps_frames << 1;
            fps_frames = 0;
        }
    }
#endif

    VW_UpdateScreen();
}

void LWMP_Preload(void)
{
    int rot_frames;
    int shapenum;
    int chunknum;

    const int total = SPR_NUMSPRITES + ENUMEND_EXTRA;

    for (shapenum = 0; shapenum < SPR_NUMSPRITES; shapenum++)
    {
        int c;

        for (LWMP_NONSPLIT(c))
        {
            PreloadUpdate (shapenum, total);
        }

        for (rot_frames = 0; rot_frames < LWMP_MAX_ROT_FRAMES; rot_frames++)
        {
            LWMP_GetSprite(shapenum, rot_frames);
        }
    }

    for (chunknum = 0; chunknum < ENUMEND_EXTRA; chunknum++)
    {
        int c;

        for (LWMP_NONSPLIT(c))
        {
            PreloadUpdate (SPR_NUMSPRITES + chunknum, total);
        }

        LWMP_GetPic(chunknum, NULL, NULL);
    }
}

int LWMP_RotateSprite(int angle, int shapenum)
{
    int rot_frames;
    LWMP_Sprite_t *lwmpSprite;

    rot_frames = angle / (ANGLES / 8);
    assert(rot_frames >= 0 && rot_frames < LWMP_REPLACEMENT_FRAME);

    lwmpSprite = LWMP_GetSprite(shapenum, rot_frames);
    if (!lwmpSprite)
    {
        lwmpSprite = LWMP_GetSprite(shapenum, LWMP_REPLACEMENT_FRAME);
    }

    return lwmpSprite ? lwmpSprite->shapenum : shapenum;
}

LWMP_Shape_t *LWMP_GetShape(int lwmpShapeNum)
{
    t_compshape *shape;
    int spriteIndex;
    int shapenum;
    LWMP_Sprite_t *lwmpSprite;

    if (lwmpShapeNum < ChunksInFile)
    {
        shapenum = lwmpShapeNum;
        shape = (t_compshape *)PM_GetSprite(shapenum);
    }
    else
    {
        spriteIndex = lwmpShapeNum - ChunksInFile;

        lwmpSprite = &vec_at(lwmp.sprites, spriteIndex);
        shape = lwmpSprite->shape;
    }

    return (LWMP_Shape_t *)shape;
}

int LWMP_MaxShapes(void)
{
    return vec_size(lwmp.sprites);
}

void LWMP_SetDataDir(const char *dataDir)
{
    snprintf(lwmp.dataDir, sizeof(lwmp.dataDir) - 1, "%s", dataDir);
    lwmp.dataDir[sizeof(lwmp.dataDir) - 1] = '\0';
}

const char *LWMP_GetDataDir(void)
{
    return lwmp.dataDir;
}

int LWMP_GetVirtualCoordX(int x)
{
    x = (int)(CM(lwmp.matCur, 0, 0) * x + CM(lwmp.matCur, 0, 2) + 0.5);
    return (x * LWMP_GetVirtualScreenWidth()) / MaxX + LWMP_GetVirtualScreenX();
}

int LWMP_GetVirtualCoordY(int y)
{
    y = (int)(CM(lwmp.matCur, 1, 1) * y + CM(lwmp.matCur, 1, 2) + 0.5);
    return (y * LWMP_GetVirtualScreenHeight()) / MaxY + LWMP_GetVirtualScreenY();
}

int LWMP_GetVirtualCoordW(int w)
{
    w = (int)(CM(lwmp.matCur, 0, 0) * w + 0.5);
    return (w * LWMP_GetVirtualScreenWidth()) / MaxX;
}

int LWMP_GetVirtualCoordH(int h)
{
    h = (int)(CM(lwmp.matCur, 1, 1) * h + 0.5);
    return (h * LWMP_GetVirtualScreenHeight()) / MaxY;
}

int LWMP_StatusLines(void)
{
    int sl = STATUSLINES;

    switch (lwmp.layout)
    {
    case LWMP_LAYOUT_1x1:
        break;
    case LWMP_LAYOUT_2x1:
        sl /= 2;
        break;
    case LWMP_LAYOUT_1x2:
        break;
    case LWMP_LAYOUT_3x1:
        sl /= 3;
        break;
    case LWMP_LAYOUT_1x3:
        break;
    case LWMP_LAYOUT_2x2:
        sl /= 2;
        break;
    case LWMP_LAYOUT_4x1:
        sl /= 4;
        break;
    case LWMP_LAYOUT_1x4:
        break;
    }

    return sl;
}

unsigned char *LWMP_GetPic(int chunknum, unsigned int *width, unsigned int *height)
{
    int picnum;
    byte *picdata = NULL;
    LWMP_ChunkPic_t *lwmpChunkPic;

    lwmpChunkPic = LWMP_GetChunkPic(chunknum);
    if (lwmpChunkPic)
    {
        picdata = lwmpChunkPic->picdata;

        if (width)
        {
            *width = lwmpChunkPic->width;
        }

        if (height)
        {
            *height = lwmpChunkPic->height;
        }
    }
    else
    {
        if (chunknum < ENUMEND)
        {
            picnum = chunknum - STARTPICS;
            picdata = grsegs[chunknum];

            if (width)
            {
                *width = pictable[picnum].width;
            }

            if (height)
            {
                *height = pictable[picnum].height;
            }
        }
    }

    return picdata;
}

SDL_Color *LWMP_GetPicPal(int chunknum)
{
    SDL_Color *palette = NULL;
    LWMP_ChunkPic_t *lwmpChunkPic;

    lwmpChunkPic = LWMP_GetChunkPic(chunknum);
    if (lwmpChunkPic)
    {
        palette = lwmpChunkPic->surf->format->palette->colors;
    }

    return palette;
}

SDL_Surface *LWMP_GetPicSurf(int chunknum)
{
    LWMP_ChunkPic_t *lwmpChunkPic;

    lwmpChunkPic = LWMP_GetChunkPic(chunknum);
    if (lwmpChunkPic)
    {
        return lwmpChunkPic->surf;
    }

    return NULL;
}

char *LWMP_Va(const char *format, ...)
{
    static char string[1024];
    va_list argptr;

    va_start(argptr, format);
    vsprintf(string, format, argptr);
    va_end(argptr);

    return string;
}

void LWMP_ChangeNumPlayers(int numPlayersChange)
{
    lwmp.numPlayers = (((lwmp.numPlayers - 1) + 
        numPlayersChange + LWMP_MAX_PLAYERS) % 
        LWMP_MAX_PLAYERS) + 1;

    switch (lwmp.numPlayers)
    {
    case 1:
        lwmp.layout = LWMP_LAYOUT_1x1;
        break;
    case 2:
        lwmp.layout = LWMP_LAYOUT_2x1;
        break;
    case 3:
    case 4:
        lwmp.layout = LWMP_LAYOUT_2x2;
        break;
    }

    LWMP_LayoutUpdate();
}

void LWMP_ChangeLayout(int layoutChange)
{
    do
    {
        lwmp.layout = (lwmp.layout + layoutChange + 
            LWMP_LAYOUT_MAX) % LWMP_LAYOUT_MAX;
    } while (!LWMP_CheckLayout());

    LWMP_LayoutUpdate();
}

void LWMP_PushMatrix(void)
{
    if (lwmp.matStackTop >= LWMP_MATRIXSTACK_MAX)
    {
        Quit("Cannot push into exhausted matrix stack!");
    }
    lwmp.matStack[lwmp.matStackTop++] = lwmp.matCur;
}

void LWMP_PopMatrix(void)
{
    if (lwmp.matStackTop <= 0)
    {
        Quit("Cannot pop from empty matrix stack!");
    }
    lwmp.matCur = lwmp.matStack[--lwmp.matStackTop];
}

void LWMP_PushMatrixIdent(void)
{
    LWMP_PushMatrix();
    lwmp.matCur = mat3f_ident();
}

void LWMP_PushMatrixCombinedHUD(void)
{
    LWMP_PushMatrixIdent();
    if (LWMP_NUM_PLAYERS > 1)
    {
        LWMP_Translate((LWMP_GetInstance() % 2) * (MaxX / 2), 
            (MaxY - STATUSLINES) + (1 - (LWMP_GetInstance() / 2)) * (STATUSLINES / 2));
        LWMP_Scale(0.5, 0.5);
        LWMP_Translate(0, -(MaxY - STATUSLINES));
    }
}

void LWMP_PushMatrixStatusBar(void)
{
    LWMP_PushMatrixIdent();

    switch (lwmp.layout)
    {
    case LWMP_LAYOUT_1x1:
        break;
    case LWMP_LAYOUT_2x1:
        LWMP_Translate(0, (MaxY - STATUSLINES) + (STATUSLINES / 2));
        LWMP_Scale(1, 1.0 / 2);
        LWMP_Translate(0, -(MaxY - STATUSLINES));
        break;
    case LWMP_LAYOUT_1x2:
        LWMP_Translate(MaxX / 2, 0);
        LWMP_Scale(1.0 / 2, 1);
        LWMP_Translate(-(MaxX / 2), 0);
        break;
    case LWMP_LAYOUT_3x1:
        LWMP_Translate(0, (MaxY - STATUSLINES) + (STATUSLINES * 2 / 3));
        LWMP_Scale(1, 1.0 / 3);
        LWMP_Translate(0, -(MaxY - STATUSLINES));
        break;
    case LWMP_LAYOUT_1x3:
        LWMP_Translate(MaxX / 2, 0);
        LWMP_Scale(1.0 / 3, 1);
        LWMP_Translate(-(MaxX / 2), 0);
        break;
    case LWMP_LAYOUT_2x2:
        break;
    case LWMP_LAYOUT_4x1:
        LWMP_Translate(0, (MaxY - STATUSLINES) + (STATUSLINES * 3 / 4));
        LWMP_Scale(1, 1.0 / 4);
        LWMP_Translate(0, -(MaxY - STATUSLINES));
        break;
    case LWMP_LAYOUT_1x4:
        LWMP_Translate(MaxX / 2, 0);
        LWMP_Scale(1.0 / 4, 1);
        LWMP_Translate(-(MaxX / 2), 0);
        break;
    }
}

void LWMP_Translate(float tx, float ty)
{
    mat3f_t tm = mat3f_ident();
    
    CM(tm, 0, 2) = tx;
    CM(tm, 1, 2) = ty;

    lwmp.matCur = mat3f_mult(lwmp.matCur, tm);
}

void LWMP_Scale(float sx, float sy)
{
    mat3f_t ts = mat3f_ident();
    
    CM(ts, 0, 0) = sx;
    CM(ts, 1, 1) = sy;

    lwmp.matCur = mat3f_mult(lwmp.matCur, ts);
}

void LWMP_DrawCombinedHUD(void)
{
    int c;

    for (LWMP_REPEAT(c))
    {
        LWMP_PushMatrixCombinedHUD();

        ColorRemap_SetShade(gamestate.player_shade_mp - 1);
        VWB_DrawPicScaledCoord (0, MaxY - STATUSLINES, STATUSBARPIC);
        ColorRemap_SetShade(COLORREMAP_SHADE_DEFAULT);

        DrawFace ();
        DrawHealth ();
        DrawLives ();
        DrawLevel ();
        DrawAmmo ();
        DrawKeys ();
        DrawWeapon ();
        DrawScore ();

        LWMP_PopMatrix();
    }
}

void LWMP_DrawMinimap(void)
{
    int i, c;
    int32_t angle;
    int mipmap;
    LWMP_MinimapDraw_t draw;
    LWMP_MinimapState_t *ms = &lwmp.minimap.state;

    if (!ms->minimap_expand_mp)
    {
        return;
    }

    memset(&draw, 0, sizeof(draw));

    if (ms->rotated)
    {
        angle = (int32_t)player_mp->angle - 90;
        draw.rotmat = mat3fixed_rot(angle);
    }
    else
    {
        draw.rotmat = mat3fixed_ident();
    }

    draw.scx = vx(ms->minimap_xoff_mp);
    draw.scy = vy(ms->minimap_yoff_mp);
    draw.scw = vw(ms->minimap_width_mp);
    draw.sch = vh(ms->minimap_height_mp);
    draw.scdims = vec3fixed(FP(draw.scw), FP(draw.sch), 0);

    if (lwmp.layout == LWMP_LAYOUT_2x1)
    {
        draw.boxstep = ms->zoombox / draw.scw;
        draw.boxw = ms->zoombox;
        draw.boxh = FixedMul(draw.boxstep, FP(draw.sch));
    }
    else if (lwmp.layout == LWMP_LAYOUT_1x2)
    {
        draw.boxstep = ms->zoombox / draw.sch;
        draw.boxh = ms->zoombox;
        draw.boxw = FixedMul(draw.boxstep, FP(draw.scw));
    }
    draw.boxdims = vec3fixed(draw.boxw, draw.boxh, 0);
    draw.dotcolor = LWMP_MINIMAP_DOTCOLOR;

    for (LWMP_REPEAT(c))
    {
        draw.playerpoint_mp = 
            vec3fixed(player_mp->x, FP(64) - player_mp->y, FP(1));
        draw.playeranglevec_mp = 
            vec3fixed_anglevec((int32_t)player_mp->angle);
        angle = (int32_t)player_mp->angle;
        draw.arrowrotmat_mp = mat3fixed_rot(angle);
    }

    mipmap = LWEX_Log2((FP_TRUNC(ms->zoombox) * TEXTURESIZE) / draw.scw);
    mipmap = MAX(MIN(mipmap, TEXTURESHIFT - 1), 0);

    draw.stepu = vec3fixed(draw.boxstep, FP(0), FP(0));
    draw.stepv = vec3fixed(FP(0), draw.boxstep, FP(0));

    draw.stepu = mat3fixed_mult_point(draw.rotmat, draw.stepu);
    draw.stepv = mat3fixed_mult_point(draw.rotmat, draw.stepv);

    draw.corner = mat3fixed_mult_point(draw.rotmat,
        vec3fixed(-(draw.boxw / 2), -(draw.boxh / 2), FP(0)));

    draw.corner = vec3fixed_add(draw.corner, 
        vec3fixed(player_mp->x, FP(64) - player_mp->y, FP(1)));

    LWMP_PrepareMinimapPix(&draw, mipmap);

    draw.vbuf = LOCK();

    draw.ptv = draw.corner;
    for (draw.y = draw.sch - 1; draw.y >= 0; draw.y--)
    {
        draw.x = 0;
        draw.ptu = draw.ptv;
        draw.vbuf_scanline = 
            &draw.vbuf[(draw.scy + draw.y) * bufferPitch + draw.scx];

        while (draw.x < draw.scw)
        {
            LWMP_CalcMinimapPix(mipmap, &draw);
        }

        X(draw.ptv) += X(draw.stepv);
        Y(draw.ptv) += Y(draw.stepv);
    }

    LWMP_DrawMinimapPlayerSpots(&draw);

    UNLOCK();
}

void LWMP_ResetMinimapReveal(void)
{
    int c;
    vec2i_t pos;

    for (LWMP_REPEAT(c))
    {
        pos = vec2i(player_mp->tilex, 63 - player_mp->tiley);
        LWMP_RevealMinimapArea(X(pos), Y(pos), true);
        LWEX_SetMinimapRevealTile(pos, 
            LWEX_GetMinimapRevealTile(pos) | minimap_rflg_vis);
    }
}

void LWMP_RevealMinimapArea(int tilex, int tiley, bool clearspot)
{
    int i;
    byte reveal;
    vec2i_t pos, neighpos;
    int tile;
    doorobj_t *door;
    dir4_t dir;

    pos = vec2i(tilex, tiley);
    if (!TILE_IN_MAP(X(pos), Y(pos)))
    {
        return;
    }

    reveal = LWEX_GetMinimapRevealTile(pos);
    if (!clearspot && (reveal & minimap_rflg_area) != 0)
    {
        return;
    }
    reveal |= minimap_rflg_area;

    LWEX_SetMinimapRevealTile(pos, reveal);

    if (LWEX_TileIsWall(pos) && LWEX_GetTile(pos) != 64)
    {
        return;
    }

    if (LWEX_TileIsDoor(pos))
    {
        door = LWEX_GetDoor(pos);
        if (door->action == dr_closed)
        {
            return;
        }
    }

    dir = dir4_east;
    for (i = 0; i < 4; i++)
    {
        neighpos = vec2i_neigh(pos, dir);
        LWMP_RevealMinimapArea(X(neighpos), Y(neighpos), false);
        dir = dir4_next(dir, 1);
    }
}

void LWMP_CheckMinimapKeys(void)
{
    fixed zoominc;
    LWMP_MinimapState_t *ms = &lwmp.minimap.state;

    if (buttonstate_mp[bt_minimap] && !buttonheld_mp[bt_minimap] && 
        LWMP_NUM_PLAYERS == 2)
    {
        ms->minimap_expand_mp = !ms->minimap_expand_mp;
        NewViewSize(viewsize);
    }

    int zoomDirection = 0;

    if (LWMP_IsFirst())
    {
        ms->minimap_tics += tics;
        if (ms->minimap_tics > 0x8000)
        {
            ms->minimap_tics = 0;
        }

        if (Keyboard[sc_RightBracket] || Keyboard[sc_LeftBracket])
        {
            if (Keyboard[sc_RightBracket])
            {
                zoomDirection = -1;
            }

            if (Keyboard[sc_LeftBracket])
            {
                zoomDirection = 1;
            }
        }
    }

    if (ms->minimap_expand_mp) {
        int joyx, joyy;
        IN_GetJoyDelta (&joyx, &joyy, joyIndex_mp, SDL_CONTROLLER_AXIS_LEFTX, SDL_CONTROLLER_AXIS_RIGHTY, false);

        zoomDirection = joyy > 48 ? -1 : (joyy < -48 ? 1 : 0);

        if (zoomDirection != 0) {
            zoominc = FP(tics) / 2;

            if (zoomDirection < 0)
            {
                ms->zoombox += zoominc;
            }

            if (zoomDirection > 0)
            {
                ms->zoombox -= zoominc;
            }

            ms->zoombox = 
                MIN(ms->zoombox, FP(256));
            ms->zoombox =
                MAX(ms->zoombox, FP(1));
        }
    }
}

bool LWMP_SetViewSize(unsigned width, unsigned height)
{
    bool is_maxsize;
    LWMP_MinimapState_t *ms = &lwmp.minimap.state;

    viewwidth_mp = width & ~15;                  // must be divisable by 16
    viewheight_mp = height & ~1;                 // must be even

    is_maxsize = (viewheight_mp == MaxY);

    if (ms->minimap_expand_mp)
    {
        if (lwmp.layout == LWMP_LAYOUT_2x1)
        {
            ms->minimap_width_mp = viewwidth_mp;
            ms->minimap_height_mp = (viewheight_mp * LWMP_MINIMAP_HEIGHTRATIO) & ~1;
            viewheight_mp -= ms->minimap_height_mp;
        }
        else if (lwmp.layout == LWMP_LAYOUT_1x2)
        {
            ms->minimap_width_mp = (viewwidth_mp * LWMP_MINIMAP_WIDTHRATIO) & ~15;
            ms->minimap_height_mp = viewheight_mp;
            viewwidth_mp -= ms->minimap_width_mp;
        }
    }

    centerx_mp = vw(viewwidth_mp) / 2 - 1;
    shootdelta_mp = vw(viewwidth_mp) / 10;

    if(is_maxsize)
    {
        if (ms->minimap_expand_mp)
        {
            if (lwmp.layout == LWMP_LAYOUT_2x1)
            {
                viewscreenx_mp = 0;
                viewscreeny_mp = MaxY - viewheight_mp;
            }
            else if (lwmp.layout == LWMP_LAYOUT_1x2)
            {
                viewscreenx_mp = ms->minimap_width_mp;
                viewscreeny_mp = 0;
            }
        }
        else
        {
            viewscreenx_mp = viewscreeny_mp = screenofs_mp = 0;
        }
    }
    else
    {
        if (ms->minimap_expand_mp)
        {
            if (lwmp.layout == LWMP_LAYOUT_2x1)
            {
                viewscreenx_mp = (MaxX - viewwidth_mp) / 2;
                viewscreeny_mp = (MaxY - LWMP_StatusLines() - 
                    (viewheight_mp + ms->minimap_height_mp)) / 2 + 
                    ms->minimap_height_mp;
            }
            else if (lwmp.layout == LWMP_LAYOUT_1x2)
            {
                viewscreenx_mp = (MaxX - 
                    (viewwidth_mp + ms->minimap_width_mp)) / 2 +
                    ms->minimap_width_mp;
                viewscreeny_mp = (MaxY - LWMP_StatusLines() - viewheight_mp) / 2;
            }
        }
        else
        {
            viewscreenx_mp = (MaxX - viewwidth_mp) / 2;
            viewscreeny_mp = (MaxY - LWMP_StatusLines() - viewheight_mp) / 2;
        }
    }
    screenofs_mp = viewscreeny_mp * MaxX + viewscreenx_mp;

    if (ms->minimap_expand_mp)
    {
        if (lwmp.layout == LWMP_LAYOUT_2x1)
        {
            ms->minimap_xoff_mp = viewscreenx_mp;
            ms->minimap_yoff_mp = viewscreeny_mp - ms->minimap_height_mp;
        }
        else if (lwmp.layout == LWMP_LAYOUT_1x2)
        {
            ms->minimap_xoff_mp = viewscreenx_mp - ms->minimap_width_mp;
            ms->minimap_yoff_mp = viewscreeny_mp;
        }
    }

    //
    // calculate trace angles and projection constants
    //
    CalcProjection (FOCALLENGTH);

    return true;
}

bool LWMP_GetMinimapRotated(void)
{
    LWMP_MinimapState_t *ms = &lwmp.minimap.state;
    return ms->rotated;
}

void LWMP_SetMinimapRotated(bool minimapRotated)
{
    LWMP_MinimapState_t *ms = &lwmp.minimap.state;
    ms->rotated = minimapRotated;
}

int LWMP_SaveTheGame(FILE *file, int checksum)
{
    int c;

    SAVE_FIELD(lwmp.instanceNum);
    SAVE_FIELD(lwmp.numPlayers);
    SAVE_FIELD(lwmp.layout);
    SAVE_FIELD(lwmp.controlsInstance);
    SAVE_FIELD(lwmp.minimap.state);
    SAVE_FIELD(rndbits_y);
    SAVE_FIELD(rndmask);
    SAVE_FIELD(viewwidth);
    SAVE_FIELD(viewheight);
    SAVE_FIELD(centerx);
    SAVE_FIELD(shootdelta);
    SAVE_FIELD(viewscreenx);
    SAVE_FIELD(viewscreeny);
    SAVE_FIELD(screenofs);
    SAVE_FIELD(viewscale);
    SAVE_FIELD(heightnumerator);

    for (LWMP_REPEAT_MAX(c))
    {
        SAVE_ARRAY(pixelangle_mp, LWMP_GetPhysicalScreenWidth());
    }

    return checksum;
}

int LWMP_LoadTheGame(FILE *file, int checksum)
{
    int c;

    LOAD_FIELD(lwmp.instanceNum);
    LOAD_FIELD(lwmp.numPlayers);
    LOAD_FIELD(lwmp.layout);
    LOAD_FIELD(lwmp.controlsInstance);
    LOAD_FIELD(lwmp.minimap.state);
    LOAD_FIELD(rndbits_y);
    LOAD_FIELD(rndmask);
    LOAD_FIELD(viewwidth);
    LOAD_FIELD(viewheight);
    LOAD_FIELD(centerx);
    LOAD_FIELD(shootdelta);
    LOAD_FIELD(viewscreenx);
    LOAD_FIELD(viewscreeny);
    LOAD_FIELD(screenofs);
    LOAD_FIELD(viewscale);
    LOAD_FIELD(heightnumerator);

    for (LWMP_REPEAT_MAX(c))
    {
        LOAD_ARRAY(pixelangle_mp, LWMP_GetPhysicalScreenWidth());
    }

    return checksum;
}

bool LWMP_ObjectIsGibActor(objtype *ob)
{
    int i;
    LWMP_GibActor_t *gibActor;

    for (i = 0; i < COUNT_ARRAY(LWMP_GibActors); i++)
    {
        gibActor = &LWMP_GibActors[i];
        if (gibActor->obclass == ob->obclass)
        {
            break;
        }
    }

    return (i != COUNT_ARRAY(LWMP_GibActors));
}

void LWMP_GibPreKillActor(objtype *ob, objtype *attacker, bool forceRocketGib)
{
    int i, c;
	int rnd;
	int dx, dy, dist;
    LWMP_GibActor_t *gibActor;
    LWMP_GibMethod_t gibMethod;
    static const int actorCanGib[] =
    {
        playerobj, mechahitlerobj, realhitlerobj, gretelobj, giftobj,
        fatobj, spectreobj, angelobj, transobj, uberobj, willobj,
        deathobj, bjmutantobj, bjmutantdogobj,
    };

    if (attacker == NULL)
    {
        return;
    }

    if (ObjectIsPlayer(attacker) && InstagibMode::enabled())
    {
        forceRocketGib = true;
    }
    else if (ObjectIsPlayer(attacker) && !forceRocketGib)
    {
        for (LWMP_REPEAT_ONCE(c, attacker->player_index))
        {
            const weapontype weapon = Player::weapon();
            if (weapon != wp_chaingun && weapon != wp_tankgun &&
                weapon != wp_tankwheel)
            {
                LWMP_ENDREPEAT_ONCE(c);
                return;
            }
        }

        if (Tank::objIsDriver(attacker))
        {
            forceRocketGib = true;
        }
    }
    else
    {
        if (attacker->obclass == rocketobj || 
            attacker->obclass == hrocketobj ||
            attacker->obclass == sparkobj)
        {
            forceRocketGib = true;
        }
        else if (!forceRocketGib)
        {
            if 
                (
                    (
                        attacker->obclass == bjmutantobj ||
                        attacker->obclass == bjmutantdogobj
                    ) &&
                    attacker->bjMutantWeapon != wp_chaingun
                )
            {
                return;
            }
            else
            {
                for (i = 0; i < COUNT_ARRAY(actorCanGib); i++)
                {
                    if (attacker->obclass == actorCanGib[i])
                    {
                        break;
                    }
                }

                if (i == COUNT_ARRAY(actorCanGib))
                {
                    return;
                }
            }
        }
    }

    for (i = 0; i < COUNT_ARRAY(LWMP_GibActors); i++)
    {
        gibActor = &LWMP_GibActors[i];
        if (gibActor->obclass == ob->obclass)
        {
            break;
        }
    }

    if (i == COUNT_ARRAY(LWMP_GibActors))
    {
        return;
    }

    dx = ABS(ob->tilex - attacker->tilex);
    dy = ABS(ob->tiley - attacker->tiley);
    dist = dx > dy ? dx : dy;

    if (forceRocketGib || Gib::extremeDamage(ob))
    {
        gibMethod = GIB_METHOD_ROCKET;
    }
    else
    {
        if (dist <= 6)
        {
            rnd = US_RndT() * 100 / 255;
            if (rnd < (dist <= 2 ? 25 : 30))
            {
                gibMethod = GIB_METHOD_CHAINGUN;
            }
            else if (!ObjectIsPlayer(attacker) &&
                rnd < (dist <= 2 ? 75 : 60))
            {
                gibMethod = GIB_METHOD_ROCKET;
            }
            else
            {
                return;
            }
        }
        else
        {
            return;
        }
    }

    ob->gibbed = true;
    ob->gibActorId = i;
    ob->gibMethod = gibMethod;
}

int LWMP_GibRemapShape(objtype *ob, int shapenum)
{
    int i;
    LWMP_GibActor_t *gibActor;
    int *spriteNums;

    if (ob->gibbed)
    {
        gibActor = &LWMP_GibActors[ob->gibActorId];
        spriteNums = gibActor->spriteNums[ob->gibMethod];

        for (i = 0; spriteNums[i] != 0; i += 2)
        {
            if (shapenum == spriteNums[i])
            {
                shapenum = spriteNums[i + 1];
                break;
            }
        }
    }

    return shapenum;
}
