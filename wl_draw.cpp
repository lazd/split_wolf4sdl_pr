// WL_DRAW.C

#include "wl_def.h"
#pragma hdrstop

#include "wl_cloudsky.h"
#include "wl_atmos.h"
#include "wl_shade.h"

/*
=============================================================================

                               LOCAL CONSTANTS

=============================================================================
*/

// the door is the last picture before the sprites
#define DOORWALL        (PMSpriteStart-8)

#define ACTORSIZE       0x4000

#ifdef MIRWALL
#define TRUCKTILE1 61 
#define TRUCKTILE2 62 
#define TRUCKTILE3 63 
#endif

/*
=============================================================================

                              GLOBAL VARIABLES

=============================================================================
*/

byte *vbuf = NULL;
unsigned vbufPitch = 0;

int32_t    lasttimecount;
int32_t    frameon;
boolean fpscounter;

int fps_frames=0, fps_time=0, fps=0;

int *wallheight;
int min_wallheight;

//
// math tables
//
short *LWMP_DECL(pixelangle);
int32_t finetangent[FINEANGLES/4];
fixed sintable[ANGLES+ANGLES/4];
fixed *costable = sintable+(ANGLES/4);

//
// refresh variables
//

// the focal point
fixed   viewx;
fixed   viewy;

short   viewangle;
fixed   viewsin,viewcos;

void    TransformActor (objtype *ob);
void    BuildTables (void);
void    ClearScreen (void);
int     CalcRotate (objtype *ob);
void    DrawScaleds (void);
void    CalcTics (void);



//
// wall optimization variables
//
int     lastside;               // true for vertical
int32_t    lastintercept;
int     lasttilehit;
int     lasttexture;

//
// ray tracing variables
//
short    focaltx,focalty,viewtx,viewty;
longword xpartialup,xpartialdown,ypartialup,ypartialdown;

short   midangle,angle;

word    tilehit;
int     pixx;

short   xtile,ytile;
short   xtilestep,ytilestep;
int32_t    xintercept,yintercept;
word    xstep,ystep;
word    xspot,yspot;
int     texdelta;

word horizwall[MAXWALLTILES],vertwall[MAXWALLTILES];

/*
============================================================================

                           3 - D  DEFINITIONS

============================================================================
*/

/*
========================
=
= TransformActor
=
= Takes paramaters:
=   gx,gy               : globalx/globaly of point
=
= globals:
=   viewx,viewy         : point of view
=   viewcos,viewsin     : sin/cos of viewangle
=   scale               : conversion from global value to screen value
=
= sets:
=   screenx,transx,transy,screenheight: projected edge location and size
=
========================
*/


//
// transform actor
//
void TransformActor (objtype *ob)
{
    fixed gx,gy,gxt,gyt,nx,ny;

//
// translate point to view centered coordinates
//
    gx = ob->x - viewx;
    gy = ob->y - viewy;

//
// calculate newx
//
    gxt = FixedMul(gx,viewcos);
    gyt = FixedMul(gy,viewsin);
    nx = gxt-gyt-ACTORSIZE;         // fudge the shape forward a bit, because
                                    // the midpoint could put parts of the shape
                                    // into an adjacent wall

//
// calculate newy
//
    gxt = FixedMul(gx,viewsin);
    gyt = FixedMul(gy,viewcos);
    ny = gyt+gxt;

//
// calculate perspective ratio
//
    ob->transx_mp = nx;
    ob->transy_mp = ny;

    if (nx<MINDIST)                 // too close, don't overflow the divide
    {
        ob->viewheight_mp = 0;
        return;
    }

    ob->viewx_mp = (word)(centerx_mp + ny*viewscale_mp/nx);

//
// calculate height (heightnumerator_mp/(nx>>8))
//
    ob->viewheight_mp = (word)(heightnumerator_mp/(nx>>8));
}

//==========================================================================

/*
========================
=
= TransformTile
=
= Takes paramaters:
=   tx,ty               : tile the object is centered in
=
= globals:
=   viewx,viewy         : point of view
=   viewcos,viewsin     : sin/cos of viewangle
=   scale               : conversion from global value to screen value
=
= sets:
=   screenx,transx,transy,screenheight: projected edge location and size
=
= Returns true if the tile is withing getting distance
=
========================
*/

boolean TransformTile (int tx, int ty, short *dispx, short *dispheight)
{
    fixed gx,gy,gxt,gyt,nx,ny;

//
// translate point to view centered coordinates
//
    gx = ((int32_t)tx<<TILESHIFT)+0x8000 - viewx;
    gy = ((int32_t)ty<<TILESHIFT)+0x8000 - viewy;

//
// calculate newx
//
    gxt = FixedMul(gx,viewcos);
    gyt = FixedMul(gy,viewsin);
    nx = gxt-gyt-0x2000;            // 0x2000 is size of object

//
// calculate newy
//
    gxt = FixedMul(gx,viewsin);
    gyt = FixedMul(gy,viewcos);
    ny = gyt+gxt;


//
// calculate height / perspective ratio
//
    if (nx<MINDIST)                 // too close, don't overflow the divide
        *dispheight = 0;
    else
    {
        *dispx = (short)(centerx_mp + ny*viewscale_mp/nx);
        *dispheight = (short)(heightnumerator_mp/(nx>>8));
    }

//
// see if it should be grabbed
//
    if (nx<TILEGLOBAL && ny>-TILEGLOBAL/2 && ny<TILEGLOBAL/2)
        return true;
    else
        return false;
}

//==========================================================================

/*
====================
=
= CalcHeight
=
= Calculates the height of xintercept,yintercept from viewx,viewy
=
====================
*/

int CalcHeight()
{
    fixed z = FixedMul(xintercept - viewx, viewcos)
        - FixedMul(yintercept - viewy, viewsin);
    if(z < MINDIST) z = MINDIST;
    int height = heightnumerator_mp / (z >> 8);
    if(height < min_wallheight) min_wallheight = height;
    return height;
}

//==========================================================================

/*
===================
=
= ScalePost
=
===================
*/

byte *postsource;
int postx;
int postwidth;

void ScalePost()
{
    int ywcount, yoffs, yw, yd, yendoffs;
    byte col;
    int midy;

    #ifdef HORIZON
    if (tilehit == 85) return;
    #endif

#ifdef USE_SHADING
    byte *curshades = shadetable[GetShade(wallheight[postx])];
#endif

    ywcount = yd = wallheight[postx] >> 3;
    if(yd <= 0) yd = 100;

    midy = fixedpt_toint((FP(1) + (Player::camZ() - 
        0x8000) * 2) * ywcount) + (vh(viewheight_mp) / 2);

    yoffs = (midy - ywcount) * vbufPitch;
    if(yoffs < 0) yoffs = 0;
    yoffs += postx;

    yendoffs = midy + ywcount - 1;
    yw=TEXTURESIZE-1;

    while(yendoffs >= vh(viewheight_mp))
    {
        ywcount -= TEXTURESIZE/2;
        while(ywcount <= 0)
        {
            ywcount += yd;
            yw--;
        }
        yendoffs--;
    }
    if(yw < 0) return;

#ifdef USE_SHADING
    col = curshades[postsource[yw]];
#else
    col = postsource[yw];
#endif
    yendoffs = yendoffs * vbufPitch + postx;
    while(yoffs <= yendoffs)
    {
        vbuf[yendoffs] = col;
        ywcount -= TEXTURESIZE/2;
        if(ywcount <= 0)
        {
            do
            {
                ywcount += yd;
                yw--;
            }
            while(ywcount <= 0);
            if(yw < 0) break;
#ifdef USE_SHADING
            col = curshades[postsource[yw]];
#else
            col = postsource[yw];
#endif
        }
        yendoffs -= vbufPitch;
    }
}

void GlobalScalePost(byte *vidbuf, unsigned pitch)
{
    vbuf = vidbuf;
    vbufPitch = pitch;
    ScalePost();
}

/*
====================
=
= HitVertWall
=
= tilehit bit 7 is 0, because it's not a door tile
= if bit 6 is 1 and the adjacent tile is a door tile, use door side pic
=
====================
*/

void HitVertWall (void)
{
    int wallpic;
    int texture;

    texture = ((yintercept+texdelta)>>TEXTUREFROMFIXEDSHIFT)&TEXTUREMASK;
    if (xtilestep == -1)
    {
        #ifdef MIRWALL
        if(tilehit!=TRUCKTILE1 && tilehit!=TRUCKTILE2 && tilehit!=TRUCKTILE3)
        #endif 
        texture = TEXTUREMASK-texture;
        xintercept += TILEGLOBAL;
    }

    if(lastside==1 && lastintercept==xtile && lasttilehit==tilehit && !(lasttilehit & 0x40))
    {
        if((pixx&3) && texture == lasttexture)
        {
            ScalePost();
            postx = pixx;
            wallheight[pixx] = wallheight[pixx-1];
            return;
        }
        ScalePost();
        wallheight[pixx] = CalcHeight();
        postsource+=texture-lasttexture;
        postwidth=1;
        postx=pixx;
        lasttexture=texture;
        return;
    }

    if(lastside!=-1) ScalePost();

    lastside=1;
    lastintercept=xtile;
    lasttilehit=tilehit;
    lasttexture=texture;
    wallheight[pixx] = CalcHeight();
    postx = pixx;
    postwidth = 1;

    if (tilehit & 0x40)
    {                                                               // check for adjacent doors
        ytile = (short)(yintercept>>TILESHIFT);
        if ( tilemap[xtile-xtilestep][ytile]&0x80 )
            wallpic = DOORWALL+3;
        else
            wallpic = vertwall[tilehit & ~0x40];
    }
    else
        wallpic = vertwall[tilehit];

    postsource = PM_GetTexture(wallpic) + texture;
}


/*
====================
=
= HitHorizWall
=
= tilehit bit 7 is 0, because it's not a door tile
= if bit 6 is 1 and the adjacent tile is a door tile, use door side pic
=
====================
*/

void HitHorizWall (void)
{
    int wallpic;
    int texture;

    texture = ((xintercept+texdelta)>>TEXTUREFROMFIXEDSHIFT)&TEXTUREMASK;
    if (ytilestep == -1)
        yintercept += TILEGLOBAL;
    else
    #ifdef MIRWALL
    if(tilehit!=TRUCKTILE1 && tilehit!=TRUCKTILE2 && tilehit!=TRUCKTILE3)
    #endif
        texture = TEXTUREMASK-texture;

    if(lastside==0 && lastintercept==ytile && lasttilehit==tilehit && !(lasttilehit & 0x40))
    {
        if((pixx&3) && texture == lasttexture)
        {
            ScalePost();
            postx=pixx;
            wallheight[pixx] = wallheight[pixx-1];
            return;
        }
        ScalePost();
        wallheight[pixx] = CalcHeight();
        postsource+=texture-lasttexture;
        postwidth=1;
        postx=pixx;
        lasttexture=texture;
        return;
    }

    if(lastside!=-1) ScalePost();

    lastside=0;
    lastintercept=ytile;
    lasttilehit=tilehit;
    lasttexture=texture;
    wallheight[pixx] = CalcHeight();
    postx = pixx;
    postwidth = 1;

    if (tilehit & 0x40)
    {                                                               // check for adjacent doors
        xtile = (short)(xintercept>>TILESHIFT);
        if ( tilemap[xtile][ytile-ytilestep]&0x80)
            wallpic = DOORWALL+2;
        else
            wallpic = horizwall[tilehit & ~0x40];
    }
    else
        wallpic = horizwall[tilehit];

    postsource = PM_GetTexture(wallpic) + texture;
}

//==========================================================================

/*
====================
=
= HitHorizDoor
=
====================
*/

void HitHorizDoor (void)
{
    int doorpage;
    int doornum;
    int texture;

    doornum = tilehit&0x7f;
    texture = ((xintercept-doorposition[doornum])>>TEXTUREFROMFIXEDSHIFT)&TEXTUREMASK;

    if(lasttilehit==tilehit)
    {
        if((pixx&3) && texture == lasttexture)
        {
            ScalePost();
            postx=pixx;
            wallheight[pixx] = wallheight[pixx-1];
            return;
        }
        ScalePost();
        wallheight[pixx] = CalcHeight();
        postsource+=texture-lasttexture;
        postwidth=1;
        postx=pixx;
        lasttexture=texture;
        return;
    }

    if(lastside!=-1) ScalePost();

    lastside=2;
    lasttilehit=tilehit;
    lasttexture=texture;
    wallheight[pixx] = CalcHeight();
    postx = pixx;
    postwidth = 1;

    switch(doorobjlist[doornum].lock)
    {
        case dr_normal:
            doorpage = DOORWALL;
            break;
        case dr_lock1:
        case dr_lock2:
        case dr_lock3:
        case dr_lock4:
            doorpage = DOORWALL+6;
            break;
        case dr_elevator:
            doorpage = DOORWALL+4;
            break;
    }

    postsource = PM_GetTexture(doorpage) + texture;
}

//==========================================================================

/*
====================
=
= HitVertDoor
=
====================
*/

void HitVertDoor (void)
{
    int doorpage;
    int doornum;
    int texture;

    doornum = tilehit&0x7f;
    texture = ((yintercept-doorposition[doornum])>>TEXTUREFROMFIXEDSHIFT)&TEXTUREMASK;

    if(lasttilehit==tilehit)
    {
        if((pixx&3) && texture == lasttexture)
        {
            ScalePost();
            postx=pixx;
            wallheight[pixx] = wallheight[pixx-1];
            return;
        }
        ScalePost();
        wallheight[pixx] = CalcHeight();
        postsource+=texture-lasttexture;
        postwidth=1;
        postx=pixx;
        lasttexture=texture;
        return;
    }

    if(lastside!=-1) ScalePost();

    lastside=2;
    lasttilehit=tilehit;
    lasttexture=texture;
    wallheight[pixx] = CalcHeight();
    postx = pixx;
    postwidth = 1;

    switch(doorobjlist[doornum].lock)
    {
        case dr_normal:
            doorpage = DOORWALL+1;
            break;
        case dr_lock1:
        case dr_lock2:
        case dr_lock3:
        case dr_lock4:
            doorpage = DOORWALL+7;
            break;
        case dr_elevator:
            doorpage = DOORWALL+5;
            break;
    }

    postsource = PM_GetTexture(doorpage) + texture;
}

//==========================================================================

#define HitHorizBorder HitHorizWall
#define HitVertBorder HitVertWall

//==========================================================================

byte vgaCeiling[]=
{
#ifndef SPEAR
 0x1d,0x1d,0x1d,0x1d,0x1d,0x1d,0x1d,0x1d,0x1d,0xbf,
 0x4e,0x4e,0x4e,0x1d,0x8d,0x4e,0x1d,0x2d,0x1d,0x8d,
 0x1d,0x1d,0x1d,0x1d,0x1d,0x2d,0xdd,0x1d,0x1d,0x98,

 0x1d,0x9d,0x2d,0xdd,0xdd,0x9d,0x2d,0x4d,0x1d,0xdd,
 0x7d,0x1d,0x2d,0x2d,0xdd,0xd7,0x1d,0x1d,0x1d,0x2d,
 0x1d,0x1d,0x1d,0x1d,0xdd,0xdd,0x7d,0xdd,0xdd,0xdd
#else
 0x6f,0x4f,0x1d,0xde,0xdf,0x2e,0x7f,0x9e,0xae,0x7f,
 0x1d,0xde,0xdf,0xde,0xdf,0xde,0xe1,0xdc,0x2e,0x1d,0xdc
#endif
};

/*
=====================
=
= VGAClearScreen
=
=====================
*/

void VGAClearScreen (void)
{
    byte ceiling=vgaCeiling[gamestate.episode*10+mapon];

    int y;
    byte *ptr = vbuf;
#ifdef USE_SHADING
    for(y = 0; y < vh(viewheight_mp) / 2; y++, ptr += vbufPitch)
        memset(ptr, shadetable[GetShade((vh(viewheight_mp) / 2 - y) << 3)][ceiling], vw(viewwidth_mp));
    for(; y < vh(viewheight_mp); y++, ptr += vbufPitch)
        memset(ptr, shadetable[GetShade((y - vh(viewheight_mp) / 2) << 3)][0x19], vw(viewwidth_mp));
#else
    for(y = 0; y < vh(viewheight_mp) / 2; y++, ptr += vbufPitch)
        memset(ptr, ceiling, vw(viewwidth_mp));
    for(; y < vh(viewheight_mp); y++, ptr += vbufPitch)
        memset(ptr, 0x19, vw(viewwidth_mp));
#endif
}

//==========================================================================

int ObjectPlayerIndex(objtype *obj)
{
    assert(obj != NULL && obj - objlist < LWMP_NUM_PLAYERS);
    return obj - objlist;
}

bool ObjectIsPlayer(objtype *obj)
{
    return obj != NULL && (obj - objlist < LWMP_NUM_PLAYERS);
}

/*
=====================
=
= CalcObjectAngle
=
=====================
*/
int CalcObjectAngle(objtype *obj)
{
    int c;
    int angle, viewangle, objangle;
    int dx, dy;
    float fangle;
    int playerindex;

    // this isn't exactly correct, as it should vary by a trig value,
    // but it is close enough with only eight rotations

    viewangle = Player::viewAngle(player_mp) + 
        (centerx_mp - obj->viewx_mp) / 8;

    if (obj->obclass == rocketobj || obj->obclass == hrocketobj ||
        obj->obclass == tankobj)
    {
        angle = (viewangle-180) - obj->angle;
    }
    else
    {
        objangle = dirangle[obj->dir];
        if (ObjectIsPlayer(obj))
        {
            objangle = Player::viewAngle(obj);
        }
        else
        {
            if ((obj->shooting && obj->targetplayer > 0) ||
                (obj->attackingPlayer > 0))
            {
                if (obj->attackingPlayer > 0)
                {
                    playerindex = obj->attackingPlayer - 1;
                }
                else // obj->shooting && obj->targetplayer > 0
                {
                    playerindex = obj->targetplayer - 1;
                }

                for (LWMP_REPEAT_ONCE(c, playerindex))
                {
                    dx = player_mp->x - obj->x;
                    dy = obj->y - player_mp->y;
                }

                // returns -pi to pi
                fangle = (float)atan2((float)dy, (float)dx);
                if (fangle < 0) // to [0, 2 * pi]
                    fangle = (float)(M_PI * 2 + fangle);

                objangle = (int) (fangle / (M_PI * 2) * ANGLES);
            }
        }

        angle = (viewangle-180) - objangle;
    }

    angle += ANGLES / 16;
    while (angle >= ANGLES)
    {
        angle -= ANGLES;
    }
    while (angle < 0)
    {
        angle += ANGLES;
    }

    return angle;
}

/*
=====================
=
= CalcRotate
=
=====================
*/

int CalcRotate (objtype *ob)
{
    int angle;

    angle = CalcObjectAngle(ob);
    if (ob->state->rotate == 2)             // 2 rotation pain frame
        return 0;               // pain with shooting frame bugfix

    return angle/(ANGLES/8);
}

void ScaleShape (int xcenter, int shapenum, unsigned height, uint32_t flags)
{
    t_compshape *shape;
    unsigned scale,pixheight;
    unsigned starty,endy;
    word *cmdptr;
    byte *cline;
    byte *line;
    byte *vmem;
    int actx,i,upperedge;
    short newstart;
    int scrstarty,screndy,lpix,rpix,pixcnt,ycnt;
    unsigned j;
    byte col;

#ifdef USE_SHADING
    byte *curshades;
    if(flags & FL_FULLBRIGHT)
        curshades = shadetable[0];
    else
        curshades = shadetable[GetShade(height)];
#endif

    shape = (t_compshape *)LWMP_GetShape(shapenum);
    scale = height >> 3;  // low three bits are fractional
    if (!scale)
    {
        return;   // too close or far away
    }

    pixheight = scale * SPRITESCALEFACTOR;
    actx = xcenter - scale;
    upperedge = (vh(viewheight_mp) / 2) - 
        fixedpt_toint((FP(1) - Player::camZ() * 2) *
        scale);

    cmdptr=(word *) shape->dataofs;
    pixcnt = shape->leftpix * pixheight;
    rpix = (pixcnt >> 6) + actx;

    for (i = shape->leftpix; i <= shape->rightpix; i++, cmdptr++)
    {
        lpix = rpix;
        if (lpix >= vw(viewwidth_mp))
        {
            break;
        }
        pixcnt += pixheight;
        rpix = (pixcnt >> 6) + actx;
        if (lpix != rpix && rpix > 0)
        {
            if (lpix < 0)
            {
                lpix = 0;
            }
            if (rpix > vw(viewwidth_mp))
            {
                rpix = vw(viewwidth_mp);
                i = shape->rightpix + 1;
            }
            cline = (byte *)shape + *cmdptr;
            while (lpix < rpix)
            {
                if (wallheight[lpix] <= (int)height)
                {
                    line = cline;
                    while ((endy = READWORD(line)) != 0)
                    {
                        endy >>= 1;
                        newstart = READWORD(line);
                        starty = READWORD(line) >> 1;
                        j = starty;
                        ycnt = j * pixheight;
                        screndy = (ycnt>>6) + upperedge;
                        if (screndy < 0)
                        {
                            vmem = vbuf + lpix;
                        }
                        else
                        {
                            vmem = vbuf + screndy * vbufPitch + lpix;
                        }
                        for (; j < endy; j++)
                        {
                            scrstarty = screndy;
                            ycnt += pixheight;
                            screndy = (ycnt>>6) + upperedge;
                            if (scrstarty != screndy && screndy > 0)
                            {
#ifdef USE_SHADING
                                col = curshades[((byte *)shape)[newstart + j]];
#else
                                col = ((byte *)shape)[newstart + j];
#endif
                                if (scrstarty < 0)
                                {
                                    scrstarty = 0;
                                }
                                if (screndy > vh(viewheight_mp))
                                {
                                    screndy = vh(viewheight_mp);
                                    j = endy;
                                }

                                while (scrstarty < screndy)
                                {
                                    *vmem = color_remap[col];
                                    vmem += vbufPitch;
                                    scrstarty++;
                                }
                            }
                        }
                    }
                }
                lpix++;
            }
        }
    }
}

void SimpleScaleShape (int shapenum)
{
    t_compshape   *shape;
    unsigned scale,pixheight;
    unsigned starty,endy;
    word *cmdptr;
    byte *cline;
    byte *line;
    int actx,i,upperedge;
    short newstart;
    int scrstarty,screndy,lpix,rpix,pixcnt,ycnt;
    unsigned j;
    byte col;
    byte *vmem;

    shape = (t_compshape *)LWMP_GetShape(shapenum);

    scale = LWMP_MIN(vh(viewheight_mp), vw(viewwidth_mp)) * 40 / 100;
    pixheight = scale * SPRITESCALEFACTOR;
    actx = vw(viewwidth_mp) / 2 - scale;
    upperedge = vh(viewheight_mp) - scale * 2;

    cmdptr=shape->dataofs;

    for(i=shape->leftpix,pixcnt=i*pixheight,rpix=(pixcnt>>6)+actx;i<=shape->rightpix;i++,cmdptr++)
    {
        lpix=rpix;
        if(lpix>=vw(viewwidth_mp)) break;
        pixcnt+=pixheight;
        rpix=(pixcnt>>6)+actx;
        if(lpix!=rpix && rpix>0)
        {
            if(lpix<0) lpix=0;
            if(rpix>vw(viewwidth_mp)) rpix=vw(viewwidth_mp),i=shape->rightpix+1;
            cline = (byte *)shape + *cmdptr;
            while(lpix<rpix)
            {
                line=cline;
                while((endy = READWORD(line)) != 0)
                {
                    endy >>= 1;
                    newstart = READWORD(line);
                    starty = READWORD(line) >> 1;
                    j=starty;
                    ycnt=j*pixheight;
                    screndy=(ycnt>>6)+upperedge;
                    if(screndy<0) vmem=vbuf+lpix;
                    else vmem=vbuf+screndy*vbufPitch+lpix;
                    for(;j<endy;j++)
                    {
                        scrstarty=screndy;
                        ycnt+=pixheight;
                        screndy=(ycnt>>6)+upperedge;
                        if(scrstarty!=screndy && screndy>0)
                        {
                            col=((byte *)shape)[newstart+j];
                            if(scrstarty<0) scrstarty=0;
                            if(screndy>vh(viewheight_mp))
                            {
                                screndy=vh(viewheight_mp);
                                j=endy;
                            }

                            while(scrstarty<screndy)
                            {
                                *vmem=color_remap[col];
                                vmem+=vbufPitch;
                                scrstarty++;
                            }
                        }
                    }
                }
                lpix++;
            }
        }
    }
}

typedef struct
{
    short      LWMP_DECL(viewx);
    short      LWMP_DECL(viewheight);
    short      shapenum;
    short      flags;          // this must be changed to uint32_t, when you
                               // you need more than 16-flags for drawing
    short      shade;
#ifdef USE_DIR3DSPR
    statobj_t *transsprite;
#endif
} visobj_t;

visobj_t vislist[MAXVISABLE];
visobj_t *visptr,*visstep,*farthest;

int CalcObjectShapeNum(objtype *obj, int shapenum)
{
    int c;
    int frames;
    int group;
    const int weapToSpriteGroup[NUMWEAPONSEXTRA] = {
        1, 2, 0, 3, 4, 5, 6, 7, 8
        };

    ZombieMode::remapShapeNum(shapenum);
    ZombieHarvesterMode::remapShapeNum(obj, shapenum);

    shapenum = LWMP_GibRemapShape(obj, shapenum);

    if (shapenum == -1)
    {
        shapenum = obj->temp1; // special shape
    }
    if (obj->state->rotate)
    {
        shapenum += CalcRotate(obj);
    }

    if (ObjectIsPlayer(obj))
    {
        for (LWMP_REPEAT_ONCE(c, ObjectPlayerIndex(obj)))
        {
            assert(player_mp == obj);

            if (player_mp->state == &s_player)
            {
                if (thrustspeed_mp > 0 || gamestate.moveanimtics_mp != 0)
                {
                    frames = FP_TRUNC(player_mp->run_dist) % 4;
                    shapenum = SPR_BJ_M1 + frames;
                }

                if (gamestate.pain_frame_mp != 0)
                {
                    shapenum = SPR_BJ_PAIN_1 + gamestate.pain_frame_mp - 1;
                }
            }

            if ((player_mp->state == &s_player || 
                player_mp->state == &s_attack) && 
                gamestate.health_mp > 0 && 
                gamestate.weapon_mp != -1)
            {
                group = weapToSpriteGroup[Player::weapon()];
                shapenum += SPR_BJ_NUMSPRITES * group;
            }

            if (player_mp->state == &s_attack && gamestate.weapon_mp != -1)
            {
                shapenum = CalcPlayerWeaponShapeNum();
                assert(shapenum != -1);
                shapenum = shapenum - SPR_KNIFEREADY + SPR_BJ_KNIFEREADY;
            }
        }
    }
    else if (obj->obclass == bjmutantobj || obj->obclass == bjmutantdogobj)
    {
        if (shapenum >= SPR_BJ_STAND && shapenum <= SPR_BJ_PAIN_2)
        {
            assert(obj->bjMutantWeapon != -1);
            group = weapToSpriteGroup[obj->bjMutantWeapon];
            shapenum += SPR_BJ_NUMSPRITES * group;
        }
    }

    shapenum = LWMP_RotateSprite(
        CalcObjectAngle(obj), shapenum);

    return shapenum;
}

bool PlayerShadeIsUsed(int shade)
{
    int c;
    bool used = false;

    if (BjMutantMode::enabled() && shade == COLORREMAP_SHADE_GREEN)
    {
        return true;
    }

    for (LWMP_REPEAT(c))
    {
        if (gamestate.player_shade_mp == shade + 1)
        {
            used = true;
        }
    }

    return used;
}

int CalcPlayerShadesFree(int *free_shades)
{
    int i;
    int num_free_shades;

    num_free_shades = 0;
    for (i = 0; i < COLORREMAP_SHADE_MAX; i++)
    {
        if (!PlayerShadeIsUsed(i))
        {
            assert(num_free_shades < COLORREMAP_SHADE_MAX);
            free_shades[num_free_shades++] = i;
        }
    }

    return num_free_shades;
}

int CalcObjectShade(objtype *obj)
{
    int shade;
    int c;

    shade = 0;
    if (obj->shade != 0)
    {
        shade = obj->shade - 1;
    }
    else if (ObjectIsPlayer(obj) || obj->obclass == bjobj || 
        obj->obclass == bjmutantobj || obj->obclass == bjmutantdogobj)
    {
        if (obj->player_index != -1)
        {
            for (LWMP_REPEAT_ONCE(c, obj->player_index))
            {
                if (gamestate.player_shade_mp != 0)
                {
                    shade = gamestate.player_shade_mp - 1;
                }
            }
        }
        if (obj->obclass == bjmutantobj || obj->obclass == bjmutantdogobj)
        {
            shade += COLORREMAP_SHADE_MAX;
        }
    }

    return shade;
}

/*
=====================
=
= DrawScaleds
=
= Draws all objects that are visable
=
=====================
*/
void DrawScaleds (void)
{
    int      i,least,numvisable,height;
    byte     *tilespot,*visspot_ptr;
    unsigned spotloc;

    statobj_t *statptr;
    objtype   *obj;

    visptr = &vislist[0];

    frameon+=tics;
    frameon%=2520;

//
// place static objects
//
    for (statptr = &statobjlist[0] ; statptr !=laststatobj ; statptr++)
    {
        if ((visptr->shapenum = statptr->shapenum) == -1)
            continue;                                               // object has been deleted

        if (!*statptr->visspot_mp)
            continue;                                               // not visable

        if (VampireMode::enabled())
        {
            VampireMode::remapShape((wl_stat_t)statptr->itemnumber, visptr->shapenum);
        }

        if (TransformTile (statptr->tilex,statptr->tiley,
            &visptr->viewx_mp,&visptr->viewheight_mp) && statptr->flags & FL_BONUS)
        {
            GetBonus (statptr);
            if(statptr->shapenum == -1)
                continue;                                           // object has been taken
        }

        if (!visptr->viewheight_mp)
            continue;                                               // to close to the object

#ifdef USE_DIR3DSPR
        if(statptr->flags & FL_DIR_MASK)
            visptr->transsprite=statptr;
        else
            visptr->transsprite=NULL;
#endif
        visptr->shade = COLORREMAP_SHADE_DEFAULT;
        if (BjMutantMode::enabled() && statptr->itemnumber == bo_gibs)
        {
            visptr->shade = COLORREMAP_SHADE_MAX;
        }

        if (visptr < &vislist[MAXVISABLE-1])    // don't let it overflow
        {
            visptr->flags = (short) statptr->flags;
            visptr->shapenum = LWMP_RotateSprite(0,
                visptr->shapenum);
            visptr++;
        }
    }

//
// place active objects
//
    for (obj = objlist; obj; obj = obj->next)
    {
        if (obj == player_mp)
        {
            continue;
        }

        if (ObjectIsPlayer(obj) && obj->victoryBjSpawn)
        {
            continue;
        }

        if ((visptr->shapenum = obj->state->shapenum)==0)
        {
            continue;                                               // no shape
        }

        spotloc = (obj->tilex<<mapshift)+obj->tiley;   // optimize: keep in struct?
        visspot_ptr = &spotvis_mp[0][0]+spotloc;
        tilespot = &tilemap[0][0]+spotloc;

        //
        // could be in any of the nine surrounding tiles
        //
        if (*visspot_ptr
            || ( *(visspot_ptr-1) && !*(tilespot-1) )
            || ( *(visspot_ptr+1) && !*(tilespot+1) )
            || ( *(visspot_ptr-65) && !*(tilespot-65) )
            || ( *(visspot_ptr-64) && !*(tilespot-64) )
            || ( *(visspot_ptr-63) && !*(tilespot-63) )
            || ( *(visspot_ptr+65) && !*(tilespot+65) )
            || ( *(visspot_ptr+64) && !*(tilespot+64) )
            || ( *(visspot_ptr+63) && !*(tilespot+63) ) )
        {
            obj->active = ac_yes;
            TransformActor (obj);
            if (!obj->viewheight_mp)
                continue;          // too close or far away

            if (obj->flags & FL_NODRAW)
            {
                continue;
            }

            visptr->viewx_mp = obj->viewx_mp;
            visptr->viewheight_mp = obj->viewheight_mp;
            visptr->shapenum = CalcObjectShapeNum(obj, 
                visptr->shapenum);
            visptr->shade = CalcObjectShade(obj);

            if (visptr < &vislist[MAXVISABLE-1])    // don't let it overflow
            {
                visptr->flags = (short) obj->flags;
#ifdef USE_DIR3DSPR
                visptr->transsprite = NULL;
#endif
                visptr++;
            }
            obj->visflags_mp |= FL_VISABLE;
        }
        else
        {
            obj->visflags_mp &= ~FL_VISABLE;
        }
    }

//
// draw from back to front
//
    numvisable = (int) (visptr-&vislist[0]);

    if (!numvisable)
        return;                                                                 // no visable objects

    for (i = 0; i<numvisable; i++)
    {
        least = 32000;
        for (visstep=&vislist[0] ; visstep<visptr ; visstep++)
        {
            height = visstep->viewheight_mp;
            if (height < least)
            {
                least = height;
                farthest = visstep;
            }
        }

        ColorRemap_SetShade(farthest->shade);

        //
        // draw farthest
        //
#ifdef USE_DIR3DSPR
        if(farthest->transsprite)
            Scale3DShape(vbuf, vbufPitch, farthest->transsprite);
        else
#endif
            ScaleShape(farthest->viewx_mp, farthest->shapenum, farthest->viewheight_mp, farthest->flags);

        ColorRemap_SetShade(COLORREMAP_SHADE_DEFAULT);

        farthest->viewheight_mp = 32000;
    }
}

//==========================================================================


int weaponscale[NUMWEAPONSEXTRA] = {SPR_KNIFEREADY, SPR_PISTOLREADY,
    SPR_MACHINEGUNREADY, SPR_CHAINREADY, SPR_TANKGUNREADY, SPR_TANKWHEELREADY,
    SPR_MG42READY, SPR_FLAGREADY, SPR_FISTSREADY};

/*
==============
=
= CalcPlayerWeaponShapeNum
=
= Returns player weapon shape to draw. In split screen this is also used to
= draw BJ shoot frames.
=
==============
*/
int CalcPlayerWeaponShapeNum(void)
{
    return Player::weaponShape();
}

/*
==============
=
= DrawPlayerWeapon
=
= Draw the player's hands
=
==============
*/

void DrawPlayerWeapon (void)
{
    int shade;
    int shapenum;

#ifndef SPEAR
    if (gamestate.victoryflag)
    {
#ifndef APOGEE_1_0
        if (player_mp->state == &s_deathcam && (GetTimeCount()&32) )
            SimpleScaleShape(SPR_DEATHCAM);
#endif
        return;
    }
#endif

    if (gamestate.weapon_mp != -1)
    {
        ColorRemap_SetShade(gamestate.player_shade_mp - 1);

        shapenum = CalcPlayerWeaponShapeNum();
        if (shapenum != -1)
        {
            shapenum = LWMP_RotateSprite(0, shapenum);
            SimpleScaleShape(shapenum);
        }

        ColorRemap_SetShade(COLORREMAP_SHADE_DEFAULT);
    }

    if (demorecord || demoplayback)
        SimpleScaleShape(SPR_DEMO);

    LWMP_DrawMinimap();
}

/*
==============
=
= DrawDamageFlash
=
= Draw damage flash
=
==============
*/

void DrawDamageFlash (void)
{
    int x, y;
    byte *xPtr, *yPtr;
    int redShade, whiteShade;
    int red, white;

    if (bonuscount_mp)
    {
        white = bonuscount_mp / 10 + 1;
        if (white > NUMWHITESHIFTS)
            white = NUMWHITESHIFTS;
        bonuscount_mp -= tics;
        if (bonuscount_mp < 0)
            bonuscount_mp = 0;
    }
    else
        white = 0;

    if (damagecount_mp)
    {
        red = damagecount_mp / 7 + 1;
        if (red > NUMREDSHIFTS)
            red = NUMREDSHIFTS;
        damagecount_mp -= tics;
        if (damagecount_mp < 0)
            damagecount_mp = 0;
    }
    else
        red = 0;

    if (red > 0)
    {
        redShade = (red - 1) * (SHADE_COUNT - 1) / (NUMREDSHIFTS - 1);
        yPtr = vbuf;

        for (y = 0; y < vh(viewheight_mp); y++, yPtr += vbufPitch)
        {
            xPtr = yPtr;
            for (x = 0; x < vw(viewwidth_mp); x++, xPtr++)
            {
                *xPtr = shadeTableDamageFlash[redShade][*xPtr];
            }
        }
    }

    if (white > 0)
    {
        whiteShade = (white - 1) * (SHADE_COUNT - 1) / 
            (NUMWHITESHIFTS - 1);
        yPtr = vbuf;

        for (y = 0; y < vh(viewheight_mp); y++, yPtr += vbufPitch)
        {
            xPtr = yPtr;
            for (x = 0; x < vw(viewwidth_mp); x++, xPtr++)
            {
                *xPtr = shadeTableBonusFlash[whiteShade][*xPtr];
            }
        }
    }
}

//==========================================================================


/*
=====================
=
= CalcTics
=
=====================
*/

void CalcTics (void)
{
//
// calculate tics since last refresh for adaptive timing
//
    if (lasttimecount > (int32_t) GetTimeCount())
    {
        // if the game was paused a LONG time
        lasttimecount = GetTimeCount();
    }

    uint32_t curtime = SDL_GetTicks();
    tics = (curtime * 7) / 100 - lasttimecount;
    if(!tics)
    {
        // wait until end of current tic
        SDL_Delay(((lasttimecount + 1) * 100) / 7 - curtime);
        tics = 1;
    }

    lasttimecount += tics;

    if (tics>MAXTICS)
        tics = MAXTICS;
}


//==========================================================================

void AsmRefresh()
{
    int32_t xstep,ystep;
    longword xpartial,ypartial;
    boolean playerInPushwallBackTile = tilemap[focaltx][focalty] == 64;

    for(pixx=0;pixx<vw(viewwidth_mp);pixx++)
    {
        short angl=midangle+pixelangle_mp[pixx];
        if(angl<0) angl+=FINEANGLES;
        if(angl>=3600) angl-=FINEANGLES;
        if(angl<900)
        {
            xtilestep=1;
            ytilestep=-1;
            xstep=finetangent[900-1-angl];
            ystep=-finetangent[angl];
            xpartial=xpartialup;
            ypartial=ypartialdown;
        }
        else if(angl<1800)
        {
            xtilestep=-1;
            ytilestep=-1;
            xstep=-finetangent[angl-900];
            ystep=-finetangent[1800-1-angl];
            xpartial=xpartialdown;
            ypartial=ypartialdown;
        }
        else if(angl<2700)
        {
            xtilestep=-1;
            ytilestep=1;
            xstep=-finetangent[2700-1-angl];
            ystep=finetangent[angl-1800];
            xpartial=xpartialdown;
            ypartial=ypartialup;
        }
        else if(angl<3600)
        {
            xtilestep=1;
            ytilestep=1;
            xstep=finetangent[angl-2700];
            ystep=finetangent[3600-1-angl];
            xpartial=xpartialup;
            ypartial=ypartialup;
        }
        yintercept=FixedMul(ystep,xpartial)+viewy;
        xtile=focaltx+xtilestep;
        xspot=(word)((xtile<<mapshift)+((uint32_t)yintercept>>16));
        xintercept=FixedMul(xstep,ypartial)+viewx;
        ytile=focalty+ytilestep;
        yspot=(word)((((uint32_t)xintercept>>16)<<mapshift)+ytile);
        texdelta=0;

        // Special treatment when player is in back tile of pushwall
        if(playerInPushwallBackTile)
        {
            if(    pwalldir == di_east && xtilestep ==  1
                || pwalldir == di_west && xtilestep == -1)
            {
                int32_t yintbuf = yintercept - ((ystep * (64 - pwallpos)) >> 6);
                if((yintbuf >> 16) == focalty)   // ray hits pushwall back?
                {
                    if(pwalldir == di_east)
                        xintercept = (focaltx << TILESHIFT) + (pwallpos << 10);
                    else
                        xintercept = (focaltx << TILESHIFT) - TILEGLOBAL + ((64 - pwallpos) << 10);
                    yintercept = yintbuf;
                    ytile = (short) (yintercept >> TILESHIFT);
                    tilehit = pwalltile;
                    HitVertWall();
                    continue;
                }
            }
            else if(pwalldir == di_south && ytilestep ==  1
                ||  pwalldir == di_north && ytilestep == -1)
            {
                int32_t xintbuf = xintercept - ((xstep * (64 - pwallpos)) >> 6);
                if((xintbuf >> 16) == focaltx)   // ray hits pushwall back?
                {
                    xintercept = xintbuf;
                    if(pwalldir == di_south)
                        yintercept = (focalty << TILESHIFT) + (pwallpos << 10);
                    else
                        yintercept = (focalty << TILESHIFT) - TILEGLOBAL + ((64 - pwallpos) << 10);
                    xtile = (short) (xintercept >> TILESHIFT);
                    tilehit = pwalltile;
                    HitHorizWall();
                    continue;
                }
            }
        }

        do
        {
            if(ytilestep==-1 && (yintercept>>16)<=ytile) goto horizentry;
            if(ytilestep==1 && (yintercept>>16)>=ytile) goto horizentry;
vertentry:
            if((uint32_t)yintercept>mapheight*65536-1 || (word)xtile>=mapwidth)
            {
                if(xtile<0) xintercept=0, xtile=0;
                else if(xtile>=mapwidth) xintercept=mapwidth<<TILESHIFT, xtile=mapwidth-1;
                else xtile=(short) (xintercept >> TILESHIFT);
                if(yintercept<0) yintercept=0, ytile=0;
                else if(yintercept>=(mapheight<<TILESHIFT)) yintercept=mapheight<<TILESHIFT, ytile=mapheight-1;
                yspot=0xffff;
                tilehit=0;
                HitHorizBorder();
                break;
            }
            if(xspot>=maparea) break;
            *((byte *)gamestate.minimap_reveal + xspot) |= 
                minimap_rflg_vis;
            tilehit=((byte *)tilemap)[xspot];
            if(tilehit)
            {
                if(tilehit&0x80)
                {
                    int32_t yintbuf=yintercept+(ystep>>1);
                    if((yintbuf>>16)!=(yintercept>>16))
                        goto passvert;
                    if((word)yintbuf<doorposition[tilehit&0x7f])
                        goto passvert;
                    yintercept=yintbuf;
                    xintercept=(xtile<<TILESHIFT)|0x8000;
                    ytile = (short) (yintercept >> TILESHIFT);
                    HitVertDoor();
                }
                else
                {
                    if(tilehit==64)
                    {
                        if(pwalldir==di_west || pwalldir==di_east)
                        {
	                        int32_t yintbuf;
                            int pwallposnorm;
                            int pwallposinv;
                            if(pwalldir==di_west)
                            {
                                pwallposnorm = 64-pwallpos;
                                pwallposinv = pwallpos;
                            }
                            else
                            {
                                pwallposnorm = pwallpos;
                                pwallposinv = 64-pwallpos;
                            }
                            if(pwalldir == di_east && xtile==pwallx && ((uint32_t)yintercept>>16)==pwally
                                || pwalldir == di_west && !(xtile==pwallx && ((uint32_t)yintercept>>16)==pwally))
                            {
                                yintbuf=yintercept+((ystep*pwallposnorm)>>6);
                                if((yintbuf>>16)!=(yintercept>>16))
                                    goto passvert;

                                xintercept=(xtile<<TILESHIFT)+TILEGLOBAL-(pwallposinv<<10);
                                yintercept=yintbuf;
                                ytile = (short) (yintercept >> TILESHIFT);
                                tilehit=pwalltile;
                                HitVertWall();
                            }
                            else
                            {
                                yintbuf=yintercept+((ystep*pwallposinv)>>6);
                                if((yintbuf>>16)!=(yintercept>>16))
                                    goto passvert;

                                xintercept=(xtile<<TILESHIFT)-(pwallposinv<<10);
                                yintercept=yintbuf;
                                ytile = (short) (yintercept >> TILESHIFT);
                                tilehit=pwalltile;
                                HitVertWall();
                            }
                        }
                        else
                        {
                            int pwallposi = pwallpos;
                            if(pwalldir==di_north) pwallposi = 64-pwallpos;
                            if(pwalldir==di_south && (word)yintercept<(pwallposi<<10)
                                || pwalldir==di_north && (word)yintercept>(pwallposi<<10))
                            {
                                if(((uint32_t)yintercept>>16)==pwally && xtile==pwallx)
                                {
                                    if(pwalldir==di_south && (int32_t)((word)yintercept)+ystep<(pwallposi<<10)
                                            || pwalldir==di_north && (int32_t)((word)yintercept)+ystep>(pwallposi<<10))
                                        goto passvert;

                                    if(pwalldir==di_south)
                                        yintercept=(yintercept&0xffff0000)+(pwallposi<<10);
                                    else
                                        yintercept=(yintercept&0xffff0000)-TILEGLOBAL+(pwallposi<<10);
                                    xintercept=xintercept-((xstep*(64-pwallpos))>>6);
                                    xtile = (short) (xintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitHorizWall();
                                }
                                else
                                {
                                    texdelta = -(pwallposi<<10);
                                    xintercept=xtile<<TILESHIFT;
                                    ytile = (short) (yintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitVertWall();
                                }
                            }
                            else
                            {
                                if(((uint32_t)yintercept>>16)==pwally && xtile==pwallx)
                                {
                                    texdelta = -(pwallposi<<10);
                                    xintercept=xtile<<TILESHIFT;
                                    ytile = (short) (yintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitVertWall();
                                }
                                else
                                {
                                    if(pwalldir==di_south && (int32_t)((word)yintercept)+ystep>(pwallposi<<10)
                                            || pwalldir==di_north && (int32_t)((word)yintercept)+ystep<(pwallposi<<10))
                                        goto passvert;

                                    if(pwalldir==di_south)
                                        yintercept=(yintercept&0xffff0000)-((64-pwallpos)<<10);
                                    else
                                        yintercept=(yintercept&0xffff0000)+((64-pwallpos)<<10);
                                    xintercept=xintercept-((xstep*pwallpos)>>6);
                                    xtile = (short) (xintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitHorizWall();
                                }
                            }
                        }
                    }
                    else
                    {
                        xintercept=xtile<<TILESHIFT;
                        ytile = (short) (yintercept >> TILESHIFT);
                        HitVertWall();
                    }
                }
                break;
            }
passvert:
            *((byte *)spotvis_mp+xspot)=1;
            xtile+=xtilestep;
            yintercept+=ystep;
            xspot=(word)((xtile<<mapshift)+((uint32_t)yintercept>>16));
        }
        while(1);
        continue;

        do
        {
            if(xtilestep==-1 && (xintercept>>16)<=xtile) goto vertentry;
            if(xtilestep==1 && (xintercept>>16)>=xtile) goto vertentry;
horizentry:
            if((uint32_t)xintercept>mapwidth*65536-1 || (word)ytile>=mapheight)
            {
                if(ytile<0) yintercept=0, ytile=0;
                else if(ytile>=mapheight) yintercept=mapheight<<TILESHIFT, ytile=mapheight-1;
                else ytile=(short) (yintercept >> TILESHIFT);
                if(xintercept<0) xintercept=0, xtile=0;
                else if(xintercept>=(mapwidth<<TILESHIFT)) xintercept=mapwidth<<TILESHIFT, xtile=mapwidth-1;
                xspot=0xffff;
                tilehit=0;
                HitVertBorder();
                break;
            }
            if(yspot>=maparea) break;
            *((byte *)gamestate.minimap_reveal + yspot) |=
                minimap_rflg_vis;
            tilehit=((byte *)tilemap)[yspot];
            if(tilehit)
            {
                if(tilehit&0x80)
                {
                    int32_t xintbuf=xintercept+(xstep>>1);
                    if((xintbuf>>16)!=(xintercept>>16))
                        goto passhoriz;
                    if((word)xintbuf<doorposition[tilehit&0x7f])
                        goto passhoriz;
                    xintercept=xintbuf;
                    yintercept=(ytile<<TILESHIFT)+0x8000;
                    xtile = (short) (xintercept >> TILESHIFT);
                    HitHorizDoor();
                }
                else
                {
                    if(tilehit==64)
                    {
                        if(pwalldir==di_north || pwalldir==di_south)
                        {
                            int32_t xintbuf;
                            int pwallposnorm;
                            int pwallposinv;
                            if(pwalldir==di_north)
                            {
                                pwallposnorm = 64-pwallpos;
                                pwallposinv = pwallpos;
                            }
                            else
                            {
                                pwallposnorm = pwallpos;
                                pwallposinv = 64-pwallpos;
                            }
                            if(pwalldir == di_south && ytile==pwally && ((uint32_t)xintercept>>16)==pwallx
                                || pwalldir == di_north && !(ytile==pwally && ((uint32_t)xintercept>>16)==pwallx))
                            {
                                xintbuf=xintercept+((xstep*pwallposnorm)>>6);
                                if((xintbuf>>16)!=(xintercept>>16))
                                    goto passhoriz;

                                yintercept=(ytile<<TILESHIFT)+TILEGLOBAL-(pwallposinv<<10);
                                xintercept=xintbuf;
                                xtile = (short) (xintercept >> TILESHIFT);
                                tilehit=pwalltile;
                                HitHorizWall();
                            }
                            else
                            {
                                xintbuf=xintercept+((xstep*pwallposinv)>>6);
                                if((xintbuf>>16)!=(xintercept>>16))
                                    goto passhoriz;

                                yintercept=(ytile<<TILESHIFT)-(pwallposinv<<10);
                                xintercept=xintbuf;
                                xtile = (short) (xintercept >> TILESHIFT);
                                tilehit=pwalltile;
                                HitHorizWall();
                            }
                        }
                        else
                        {
                            int pwallposi = pwallpos;
                            if(pwalldir==di_west) pwallposi = 64-pwallpos;
                            if(pwalldir==di_east && (word)xintercept<(pwallposi<<10)
                                    || pwalldir==di_west && (word)xintercept>(pwallposi<<10))
                            {
                                if(((uint32_t)xintercept>>16)==pwallx && ytile==pwally)
                                {
                                    if(pwalldir==di_east && (int32_t)((word)xintercept)+xstep<(pwallposi<<10)
                                            || pwalldir==di_west && (int32_t)((word)xintercept)+xstep>(pwallposi<<10))
                                        goto passhoriz;

                                    if(pwalldir==di_east)
                                        xintercept=(xintercept&0xffff0000)+(pwallposi<<10);
                                    else
                                        xintercept=(xintercept&0xffff0000)-TILEGLOBAL+(pwallposi<<10);
                                    yintercept=yintercept-((ystep*(64-pwallpos))>>6);
                                    ytile = (short) (yintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitVertWall();
                                }
                                else
                                {
                                    texdelta = -(pwallposi<<10);
                                    yintercept=ytile<<TILESHIFT;
                                    xtile = (short) (xintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitHorizWall();
                                }
                            }
                            else
                            {
                                if(((uint32_t)xintercept>>16)==pwallx && ytile==pwally)
                                {
                                    texdelta = -(pwallposi<<10);
                                    yintercept=ytile<<TILESHIFT;
                                    xtile = (short) (xintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitHorizWall();
                                }
                                else
                                {
                                    if(pwalldir==di_east && (int32_t)((word)xintercept)+xstep>(pwallposi<<10)
                                            || pwalldir==di_west && (int32_t)((word)xintercept)+xstep<(pwallposi<<10))
                                        goto passhoriz;

                                    if(pwalldir==di_east)
                                        xintercept=(xintercept&0xffff0000)-((64-pwallpos)<<10);
                                    else
                                        xintercept=(xintercept&0xffff0000)+((64-pwallpos)<<10);
                                    yintercept=yintercept-((ystep*pwallpos)>>6);
                                    ytile = (short) (yintercept >> TILESHIFT);
                                    tilehit=pwalltile;
                                    HitVertWall();
                                }
                            }
                        }
                    }
                    else
                    {
                        yintercept=ytile<<TILESHIFT;
                        xtile = (short) (xintercept >> TILESHIFT);
                        HitHorizWall();
                    }
                }
                break;
            }
passhoriz:
            *((byte *)spotvis_mp+yspot)=1;
            ytile+=ytilestep;
            xintercept+=xstep;
            yspot=(word)((((uint32_t)xintercept>>16)<<mapshift)+ytile);
        }
        while(1);
    }
}

/*
====================
=
= WallRefresh
=
====================
*/

void WallRefresh (void)
{
    xpartialdown = viewx&(TILEGLOBAL-1);
    xpartialup = TILEGLOBAL-xpartialdown;
    ypartialdown = viewy&(TILEGLOBAL-1);
    ypartialup = TILEGLOBAL-ypartialdown;

    min_wallheight = vh(viewheight_mp);
    lastside = -1;                  // the first pixel is on a new wall
    AsmRefresh ();
    ScalePost ();                   // no more optimization on last post
}

void CalcViewVariables()
{
    viewangle = Player::viewAngle(player_mp);
    midangle = viewangle*(FINEANGLES/ANGLES);
    viewsin = sintable[viewangle];
    viewcos = costable[viewangle];

    const lwlib::Point2f pos = Player::viewPos();
    viewx = fixedpt_rconst(pos.v[0]) - FixedMul(focallength,viewcos);
    viewy = fixedpt_rconst(pos.v[1]) + FixedMul(focallength,viewsin);

    focaltx = (short)(viewx>>TILESHIFT);
    focalty = (short)(viewy>>TILESHIFT);

    viewtx = (short)(player_mp->x >> TILESHIFT);
    viewty = (short)(player_mp->y >> TILESHIFT);
}

//==========================================================================

/*
========================
=
= SimulatePlayerCamZ
=
========================
*/

static void SimulatePlayerCamZ(void)
{
    static fixed camz = 0;
    static int camdir = 1;
    static fixed camspeed = FP(1) / 1024;
    camz += tics * camspeed * camdir;
    if (camz >= 0x8000 - camspeed || camz <= -0x8000 + camspeed)
    {
        camdir = -camdir;
    }
    fprintf(stderr, "player_camz[%d] %s\n",
        LWMP_GetInstance(), fixedpt_cstr(camz));
    gamestate.player_camz_mp = camz;
}

/*
========================
=
= ThreeDRefresh
=
========================
*/

void ThreeDRefresh(void)
{
//
// clear out the traced array
//
    memset(spotvis_mp,0,maparea);
    spotvis_mp[player_mp->tilex][player_mp->tiley] = 1;       // Detect all sprites over player fix

    vbuf = VL_LockSurface(screenBuffer);
    vbuf += vy(screenofs_mp / MaxX) * bufferPitch + 
        vx(screenofs_mp % MaxX);
    vbufPitch = bufferPitch;

    CalcViewVariables();

//
// follow the walls from there to the right, drawing as we go
//
    VGAClearScreen ();
#if defined(USE_FEATUREFLAGS) && defined(USE_STARSKY)
    if(GetFeatureFlags() & FF_STARSKY)
        DrawStarSky(vbuf, vbufPitch);
#endif
#ifdef HORIZON
#if defined(USE_FEATUREFLAGS) && defined(USE_PARALLAX)
    if(GetFeatureFlags() & FF_PARALLAXSKY)
        DrawParallax(vbuf, vbufPitch);
#endif
#endif
    WallRefresh ();

#ifndef HORIZON
#if defined(USE_FEATUREFLAGS) && defined(USE_PARALLAX)
    if(GetFeatureFlags() & FF_PARALLAXSKY)
        DrawParallax(vbuf, vbufPitch);
#endif
#endif
#if defined(USE_FEATUREFLAGS) && defined(USE_CLOUDSKY)
    if(GetFeatureFlags() & FF_CLOUDSKY)
        DrawClouds(vbuf, vbufPitch, min_wallheight);
#endif
#ifdef USE_FLOORCEILINGTEX
    DrawFloorAndCeiling(vbuf, vbufPitch, min_wallheight);
#endif

//
// draw all the scaled images
//
    DrawScaleds();                  // draw scaled stuff

#if defined(USE_FEATUREFLAGS) && defined(USE_RAIN)
    if(GetFeatureFlags() & FF_RAIN)
        DrawRain(vbuf, vbufPitch);
#endif
#if defined(USE_FEATUREFLAGS) && defined(USE_SNOW)
    if(GetFeatureFlags() & FF_SNOW)
        DrawSnow(vbuf, vbufPitch);
#endif

    DrawPlayerWeapon ();    // draw player's hands
    DrawDamageFlash ();

    if(Keyboard[sc_Tab] && viewsize == 21 && gamestate.weapon_mp != -1)
        ShowActStatus();

    if (RampageMode::enabled())
    {
        RampageMode::Player::drawRageBar();
    }

    VL_UnlockSurface(screenBuffer);
    vbuf = NULL;
}

/*
=============================================================================

                               COLOR REMAP

=============================================================================
*/

typedef byte color_remap_table_t[256];

static byte color_remap_start[COLORREMAP_SHADE_MAX] =
{
    0x60, // COLORREMAP_SHADE_GREEN
    0x10, // COLORREMAP_SHADE_GREY
    0x20, // COLORREMAP_SHADE_RED
    0x40, // COLORREMAP_SHADE_YELLOW
    0x70, // COLORREMAP_SHADE_LIGHTBLUE
    0x90, // COLORREMAP_SHADE_DARKBLUE
    0xB0, // COLORREMAP_SHADE_VIOLET
    0xD0, // COLORREMAP_SHADE_BROWN
};

static color_remap_table_t color_remap_tables[COLORREMAP_SHADE_MAX * 2 + 1];

byte *color_remap;

void ColorRemap_InitTables(void)
{
    int i, j, k;
    int key;
    const byte *mutantRemap;

    for (i = 0; i < 256; i++)
    {
        color_remap_tables[COLORREMAP_SHADE_DEFAULT][i] = (byte)i;
    }

    key = color_remap_start[COLORREMAP_SHADE_DEFAULT];
    mutantRemap = MutantRemap_GetTable();

    for (i = 0; i < (int)COLORREMAP_SHADE_MAX * 2; i++)
    {
        memcpy(color_remap_tables[i], 
            color_remap_tables[COLORREMAP_SHADE_DEFAULT], 
            sizeof(color_remap_table_t));

        k = i % COLORREMAP_SHADE_MAX;

        if (i >= COLORREMAP_SHADE_MAX)
        {
            for (j = 0; j < 256; j++)
            {
                color_remap_tables[i][j] = mutantRemap[
                    color_remap_tables[i][j]];
            }
        }

        for (j = 0; j < 16; j++)
        {
            color_remap_tables[i][j + key] = color_remap_start[k] + j;
        }
    }

    memcpy(color_remap_tables[COLORREMAP_SHADE_ZOMBIE],
        ZombieRemap_GetTable(), sizeof(color_remap_table_t));

    ColorRemap_SetShade(COLORREMAP_SHADE_DEFAULT);
}

void ColorRemap_SetShade(color_remap_shade_t shade)
{
    if (shade < 0 || shade >= COLORREMAP_SHADE_MAX * 2 + 1)
    {
        return;
    }

    color_remap = color_remap_tables[shade];
}
