// ID_VL.C

#include <string.h>
#include "wl_def.h"
#pragma hdrstop

// Uncomment the following line, if you get destination out of bounds
// assertion errors and want to ignore them during debugging
//#define IGNORE_BAD_DEST

#ifdef IGNORE_BAD_DEST
#undef assert
#define assert(x) if(!(x)) return
#define assert_ret(x) if(!(x)) return 0
#else
#define assert_ret(x) assert(x)
#endif

boolean fullscreen = true;
#if defined(_arch_dreamcast)
boolean usedoublebuffering = false;
unsigned screenBits = 8;
#elif defined(GP2X)
boolean usedoublebuffering = true;
#if defined(GP2X_940)
unsigned screenBits = 8;
#else
unsigned screenBits = 16;
#endif
#else
boolean usedoublebuffering = true;
unsigned screenBits = 0;      // use "best" color depth according to libSDL
#endif

SDL_Window *window;
static SDL_Renderer *renderer;
static SDL_Texture *texture;

SDL_Surface *screen = NULL;
unsigned screenPitch;

SDL_Surface *screenBuffer = NULL;
unsigned bufferPitch;

boolean	 screenfaded;
unsigned bordercolor;

SDL_Color palette1[256], palette2[256];
SDL_Color curpal[256];


#define CASSERT(x) extern int ASSERT_COMPILE[((x) != 0) * 2 - 1];
#define RGB(r, g, b) {(r)*255/63, (g)*255/63, (b)*255/63, 255}

SDL_Color gamepal[]={
#ifdef SPEAR
    #include "sodpal.inc"
#else
    #include "wolfpal.inc"
#endif
};

CASSERT(lengthof(gamepal) == 256)

//===========================================================================


/*
=======================
=
= VL_Shutdown
=
=======================
*/

void	VL_Shutdown (void)
{
	//VL_SetTextMode ();
}

/*
=======================
=
= VL_SetVGAPlaneMode
=
=======================
*/

void	VL_SetVGAPlaneMode (void)
{
    int c;

    const char *title;
#ifdef SPEAR
    title = "Spear of Destiny";
#else
    title = "Wolfenstein 3D";
#endif

    window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, LWMP_GetPhysicalScreenWidth(),
                              LWMP_GetPhysicalScreenHeight(), SDL_WINDOW_ALLOW_HIGHDPI
                              | (fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));
    if(!window)
    {
        printf("Unable to create %ix%ix%i window: %s\n", LWMP_GetPhysicalScreenWidth(),
               LWMP_GetPhysicalScreenHeight(), screenBits, SDL_GetError());
        exit(1);
    }

    renderer = SDL_CreateRenderer(window, -1, 0);
    if(!renderer)
    {
        printf("Unable to create renderer: %s\n", SDL_GetError());
        exit(1);
    }
    SDL_RendererInfo info;
    if(SDL_GetRendererInfo(renderer, &info) < 0)
    {
        printf("Unable to get renderer info: %s\n", SDL_GetError());
        exit(1);
    }
    for(Uint32 i = 0; i < info.num_texture_formats; ++i)
    {
        // TODO: debug this
        if(screenBits == -1)
        {
            screenBits = SDL_BITSPERPIXEL(info.texture_formats[i]);
            break;
        }
    }

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888,
                                SDL_TEXTUREACCESS_STREAMING, LWMP_GetPhysicalScreenWidth(),
                                LWMP_GetPhysicalScreenHeight());
    if(!texture)
    {
        printf("Unable to create texture: %s\n", SDL_GetError());
        exit(1);
    }


//    screen = SDL_SetVideoMode(LWMP_GetPhysicalScreenWidth(), LWMP_GetPhysicalScreenHeight(), screenBits,
//          (usedoublebuffering ? SDL_HWSURFACE | SDL_DOUBLEBUF : 0)
//        | (screenBits == 8 ? SDL_HWPALETTE : 0)
//        | (fullscreen ? SDL_FULLSCREEN : 0));
    screen = SDL_CreateRGBSurface(0, LWMP_GetPhysicalScreenWidth(), LWMP_GetPhysicalScreenHeight(), 32, 0, 0, 0, 0);
    if(!screen)
    {
        printf("Unable to set %ix%ix%i video mode: %s\n", 
            LWMP_GetPhysicalScreenWidth(), LWMP_GetPhysicalScreenHeight(), 
            screenBits, SDL_GetError());
        exit(1);
    }
    // if((screen->flags & SDL_DOUBLEBUF) != SDL_DOUBLEBUF)
    //     usedoublebuffering = false;
    SDL_ShowCursor(SDL_DISABLE);

    // SDL_SetColors(screen, gamepal, 0, 256);
    memcpy(curpal, gamepal, sizeof(SDL_Color) * 256);

    screenBuffer = SDL_CreateRGBSurface(SDL_SWSURFACE, LWMP_GetPhysicalScreenWidth(),
        LWMP_GetPhysicalScreenHeight(), 8, 0, 0, 0, 0);
    if(!screenBuffer)
    {
        printf("Unable to create screen buffer surface: %s\n", SDL_GetError());
        exit(1);
    }
    SDL_Palette *sdlpal = SDL_AllocPalette(256);
    if(!sdlpal)
        exit(1);
    if(SDL_SetPaletteColors(sdlpal, gamepal, 0, 256) < 0)
    {
        printf("Unable to set palette colors: %s\n", SDL_GetError());
        exit(1);
    }
    if(SDL_SetSurfacePalette(screenBuffer, sdlpal) < 0)
    {
        printf("Unable to set surface palette: %s\n", SDL_GetError());
        exit(1);
    }
    // SDL_SetColors(screenBuffer, gamepal, 0, 256);

    screenPitch = screen->pitch;
    bufferPitch = screenBuffer->pitch;

    for (LWMP_REPEAT_MAX(c))
    {
        pixelangle_mp = (short *) malloc(LWMP_GetPhysicalScreenWidth() * sizeof(short));
        CHECKMALLOCRESULT(pixelangle_mp);
    }
    wallheight = (int *) malloc(LWMP_GetPhysicalScreenWidth() * sizeof(int));
    CHECKMALLOCRESULT(wallheight);
}

/*
=============================================================================

						PALETTE OPS

		To avoid snow, do a WaitVBL BEFORE calling these

=============================================================================
*/

/*
=================
=
= VL_ConvertPalette
=
=================
*/

void VL_ConvertPalette(byte *srcpal, SDL_Color *destpal, int numColors)
{
    for(int i=0; i<numColors; i++)
    {
        destpal[i].r = *srcpal++ * 255 / 63;
        destpal[i].g = *srcpal++ * 255 / 63;
        destpal[i].b = *srcpal++ * 255 / 63;
    }
}

/*
=================
=
= VL_FillPalette
=
=================
*/

void VL_FillPalette (int red, int green, int blue)
{
    int i;
    SDL_Color pal[256];

    for(i=0; i<256; i++)
    {
        pal[i].r = red;
        pal[i].g = green;
        pal[i].b = blue;
    }

    VL_SetPalette(pal, true);
}

//===========================================================================

/*
=================
=
= VL_GetColor
=
=================
*/

void VL_GetColor	(int color, int *red, int *green, int *blue)
{
    SDL_Color *col = &curpal[color];
    *red = col->r;
    *green = col->g;
    *blue = col->b;
}

//===========================================================================

/*
 =================
 =
 = VL_Flip
 = For SDL2
 =
 =================
 */

// IOANCH: major thanks to http://sandervanderburg.blogspot.ro/2014/05/rendering-8-bit-palettized-surfaces-in.html
void VL_Flip()
{
    void *pixels;
    int pitch;
    SDL_LockTexture(texture, NULL, &pixels, &pitch);
    SDL_ConvertPixels(screen->w, screen->h, screen->format->format,
                      screen->pixels, screen->pitch, SDL_PIXELFORMAT_ABGR8888,
                      pixels, pitch);

    SDL_UnlockTexture(texture);

    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
}

//===========================================================================

/*
=================
=
= VL_SetPalette
=
=================
*/

void VL_SetPalette (SDL_Color *palette, bool forceupdate)
{
    memcpy(curpal, palette, sizeof(SDL_Color) * 256);

    SDL_Palette *pal = SDL_AllocPalette(256);
    SDL_SetPaletteColors(pal, curpal, 0, 256);
    SDL_SetSurfacePalette(screenBuffer, pal);
    SDL_FreePalette(pal);

    // This trick is used due to a SDL 2.0.3 bug, to invalidate the palette
    // cache blit map.
    SDL_SetSurfaceRLE(screenBuffer, 1);
    SDL_SetSurfaceRLE(screenBuffer, 0);

    if(forceupdate)
    {
        SDL_BlitSurface(screenBuffer, NULL, screen, NULL);
        VL_Flip();
    }
}


//===========================================================================

/*
=================
=
= VL_GetPalette
=
=================
*/

void VL_GetPalette (SDL_Color *palette)
{
    memcpy(palette, curpal, sizeof(SDL_Color) * 256);
}


//===========================================================================

/*
=================
=
= VL_FadeOut
=
= Fades the current palette to the given color in the given number of steps
=
=================
*/

void VL_FadeOut (int start, int end, int red, int green, int blue, int steps, SDL_Color *fadeInPal)
{
	int		    i,j,orig,delta;
	SDL_Color   *origptr, *newptr;

    red = red * 255 / 63;
    green = green * 255 / 63;
    blue = blue * 255 / 63;

	VL_WaitVBL(1);
	VL_GetPalette(palette1);
	memcpy(palette2, palette1, sizeof(SDL_Color) * 256);

  #ifdef NOFADE
  steps = 0;
  #endif

//
// fade through intermediate frames
//
	for (i=0;i<steps;i++)
	{
		origptr = &palette1[start];
		newptr = &palette2[start];
		for (j=start;j<=end;j++)
		{
			orig = origptr->r;
			delta = red-orig;
			newptr->r = orig + delta * i / steps;
			orig = origptr->g;
			delta = green-orig;
			newptr->g = orig + delta * i / steps;
			orig = origptr->b;
			delta = blue-orig;
			newptr->b = orig + delta * i / steps;
			origptr++;
			newptr++;
		}

		if(!usedoublebuffering || screenBits == 8) VL_WaitVBL(1);
		VL_SetPalette (palette2, true);
	}

//
// final color
//
	VL_FillPalette (red,green,blue);

	screenfaded = true;
}

/*
=================
=
= VL_FadeIn
=
=================
*/

void VL_FadeIn (int start, int end, SDL_Color *palette, int steps)
{
	int i,j,delta;

	VL_WaitVBL(1);
	VL_GetPalette(palette1);
	memcpy(palette2, palette1, sizeof(SDL_Color) * 256);

  #ifdef NOFADE
  steps = 0;
  #endif

//
// fade through intermediate frames
//
	for (i=0;i<steps;i++)
	{
		for (j=start;j<=end;j++)
		{
			delta = palette[j].r-palette1[j].r;
			palette2[j].r = palette1[j].r + delta * i / steps;
			delta = palette[j].g-palette1[j].g;
			palette2[j].g = palette1[j].g + delta * i / steps;
			delta = palette[j].b-palette1[j].b;
			palette2[j].b = palette1[j].b + delta * i / steps;
		}

		if(!usedoublebuffering || screenBits == 8) VL_WaitVBL(1);
		VL_SetPalette(palette2, true);
	}

//
// final color
//
	VL_SetPalette (palette, true);
	screenfaded = false;
}

/*
=============================================================================

							PIXEL OPS

=============================================================================
*/

byte *VL_LockSurface(SDL_Surface *surface)
{
    if(SDL_MUSTLOCK(surface))
    {
        if(SDL_LockSurface(surface) < 0)
            return NULL;
    }
    return (byte *) surface->pixels;
}

void VL_UnlockSurface(SDL_Surface *surface)
{
    if(SDL_MUSTLOCK(surface))
    {
        SDL_UnlockSurface(surface);
    }
}

/*
=================
=
= VL_Plot
=
=================
*/

void VL_Plot (int x, int y, int color)
{
    int virtx, virty;
    byte *pixels;

    assert(x >= 0 && (unsigned) x < MaxX
            && y >= 0 && (unsigned) y < MaxY
            && "VL_Plot: Pixel out of bounds!");

    VL_LockSurface(screenBuffer);
    pixels = (byte *)screenBuffer->pixels;
    pixels[vy(y) * bufferPitch + vx(x)] = color;
    VL_UnlockSurface(screenBuffer);
}

/*
=================
=
= VL_GetPixel
=
=================
*/

byte VL_GetPixel (int x, int y)
{
    byte col;

    assert_ret(x >= 0 && (unsigned) x < MaxX
            && y >= 0 && (unsigned) y < MaxY
            && "VL_GetPixel: Pixel out of bounds!");

    VL_LockSurface(screenBuffer);
    col = ((byte *) screenBuffer->pixels)[vy(y) * bufferPitch + vx(x)];
    VL_UnlockSurface(screenBuffer);
    return col;
}


/*
=================
=
= VL_Hlin
=
=================
*/

void VL_Hlin (unsigned x, unsigned y, unsigned width, int color)
{
    Uint8 *dest;

    assert(x >= 0 && x + width <= MaxX
            && y >= 0 && y < MaxY
            && "VL_Hlin: Destination rectangle out of bounds!");

    VL_LockSurface(screenBuffer);
    dest = ((byte *) screenBuffer->pixels) + 
        vy(y) * bufferPitch + vx(x);
    memset(dest, color, vw(width));
    VL_UnlockSurface(screenBuffer);
}


/*
=================
=
= VL_Vlin
=
=================
*/

void VL_Vlin (int x, int y, int height, int color)
{
    int h;
    Uint8 *dest;

    assert(x >= 0 && (unsigned) x < MaxX
        && y >= 0 && (unsigned) y + height <= MaxY
        && "VL_Vlin: Destination rectangle out of bounds!");

    VL_LockSurface(screenBuffer);
    dest = ((byte *) screenBuffer->pixels) + 
        vy(y) * bufferPitch + vx(x);

    h = vh(height);
    while (h--)
    {
      *dest = color;
      dest += bufferPitch;
    }
    VL_UnlockSurface(screenBuffer);
}


/*
=================
=
= VL_Bar
=
=================
*/

void VL_BarScaledCoord (int scx, int scy, int scwidth, int scheight, int color)
{
    int h;
    Uint8 *dest;

    assert(scx >= 0 && (unsigned) scx + scwidth <= MaxX
        && scy >= 0 && (unsigned) scy + scheight <= MaxY
        && "VL_BarScaledCoordSDL: Destination rectangle out of bounds!");

    VL_LockSurface(screenBuffer);
    dest = ((byte *) screenBuffer->pixels) + 
        vy(scy) * bufferPitch + vx(scx);

    h = vh(scheight);
    while (h--)
    {
      memset(dest, color, vw(scwidth));
      dest += bufferPitch;
    }
    VL_UnlockSurface(screenBuffer);
}

/*
============================================================================

							MEMORY OPS

============================================================================
*/

/*
=================
=
= VL_MemToLatch
=
=================
*/

void VL_MemToLatch(byte *source, int width, int height,
    SDL_Surface *destSurface, int x, int y)
{
    assert(x >= 0 && (unsigned) x + width <= MaxX
            && y >= 0 && (unsigned) y + height <= MaxY
            && "VL_MemToLatch: Destination rectangle out of bounds!");

    VL_LockSurface(destSurface);
    int pitch = destSurface->pitch;
    byte *dest = (byte *) destSurface->pixels + y * pitch + x;
    for(int ysrc = 0; ysrc < height; ysrc++)
    {
        for(int xsrc = 0; xsrc < width; xsrc++)
        {
            dest[ysrc * pitch + xsrc] = source[(ysrc * (width >> 2) + (xsrc >> 2))
                + (xsrc & 3) * (width >> 2) * height];
        }
    }
    VL_UnlockSurface(destSurface);
}

//===========================================================================


/*
=================
=
= VL_MemToScreenScaledCoord
=
= Draws a block of data to the screen.
=
=================
*/

void VL_MemToScreenScaledCoord (byte *source, int width, int height, int destx, int desty)
{
    byte *vbuf;
    int sci, scj;
    LWMP_Frac_t i, j;

    assert(destx >= 0 && destx + width <= MaxX
            && desty >= 0 && desty + height <= MaxY
            && "VL_MemToScreenScaledCoord: Destination rectangle out of bounds!");

    VL_LockSurface(screenBuffer);
    vbuf = (byte *) screenBuffer->pixels;

    LWMP_FRACINIT(j, 0, 0, height, vh(height));
    for (scj = 0; scj < vh(height); scj++)
    {
        LWMP_FRACINIT(i, 0, 0, width, vw(width));
        for (sci = 0; sci < vw(width); sci++)
        {
            byte col = source[(j.val * (width >> 2) + (i.val >> 2)) + 
                (i.val & 3) * (width >> 2) * height];
            vbuf[(vy(desty) + scj) * bufferPitch + (vx(destx) + sci)] = 
                color_remap[col];
            LWMP_FRACINC(i);
        }
        LWMP_FRACINC(j);
    }
    VL_UnlockSurface(screenBuffer);
}

/*
=================
=
= VL_MemToScreenScaledCoord
=
= Draws a part of a block of data to the screen.
= The block has the size origwidth*origheight.
= The part at (srcx, srcy) has the size width*height
= and will be painted to (destx, desty).
=
=================
*/

void VL_MemToScreenScaledCoord (byte *source, int origwidth, int origheight, int srcx, int srcy,
                                int destx, int desty, int width, int height)
{
    byte *vbuf;
    LWMP_Frac_t i, j;
    int sci, scj;

    assert(destx >= 0 && destx + width <= MaxX
            && desty >= 0 && desty + height <= MaxY
            && "VL_MemToScreenScaledCoord: Destination rectangle out of bounds!");

    VL_LockSurface(screenBuffer);
    vbuf = (byte *) screenBuffer->pixels;

    LWMP_FRACINIT(j, 0, 0, height, vh(height));
    for (scj = 0; scj < vh(height); scj++)
    {
        LWMP_FRACINIT(i, 0, 0, width, vw(width));
        for (sci = 0; sci < vw(width); sci++)
        {
            byte col = source[((j.val + srcy) * (origwidth >> 2) + 
                ((i.val + srcx) >> 2)) + ((i.val + srcx) & 3) * 
                (origwidth >> 2) * origheight];
            vbuf[(vy(desty) + scj) * bufferPitch + (vx(destx) + sci)] = 
                color_remap[col];
            LWMP_FRACINC(i);
        }
        LWMP_FRACINC(j);
    }
    VL_UnlockSurface(screenBuffer);
}

//==========================================================================

/*
=================
=
= VL_LatchToScreenScaledCoord
=
=================
*/

void VL_LatchToScreenScaledCoord(SDL_Surface *source, int xsrc, int ysrc,
    int width, int height, int scxdest, int scydest, int lwglPicNum)
{
    LWMP_Frac_t i, j;
    int sci, scj;

    assert(scxdest >= 0 && scxdest + width <= MaxX
        && scydest >= 0 && scydest + height <= MaxY
        && "VL_LatchToScreenScaledCoordSDL: Destination rectangle out of bounds!");

    VL_LockSurface(source);
    byte *src = (byte *) source->pixels;
    unsigned srcPitch = source->pitch;

    VL_LockSurface(screenBuffer);
    byte *vbuf = (byte *) screenBuffer->pixels;

    LWMP_FRACINIT(j, 0, 0, height, vh(height));
    for (scj = 0; scj < vh(height); scj++)
    {
        LWMP_FRACINIT(i, 0, 0, width, vw(width));
        for (sci = 0; sci < vw(width); sci++)
        {
            byte col = src[(ysrc + j.val) * srcPitch + 
                xsrc + i.val];
            vbuf[(vy(scydest) + scj) * bufferPitch + 
                (vx(scxdest) + sci)] = color_remap[col];
            LWMP_FRACINC(i);
        }
        LWMP_FRACINC(j);
    }

    VL_UnlockSurface(screenBuffer);
    VL_UnlockSurface(source);
}

//===========================================================================

/*
=================
=
= VL_ScreenToScreen
=
=================
*/

void VL_ScreenToScreen (SDL_Surface *source, SDL_Surface *dest)
{
    SDL_BlitSurface(source, NULL, dest, NULL);
}
