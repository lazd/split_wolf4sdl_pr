CONFIG ?= config.default
-include $(CONFIG)

ifeq ($(SPLITWOLF_ENGINE_ARCH_X64),y)
CFLAGS ?= -g -gdwarf-2 -m32 -O0
LDFLAGS ?= -L/usr/lib32 -m32
else
CFLAGS += -g -gdwarf-2 -O0
endif

#VERSION_WOLF3D := y
#VERSION_SPEAR := y
#VERSION_WOLF3D_SHAREWARE := y
#VERSION_WOLF3D_APOGEE := y
#VERSION_SPEAR_DEMO := y

ifeq ($(VERSION_WOLF3D_SHAREWARE),y)
GAME_VERSION_ID := wolf3d
CFLAGS += -DVERSION_WOLF3D_SHAREWARE
else
ifeq ($(VERSION_SPEAR),y)
GAME_VERSION_ID := sod
CFLAGS += -DVERSION_SPEAR
else
ifeq ($(VERSION_WOLF3D_APOGEE),y)
GAME_VERSION_ID := wolf3d_apogee
CFLAGS += -DVERSION_WOLF3D_APOGEE
else
ifeq ($(VERSION_SPEAR_DEMO),y)
GAME_VERSION_ID := spear_demo
CFLAGS += -DVERSION_SPEAR_DEMO
else
GAME_VERSION_ID := wolf3d_full
CFLAGS += -DVERSION_WOLF3D
endif
endif
endif
endif

ifeq ($(BATENGINE_CROSSCOMPILE),y)
BINARY    ?= splitwolf-$(GAME_VERSION_ID).exe
else
BINARY    ?= splitwolf-$(GAME_VERSION_ID)
endif
PREFIX    ?= /usr/local
MANPREFIX ?= $(PREFIX)
DATADIR   ?= $(PREFIX)/share/games/wolf3d/

INSTALL         ?= install
INSTALL_PROGRAM ?= $(INSTALL) -m 555 -s
INSTALL_MAN     ?= $(INSTALL) -m 444
INSTALL_DATA    ?= $(INSTALL) -m 444

ifeq ($(BATENGINE_CROSSCOMPILE),y)
CC            = i586-mingw32msvc-g++
CXX           = $(CC)
endif

ifeq ($(BATENGINE_CROSSCOMPILE),y)
SDL_CONFIG    ?= /opt/SDL-1.2.13/bin/i586-mingw32msvc-sdl-config
else
SDL_CONFIG    ?= sdl2-config
endif

CFLAGS_SDL    ?= $(shell $(SDL_CONFIG) --cflags)

ifeq ($(BATENGINE_CROSSCOMPILE),y)
CFLAGS_SDL    += -I/opt/SDL-1.2.13/include
endif

LDFLAGS_SDL   ?= $(shell $(SDL_CONFIG) --libs)
#SPLIT_PLAYERS ?= --split 2
#SPLIT_LAYOUT  ?= --splitlayout 2x1
#SPLIT_LAYOUT  ?= --splitlayout 1x2
#SPLIT_RES     ?= --res 320 200
#SPLIT_RES     ?= --res 1024 600
#SPLIT_RES     ?= --res 1600 900
#SPLIT_RES     ?= --res 800 600
SPLIT_RES     ?= --res 640 400
#SPLIT_RES     ?= --res 1024 768
#SPLIT_RES     ?= --res 1280 720
SPLIT_DATADIR  ?= --splitdatadir "$(DROPBOX_LWMP_DIR)"
WINDOWED      ?= --windowed
#ENABLE_OPENGL ?= --gl
TEDLEVEL      ?= --tedlevel 0 --nowait --hard#--normal
TEDLEVEL      ?= --nowait
#NOSOUND       ?= --nosound
#GAMEMODE       ?= --instagibmode
#GAMEMODE       ?= --defusemode
#GAMEMODE       ?= --vampiremode
#GAMEMODE       ?= --ctfmode
#GAMEMODE       ?= --harvestermode
#GAMEMODE       ?= --bjmutantmode
#GAMEMODE       ?= --rampagemode
#GAMEMODE       ?= --zombiemode
#GAMEMODE       ?= --zombieharvestermode
#JOYSTICK      ?= --joystick -1
#SOUNDDIR      ?= --sounddir "/home/jasonh/Dropbox/SplitWolf/beta3_test/lwmp/sounds"

ifneq ($(BATENGINE_CROSSCOMPILE),y)
CFLAGS += -DNOFADE
endif

DATESTRING := $(shell date +%d%m%Y)

ifeq ($(PROFILING),y)
CFLAGS += -pg
endif

ZIP_PACKAGE := lwolf_splitsrc
GAME_ZIP_PACKAGE := SplitWolf-Beta-$(DATESTRING)

#DROPBOX_DIR   ?= $(HOME)/Dropbox/SplitWolf
DROPBOX_DIR   ?= $(HOME)/Dropbox
SPLITWOLF_DROPBOX_DIR   ?= $(HOME)/Dropbox/SplitWolf
#DROPBOX_LWMP_DIR ?= $(HOME)/Dropbox/SplitWolf/LatestWolf3DFull/Artpack 1 Beta 2 release/lwmp
DROPBOX_LWMP_DIR ?= $(SPLITWOLF_DROPBOX_DIR)/beta3_test/lwmp
#DROPBOX_LWMP_DIR ?= $(HOME)/tasks/tr/split_wolf4sdl/games/splitwolf/lwmp
ifeq ($(BATENGINE_CROSSCOMPILE),y)
VBOX_PROJ     ?= $(SPLITWOLF_DROPBOX_DIR)/beta3_test
else
VBOX_PROJ     ?= $(HOME)/tasks/windows/ntshare/split_wolf4sdl
endif
RUN_GAME_DIR  ?= games/$(GAME_VERSION_ID)
DROPBOX_GAME_DIR  ?= $(DROPBOX_DIR)/LatestWolf3DFull
DROPBOX_SRC_DIR   ?= $(DROPBOX_DIR)/linuxwolf_srcs

RUN_GAME_ARGS ?= $(WINDOWED) $(GAME_RES) --debugmode $(NOSOUND) \
	$(SPLIT_PLAYERS) $(SPLIT_LAYOUT) $(SPLIT_DATADIR) $(SPLIT_RES) \
	$(TEDLEVEL) $(ENABLE_OPENGL) $(JOYSTICK) $(SOUNDDIR) $(GAMEMODE)

CFLAGS += $(CFLAGS_SDL)

#CFLAGS += -Wall
#CFLAGS += -W
CFLAGS += -Wpointer-arith
CFLAGS += -Wreturn-type
CFLAGS += -Wwrite-strings
CFLAGS += -Wcast-align

ifdef DATADIR
    CFLAGS += -DDATADIR=\"$(DATADIR)\"
endif

CCFLAGS += $(CFLAGS)
CCFLAGS += -std=gnu99
CCFLAGS += -Werror-implicit-function-declaration
CCFLAGS += -Wimplicit-int
CCFLAGS += -Wsequence-point

CXXFLAGS += $(CFLAGS)

LDFLAGS += $(LDFLAGS_SDL)
LDFLAGS += -lSDL2_mixer

MAKE_LINKS_LWMP := ln -s ../splitwolf/lwmp
DESTROY_LINKS_LWMP := rm -f lwmp

SRCS :=
SRCS += fmopl.cpp
SRCS += id_ca.cpp
SRCS += id_in.cpp
SRCS += id_pm.cpp
SRCS += id_sd.cpp
SRCS += id_us_1.cpp
SRCS += id_vh.cpp
SRCS += id_vl.cpp
SRCS += id_lwmp.cpp
SRCS += id_lwex.cpp
SRCS += signon.cpp
SRCS += wl_act1.cpp
SRCS += wl_act2.cpp
SRCS += wl_agent.cpp
SRCS += wl_atmos.cpp
SRCS += wl_cloudsky.cpp
SRCS += wl_debug.cpp
SRCS += wl_draw.cpp
SRCS += wl_floorceiling.cpp
SRCS += wl_game.cpp
SRCS += wl_inter.cpp
SRCS += wl_main.cpp
SRCS += wl_menu.cpp
SRCS += wl_parallax.cpp
SRCS += wl_play.cpp
SRCS += wl_state.cpp
SRCS += wl_text.cpp
SRCS += wl_shade.cpp
SRCS += wl_dir3dspr.cpp
SRCS += wl_autoplay.cpp
SRCS += wl_mutantremap.cpp
SRCS += wl_zombieremap.cpp
SRCS += lw_vec.cpp
SRCS += lw_misc.cpp
SRCS += lw_stream.cpp

DEPS = $(filter %.d, $(SRCS:.c=.d) $(SRCS:.cpp=.d))
OBJS = $(filter %.o, $(SRCS:.c=.o) $(SRCS:.cpp=.o))

.SUFFIXES:
.SUFFIXES: .c .cpp .d .o

Q ?= @

all: $(BINARY)

ifndef NO_DEPS
depend: $(DEPS)

ifeq ($(findstring $(MAKECMDGOALS), clean depend Data latest latest2 zip to_dropbox listsrc to_vboxshare from_vboxshare releasepkg release),)
-include $(DEPS)
endif
endif

$(BINARY): $(OBJS)
	cp SDL_GameControllerDB/gamecontrollerdb.txt .
	@echo '===> LD $@'
	$(Q)$(CXX) $(CFLAGS) $(OBJS) $(LDFLAGS) -o $@

.c.o:
	@echo '===> CC $<'
	$(Q)$(CC) $(CCFLAGS) -c $< -o $@

.cpp.o:
	@echo '===> CXX $<'
	$(Q)$(CXX) $(CXXFLAGS) -c $< -o $@

.c.d:
	@echo '===> DEP $<'
	$(Q)$(CC) $(CCFLAGS) -MM $< | sed 's#^$(@F:%.d=%.o):#$@ $(@:%.d=%.o):#' > $@

.cpp.d:
	@echo '===> DEP $<'
	$(Q)$(CXX) $(CXXFLAGS) -MM $< | sed 's#^$(@F:%.d=%.o):#$@ $(@:%.d=%.o):#' > $@

clean distclean:
	@echo '===> CLEAN'
	rm -f gamecontrollerdb.txt
	$(Q)rm -fr $(DEPS) $(OBJS) $(BINARY)

install: $(BINARY)
	@echo '===> INSTALL'
	$(Q)$(INSTALL) -d $(PREFIX)/bin
	$(Q)$(INSTALL_PROGRAM) $(BINARY) $(PREFIX)/bin

run: $(BINARY)
	( \
	  cd $(RUN_GAME_DIR); \
	  $(MAKE_LINKS_LWMP); \
	  $(PWD)/$(BINARY) $(RUN_GAME_ARGS); \
	  $(DESTROY_LINKS_LWMP) \
	)

debug-run: $(BINARY)
	( \
	  cd $(RUN_GAME_DIR); \
	  $(MAKE_LINKS_LWMP); \
	  gdb --args $(PWD)/$(BINARY) $(RUN_GAME_ARGS); \
	  $(DESTROY_LINKS_LWMP); \
	)

zip: clean
	rm -f $(ZIP_PACKAGE).zip
	zip -r $(ZIP_PACKAGE).zip *

latest:
	# create dirs
	rm -rf latest
	# copy stuff
	rm -rf lwmp
	cp -a "$(DROPBOX_LWMP_DIR)" .
	# pics
	mkdir -p latest/pics
	for f in lwmp/pics/*.BMP \
	  lwmp/pics/*.bmp; do \
	  cp $$f latest/pics; \
	done
	scripts/lcase.sh -u latest/pics/*.bmp latest/pics/*.BMP
	# sprites
	mkdir -p latest/sprites
	for f in lwmp/sprites/*.BMP lwmp/sprites/*.bmp; do \
	  cp $$f latest/sprites; \
	done
	scripts/lcase.sh -u latest/sprites/*.bmp latest/sprites/*.BMP
	# sprites/dir*
	for i in `seq 8`; do \
	  mkdir -p latest/sprites/dir$$i; \
	  for f in lwmp/sprites/dir$$i/*.BMP \
	    lwmp/sprites/dir$$i/*.bmp; do \
	    cp $$f latest/sprites/dir$$i; \
	  done; \
	  scripts/lcase.sh -u latest/sprites/dir$$i/*.bmp latest/sprites/dir$$i/*.BMP; \
	done
	# unwanted files
	rm -f latest/pics/L_BJWINSPIC_AK47.BMP
	rm -f latest/pics/L_BJWINSPIC_GRENADE.BMP
	rm -f latest/pics/L_BJWINSPIC_GRENADE_HURT*.BMP
	rm -f latest/pics/L_BJWINSPIC_RPG.BMP
	rm -f latest/pics/L_GUYPIC_AK47.BMP
	rm -f latest/pics/L_GUYPIC_AK47_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_FLAMEGUN.BMP
	rm -f latest/pics/L_GUYPIC_FLAMEGUN_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_GEWEHR.BMP
	rm -f latest/pics/L_GUYPIC_GEWEHR_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_GRENADE.BMP
	rm -f latest/pics/L_GUYPIC_GRENADE_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_M16_INWORK.BMP
	rm -f latest/pics/L_GUYPIC_MUTANT2.BMP
	rm -f latest/pics/L_GUYPIC_MUTANT3.BMP
	rm -f latest/pics/L_GUYPIC_PPSH.BMP
	rm -f latest/pics/L_GUYPIC_PPSH_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_RPG.BMP
	rm -f latest/pics/L_GUYPIC_RPG_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_SHOTGUN1.BMP
	rm -f latest/pics/L_GUYPIC_SHOTGUN1_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_SHOTGUN2.BMP
	rm -f latest/pics/L_GUYPIC_SHOTGUN2_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_SHOTGUN3.BMP
	rm -f latest/pics/L_GUYPIC_SHOTGUN3_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_SNIPER.BMP
	rm -f latest/pics/L_GUYPIC_SNIPER_HURT*.BMP
	rm -f latest/pics/L_GUYPIC_UZI.BMP
	rm -f latest/pics/L_GUYPIC_UZI_HURT*.BMP
	find latest -name 'SPR_BJ_*FLAME*.BMP' -exec rm -f '{}' \;
	find latest -name 'SPR_BJ_*GRENADE*.BMP' -exec rm -f '{}' \;
	find latest -name 'SPR_BJ_*RIFLE*.BMP' -exec rm -f '{}' \;
	find latest -name 'SPR_BJ_*RPG*.BMP' -exec rm -f '{}' \;
	# diff
	diff -rq games/splitwolf/lwmp latest

latest2:
	rm -rf latest
	cp -a $(DROPBOX_GAME_DIR)/lwmp latest
	find latest -name '*.bmp' -exec scripts/lcase.sh -u '{}' \;
	rm -f latest/sprites/dir7/_W4.BMP
	# diff
	diff -rq games/splitwolf/lwmp latest

to_dropbox:
	mkdir -p $(DROPBOX_GAME_DIR)
	#cp -a games/splitwolf/lwmp $(DROPBOX_GAME_DIR)
	cp -a games/splitwolf/*.exe games/splitwolf/*.dll \
	  games/splitwolf/*.bat $(RUN_GAME_DIR)/* $(DROPBOX_GAME_DIR)
	$(MAKE) ZIP_PACKAGE="$(DROPBOX_SRC_DIR)/lwolf_splitsrc$(NUM)" zip

listsrc:
	ls "$(DROPBOX_SRC_DIR)"

profile:
	( cd $(RUN_GAME_DIR); gprof $(PWD)/$(BINARY) )

ifeq ($(BATENGINE_CROSSCOMPILE),y)
to_vboxshare:
	#rm -rf $(VBOX_PROJ)
	mkdir -p $(VBOX_PROJ)
	cp -a $(BINARY) $(VBOX_PROJ)
	#cp -a games/splitwolf/lwmp $(VBOX_PROJ)
	#cp -a games/wolf3d_full/* $(VBOX_PROJ)
	#cp -a /opt/SDL-1.2.13/runtime/* $(VBOX_PROJ)
else
to_vboxshare:
	rm -rf $(VBOX_PROJ)
	mkdir -p $(VBOX_PROJ)
	cp -a * $(VBOX_PROJ)
	mkdir -p $(VBOX_PROJ)/$(RUN_GAME_DIR)
	cp -a games/splitwolf/* $(VBOX_PROJ)/$(RUN_GAME_DIR)
endif

from_vboxshare:
	cp $(VBOX_PROJ)/$(RUN_GAME_DIR)/Wolf4SDL.exe \
	  games/splitwolf/Wolf4SDL-$(GAME_VERSION_ID).exe
	cp $(VBOX_PROJ)/Wolf4SDL.dev .

releasepkg:
	( cd games/splitwolf/; zip -r ../../$(GAME_ZIP_PACKAGE).zip * )

release:
	make VERSION_WOLF3D=y BATENGINE_CROSSCOMPILE=y && make VERSION_WOLF3D=y BATENGINE_CROSSCOMPILE=y to_vboxshare && make VERSION_WOLF3D=y BATENGINE_CROSSCOMPILE=y clean
	make VERSION_SPEAR=y BATENGINE_CROSSCOMPILE=y && make VERSION_SPEAR=y BATENGINE_CROSSCOMPILE=y to_vboxshare && make VERSION_SPEAR=y BATENGINE_CROSSCOMPILE=y clean
	make VERSION_WOLF3D_SHAREWARE=y BATENGINE_CROSSCOMPILE=y && make VERSION_WOLF3D_SHAREWARE=y BATENGINE_CROSSCOMPILE=y to_vboxshare && make VERSION_WOLF3D_SHAREWARE=y BATENGINE_CROSSCOMPILE=y clean
	make VERSION_WOLF3D_APOGEE=y BATENGINE_CROSSCOMPILE=y && make VERSION_WOLF3D_APOGEE=y BATENGINE_CROSSCOMPILE=y to_vboxshare && make VERSION_WOLF3D_APOGEE=y BATENGINE_CROSSCOMPILE=y clean
	make VERSION_SPEAR_DEMO=y BATENGINE_CROSSCOMPILE=y && make VERSION_SPEAR_DEMO=y BATENGINE_CROSSCOMPILE=y to_vboxshare && make VERSION_SPEAR_DEMO=y BATENGINE_CROSSCOMPILE=y clean
