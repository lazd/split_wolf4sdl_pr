// ID_LWMP.H
// Created Sun Feb 26 17:54:03 2012
// Author: LinuxWolf - Team RayCAST

#ifndef ID_LWMP_H
#define ID_LWMP_H

#include <SDL2/SDL.h>
#include "wl_types.h"

#define LWMP_NUM_PLAYERS LWMP_GetNumPlayers()
#define LWMP_MAX_PLAYERS 4
#define LWMP_DECL(x) x[LWMP_MAX_PLAYERS]
#define LWMP_DECL_VAR(x, y) x[LWMP_MAX_PLAYERS] = { y, y, y, y }
#define LWMP_DECL_ARR(x, y, z) x[LWMP_MAX_PLAYERS][y] = { z, z, z, z }
#define LWMP_VAR(x) x[LWMP_GetInstance()]
#define LWMP_VAR_INIT(x) { x, x, x, x }
#define LWMP_VAR_ANY(x, y) x[y]

#define LWMP_REPEAT(c) LWMP_Repeat(0, &(c), false); LWMP_Repeat(1, &(c), false); LWMP_Repeat(2, &(c), false)
#define LWMP_ENDREPEAT(c) while (LWMP_Repeat(1, &(c), false)) LWMP_Repeat(2, &(c), false)

#define LWMP_REPEAT_MAX(c) LWMP_Repeat(0, &(c), true); LWMP_Repeat(1, &(c), true); LWMP_Repeat(2, &(c), true)
#define LWMP_ENDREPEAT_MAX(c) while (LWMP_Repeat(1, &(c), true)) LWMP_Repeat(2, &(c), true)

#define LWMP_REPEAT_ONCE(c, x) LWMP_Repeat_Once(0, &(c), x); LWMP_Repeat_Once(1, &(c), 0); LWMP_Repeat_Once(2, &(c), 0)
#define LWMP_ENDREPEAT_ONCE(c) while (LWMP_Repeat_Once(1, &(c), 0)) LWMP_Repeat_Once(2, &(c), 0)

#define LWMP_NONSPLIT(c) LWMP_NonSplit(0, &(c), false); LWMP_NonSplit(1, &(c), false); LWMP_NonSplit(2, &(c), false)
#define LWMP_ENDNONSPLIT(c) while (LWMP_NonSplit(1, &(c), false)) LWMP_NonSplit(2, &(c), false)

#define LWMP_NONSPLIT_OVERRIDE(c) LWMP_NonSplit(0, &(c), true); LWMP_NonSplit(1, &(c), true); LWMP_NonSplit(2, &(c), true)
#define LWMP_ENDNONSPLIT_OVERRIDE(c) while (LWMP_NonSplit(1, &(c), true)) LWMP_NonSplit(2, &(c), true)

#define LWMP_FRACINIT(x, argVal, argResidual, argNumerator, argDivider) \
do { \
    memset(&(x), 0, sizeof(x)); \
    (x).val = argVal; \
    (x).residual = argResidual; \
    (x).numerator = argNumerator; \
    (x).divider = argDivider; \
} while (0)

#define LWMP_FRACINC(x) \
do { \
    (x).residual += (x).numerator; \
    while ((x).residual >= (x).divider) \
    { \
        (x).val++; \
        (x).residual -= (x).divider; \
    } \
} while (0)

#define LWMP_MIN(a, b) ((a) < (b) ? (a) : (b))
#define LWMP_MAX(a, b) ((a) > (b) ? (a) : (b))

#define LWMP_BOSSHPFACTOR (100 + (75 * (LWMP_NUM_PLAYERS - 1))) / 100

#define anglefrac_mp LWMP_VAR(anglefrac)
#define LastAttacker_mp LWMP_VAR(LastAttacker)
#define joyIndex_mp LWMP_VAR(joyIndex)
#define joyadjustment_mp LWMP_VAR(joyadjustment)
#define lives_mp LWMP_VAR(lives)
#define health_mp LWMP_VAR(health)
#define ammo_mp LWMP_VAR(ammo)
#define keys_mp LWMP_VAR(keys)
#define bestweapon_mp LWMP_VAR(bestweapon)
#define weapon_mp LWMP_VAR(weapon)
#define chosenweapon_mp LWMP_VAR(chosenweapon)
#define bobdir_mp LWMP_VAR(bobdir)
#define bobdir2_mp LWMP_VAR(bobdir2)
#define bobber_mp LWMP_VAR(bobber)
#define bobber2_mp LWMP_VAR(bobber2)
#define faceframe_mp LWMP_VAR(faceframe)
#define attackframe_mp LWMP_VAR(attackframe)
#define attackcount_mp LWMP_VAR(attackcount)
#define attacktics_mp LWMP_VAR(attacktics)
#define weaponframe_mp LWMP_VAR(weaponframe)
#define heartbeat_tics_mp LWMP_VAR(heartbeat_tics)
#define shocker_tics_mp LWMP_VAR(shocker_tics)
#define controlx_mp LWMP_VAR(controlx)
#define controly_mp LWMP_VAR(controly)
#define funnyticount_mp LWMP_VAR(funnyticount)
#define facecount_mp LWMP_VAR(facecount)
#define facetimes_mp LWMP_VAR(facetimes)
#define player_mp LWMP_VAR(player)
#define iangle_mp LWMP_VAR(iangle)
#define curangle_mp LWMP_VAR(curangle)
#define clockwise_mp LWMP_VAR(clockwise)
#define counter_mp LWMP_VAR(counter)
#define change_mp LWMP_VAR(change)
#define areabyplayer_mp LWMP_VAR(areabyplayer)
#define plux_mp LWMP_VAR(plux)
#define pluy_mp LWMP_VAR(pluy)
#define buttonstate_mp LWMP_VAR(buttonstate)
#define buttonheld_mp LWMP_VAR(buttonheld)
#define buttonscan_mp LWMP_VAR(buttonscan)
#define buttonjoy_mp LWMP_VAR(buttonjoy)
#define dirscan_mp LWMP_VAR(dirscan)
#define madenoise_mp LWMP_VAR(madenoise)
#define thrustspeed_mp LWMP_VAR(thrustspeed)
#define spotvis_mp LWMP_VAR(spotvis)
#define visdata_mp LWMP_VAR(visdata)
#define visspot_mp LWMP_VAR(visspot)
#define visflags_mp LWMP_VAR(visflags)
#define transx_mp LWMP_VAR(transx)
#define transy_mp LWMP_VAR(transy)
#define viewx_mp LWMP_VAR(viewx)
#define viewy_mp LWMP_VAR(viewy)
#define hurt_tics_mp LWMP_VAR(hurt_tics)
#define damage_tics_mp LWMP_VAR(damage_tics)
#define pain_frame_mp LWMP_VAR(pain_frame)
#define player_shade_mp LWMP_VAR(player_shade)
#define player_spawnx_mp LWMP_VAR(player_spawnx)
#define player_spawny_mp LWMP_VAR(player_spawny)
#define player_spawndir_mp LWMP_VAR(player_spawndir)
#define player_camz_mp LWMP_VAR(player_camz)
#define player_fall_mp LWMP_VAR(player_fall)
#define minimap_expand_mp LWMP_VAR(minimap_expand)
#define playerpoint_mp LWMP_VAR(playerpoint)
#define playeranglevec_mp LWMP_VAR(playeranglevec)
#define viewwidth_mp LWMP_VAR(viewwidth)
#define viewheight_mp LWMP_VAR(viewheight)
#define centerx_mp LWMP_VAR(centerx)
#define shootdelta_mp LWMP_VAR(shootdelta)
#define viewscreenx_mp LWMP_VAR(viewscreenx)
#define viewscreeny_mp LWMP_VAR(viewscreeny)
#define screenofs_mp LWMP_VAR(screenofs)
#define minimap_xoff_mp LWMP_VAR(minimap_xoff)
#define minimap_yoff_mp LWMP_VAR(minimap_yoff)
#define minimap_width_mp LWMP_VAR(minimap_width)
#define minimap_height_mp LWMP_VAR(minimap_height)
#define viewscale_mp LWMP_VAR(viewscale)
#define heightnumerator_mp LWMP_VAR(heightnumerator)
#define pixelangle_mp LWMP_VAR(pixelangle)
#define thrustvector_mp LWMP_VAR(thrustvector)
#define visshade_buf_mp LWMP_VAR(visshade_buf)
#define arrowrotmat_mp LWMP_VAR(arrowrotmat)
#define autoplay_mp LWMP_VAR(autoplay)
#define damagecount_mp LWMP_VAR(damagecount)
#define bonuscount_mp LWMP_VAR(bonuscount)
#define moveanimtics_mp LWMP_VAR(moveanimtics)

typedef struct LWMP_Frac_s
{
    int val;
    int residual;
    int numerator, divider;
} LWMP_Frac_t;

// this type can be safely casted to t_compshape
typedef void *LWMP_Shape_t;

typedef enum LWMP_GibMethod_e
{
    GIB_METHOD_CHAINGUN,
    GIB_METHOD_ROCKET,
    GIB_METHOD_MAX,
} LWMP_GibMethod_t;

void LWMP_Startup(void);

void LWMP_SetResolution(int width, int height);

void LWMP_SetLayout(const char *layoutStr);

const char *LWMP_GetLayoutString(void);

void LWMP_SetInstance(int instanceNum);

int LWMP_GetInstance(void);

bool LWMP_IsFirst(void);

bool LWMP_Repeat(int cmd, int *count, bool doMax);

bool LWMP_Repeat_Once(int cmd, int *count, int onceIndex);

bool LWMP_NonSplit(int cmd, int *count, bool override);

int LWMP_GetNumPlayers(void);

void LWMP_SetNumPlayers(int numPlayers);

int LWMP_GetPhysicalScreenWidth(void);

int LWMP_GetPhysicalScreenHeight(void);

int LWMP_GetVirtualScreenX(void);

int LWMP_GetVirtualScreenY(void);

int LWMP_GetVirtualScreenWidth(void);

int LWMP_GetVirtualScreenHeight(void);

int LWMP_GetControlsInstance(void);

void LWMP_SetControlsInstance(int controlsInstance);

void LWMP_CycleControlsInstance(int dir);

void LWMP_ThreeDRefresh(void);

void LWMP_Preload(void);

int LWMP_RotateSprite(int angle, int shapenum);

LWMP_Shape_t *LWMP_GetShape(int lwmpShapeNum);

int LWMP_MaxShapes(void);

void LWMP_SetDataDir(const char *dataDir);

const char *LWMP_GetDataDir(void);

int LWMP_GetVirtualCoordX(int x);

int LWMP_GetVirtualCoordY(int y);

int LWMP_GetVirtualCoordW(int w);

int LWMP_GetVirtualCoordH(int h);

int LWMP_StatusLines(void);

unsigned char *LWMP_GetPic(int chunknum, unsigned int *width, unsigned int *height);

SDL_Color *LWMP_GetPicPal(int chunknum);

SDL_Surface *LWMP_GetPicSurf(int chunknum);

void LWMP_CycleInstance(int mode);

char *LWMP_Va(const char *format, ...);

void LWMP_ChangeNumPlayers(int numPlayersChange);

void LWMP_ChangeLayout(int layoutChange);

void LWMP_PushMatrix(void);

void LWMP_PopMatrix(void);

void LWMP_PushMatrixIdent(void);

void LWMP_PushMatrixCombinedHUD(void);

void LWMP_PushMatrixStatusBar(void);

void LWMP_Translate(float tx, float ty);

void LWMP_Scale(float sx, float sy);

void LWMP_DrawCombinedHUD(void);

void LWMP_DrawMinimap(void);

void LWMP_ResetMinimapReveal(void);

void LWMP_RevealMinimapArea(int tilex, int tiley, 
    bool clearspot);

void LWMP_CheckMinimapKeys(void);

bool LWMP_SetViewSize(unsigned width, unsigned height);

bool LWMP_GetMinimapRotated(void);

void LWMP_SetMinimapRotated(bool minimapRotated);

int LWMP_SaveTheGame(FILE *file, int checksum);

int LWMP_LoadTheGame(FILE *file, int checksum);

void LWMP_SoundPath(const char *filename, char *path, int pathsize,
    const char *ext);

#endif
