#ifndef LWLIB_STREAM_H
#define LWLIB_STREAM_H

#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <map>
#include <set>

#define LWLIB_ENUMCASTTOINT(x) *((int *)&(x))

namespace lwlib
{
	namespace StreamDirection
	{
		enum e
		{
			in,
			out,
		};
	}

	namespace StreamFormat
	{
		enum e
		{
			binary,
			text,
		};

		class Permitted
		{
			enum e vals[2];
			int len;

		public:
			Permitted();

			explicit Permitted(enum e x);

			Permitted(enum e x, enum e y);

			void add(enum e x);

			bool enabled(enum e x) const;
		};

		extern const Permitted allPermitted;
		extern const Permitted binaryPermitted;
		extern const Permitted textPermitted;
	}

	namespace StreamTagBits
	{
		enum e
		{
			addPadding = 0x01,
			addNewLine = 0x02,
			isEnd = 0x04,
		};
	}

	typedef int StreamTagBitMask;

	template <typename T>
	class StreamNvp
	{
	public:
		std::string name;
		T &val;
		StreamFormat::Permitted perm;

		StreamNvp(std::string name_, T &val_, StreamFormat::Permitted perm_) :
			name(name_), val(val_), perm(perm_)
		{
		}
	};

	template <typename T>
	StreamNvp<T> makenvp(std::string name, T &val)
	{
		return StreamNvp<T>(name, val, StreamFormat::allPermitted);
	}

	template <typename T>
	StreamNvp<T> makenvp_bin(std::string name, T &val)
	{
		return StreamNvp<T>(name, val, StreamFormat::binaryPermitted);
	}

	template <typename T>
	StreamNvp<T> makenvp_text(std::string name, T &val)
	{
		return StreamNvp<T>(name, val, StreamFormat::textPermitted);
	}

	class Stream
	{
	public:
		FILE *fp;
		bool ownsFileHandle;
		StreamDirection::e dir;
		StreamFormat::e format;
		std::string pad;
		int totalBytes;

		Stream(const char *fileName, StreamDirection::e dir, 
			StreamFormat::e format);

		Stream(FILE *fp, StreamDirection::e dir, 
			StreamFormat::e format);

		~Stream();

		std::string serializeTag(std::string name, 
			StreamTagBitMask tagBits);

		std::string serializeStartTag(const std::string &name);

		std::string serializeEndTag(const std::string &name);

		template <typename T>
		void serialize(T &x, const char *fmt = NULL)
		{
			if (format == StreamFormat::binary)
			{
				if (dir == StreamDirection::out)
				{
					fwrite(&x, sizeof(T), 1, fp);
				}
				else
				{
					fread(&x, sizeof(T), 1, fp);
				}
				/*{
					int i;
					for (i = 0; i < sizeof(T); i++)
					{
						fprintf(stderr, "%02x ", 
							(int)*((unsigned char *)&x + i));
					}
					fprintf(stderr, "\n");
					totalBytes += sizeof(T);
				}*/
			}
			else if (format == StreamFormat::text && fmt != NULL)
			{
				if (dir == StreamDirection::out)
				{
					fprintf(fp, fmt, x);
				}
				else
				{
					if (fscanf(fp, fmt, &x) != 1)
					{
						throw "failed to parse item";
					}
				}
			}
		}

		bool isIn(void) const;

		bool isOut(void) const;
	};

	namespace StreamStartTag
	{
		enum e
		{
			maxArgs = 4,
		};

		class Info
		{
			bool findArg(const std::string &arg) const;

		public:
			std::string w[maxArgs];

			// startTag format
			// <tag> [(copy|store) <storeName>] [noread]
			explicit Info(const std::string &startTag);

			bool needsStore(void) const;

			bool copy(void) const;

			bool noread(void) const;

			bool store(void) const;

			const std::string getTag(void) const;

			const std::string getStoreName(void) const;
		};
	}

	template <typename T>
	void serialize(Stream &stream, StreamNvp<T> nvp)
	{
		std::string startTag, endTag;

		if (!nvp.perm.enabled(stream.format))
		{
			return;
		}

		if (stream.format == StreamFormat::text && stream.isIn())
		{
			startTag = stream.serializeStartTag(nvp.name);

			StreamStartTag::Info startTagInfo(startTag);
			if (startTagInfo.getTag() != nvp.name)
			{
				throw "unexpected tag";
			}

			if (startTagInfo.needsStore())
			{
				static std::map<std::string, T> objStore;

				if (startTagInfo.copy())
				{
					nvp.val = objStore.at(startTagInfo.getStoreName());
				}

				if (!startTagInfo.noread())
				{
					stream & nvp.val;
				}

				if (startTagInfo.store())
				{
					objStore.insert(std::make_pair(startTagInfo.getStoreName(),
						nvp.val));
				}
			}
			else
			{
				if (!startTagInfo.noread())
				{
					stream & nvp.val;
				}
			}

			endTag = stream.serializeEndTag(nvp.name);
			if (endTag != startTagInfo.getTag())
			{
				throw "tag mismatch";
			}
		}
		else
		{
			startTag = stream.serializeStartTag(nvp.name);
			if (startTag != nvp.name)
			{
				throw "unexpected tag";
			}
			stream & nvp.val;
			endTag = stream.serializeEndTag(nvp.name);
			if (endTag != startTag)
			{
				throw "tag mismatch";
			}
		}
	}

	void serialize(Stream &stream, int &x);

	void serialize(Stream &stream, unsigned int &x);

	void serialize(Stream &stream, short int &x);

	void serialize(Stream &stream, unsigned long &x);

	void serialize(Stream &stream, double &x);

	void serialize(Stream &stream, bool &x);

	void serialize(Stream &stream, unsigned char &x);

	void serialize(Stream &stream, std::string &x);

	template <typename T>
	void serialize(Stream &stream, std::vector<T> &x)
	{
		typedef typename std::vector<T>::size_type size_type;

		size_type count = x.size();
		stream & makenvp("count", count);

		if (stream.isIn())
		{
			x.clear();
			for (size_type i = 0; i < count; i++)
			{
				T y;
				stream & makenvp("item", y);
				x.push_back(y);
			}
		}
		else if (stream.isOut())
		{
			for (size_type i = 0; i < count; i++)
			{
				stream & makenvp("item", x[i]);
			}
		}
	}

	template <typename K, typename T>
	void serialize(Stream &stream, std::map<K, T> &x)
	{
		typedef typename std::map<K, T>::size_type size_type;
		typedef typename std::map<K, T>::iterator iterator_type;

		size_type count = x.size();
		stream & makenvp("count", count);

		if (stream.isIn())
		{
			x.clear();
			for (size_type i = 0; i < count; i++)
			{
				std::pair<K, T> y;
				stream & makenvp("item", y);
				x.insert(x.begin(), y);
			}
		}
		else if (stream.isOut())
		{
			for (iterator_type it = x.begin(); it != x.end(); it++)
			{
				std::pair<K, T> y = *it;
				stream & makenvp("item", y);
			}
		}
	}

	template <typename K, typename T>
	void serialize(Stream &stream, std::pair<K, T> &x)
	{
		stream & makenvp("first", x.first);
		stream & makenvp("second", x.second);
	}

	template <typename T>
	void serialize(Stream &stream, std::set<T> &x)
	{
		typedef typename std::set<T>::size_type size_type;
		typedef typename std::set<T>::iterator iterator_type;

		size_type count = x.size();
		stream & makenvp("count", count);

		if (stream.isIn())
		{
			x.clear();
			for (size_type i = 0; i < count; i++)
			{
				T y;
				stream & makenvp("item", y);
				x.insert(x.begin(), y);
			}
		}
		else if (stream.isOut())
		{
			for (iterator_type it = x.begin(); it != x.end(); ++it)
			{
				stream & makenvp("item", *it);
			}
		}
	}


	template <typename Iter>
	void serialize(Stream &stream, Iter start, Iter end)
	{
		for (Iter it = start; it != end; ++it)
		{
			stream & makenvp("item", *it);
		}
	}

	template <typename T>
	void serializeEnum(Stream &stream, T &x)
	{
		int y = (int)x;
		serialize(stream, y);
		x = (T)y;
	}

	template <typename T>
	void serializeEnum(Stream &stream, StreamNvp<T> nvp)
	{
		std::string startTag, endTag;

		if (!nvp.perm.enabled(stream.format))
		{
			return;
		}

		startTag = stream.serializeStartTag(nvp.name);
		if (startTag != nvp.name)
		{
			throw "unexpected tag";
		}
		serializeEnum(stream, nvp.val);
		endTag = stream.serializeEndTag(nvp.name);
		if (endTag != startTag)
		{
			throw "tag mismatch";
		}
	}

	template <typename T>
	void serializeEnumArr(Stream &stream, T *x, int n)
	{
		int i;

		for (i = 0; i < n; i++)
		{
			serializeEnum(stream, makenvp("item", x[i]));
		}
	}

	template <typename T>
	Stream &operator&(Stream &stream, T &x)
	{
		serialize(stream, x);
		return stream;
	}

	template <typename T>
	Stream &operator&(Stream &stream, StreamNvp<T> x)
	{
		serialize(stream, x);
		return stream;
	}
}

#endif
