// WL_PLAY.C

#include <iostream>
#include "wl_def.h"
#pragma hdrstop

#include "wl_cloudsky.h"
#include "wl_shade.h"

/*
=============================================================================

                                                 LOCAL CONSTANTS

=============================================================================
*/

#define sc_Question     0x35

/*
=============================================================================

                                                 GLOBAL VARIABLES

=============================================================================
*/

boolean LWMP_DECL(madenoise);              // true when shooting or screaming

exit_t playstate;
int levelcompletedby = 0;

static musicnames lastmusicchunk = (musicnames) 0;

static int DebugOk;

extern SDL_GameController *GameControllers[];

objtype objlist[MAXACTORS];
objtype *newobj, *obj, *lastobj, *objfreelist, *killerobj;
objtype *LWMP_DECL(player);

boolean noclip, ammocheat;
int godmode, notargetmode, singlestep, extravbls = 0;

byte tilemap[MAPSIZE][MAPSIZE]; // wall values only
byte LWMP_DECL(spotvis)[MAPSIZE][MAPSIZE];
objtype *actorat[MAPSIZE][MAPSIZE];

//
// replacing refresh manager
//
unsigned tics;

//
// control info
//
boolean mouseenabled, joystickenabled, movewithmouse;
#define dirscan_mp_init { sc_UpArrow, sc_RightArrow, sc_DownArrow, sc_LeftArrow }
int LWMP_DECL(dirscan)[4] =
{
    { sc_UpArrow, sc_RightArrow, sc_DownArrow, sc_LeftArrow },
    { sc_W, sc_D, sc_S, sc_A },
    { sc_Kp8, sc_Kp6, sc_Kp5, sc_Kp4 },
    { sc_I, sc_L, sc_K, sc_J },
};
int LWMP_DECL(buttonscan)[NUMBUTTONS] = 
{
    {
        sc_RCtrl,         /* bt_attack */
        sc_RAlt,          /* bt_strafe */
        sc_RShift,        /* bt_run */
        sc_Space,         /* bt_use */
        sc_1,             /* bt_readyknife */
        sc_2,             /* bt_readypistol */
        sc_3,             /* bt_readymachinegun */
        sc_4,             /* bt_readychaingun */
        sc_None,          /* bt_nextweapon */
        sc_None,          /* bt_prevweapon */
        sc_None,          /* bt_esc */
        sc_None,          /* bt_pause */
        sc_None,          /* bt_strafeleft */
        sc_None,          /* bt_straferight */
        sc_PgDn,          /* bt_nextcolor */
        sc_PgUp,          /* bt_minimap */
    },
    {
        sc_LCtrl,         /* bt_attack */
        sc_LAlt,          /* bt_strafe */
        sc_LShift,        /* bt_run */
        sc_E,             /* bt_use */
        sc_None,          /* bt_readyknife */
        sc_None,          /* bt_readypistol */
        sc_None,          /* bt_readymachinegun */
        sc_None,          /* bt_readychaingun */
        sc_Z,             /* bt_nextweapon */
        sc_None,          /* bt_prevweapon */
        sc_None,          /* bt_esc */
        sc_None,          /* bt_pause */
        sc_None,          /* bt_strafeleft */
        sc_None,          /* bt_straferight */
        sc_C,             /* bt_nextcolor */
        sc_X,             /* bt_minimap */
    },
    {
        sc_Kp7,           /* bt_attack */
        sc_None,          /* bt_strafe */
        sc_Enter,         /* bt_run */
        sc_Kp9,           /* bt_use */
        sc_None,          /* bt_readyknife */
        sc_None,          /* bt_readypistol */
        sc_None,          /* bt_readymachinegun */
        sc_None,          /* bt_readychaingun */
        sc_Kp1,           /* bt_nextweapon */
        sc_None,          /* bt_prevweapon */
        sc_None,          /* bt_esc */
        sc_None,          /* bt_pause */
        sc_None,          /* bt_strafeleft */
        sc_None,          /* bt_straferight */
        sc_Kp3,           /* bt_nextcolor */
        sc_Kp2,           /* bt_minimap */
    },
    {
        sc_U,             /* bt_attack */
        sc_None,          /* bt_strafe */
        sc_B,             /* bt_run */
        sc_O,             /* bt_use */
        sc_None,          /* bt_readyknife */
        sc_None,          /* bt_readypistol */
        sc_None,          /* bt_readymachinegun */
        sc_None,          /* bt_readychaingun */
        sc_M,             /* bt_nextweapon */
        sc_None,          /* bt_prevweapon */
        sc_None,          /* bt_esc */
        sc_None,          /* bt_pause */
        sc_None,          /* bt_strafeleft */
        sc_None,          /* bt_straferight */
        sc_Gt,            /* bt_nextcolor */
        sc_Lt,            /* bt_minimap */
    },
};
int buttonmouse[4] = { bt_attack, bt_use, bt_minimap, bt_nobutton };
int LWMP_DECL(buttonjoy)[32] = {
    {
    #ifdef _arch_dreamcast
        bt_attack, bt_strafe, bt_use, bt_run, bt_esc, bt_prevweapon, bt_nobutton, bt_nextweapon,
        bt_pause, bt_strafeleft, bt_straferight, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton,
    #else
        bt_attack, bt_use, bt_strafe, bt_run, bt_prevweapon, bt_nextweapon, bt_use, bt_attack,
        bt_run, bt_run, bt_minimap, bt_esc, bt_pause, bt_nobutton, bt_nobutton, bt_nobutton,
    #endif
        bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton,
        bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton
    },
    {
    #ifdef _arch_dreamcast
        bt_attack, bt_strafe, bt_use, bt_run, bt_esc, bt_prevweapon, bt_nobutton, bt_nextweapon,
        bt_pause, bt_strafeleft, bt_straferight, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton,
    #else
        bt_attack, bt_use, bt_strafe, bt_run, bt_prevweapon, bt_nextweapon, bt_use, bt_attack,
        bt_run, bt_run, bt_minimap, bt_esc, bt_pause, bt_nobutton, bt_nobutton, bt_nobutton,
    #endif
        bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton,
        bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton
    },
    {
    #ifdef _arch_dreamcast
        bt_attack, bt_strafe, bt_use, bt_run, bt_esc, bt_prevweapon, bt_nobutton, bt_nextweapon,
        bt_pause, bt_strafeleft, bt_straferight, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton,
    #else
        bt_attack, bt_use, bt_strafe, bt_run, bt_prevweapon, bt_nextweapon, bt_use, bt_attack,
        bt_run, bt_run, bt_minimap, bt_esc, bt_pause, bt_nobutton, bt_nobutton, bt_nobutton,
    #endif
        bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton,
        bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton
    },
    {
    #ifdef _arch_dreamcast
        bt_attack, bt_strafe, bt_use, bt_run, bt_esc, bt_prevweapon, bt_nobutton, bt_nextweapon,
        bt_pause, bt_strafeleft, bt_straferight, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton,
    #else
        bt_attack, bt_use, bt_strafe, bt_run, bt_prevweapon, bt_nextweapon, bt_use, bt_attack,
        bt_run, bt_run, bt_pause, bt_esc, bt_minimap, bt_nobutton, bt_nobutton, bt_nobutton,
    #endif
        bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton,
        bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton, bt_nobutton
    },
};

int viewsize;

boolean LWMP_DECL(buttonheld)[NUMBUTTONS];

boolean demorecord, demoplayback;
int8_t *demoptr, *lastdemoptr;
memptr demobuffer;

//
// current user input
//
int LWMP_DECL(controlx), LWMP_DECL(controly);
    // range from -100 to 100 per tic
boolean LWMP_DECL(buttonstate)[NUMBUTTONS];

int LWMP_DECL(joyIndex);

int lastgamemusicoffset = 0;


//===========================================================================


void CenterWindow (word w, word h);
void InitObjList (void);
void RemoveObj (objtype * gone);
void PollControls (void);
int StopMusic (void);
void StartMusic (void);
void ContinueMusic (int offs);
void PlayLoop (void);

/*
=============================================================================

                                                 LOCAL VARIABLES

=============================================================================
*/


objtype dummyobj;

//
// LIST OF SONGS FOR EACH VERSION
//
int songs[] = {
#ifndef SPEAR
    //
    // Episode One
    //
    GETTHEM_MUS,
    SEARCHN_MUS,
    POW_MUS,
    SUSPENSE_MUS,
    GETTHEM_MUS,
    SEARCHN_MUS,
    POW_MUS,
    SUSPENSE_MUS,

    WARMARCH_MUS,               // Boss level
    CORNER_MUS,                 // Secret level

    //
    // Episode Two
    //
    NAZI_OMI_MUS,
    PREGNANT_MUS,
    GOINGAFT_MUS,
    HEADACHE_MUS,
    NAZI_OMI_MUS,
    PREGNANT_MUS,
    HEADACHE_MUS,
    GOINGAFT_MUS,

    WARMARCH_MUS,               // Boss level
    DUNGEON_MUS,                // Secret level

    //
    // Episode Three
    //
    INTROCW3_MUS,
    NAZI_RAP_MUS,
    TWELFTH_MUS,
    ZEROHOUR_MUS,
    INTROCW3_MUS,
    NAZI_RAP_MUS,
    TWELFTH_MUS,
    ZEROHOUR_MUS,

    ULTIMATE_MUS,               // Boss level
    PACMAN_MUS,                 // Secret level

    //
    // Episode Four
    //
    GETTHEM_MUS,
    SEARCHN_MUS,
    POW_MUS,
    SUSPENSE_MUS,
    GETTHEM_MUS,
    SEARCHN_MUS,
    POW_MUS,
    SUSPENSE_MUS,

    WARMARCH_MUS,               // Boss level
    CORNER_MUS,                 // Secret level

    //
    // Episode Five
    //
    NAZI_OMI_MUS,
    PREGNANT_MUS,
    GOINGAFT_MUS,
    HEADACHE_MUS,
    NAZI_OMI_MUS,
    PREGNANT_MUS,
    HEADACHE_MUS,
    GOINGAFT_MUS,

    WARMARCH_MUS,               // Boss level
    DUNGEON_MUS,                // Secret level

    //
    // Episode Six
    //
    INTROCW3_MUS,
    NAZI_RAP_MUS,
    TWELFTH_MUS,
    ZEROHOUR_MUS,
    INTROCW3_MUS,
    NAZI_RAP_MUS,
    TWELFTH_MUS,
    ZEROHOUR_MUS,

    ULTIMATE_MUS,               // Boss level
    FUNKYOU_MUS                 // Secret level
#else

    //////////////////////////////////////////////////////////////
    //
    // SPEAR OF DESTINY TRACKS
    //
    //////////////////////////////////////////////////////////////
    XTIPTOE_MUS,
    XFUNKIE_MUS,
    XDEATH_MUS,
    XGETYOU_MUS,                // DON'T KNOW
    ULTIMATE_MUS,               // Trans Gr�sse

    DUNGEON_MUS,
    GOINGAFT_MUS,
    POW_MUS,
    TWELFTH_MUS,
    ULTIMATE_MUS,               // Barnacle Wilhelm BOSS

    NAZI_OMI_MUS,
    GETTHEM_MUS,
    SUSPENSE_MUS,
    SEARCHN_MUS,
    ZEROHOUR_MUS,
    ULTIMATE_MUS,               // Super Mutant BOSS

    XPUTIT_MUS,
    ULTIMATE_MUS,               // Death Knight BOSS

    XJAZNAZI_MUS,               // Secret level
    XFUNKIE_MUS,                // Secret level (DON'T KNOW)

    XEVIL_MUS                   // Angel of Death BOSS
#endif
};


/*
=============================================================================

                               USER CONTROL

=============================================================================
*/

/*
===================
=
= PollKeyboardButtons
=
===================
*/

void PollKeyboardButtons (void)
{
    int i;

    for (i = 0; i < NUMBUTTONS; i++)
        if (Keyboard[buttonscan_mp[i]])
            buttonstate_mp[i] = true;
}


/*
===================
=
= PollMouseButtons
=
===================
*/

void PollMouseButtons (void)
{
    int buttons = IN_MouseButtons ();

    if (buttons & 1)
        buttonstate_mp[buttonmouse[0]] = true;
    if (buttons & 2)
        buttonstate_mp[buttonmouse[1]] = true;
    if (buttons & 4)
        buttonstate_mp[buttonmouse[2]] = true;
    if (buttons & 8)
        buttonstate_mp[bt_nextweapon] = true;
    if (buttons & 16)
        buttonstate_mp[bt_prevweapon] = true;
}



/*
===================
=
= PollJoystickButtons
=
===================
*/

void PollJoystickButtons (void)
{
    int buttons = IN_JoyButtons(joyIndex_mp);

    for(int i = 0, val = 1; i < JOYBUTTONS; i++, val <<= 1)
    {
        if(buttons & val)
            buttonstate_mp[buttonjoy_mp[i]] = true;
    }
}


/*
===================
=
= PollKeyboardMove
=
===================
*/

void PollKeyboardMove (void)
{
    int delta = buttonstate_mp[bt_run] ? RUNMOVE * tics : BASEMOVE * tics;

    if (Keyboard[dirscan_mp[di_north]])
        controly_mp -= delta;
    if (Keyboard[dirscan_mp[di_south]])
        controly_mp += delta;
    if (Keyboard[dirscan_mp[di_west]])
        controlx_mp -= delta;
    if (Keyboard[dirscan_mp[di_east]])
        controlx_mp += delta;
}


/*
===================
=
= PollMouseMove
=
===================
*/

void PollMouseMove (void)
{
    int mousexmove, mouseymove;

    SDL_GetMouseState(&mousexmove, &mouseymove);
    if(IN_IsInputGrabbed())
        IN_CenterMouse();

    mousexmove -= CENTERX;
    mouseymove -= CENTERY;

    controlx_mp += mousexmove * 10 / (13 - mouseadjustment);

    if (movewithmouse) {
        controly_mp += mouseymove * 20 / (13 - mouseadjustment);
    }
}


/*
===================
=
= PollJoystickMove
=
===================
*/

void PollJoystickMove (void)
{
    int joyx, joyy;

    IN_GetJoyDelta (&joyx, &joyy, joyIndex_mp, SDL_CONTROLLER_AXIS_RIGHTX, SDL_CONTROLLER_AXIS_LEFTY, true);

    int delta = buttonstate_mp[bt_run] ? RUNMOVE * tics : BASEMOVE * tics;

    int newcontrolx = delta;
    int newcontroly = delta;

    if (joyx != 0) {
        newcontrolx = (int) ((abs(joyx) / 127.0) * delta);
        newcontrolx = (int) (newcontrolx * (((joyadjustment_mp + 1) + 5) / 10.0));
    }
    if (joyy != 0) {
        newcontroly = (int) ((abs(joyy) / 127.0) * delta);
    }

    if (joyx > JOYDEADZONE || buttonstate_mp[bt_turnright])
        controlx_mp += newcontrolx;
    else if (joyx < -JOYDEADZONE || buttonstate_mp[bt_turnleft])
        controlx_mp -= newcontrolx;
    if (joyy > JOYDEADZONE || buttonstate_mp[bt_movebackward])
        controly_mp += newcontroly;
    else if (joyy < -JOYDEADZONE || buttonstate_mp[bt_moveforward])
        controly_mp -= newcontroly;
}

/*
===================
=
= PollControls
=
= Gets user or demo input, call once each frame
=
= controlx              set between -150 and 150 per tic
= controly              set between -100 and 100 per tic
= buttonheld[]  the state of the buttons LAST frame
= buttonstate[] the state of the buttons THIS frame
=
===================
*/

void PollControls (void)
{
    int max, min, i;
    byte buttonbits;
    int c;

    IN_ProcessEvents();

    //
    // get timing info for last frame
    //
    if (demoplayback || demorecord)   // demo recording and playback needs to be constant
    {
        // wait up to DEMOTICS Wolf tics
        uint32_t curtime = SDL_GetTicks();
        lasttimecount += DEMOTICS;
        int32_t timediff = (lasttimecount * 100) / 7 - curtime;
        if(timediff > 0)
            SDL_Delay(timediff);

        if(timediff < -2 * DEMOTICS)       // more than 2-times DEMOTICS behind?
        {
            // yes, set to current timecount
            lasttimecount = (curtime * 7) / 100;
        }

        tics = DEMOTICS;
    }
    else
    {
        CalcTics ();
    }

    for (LWMP_REPEAT(c))
    {
        controlx_mp = 0;
        controly_mp = 0;
        memcpy (buttonheld_mp, buttonstate_mp, sizeof (buttonstate_mp));
        memset (buttonstate_mp, 0, sizeof (buttonstate_mp));
    }

    if (demoplayback)
    {
        for (LWMP_REPEAT(c))
        {
            //
            // read commands from demo buffer
            //
            buttonbits = *demoptr++;
            for (i = 0; i < NUMBUTTONS; i++)
            {
                buttonstate_mp[i] = buttonbits & 1;
                buttonbits >>= 1;
            }

            controlx_mp = *demoptr++;
            controly_mp = *demoptr++;

            controlx_mp *= (int) tics;
            controly_mp *= (int) tics;

            if (demoptr == lastdemoptr)
            {
                SetPlayState(ex_completed);   // demo is done
                LWMP_ENDREPEAT(c);
                break;
            }
        }

        return;
    }


    //
    // get button states
    //
    for (LWMP_REPEAT(c))
    {
        PollKeyboardButtons ();
    }

    if (mouseenabled && IN_IsInputGrabbed())
        PollMouseButtons ();


    for (LWMP_REPEAT(c))
    {
        if (IN_JoyPresent(joyIndex_mp))
            PollJoystickButtons ();
    }

    //
    // get movements
    //
    for (LWMP_REPEAT(c))
    {
        PollKeyboardMove ();
    }

    if (mouseenabled && IN_IsInputGrabbed())
        PollMouseMove ();

    for (LWMP_REPEAT(c))
    {
        if (IN_JoyPresent(joyIndex_mp))
            PollJoystickMove ();
    }

    for (LWMP_REPEAT(c))
    {
        //
        // bound movement to a maximum
        //
        max = 150 * tics;
        min = -max;
        if (controlx_mp > max)
            controlx_mp = max;
        else if (controlx_mp < min)
            controlx_mp = min;

        max = 100 * tics;
        min = -max;
        if (controly_mp > max)
            controly_mp = max;
        else if (controly_mp < min)
            controly_mp = min;
    }

    if (demorecord)
    {
        for (LWMP_REPEAT(c))
        {
            //
            // save info out to demo buffer
            //
            controlx_mp /= (int) tics;
            controly_mp /= (int) tics;

            buttonbits = 0;

            // TODO: Support 32-bit buttonbits
            for (i = NUMBUTTONS - 1; i >= 0; i--)
            {
                buttonbits <<= 1;
                if (buttonstate_mp[i])
                    buttonbits |= 1;
            }

            *demoptr++ = buttonbits;
            *demoptr++ = controlx_mp;
            *demoptr++ = controly_mp;

            controlx_mp *= (int) tics;
            controly_mp *= (int) tics;

            if (demoptr >= lastdemoptr - 8)
            {
                SetPlayState(ex_completed);
                LWMP_ENDREPEAT(c);
                break;
            }
        }

    }
}



//==========================================================================



///////////////////////////////////////////////////////////////////////////
//
//      CenterWindow() - Generates a window of a given width & height in the
//              middle of the screen
//
///////////////////////////////////////////////////////////////////////////
#define MAXX    320
#define MAXY    160

void CenterWindow (word w, word h)
{
    US_DrawWindow (((MAXX / 8) - w) / 2, ((MAXY / 8) - h) / 2, w, h);
}

//===========================================================================


/*
=====================
=
= CheckKeys
=
=====================
*/

void CheckKeys (void)
{
    int i;
    ScanCode scan;
    int c;
    #ifdef DEBUGKEYS
    int ret;
    #endif

    if (screenfaded || demoplayback)    // don't do anything with a faded screen
        return;

    scan = LastScan;


#ifdef SPEAR
    //
    // SECRET CHEAT CODE: TAB-G-F10
    //
    if (Keyboard[sc_Tab] && Keyboard[sc_G] && Keyboard[sc_F10])
    {
        WindowH = 160;
        if (godmode)
        {
            Message ("God mode OFF");
            SD_PlaySound (NOBONUSSND);
        }
        else
        {
            Message ("God mode ON");
            SD_PlaySound (ENDBONUS2SND);
        }

        IN_Ack ();
        godmode ^= 1;
        DrawPlayBorderSides ();
        IN_ClearKeysDown ();
        return;
    }
#endif


    //
    // SECRET CHEAT CODE: 'MLI'
    //
    if (Keyboard[sc_M] && Keyboard[sc_L] && Keyboard[sc_I])
    {
        for (LWMP_REPEAT(c))
        {
            gamestate.health_mp = 100;
            gamestate.ammo_mp = 99;
            gamestate.keys_mp = 0;
            for (i = dr_lock1; i <= dr_lock4; i++)
            {
                GiveKey(i - dr_lock1);
            }
            GiveWeapon (wp_chaingun);

            sb(DrawWeapon ());
            sb(DrawHealth ());
            sb(DrawKeys ());
            sb(DrawAmmo ());
            sb(DrawScore ());
        }
        //gamestate.TimeCount += 42000L;
        gamestate.score = 0;

        ClearMemory ();
        CA_CacheGrChunk (STARTFONT + 1);
        ClearSplitVWB ();

        for (LWMP_NONSPLIT(c))
        {
            Message (STR_CHEATER1 "\n"
                STR_CHEATER2 "\n\n" STR_CHEATER3 "\n"
                STR_CHEATER4 "\n" STR_CHEATER5);
        }

        UNCACHEGRCHUNK (STARTFONT + 1);
        IN_ClearKeysDown ();
        IN_Ack ();

        DrawPlayScreen();
    }

    //
    // OPEN UP DEBUG KEYS
    //
#ifdef DEBUGKEYS
    if (Keyboard[sc_BackSpace] && Keyboard[sc_LShift] && Keyboard[sc_Alt] && param_debugmode)
    {
        ClearMemory ();
        CA_CacheGrChunk (STARTFONT + 1);
        ClearSplitVWB ();

        for (LWMP_NONSPLIT(c))
        {
            Message ("Debugging keys are\nnow available!");
        }
        UNCACHEGRCHUNK (STARTFONT + 1);
        IN_ClearKeysDown ();
        IN_Ack ();

        DrawPlayScreen ();
        DebugOk = 1;
    }
#endif

    //
    // TRYING THE KEEN CHEAT CODE!
    //
    if (Keyboard[sc_B] && Keyboard[sc_A] && Keyboard[sc_T])
    {
        ClearMemory ();
        CA_CacheGrChunk (STARTFONT + 1);
        ClearSplitVWB ();

        Message ("Commander Keen is also\n"
                 "available from Apogee, but\n"
                 "then, you already know\n" "that - right, Cheatmeister?!");

        UNCACHEGRCHUNK (STARTFONT + 1);
        IN_ClearKeysDown ();
        IN_Ack ();

        if (viewsize < 18)
            DrawPlayBorder ();
    }

//
// pause key weirdness can't be checked as a scan code
//
    for (LWMP_REPEAT(c))
    {
        if(buttonstate_mp[bt_pause])
        {
            Paused = true;
        }
    }
    if(Paused)
    {
        int lastoffs = StopMusic();
        LatchDrawPic (20 - 4, 80 - 2 * 8, PAUSEDPIC);
        VW_UpdateScreen();
        IN_Ack ();
        Paused = false;
        ContinueMusic(lastoffs);
        if (MousePresent && IN_IsInputGrabbed())
            IN_CenterMouse();     // Clear accumulated mouse movement
        lasttimecount = GetTimeCount();
        return;
    }

//
// F1-F7/ESC to enter control panel
//
    if (
#ifndef DEBCHECK
           scan == sc_F10 ||
#endif
           scan == sc_F9 || scan == sc_F7 || scan == sc_F8)     // pop up quit dialog
    {
        short oldmapon = gamestate.mapon;
        short oldepisode = gamestate.episode;
        ClearMemory ();
        ClearSplitVWB ();
        US_ControlPanel (scan);

        for (LWMP_REPEAT(c))
        {
            DrawPlayBorder ();
        }
        SETFONTCOLOR (0, 15);

        IN_ClearKeysDown ();
        return;
    }

    bool doEscape = false;
    for (LWMP_REPEAT(c))
    {
        if((scan >= sc_F1 && scan <= sc_F9) || scan == sc_Escape || buttonstate_mp[bt_esc])
        {
            doEscape = true;
        }
    }
    if (doEscape)
    {
        int lastoffs = StopMusic ();
        ClearMemory ();
        VW_FadeOut ();

        US_ControlPanel (buttonstate_mp[bt_esc] ? sc_Escape : scan);

        SETFONTCOLOR (0, 15);
        IN_ClearKeysDown ();
        VW_FadeOut();
        if(viewsize != 21)
            DrawPlayScreen ();
        if (!startgame && !loadedgame)
            ContinueMusic (lastoffs);
        if (loadedgame)
            playstate = ex_abort;
        lasttimecount = GetTimeCount();
        if (MousePresent && IN_IsInputGrabbed())
            IN_CenterMouse();     // Clear accumulated mouse movement
        return;
    }

//
// TAB-? debug keys
//
#ifdef DEBUGKEYS
    if (Keyboard[sc_Tab] && DebugOk)
    {
        CA_CacheGrChunk (STARTFONT);
        fontnumber = 0;
        SETFONTCOLOR (0, 15);
        for (LWMP_NONSPLIT(c))
        {
            ret = DebugKeys();
        }
        if (ret)
        {
            DrawPlayScreen ();
        }

        if (MousePresent && IN_IsInputGrabbed())
            IN_CenterMouse();     // Clear accumulated mouse movement

        lasttimecount = GetTimeCount();
        return;
    }
#endif
}


//===========================================================================

/*
#############################################################################

                                  The objlist data structure

#############################################################################

objlist containt structures for every actor currently playing.  The structure
is accessed as a linked list starting at *player, ending when ob->next ==
NULL.  GetNewObj inserts a new object at the end of the list, meaning that
if an actor spawn another actor, the new one WILL get to think and react the
same frame.  RemoveObj unlinks the given object and returns it to the free
list, but does not damage the objects ->next pointer, so if the current object
removes itself, a linked list following loop can still safely get to the
next element.

<backwardly linked free list>

#############################################################################
*/


/*
=========================
=
= InitActorList
=
= Call to clear out the actor object lists returning them all to the free
= list.  Allocates a special spot for the player.
=
=========================
*/

int objcount;

void InitActorList (void)
{
    int i;
    int c;

//
// init the actor lists
//
    for (i = 0; i < MAXACTORS; i++)
    {
        objlist[i].prev = &objlist[i + 1];
        objlist[i].next = NULL;
    }

    objlist[MAXACTORS - 1].prev = NULL;

    objfreelist = &objlist[0];
    lastobj = NULL;

    objcount = 0;

    for (LWMP_REPEAT(c))
    {
        //
        // give the player the first free spots
        //
        GetNewActor ();
        player_mp = newobj;
        player_mp->player_index = joyIndex_mp = LWMP_GetInstance();
    }
}

//===========================================================================

/*
=========================
=
= GetNewActor
=
= Sets the global variable new to point to a free spot in objlist.
= The free spot is inserted at the end of the liked list
=
= When the object list is full, the caller can either have it bomb out ot
= return a dummy object pointer that will never get used
=
=========================
*/

void GetNewActor (void)
{
    if (!objfreelist)
        Quit ("GetNewActor: No free spots in objlist!");

    newobj = objfreelist;
    objfreelist = newobj->prev;
    memset (newobj, 0, sizeof (*newobj));

    if (lastobj)
        lastobj->next = newobj;
    newobj->prev = lastobj;     // new->next is allready NULL from memset

    newobj->active = ac_no;
    lastobj = newobj;

    objcount++;
}

//===========================================================================

/*
=========================
=
= RemoveObj
=
= Add the given object back into the free list, and unlink it from it's
= neighbors
=
=========================
*/

void RemoveObj (objtype * gone)
{
    int c;

    for (LWMP_REPEAT(c))
    {
        if (gone == player_mp)
        {
            LWMP_ENDREPEAT(c);
            Quit ("RemoveObj: Tried to remove the player!");
        }
    }

    gone->state = NULL;

    //
    // fix the next object's back link
    //
    if (gone == lastobj)
        lastobj = (objtype *) gone->prev;
    else
        gone->next->prev = gone->prev;

    //
    // fix the previous object's forward link
    //
    gone->prev->next = gone->next;

    //
    // add it back in to the free list
    //
    gone->prev = objfreelist;
    objfreelist = gone;

    objcount--;
}

/*
=============================================================================

                                                MUSIC STUFF

=============================================================================
*/


/*
=================
=
= StopMusic
=
=================
*/
int StopMusic (void)
{
    int lastoffs = SD_MusicOff ();

    UNCACHEAUDIOCHUNK (STARTMUSIC + lastmusicchunk);

    return lastoffs;
}

//==========================================================================


/*
=================
=
= StartMusic
=
=================
*/

void StartMusic ()
{
    SD_MusicOff ();
    lastmusicchunk = (musicnames) songs[gamestate.mapon + gamestate.episode * 10];
    SD_StartMusic(STARTMUSIC + lastmusicchunk);
}

void ContinueMusic (int offs)
{
    SD_MusicOff ();
    lastmusicchunk = (musicnames) songs[gamestate.mapon + gamestate.episode * 10];
    SD_ContinueMusic(STARTMUSIC + lastmusicchunk, offs);
}

/*
=============================================================================

                                        PALETTE SHIFTING STUFF

=============================================================================
*/

SDL_Color redshifts[NUMREDSHIFTS][256];
SDL_Color whiteshifts[NUMWHITESHIFTS][256];

int LWMP_DECL(damagecount), LWMP_DECL(bonuscount);

/*
=====================
=
= InitRedShifts
=
=====================
*/

void InitRedShifts (void)
{
    SDL_Color *workptr, *baseptr;
    int i, j, delta;


//
// fade through intermediate frames
//
    for (i = 1; i <= NUMREDSHIFTS; i++)
    {
        workptr = redshifts[i - 1];
        baseptr = gamepal;

        for (j = 0; j <= 255; j++)
        {
            delta = 256 - baseptr->r;
            workptr->r = baseptr->r + delta * i / REDSTEPS;
            delta = -baseptr->g;
            workptr->g = baseptr->g + delta * i / REDSTEPS;
            delta = -baseptr->b;
            workptr->b = baseptr->b + delta * i / REDSTEPS;
            baseptr++;
            workptr++;
        }
    }

    for (i = 1; i <= NUMWHITESHIFTS; i++)
    {
        workptr = whiteshifts[i - 1];
        baseptr = gamepal;

        for (j = 0; j <= 255; j++)
        {
            delta = 256 - baseptr->r;
            workptr->r = baseptr->r + delta * i / WHITESTEPS;
            delta = 248 - baseptr->g;
            workptr->g = baseptr->g + delta * i / WHITESTEPS;
            delta = 0-baseptr->b;
            workptr->b = baseptr->b + delta * i / WHITESTEPS;
            baseptr++;
            workptr++;
        }
    }
}


/*
=====================
=
= ClearPaletteShifts
=
=====================
*/

void ClearPaletteShifts (void)
{
    bonuscount_mp = damagecount_mp = 0;
}


/*
=====================
=
= StartBonusFlash
=
=====================
*/

void StartBonusFlash (void)
{
    bonuscount_mp = NUMWHITESHIFTS * WHITETICS;  // white shift palette
}


/*
=====================
=
= StartDamageFlash
=
=====================
*/

void StartDamageFlash (int damage)
{
    damagecount_mp += damage;
    damagecount_mp = std::min(damagecount_mp, (NUMREDSHIFTS - 1) * 10);
}

/*
=====================
=
= FinishPaletteShifts
=
= Resets palette to normal if needed
=
=====================
*/

void FinishPaletteShifts (void)
{
    int c;
    for (LWMP_REPEAT(c))
    {
        ClearPaletteShifts();
    }
}

/*
=============================================================================

                                                CORE PLAYLOOP

=============================================================================
*/


/*
=====================
=
= DoActor
=
=====================
*/

void DoActor (objtype * ob)
{
    void (*think) (objtype *);
    int c;
    //bool foundarea;
    statetype *state;

    //foundarea = false;
    //for (LWMP_REPEAT(c))
    //{
    //    if (areabyplayer_mp[ob->areanumber])
    //    {
    //        foundarea = true;
    //        LWMP_ENDREPEAT(c);
    //        break;
    //    }
    //}

    if (!ob->active/* || !foundarea*/)
        return;

    if (!(ob->flags & (FL_NONMARK | FL_NEVERMARK)))
        actorat[ob->tilex][ob->tiley] = NULL;

    //
    // non transitional object
    //
    if (!ob->ticcount)
    {
        think = (void (*)(objtype *)) ob->state->think;
        if (think)
        {
            think (ob);
            if (!ob->state)
            {
                RemoveObj (ob);
                return;
            }
        }

        if (ob->flags & FL_NEVERMARK)
            return;

        if ((ob->flags & FL_NONMARK) && actorat[ob->tilex][ob->tiley])
            return;

        actorat[ob->tilex][ob->tiley] = ob;
        return;
    }

    //
    // transitional object
    //
    ob->ticcount -= (short) tics;
    while (ob->ticcount <= 0)
    {
        state = ob->state;

        think = (void (*)(objtype *)) ob->state->action;        // end of state action
        if (think)
        {
            think (ob);
            if (!ob->state)
            {
                RemoveObj (ob);
                return;
            }
        }

        if (ob->state == state)
        {
            ob->state = ob->state->next;
        }

        if (!ob->state)
        {
            RemoveObj (ob);
            return;
        }

        if (!ob->state->tictime)
        {
            ob->ticcount = 0;
            goto think;
        }

        ob->ticcount += ob->state->tictime;
    }

think:
    //
    // think
    //
    think = (void (*)(objtype *)) ob->state->think;
    if (think)
    {
        think (ob);
        if (!ob->state)
        {
            RemoveObj (ob);
            return;
        }
    }

    if (ob->flags & FL_NEVERMARK)
        return;

    if ((ob->flags & FL_NONMARK) && actorat[ob->tilex][ob->tiley])
        return;

    actorat[ob->tilex][ob->tiley] = ob;
}

//==========================================================================

/*
===================
=
= PL_HurtAnim
=
===================
*/

static void PL_HurtAnim(void)
{
    if (gamestate.pain_frame_mp == 0)
    {
        return;
    }

    if (gamestate.health_mp > 0)
    {
        gamestate.hurt_tics_mp += tics;
    }

    if (gamestate.hurt_tics_mp >= gamestate.damage_tics_mp)
    {
        PL_ResetHurtAnim();
    }
}

/*
===================
=
= PL_ResetHurtAnim
=
===================
*/

void PL_ResetHurtAnim(void)
{
    gamestate.pain_frame_mp = gamestate.hurt_tics_mp = 
        gamestate.damage_tics_mp = 0;
}

/*
===================
=
= PL_FallAnim
=
===================
*/

static void PL_FallAnim(void)
{
    fixed t;
    Fall_t *fall;
    const fixed ground = -0x7000;
    const fixed gravity = -FP(6);
    const fixed damping = FP(1) * 2 / 5;

    if (gamestate.health_mp > 0)
    {
        return;
    }

    fall = &gamestate.player_fall_mp;
    if (fall->bounce == 2)
    {
        return;
    }
    t = TICS2SECFP(tics);

    fall->vel += fixedpt_mul(gravity, t);
    fall->pos += fixedpt_mul(fall->vel, t);

    if (fall->pos < ground)
    {
        if (fall->bounce == 0)
        {
            fall->vel = -fixedpt_mul(fall->vel, damping);
            SD_PlaySound(BJFALLSND);
        }
        fall->bounce++;
        fall->pos = ground;
    }

    gamestate.player_camz_mp = fall->pos;
}

/*
===================
=
= PL_ResetFallAnim
=
===================
*/

void PL_ResetFallAnim(void)
{
    memset(&gamestate.player_fall_mp, 0,
        sizeof(gamestate.player_fall_mp));
    gamestate.player_camz_mp = 0;
}


int32_t LWMP_DECL(funnyticount);

/*
===================
=
= PlayLoop
=
===================
*/
void PlayLoop (void)
{
    int i;
    int c;

#if defined(USE_FEATUREFLAGS) && defined(USE_CLOUDSKY)
    if(GetFeatureFlags() & FF_CLOUDSKY)
        InitSky();
#endif

#if defined(USE_SHADING) || defined(USE_SHADING_LW)
    InitLevelShadeTable();
#endif

    playstate = ex_stillplaying;
    lasttimecount = GetTimeCount();
    frameon = 0;
    for (LWMP_REPEAT(c))
    {
        anglefrac_mp = 0;
        facecount_mp = 0;
        funnyticount_mp = 0;
        memset (buttonstate_mp, 0, sizeof (buttonstate_mp));
        ClearPaletteShifts ();
    }

    if (MousePresent && IN_IsInputGrabbed())
        IN_CenterMouse();         // Clear accumulated mouse movement

    if (demoplayback)
        IN_StartAck ();

    do
    {
        PollControls ();

        //
        // actor thinking
        //
        for (LWMP_REPEAT(c))
        {
            madenoise_mp = false;
        }

        MoveDoors ();
        MovePWalls ();
        if (VampireMode::enabled())
        {
            VampireMode::drain();
        }
        if (HarvesterMode::enabled())
        {
            HarvesterMode::poll();
        }
        if (ZombieHarvesterMode::enabled())
        {
            ZombieHarvesterMode::poll();
        }

        for (LWMP_REPEAT(c))
        {
            DoActor(player_mp);
        }

        for (i = 0, obj = objlist; obj; i++, obj = obj->next)
        {
            // skip player objects
            if (i < LWMP_NUM_PLAYERS)
            {
                continue;
            }
            for (LWMP_REPEAT_ONCE(c, TARGETPLAYER_INDEX(obj)))
            {
                DoActor (obj);
            }
        }

        LWMP_ThreeDRefresh();

        //
        // MAKE FUNNY FACE IF BJ DOESN'T MOVE FOR AWHILE
        //
        for (LWMP_REPEAT(c))
        {
            #ifdef SPEAR
            funnyticount_mp += tics;
            if (funnyticount_mp > 30l * 70)
            {
                funnyticount_mp = 0;
                if(viewsize != 21)
                {
                    sb(StatusDrawFace(BJWAITING1PIC + 
                        (US_RndT () & 1)));
                }
                facecount_mp = 0;
            }
            #endif

            PL_HurtAnim();
            PL_FallAnim();
            PL_Autoplay();
        }

        gamestate.TimeCount += tics;

        UpdateSoundLoc ();      // JAB
        if (screenfaded)
            VW_FadeIn ();

        CheckKeys ();

//
// debug aids
//
        if (singlestep)
        {
            VW_WaitVBL (singlestep);
            lasttimecount = GetTimeCount();
        }
        if (extravbls)
            VW_WaitVBL (extravbls);

        if (demoplayback)
        {
            if (IN_CheckAck ())
            {
                IN_ClearKeysDown ();
                playstate = ex_abort;
            }
        }
    }
    while (!playstate && !startgame);

    if (playstate != ex_died)
        FinishPaletteShifts ();
}

void PL_ToggleAutoplay(void)
{
    Autoplay_ToggleEnabled();
}

void PL_Autoplay(void)
{
    int x, y;
    int dir;
    int angs[4] = { 0, 90, 180, 270 };
    dirtype dirs[4] = { east, north, west, south };
    int dx4dir[4] = { 1, 0, -1, 0 };
    int dy4dir[4] = { 0, 1, 0, -1 };
    Autoplay_Pos_t autoplayPos;

    if (!Autoplay_UpdateRequired())
    {
        return;
    }

    Autoplay_Update();
    autoplayPos = Autoplay_GetPos();

    player_mp->tilex = autoplayPos.x;
    player_mp->tiley = 63 - autoplayPos.y;
    player_mp->x = TILE2POS(autoplayPos.x);
    player_mp->y = TILE2POS(63 - autoplayPos.y);
    player_mp->angle = angs[autoplayPos.dir];

    //Pow_PickUp(self->tilex, self->tiley);

    // push walls and doors
    for (dir = 0; dir < 4; dir++)
    {
        x = autoplayPos.x + dx4dir[dir];
        y = autoplayPos.y + dy4dir[dir];

        if (TileIsDoor(x, y) ||
            TileIsPushable(x, y) ||
            TileIsElevator(x, y))
        {
            player_mp->angle = angs[dir];
            Cmd_Use();
        }
    }
}


/*
===============
=
= FillBackpack
=
===============
*/

void PL_FillBackpack(Backpack_t *backpack)
{
    int i;
    weapontype wp;

    memset(backpack, 0, sizeof(Backpack_t));
    backpack->valid = true;
    backpack->tilex = player_mp->tilex;
    backpack->tiley = player_mp->tiley;
    backpack->ammo = gamestate.ammo_mp;
    backpack->keys = 0/*gamestate.keys_mp*/;
    for (i = 0; i < wp_max; i++)
    {
        wp = (weapontype)i;
        if (wp <= wp_pistol || gamestate.bestweapon_mp >= wp)
        {
            backpack->hasWeapon[i] = true;
        }
    }
}


/*
===============
=
= SpawnBackpack
=
===============
*/

bool SpawnBackpack(int tilex, int tiley, Backpack_t filledBackpack)
{
    int i;
    Backpack_t *backpack;

    for (i = 0; i < MAX_BACKPACKS; i++)
    {
        backpack = &gamestate.backpacks[i];
        if (!backpack->valid)
        {
            break;
        }
    }

    if (i < MAX_BACKPACKS)
    {
        PlaceItemType (bo_backpack, tilex, tiley);
        filledBackpack.tilex = tilex;
        filledBackpack.tiley = tiley;
        *backpack = filledBackpack;
        return true;
    }

    return false;
}
