/////////////////////////////////////////////////
//
// MUSE Header for .WL6
// Created Tue Jul 14 15:04:53 1992
//
/////////////////////////////////////////////////

//
// Sound names & indexes
//
typedef enum {
	HITWALLSND,              // 0
	SELECTWPNSND,            // 1
	SELECTITEMSND,           // 2
	HEARTBEATSND,            // 3
	MOVEGUN2SND,             // 4
	MOVEGUN1SND,             // 5
	NOWAYSND,                // 6
	NAZIHITPLAYERSND,        // 7
	SCHABBSTHROWSND,         // 8
	PLAYERDEATHSND,          // 9
	DOGDEATHSND,             // 10
	ATKGATLINGSND,           // 11
	GETKEYSND,               // 12
	NOITEMSND,               // 13
	WALK1SND,                // 14
	WALK2SND,                // 15
	TAKEDAMAGESND,           // 16
	GAMEOVERSND,             // 17
	OPENDOORSND,             // 18
	CLOSEDOORSND,            // 19
	DONOTHINGSND,            // 20
	HALTSND,                 // 21
	DEATHSCREAM2SND,         // 22
	ATKKNIFESND,             // 23
	ATKPISTOLSND,            // 24
	DEATHSCREAM3SND,         // 25
	ATKMACHINEGUNSND,        // 26
	HITENEMYSND,             // 27
	SHOOTDOORSND,            // 28
	DEATHSCREAM1SND,         // 29
	GETMACHINESND,           // 30
	GETAMMOSND,              // 31
	SHOOTSND,                // 32
	HEALTH1SND,              // 33
	HEALTH2SND,              // 34
	BONUS1SND,               // 35
	BONUS2SND,               // 36
	BONUS3SND,               // 37
	GETGATLINGSND,           // 38
	ESCPRESSEDSND,           // 39
	LEVELDONESND,            // 40
	DOGBARKSND,              // 41
	ENDBONUS1SND,            // 42
	ENDBONUS2SND,            // 43
	BONUS1UPSND,             // 44
	BONUS4SND,               // 45
	PUSHWALLSND,             // 46
	NOBONUSSND,              // 47
	PERCENT100SND,           // 48
	BOSSACTIVESND,           // 49
	MUTTISND,                // 50
	SCHUTZADSND,             // 51
	AHHHGSND,                // 52
	DIESND,                  // 53
	EVASND,                  // 54
	GUTENTAGSND,             // 55
	LEBENSND,                // 56
	SCHEISTSND,              // 57
	NAZIFIRESND,             // 58
	BOSSFIRESND,             // 59
	SSFIRESND,               // 60
	SLURPIESND,              // 61
	TOT_HUNDSND,             // 62
	MEINGOTTSND,             // 63
	SCHABBSHASND,            // 64
	HITLERHASND,             // 65
	SPIONSND,                // 66
	NEINSOVASSND,            // 67
	DOGATTACKSND,            // 68
	FLAMETHROWERSND,         // 69
	MECHSTEPSND,             // 70
	GOOBSSND,                // 71
	YEAHSND,                 // 72
#ifndef APOGEE_1_0
	DEATHSCREAM4SND,         // 73
	DEATHSCREAM5SND,         // 74
	DEATHSCREAM6SND,         // 75
	DEATHSCREAM7SND,         // 76
	DEATHSCREAM8SND,         // 77
	DEATHSCREAM9SND,         // 78
	DONNERSND,               // 79
	EINESND,                 // 80
	ERLAUBENSND,             // 81
	KEINSND,                 // 82
	MEINSND,                 // 83
	ROSESND,                 // 84
	MISSILEFIRESND,          // 85
	MISSILEHITSND,           // 86
#endif
	BJDIESND,                // 87
	GETBACKPACKSND,          // 88
	BJFALLSND,               // 89
	BJRESPAWNSND,            // 90
	WBARRELDEATHSND,         // 91
	TBARRELDEATHSND,         // 92
	EBARRELDEATHSND,         // 93
	TANKDEATHSND,            // 94
	ATKTANKGUNSND,           // 95
	TANKACCELSND,            // 96
	TANKDECELSND,            // 97
	TANKMOVINGSND,           // 98
	ATKMG42SND,              // 99
	MG42USESND,              // 100
	TANKUSEWHEELSND,         // 101
	TANKUSEGUNSND,           // 102
	TANKFASTACCELSND,        // 103
	TANKFASTDECELSND,        // 104
	TANKFASTMOVINGSND,       // 105
	GETHEARTSND,             // 106
	GETFLAGSND,              // 107
	DROPFLAGSND,             // 108
	ATKFLAGSND,              // 109
	HSKULLSPAWNSND,          // 110
	HSKULLCLEARSND,          // 111
	GIBEXPLODESND,           // 112
	ATKPUNCHHITSND,          // 113
	ATKPUNCHMISSEDSND,       // 114
	ATKPUNCHCHARGESND,       // 115
	ATKFLAGMISSEDSND,        // 116
	GETRAMPAGEAMMOSND,       // 117
	ZOMBIEATTACKSND,         // 118
	RTNFLAGSND,              // 119
	TANKIDLESND,             // 120
	TANKIGNITIONSND,         // 121
	TANKREVSND,              // 122
	LASTSOUND,

	EXTRASND_START = BJDIESND,
	OLDLASTSOUND = EXTRASND_START,

	HALTDIGISND = 0,
	DOGBARKDIGISND = 1,
	CLOSEDOORDIGISND = 2,
	OPENDOORDIGISND = 3,
	ATKMACHINEGUNDIGISND = 4,
	ATKPISTOLDIGISND = 5,
	ATKGATLINGDIGISND = 6,
	SCHUTZADDIGISND = 7,
	GUTENTAGDIGISND = 8,
	MUTTIDIGISND = 9,
	BOSSFIREDIGISND = 10,
	SSFIREDIGISND = 11,
	DEATHSCREAM1DIGISND = 12,
	DEATHSCREAM2DIGISND = 13,
	DEATHSCREAM3DIGISND = 13,
	TAKEDAMAGEDIGISND = 14,
	PUSHWALLDIGISND = 15,
	LEBENDIGISND = 20,
	NAZIFIREDIGISND = 21,
	SLURPIEDIGISND = 22,
	YEAHDIGISND = 32,
#ifndef UPLOAD
	DOGDEATHDIGISND = 16,
	AHHHGDIGISND = 17,
	DIEDIGISND = 18,
	EVADIGISND = 19,
	TOT_HUNDDIGISND = 23,
	MEINGOTTDIGISND = 24,
	SCHABBSHADIGISND = 25,
	HITLERHADIGISND = 26,
	SPIONDIGISND = 27,
	NEINSOVASDIGISND = 28,
	DOGATTACKDIGISND = 29,
	LEVELDONEDIGISND = 30,
	MECHSTEPDIGISND = 31,
	SCHEISTDIGISND = 33,
	DEATHSCREAM4DIGISND = 34,
	DEATHSCREAM5DIGISND = 35,
	DONNERDIGISND = 36,
	EINEDIGISND = 37,
	ERLAUBENDIGISND = 38,
	DEATHSCREAM6DIGISND = 39,
	DEATHSCREAM7DIGISND = 40,
	DEATHSCREAM8DIGISND = 41,
	DEATHSCREAM9DIGISND = 42,
	KEINDIGISND = 43,
	MEINDIGISND = 44,
	ROSEDIGISND = 45,
#endif
	EXTERNALDIGISND = 46,
	BJDIEDIGISND = EXTERNALDIGISND,
	GETBACKPACKDIGISND = 47,
	BJFALLDIGISND = 48,
	BJRESPAWNDIGISND = 49,
	WBARRELDEATHDIGISND = 50,
	TBARRELDEATHDIGISND = 51,
	EBARRELDEATHDIGISND = 52,
	TANKDEATHDIGISND = 53,
	ATKTANKGUNDIGISND = 54,
	TANKACCELDIGISND = 55,
	TANKDECELDIGISND = 56,
	TANKMOVINGDIGISND = 57,
	ATKMG42DIGISND = 58,
	MG42USEDIGISND = 59,
	TANKUSEWHEELDIGISND = 60,
	TANKUSEGUNDIGISND = 61,
	TANKFASTACCELDIGISND = 62,
	TANKFASTDECELDIGISND = 63,
	TANKFASTMOVINGDIGISND = 64,
	GETHEARTDIGISND = 65,
	GETFLAGDIGISND = 66,
	DROPFLAGDIGISND = 67,
	ATKFLAGDIGISND = 68,
	HSKULLSPAWNDIGISND = 69,
	HSKULLCLEARDIGISND = 70,
	GIBEXPLODEDIGISND = 71,
	ATKPUNCHHITDIGISND = 72,
	ATKPUNCHMISSEDDIGISND = 73,
	ATKPUNCHCHARGEDIGISND = 74,
	ATKFLAGMISSEDDIGISND = 75,
	GETRAMPAGEAMMODIGISND = 76,
	ZOMBIEATTACKDIGISND = 77,
	RTNFLAGDIGISND = 78,
	TANKIDLEDIGISND = 79,
	TANKIGNITIONDIGISND = 80,
	TANKREVDIGISND = 81,
	LASTDIGISOUND = 82,
} soundnames;

static const char *soundnames_str[LASTSOUND] =
{
	"HITWALLSND",            // 0
	"SELECTWPNSND",          // 1
	"SELECTITEMSND",         // 2
	"HEARTBEATSND",          // 3
	"MOVEGUN2SND",           // 4
	"MOVEGUN1SND",           // 5
	"NOWAYSND",              // 6
	"NAZIHITPLAYERSND",      // 7
	"SCHABBSTHROWSND",       // 8
	"PLAYERDEATHSND",        // 9
	"DOGDEATHSND",           // 10
	"ATKGATLINGSND",         // 11
	"GETKEYSND",             // 12
	"NOITEMSND",             // 13
	"WALK1SND",              // 14
	"WALK2SND",              // 15
	"TAKEDAMAGESND",         // 16
	"GAMEOVERSND",           // 17
	"OPENDOORSND",           // 18
	"CLOSEDOORSND",          // 19
	"DONOTHINGSND",          // 20
	"HALTSND",               // 21
	"DEATHSCREAM2SND",       // 22
	"ATKKNIFESND",           // 23
	"ATKPISTOLSND",          // 24
	"DEATHSCREAM3SND",       // 25
	"ATKMACHINEGUNSND",      // 26
	"HITENEMYSND",           // 27
	"SHOOTDOORSND",          // 28
	"DEATHSCREAM1SND",       // 29
	"GETMACHINESND",         // 30
	"GETAMMOSND",            // 31
	"SHOOTSND",              // 32
	"HEALTH1SND",            // 33
	"HEALTH2SND",            // 34
	"BONUS1SND",             // 35
	"BONUS2SND",             // 36
	"BONUS3SND",             // 37
	"GETGATLINGSND",         // 38
	"ESCPRESSEDSND",         // 39
	"LEVELDONESND",          // 40
	"DOGBARKSND",            // 41
	"ENDBONUS1SND",          // 42
	"ENDBONUS2SND",          // 43
	"BONUS1UPSND",           // 44
	"BONUS4SND",             // 45
	"PUSHWALLSND",           // 46
	"NOBONUSSND",            // 47
	"PERCENT100SND",         // 48
	"BOSSACTIVESND",         // 49
	"MUTTISND",              // 50
	"SCHUTZADSND",           // 51
	"AHHHGSND",              // 52
	"DIESND",                // 53
	"EVASND",                // 54
	"GUTENTAGSND",           // 55
	"LEBENSND",              // 56
	"SCHEISTSND",            // 57
	"NAZIFIRESND",           // 58
	"BOSSFIRESND",           // 59
	"SSFIRESND",             // 60
	"SLURPIESND",            // 61
	"TOT_HUNDSND",           // 62
	"MEINGOTTSND",           // 63
	"SCHABBSHASND",          // 64
	"HITLERHASND",           // 65
	"SPIONSND",              // 66
	"NEINSOVASSND",          // 67
	"DOGATTACKSND",          // 68
	"FLAMETHROWERSND",       // 69
	"MECHSTEPSND",           // 70
	"GOOBSSND",              // 71
	"YEAHSND",               // 72
#ifndef APOGEE_1_0
	"DEATHSCREAM4SND",       // 73
	"DEATHSCREAM5SND",       // 74
	"DEATHSCREAM6SND",       // 75
	"DEATHSCREAM7SND",       // 76
	"DEATHSCREAM8SND",       // 77
	"DEATHSCREAM9SND",       // 78
	"DONNERSND",             // 79
	"EINESND",               // 80
	"ERLAUBENSND",           // 81
	"KEINSND",               // 82
	"MEINSND",               // 83
	"ROSESND",               // 84
	"MISSILEFIRESND",        // 85
	"MISSILEHITSND",         // 86
#endif
	"BJDIESND",              // 87
	"GETBACKPACKSND",        // 88
	"BJFALLSND",             // 89
	"BJRESPAWNSND",          // 90
	"WBARRELDEATHSND",       // 91
	"TBARRELDEATHSND",       // 92
	"EBARRELDEATHSND",       // 93
	"TANKDEATHSND",          // 94
	"ATKTANKGUNSND",         // 95
	"TANKACCELSND",          // 96
	"TANKDECELSND",          // 97
	"TANKMOVINGSND",         // 98
	"ATKMG42SND",            // 99
	"MG42USESND",            // 100
	"TANKUSEWHEELSND",       // 101
	"TANKUSEGUNSND",         // 102
	"TANKFASTACCELSND",      // 103
	"TANKFASTDECELSND",      // 104
	"TANKFASTMOVINGSND",     // 105
	"GETHEARTSND",           // 106
	"GETFLAGSND",            // 107
	"DROPFLAGSND",           // 108
	"ATKFLAGSND",            // 109
	"HSKULLSPAWNSND",        // 110
	"HSKULLCLEARSND",        // 111
	"GIBEXPLODESND",         // 112
	"ATKPUNCHHITSND",        // 113
	"ATKPUNCHMISSEDSND",     // 114
	"ATKPUNCHCHARGESND",     // 115
	"ATKFLAGMISSEDSND",      // 116
	"GETRAMPAGEAMMOSND",     // 117
	"ZOMBIEATTACKSND",       // 118
	"RTNFLAGSND",            // 119
	"TANKIDLESND",           // 120
	"TANKIGNITIONSND",       // 121
	"TANKREVSND",            // 122
};

//
// Base offsets
//
#define STARTPCSOUNDS		0
#define STARTADLIBSOUNDS	OLDLASTSOUND
#define STARTDIGISOUNDS		(2*OLDLASTSOUND)
#define STARTMUSIC		    (3*OLDLASTSOUND)

//
// Music names & indexes
//
typedef enum {
	CORNER_MUS,              // 0
	DUNGEON_MUS,             // 1
	WARMARCH_MUS,            // 2
	GETTHEM_MUS,             // 3
	HEADACHE_MUS,            // 4
	HITLWLTZ_MUS,            // 5
	INTROCW3_MUS,            // 6
	NAZI_NOR_MUS,            // 7
	NAZI_OMI_MUS,            // 8
	POW_MUS,                 // 9
	SALUTE_MUS,              // 10
	SEARCHN_MUS,             // 11
	SUSPENSE_MUS,            // 12
	VICTORS_MUS,             // 13
	WONDERIN_MUS,            // 14
	FUNKYOU_MUS,             // 15
	ENDLEVEL_MUS,            // 16
	GOINGAFT_MUS,            // 17
	PREGNANT_MUS,            // 18
	ULTIMATE_MUS,            // 19
	NAZI_RAP_MUS,            // 20
	ZEROHOUR_MUS,            // 21
	TWELFTH_MUS,             // 22
	ROSTER_MUS,              // 23
	URAHERO_MUS,             // 24
	VICMARCH_MUS,            // 25
	PACMAN_MUS,              // 26
	LASTMUSIC
} musicnames;

#define NUMSOUNDS		    EXTRASND_START
#define NUMSNDCHUNKS		(STARTMUSIC + LASTMUSIC)

/////////////////////////////////////////////////
//
// Thanks for playing with MUSE!
//
/////////////////////////////////////////////////
