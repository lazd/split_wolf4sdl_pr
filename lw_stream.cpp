#include "lw_stream.h"
#include <algorithm>
#include <sstream>
using namespace lwlib;

Stream::Stream(const char *fileName, StreamDirection::e dir_, 
	StreamFormat::e format_) :
	fp(NULL), dir(dir_), format(format_), totalBytes(0)
{
	std::string mode;

	if (dir == StreamDirection::out)
	{
		mode += "w";
	}
	else if (dir == StreamDirection::in)
	{
		mode += "r";
	}
	if (format == StreamFormat::binary)
	{
		mode += "b";
	}

	fp = fopen(fileName, mode.c_str());
	if (fp != NULL)
	{
		ownsFileHandle = true;
	}
}

Stream::Stream(FILE *fp_, StreamDirection::e dir_, 
	StreamFormat::e format_) :
	fp(fp_), ownsFileHandle(false), dir(dir_), format(format_),
	totalBytes(0)
{
}

Stream::~Stream()
{
	if (ownsFileHandle && fp != NULL)
	{
		fclose(fp);
	}
}

std::string Stream::serializeTag(std::string name, 
	StreamTagBitMask tagBits)
{
	int ch;

	if (format == StreamFormat::binary)
	{
		return name;
	}

	if (dir == StreamDirection::out)
	{
		fprintf(
			fp, "%s<%s%s>%s", 
			(tagBits & StreamTagBits::addPadding) ? pad.c_str() : "", 
			(tagBits & StreamTagBits::isEnd) ? "/" : "",
			name.c_str(),
			(tagBits & StreamTagBits::addNewLine) ? "\n" : ""
			);
	}
	else
	{
		name.clear();

		ch = fgetc(fp);
		while (ch != EOF && isspace(ch))
		{
			ch = fgetc(fp);
		}
		if (ch != EOF && ch == '<')
		{
			ch = fgetc(fp);
			while (ch != EOF && ch != '>')
			{
				if (ch != '/')
				{
					name.push_back(ch);
				}
				ch = fgetc(fp);
			}
		}
	}

	return name;
}

std::string Stream::serializeStartTag(const std::string &name)
{
    StreamTagBitMask bitMask = 0;
    bitMask |= StreamTagBits::addPadding;
    return serializeTag(name, bitMask);
}

std::string Stream::serializeEndTag(const std::string &name)
{
    StreamTagBitMask bitMask = 0;
    bitMask |= StreamTagBits::addNewLine;
    bitMask |= StreamTagBits::isEnd;
    return serializeTag(name, bitMask);
}

bool Stream::isIn(void) const
{
	return dir == StreamDirection::in;
}

bool Stream::isOut(void) const
{
	return dir == StreamDirection::out;
}

namespace lwlib
{
	namespace StreamStartTag
	{
		Info::Info(const std::string &startTag)
		{
			std::stringstream ss(startTag);
			for (int i = 0; i < maxArgs; i++)
			{
				ss >> w[i];
			}
		}

		bool Info::findArg(const std::string &arg) const
		{
			return std::find(&w[0], &w[maxArgs], arg) != w + maxArgs;
		}

		bool Info::needsStore(void) const
		{
			return copy() || store();
		}

		bool Info::copy(void) const
		{
			return findArg("copy");
		}

		bool Info::noread(void) const
		{
			return findArg("noread");
		}

		bool Info::store(void) const
		{
			return findArg("store");
		}

		const std::string Info::getTag(void) const
		{
			return w[0];
		}

		const std::string Info::getStoreName(void) const
		{
			return w[2];
		}
	}

	void serialize(Stream &stream, int &x)
	{
		stream.serialize(x, "%d");
	}

	void serialize(Stream &stream, unsigned int &x)
	{
		stream.serialize(x, "%u");
	}

	void serialize(Stream &stream, short int &x)
	{
		stream.serialize(x, "%hd");
	}

	void serialize(Stream &stream, unsigned long &x)
	{
		stream.serialize(x, "%lu");
	}

	void serialize(Stream &stream, double &x)
	{
		stream.serialize(x, "%lf");
	}

	void serialize(Stream &stream, std::string &x)
	{
		int i, len, ch;

		if (stream.format == StreamFormat::binary)
		{
			if (stream.dir == StreamDirection::out)
			{
				len = (int)x.size();
				fwrite(&len, sizeof(len), 1, stream.fp);
				fwrite(x.c_str(), len, 1, stream.fp);
			}
			else
			{
				fread(&len, sizeof(len), 1, stream.fp);

				x.clear();
				for (i = 0; i < len; i++)
				{
					ch = fgetc(stream.fp);
					x.push_back((char)ch);
				}
			}
		}
		else if (stream.format == StreamFormat::text)
		{
			if (stream.dir == StreamDirection::out)
			{
				fprintf(stream.fp, "%s", x.c_str());
			}
			else
			{
				x.clear();
				ch = fgetc(stream.fp);
				while (ch != EOF && (char)ch != '<')
				{
					x.push_back((char)ch);
					ch = fgetc(stream.fp);
				}
				if (ch == '<')
				{
					ungetc(ch, stream.fp);
				}
			}
		}
	}

	void serialize(Stream &stream, bool &x)
	{
		if (stream.format == StreamFormat::binary)
		{
			stream.serialize(x);
		}
		else if (stream.format == StreamFormat::text)
		{
			std::string s = x ? "true" : "false";
			serialize(stream, s);
			x = (strstr(s.c_str(), "true") != NULL ? true : false);
		}
	}

	void serialize(Stream &stream, unsigned char &x)
	{
		stream.serialize(x, "%hhd");
	}

	namespace StreamFormat
	{
		Permitted::Permitted() : len(0)
		{
		}

		Permitted::Permitted(enum e x) : len(1)
		{
			vals[0] = x;
		}

		Permitted::Permitted(enum e x, enum e y) : len(2)
		{
			vals[0] = x;
			vals[1] = y;
		}

		void Permitted::add(enum e x)
		{
			if (len >= 2)
			{
				throw "cannot insert format";
			}

			vals[len++] = x;
		}
		
		bool Permitted::enabled(enum e x) const
		{
			return 
				(
					(len == 2) ?
						(vals[0] == x || vals[1] == x) :
						(
							(len == 1) ?
								(vals[0] == x) :
								false
						)
				);
		}

		const Permitted allPermitted = Permitted(binary, text);
		const Permitted binaryPermitted = Permitted(binary);
		const Permitted textPermitted = Permitted(text);
	}
}
