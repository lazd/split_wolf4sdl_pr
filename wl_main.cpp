// WL_MAIN.C

#ifdef _WIN32
    #include <io.h>
#else
    #include <unistd.h>
#endif

#include "wl_def.h"
#pragma hdrstop
#include "wl_atmos.h"
#include <SDL2/SDL_syswm.h>


/*
=============================================================================

                             WOLFENSTEIN 3-D

                        An Id Software production

                             by John Carmack

=============================================================================
*/

extern byte signon[];

/*
=============================================================================

                             LOCAL CONSTANTS

=============================================================================
*/


#define VIEWGLOBAL      0x10000                 // globals visable flush to wall

#define VIEWWIDTH       256                     // size of view window
#define VIEWHEIGHT      144

/*
=============================================================================

                            GLOBAL VARIABLES

=============================================================================
*/

char    str[80];
int     dirangle[9] = {0,ANGLES/8,2*ANGLES/8,3*ANGLES/8,4*ANGLES/8,
                       5*ANGLES/8,6*ANGLES/8,7*ANGLES/8,ANGLES};

//
// proejection variables
//
fixed    focallength;
unsigned LWMP_DECL(screenofs);
int      LWMP_DECL(viewscreenx), LWMP_DECL(viewscreeny);
int      LWMP_DECL(viewwidth);
int      LWMP_DECL(viewheight);
short    LWMP_DECL(centerx);
int      LWMP_DECL(shootdelta);           // pixels away from centerx a target can be
fixed    LWMP_DECL(viewscale);
int32_t  LWMP_DECL(heightnumerator);
int      LWMP_DECL(joyadjustment);

void    Quit (const char *error,...);

boolean startgame;
boolean loadedgame;
int     mouseadjustment;

char    configdir[256] = "";
char    configname[13] = "config-sw.";
char    sounddir[256] = "";

//
// Command line parameter variables
//
boolean param_debugmode = false;
boolean param_nowait = false;
int     param_difficulty = 1;           // default is "normal"
int     param_tedlevel = -1;            // default is not to start a level
int     param_joystickindex = 0;

#if defined(_arch_dreamcast)
int     param_samplerate = 11025;       // higher samplerates result in "out of memory"
int     param_audiobuffer = 4096 / (44100 / param_samplerate);
#elif defined(GP2X_940)
int     param_samplerate = 11025;       // higher samplerates result in "out of memory"
int     param_audiobuffer = 128;
#else
int     param_samplerate = 44100;
int     param_audiobuffer = 2048 / (44100 / param_samplerate);
#endif

int     param_mission = 0;
boolean param_goodtimes = false;
boolean param_ignorenumchunks = false;
boolean param_nosound = false;
GameMode::e param_defaultgamemode = GameMode::classic;

/*
=============================================================================

                            LOCAL VARIABLES

=============================================================================
*/


/*
====================
=
= ReadConfig
=
====================
*/

void ReadConfig(void)
{
    SDMode  sd;
    SMMode  sm;
    SDSMode sds;
    int c;

    char configpath[300];

#ifdef _arch_dreamcast
    DC_LoadFromVMU(configname);
#endif

    if(configdir[0])
        snprintf(configpath, sizeof(configpath), "%s/%s", configdir, configname);
    else
        strcpy(configpath, configname);

    const int file = open(configpath, O_RDONLY | O_BINARY);
    if (file != -1)
    {
        //
        // valid config file
        //
        word tmp;
        read(file,&tmp,sizeof(tmp));
        if(tmp!=0xfefa)
        {
            close(file);
            goto noconfig;
        }
        read(file,Scores,sizeof(HighScore) * MaxScores);

        read(file,&sd,sizeof(sd));
        read(file,&sm,sizeof(sm));
        read(file,&sds,sizeof(sds));

        read(file,&mouseenabled,sizeof(mouseenabled));
        read(file,&joystickenabled,sizeof(joystickenabled));
        boolean dummyJoypadEnabled;
        read(file,&dummyJoypadEnabled,sizeof(dummyJoypadEnabled));
        boolean dummyJoystickProgressive;
        read(file,&dummyJoystickProgressive,sizeof(dummyJoystickProgressive));
        int dummyJoystickPort = 0;
        read(file,&dummyJoystickPort,sizeof(dummyJoystickPort));

        for (LWMP_REPEAT_MAX(c))
        {
            read(file,dirscan_mp,sizeof(dirscan_mp));
            read(file,buttonscan_mp,sizeof(buttonscan_mp));
            read(file,buttonjoy_mp,sizeof(buttonjoy_mp));
        }
        read(file,buttonmouse,sizeof(buttonmouse));

        read(file,&viewsize,sizeof(viewsize));
        read(file,&mouseadjustment,sizeof(mouseadjustment));
        read(file,GameMode::Lock::state,sizeof(GameMode::Lock::state));
        read(file,&movewithmouse,sizeof(movewithmouse));
        for (LWMP_REPEAT_MAX(c))
        {
            read(file,&joyadjustment_mp,sizeof(joyadjustment_mp));
        }

        close(file);

        if ((sd == sdm_AdLib || sm == smm_AdLib) && !AdLibPresent
                && !SoundBlasterPresent)
        {
            sd = sdm_PC;
            sm = smm_Off;
        }

        if ((sds == sds_SoundBlaster && !SoundBlasterPresent))
            sds = sds_Off;

        // make sure values are correct

        if(mouseenabled) mouseenabled=true;
        if(movewithmouse) movewithmouse=true;
        if(joystickenabled) joystickenabled=true;

        if (!MousePresent)
        {
            mouseenabled = false;
            movewithmouse = false;
        }
        if (!IN_JoyPresent())
            joystickenabled = false;

        if(mouseadjustment<0) mouseadjustment=0;
        else if(mouseadjustment>9) mouseadjustment=9;

        for (LWMP_REPEAT_MAX(c))
        {
            if(joyadjustment_mp<0) joyadjustment_mp=0;
            else if(joyadjustment_mp>9) joyadjustment_mp=9;
        }

        if(viewsize<4) viewsize=4;
        else if(viewsize>21) viewsize=21;

        MainMenu[6].active=1;
        MainItems.curpos=0;
    }
    else
    {
        //
        // no config file, so select by hardware
        //
noconfig:
        if (SoundBlasterPresent || AdLibPresent)
        {
            sd = sdm_AdLib;
            sm = smm_AdLib;
        }
        else
        {
            sd = sdm_PC;
            sm = smm_Off;
        }

        if (SoundBlasterPresent)
            sds = sds_SoundBlaster;
        else
            sds = sds_Off;

        if (MousePresent)
            mouseenabled = true;

        if (IN_JoyPresent())
            joystickenabled = true;

        viewsize = 19;                          // start with a good size
        mouseadjustment=5;

        for (LWMP_REPEAT_MAX(c))
        {
            joyadjustment_mp = 8;
        }
    }

    SD_SetMusicMode (sm);
    SD_SetSoundMode (sd);
    SD_SetDigiDevice (sds);
}

/*
====================
=
= WriteConfig
=
====================
*/

void WriteConfig(void)
{
    char configpath[300];
    int c;

#ifdef _arch_dreamcast
    fs_unlink(configname);
#endif

    if(configdir[0])
        snprintf(configpath, sizeof(configpath), "%s/%s", configdir, configname);
    else
        strcpy(configpath, configname);

    const int file = open(configpath, O_CREAT | O_WRONLY | O_BINARY, 0644);
    if (file != -1)
    {
        word tmp=0xfefa;
        write(file,&tmp,sizeof(tmp));
        write(file,Scores,sizeof(HighScore) * MaxScores);

        write(file,&SoundMode,sizeof(SoundMode));
        write(file,&MusicMode,sizeof(MusicMode));
        write(file,&DigiMode,sizeof(DigiMode));

        write(file,&mouseenabled,sizeof(mouseenabled));
        write(file,&joystickenabled,sizeof(joystickenabled));
        boolean dummyJoypadEnabled = false;
        write(file,&dummyJoypadEnabled,sizeof(dummyJoypadEnabled));
        boolean dummyJoystickProgressive = false;
        write(file,&dummyJoystickProgressive,sizeof(dummyJoystickProgressive));
        int dummyJoystickPort = 0;
        write(file,&dummyJoystickPort,sizeof(dummyJoystickPort));

        for (LWMP_REPEAT_MAX(c))
        {
            write(file,dirscan_mp,sizeof(dirscan_mp));
            write(file,buttonscan_mp,sizeof(buttonscan_mp));
            write(file,buttonjoy_mp,sizeof(buttonjoy_mp));
        }
        write(file,buttonmouse,sizeof(buttonmouse));

        write(file,&viewsize,sizeof(viewsize));
        write(file,&mouseadjustment,sizeof(mouseadjustment));
        //GameMode::Lock::setAll();
        write(file,GameMode::Lock::state,sizeof(GameMode::Lock::state));
        write(file,&movewithmouse,sizeof(movewithmouse));
        for (LWMP_REPEAT_MAX(c))
        {
            write(file,&joyadjustment_mp,sizeof(joyadjustment_mp));
        }

        close(file);
    }
#ifdef _arch_dreamcast
    DC_SaveToVMU(configname, NULL);
#endif
}


//===========================================================================

/*
=====================
=
= NewGame
=
= Set up new game to start from the beginning
=
=====================
*/

void NewGame (int difficulty,int episode,GameMode::e mode)
{
    int i;
    int c;
    memset (&gamestate,0,sizeof(gamestate));
    gamestate.difficulty = difficulty;
    gamestate.mode = mode;
    for (LWMP_REPEAT(c))
    {
        gamestate.weapon_mp = gamestate.bestweapon_mp
                = gamestate.chosenweapon_mp = wp_pistol;
        gamestate.health_mp = 100;
        int ammo = STARTAMMO;
        ZombieMode::scaleAmmo(ammo);
        gamestate.ammo_mp = ammo;
        if (AllWeaponsMode::enabled())
        {
            gamestate.weapon_mp = gamestate.bestweapon_mp
                    = gamestate.chosenweapon_mp = wp_chaingun;
        }
        if (InfiniteAmmoMode::enabled())
        {
            gamestate.ammo_mp = 99;
        }
        gamestate.lives_mp = 3;
        if (player_mp != NULL)
        {
            player_mp->gibbed = false;
        }

        InitPlayerColor();
    }
    gamestate.nextextra = EXTRAPOINTS;
    gamestate.episode=episode;

    startgame = true;
}

//===========================================================================

void DiskFlopAnim(int x,int y)
{
    static int8_t which=0;
    if (!x && !y)
        return;
    VWB_DrawPic(x,y,C_DISKLOADING1PIC+which);
    VW_UpdateScreen();
    which^=1;
}


int32_t DoChecksum(byte *source,unsigned size,int32_t checksum)
{
    unsigned i;

    for (i=0;i<size-1;i++)
    checksum += source[i]^source[i+1];

    return checksum;
}


/*
==================
=
= SaveTheGame
=
==================
*/

extern statetype s_grdstand;
extern statetype s_player;

boolean SaveTheGame(FILE *file,int x,int y)
{
//    struct diskfree_t dfree;
//    int32_t avail,size,checksum;
    int checksum;
    objtype *ob;
    objtype nullobj;
    statobj_t nullstat;
    int i;
    int c;

/*    if (_dos_getdiskfree(0,&dfree))
        Quit("Error in _dos_getdiskfree call");

    avail = (int32_t)dfree.avail_clusters *
                  dfree.bytes_per_sector *
                  dfree.sectors_per_cluster;

    size = 0;
    for (ob = player_mp; ob ; ob=ob->next)
        size += sizeof(*ob);
    size += sizeof(nullobj);

    size += sizeof(gamestate) +
            sizeof(LRstruct)*LRpack +
            sizeof(tilemap) +
            sizeof(actorat) +
            sizeof(laststatobj) +
            sizeof(statobjlist) +
            sizeof(doorposition) +
            sizeof(pwallstate) +
            sizeof(pwalltile) +
            sizeof(pwallx) +
            sizeof(pwally) +
            sizeof(pwalldir) +
            sizeof(pwallpos);

    if (avail < size)
    {
        Message(STR_NOSPACE1"\n"STR_NOSPACE2);
        return false;
    }*/

    checksum = 0;

    DiskFlopAnim(x,y);
    checksum = LWMP_SaveTheGame(file, checksum);

    DiskFlopAnim(x,y);
    fwrite(&gamestate,sizeof(gamestate),1,file);
    checksum = DoChecksum((byte *)&gamestate,sizeof(gamestate),checksum);

    DiskFlopAnim(x,y);
    fwrite(&LevelRatios[0],sizeof(LRstruct)*LRpack,1,file);
    checksum = DoChecksum((byte *)&LevelRatios[0],sizeof(LRstruct)*LRpack,checksum);

    DiskFlopAnim(x,y);
    fwrite(tilemap,sizeof(tilemap),1,file);
    checksum = DoChecksum((byte *)tilemap,sizeof(tilemap),checksum);
    DiskFlopAnim(x,y);

    for(i=0;i<MAPSIZE;i++)
    {
        for(int j=0;j<MAPSIZE;j++)
        {
            word actnum;
            objtype *objptr=actorat[i][j];
            if(ISPOINTER(objptr))
                actnum=0x8000 | (word)(objptr-objlist);
            else
                actnum=(word)(uintptr_t)objptr;
            fwrite(&actnum,sizeof(actnum),1,file);
            checksum = DoChecksum((byte *)&actnum,sizeof(actnum),checksum);
        }
    }

    fwrite (areaconnect,sizeof(areaconnect),1,file);
    for (LWMP_REPEAT(c))
    {
        fwrite (areabyplayer_mp,sizeof(areabyplayer_mp),1,file);
    }

    // player object needs special treatment as it's in WL_AGENT.CPP and not in
    // WL_ACT2.CPP which could cause problems for the relative addressing

    DiskFlopAnim(x,y);
    for (LWMP_REPEAT(c))
    {
        ob = player_mp;
        memcpy(&nullobj,ob,sizeof(nullobj));
        nullobj.state=(statetype *) ((uintptr_t)nullobj.state-(uintptr_t)&s_player);
        fwrite(&nullobj,sizeof(nullobj),1,file);
    }
    ob = ob->next;

    DiskFlopAnim(x,y);
    for (i = 0, ob = objlist; ob ; i++, ob=ob->next)
    {
        // skip player objects
        if (i < LWMP_NUM_PLAYERS)
        {
            continue;
        }
        memcpy(&nullobj,ob,sizeof(nullobj));
        nullobj.state=(statetype *) ((uintptr_t)nullobj.state-(uintptr_t)&s_grdstand);
        fwrite(&nullobj,sizeof(nullobj),1,file);
    }
    nullobj.active = ac_badobject;          // end of file marker
    DiskFlopAnim(x,y);
    fwrite(&nullobj,sizeof(nullobj),1,file);

    DiskFlopAnim(x,y);
    word laststatobjnum=(word) (laststatobj-statobjlist);
    fwrite(&laststatobjnum,sizeof(laststatobjnum),1,file);
    checksum = DoChecksum((byte *)&laststatobjnum,sizeof(laststatobjnum),checksum);

    DiskFlopAnim(x,y);
    for(i=0;i<MAXSTATS;i++)
    {
        memcpy(&nullstat,statobjlist+i,sizeof(nullstat));
        for (LWMP_REPEAT(c))
        {
            nullstat.visspot_mp=(byte *) ((uintptr_t) nullstat.visspot_mp-(uintptr_t)spotvis_mp);
        }
        fwrite(&nullstat,sizeof(nullstat),1,file);
        checksum = DoChecksum((byte *)&nullstat,sizeof(nullstat),checksum);
    }

    DiskFlopAnim(x,y);
    fwrite (doorposition,sizeof(doorposition),1,file);
    checksum = DoChecksum((byte *)doorposition,sizeof(doorposition),checksum);
    DiskFlopAnim(x,y);
    fwrite (doorobjlist,sizeof(doorobjlist),1,file);
    checksum = DoChecksum((byte *)doorobjlist,sizeof(doorobjlist),checksum);

    DiskFlopAnim(x,y);
    fwrite (&pwallstate,sizeof(pwallstate),1,file);
    checksum = DoChecksum((byte *)&pwallstate,sizeof(pwallstate),checksum);
    fwrite (&pwalltile,sizeof(pwalltile),1,file);
    checksum = DoChecksum((byte *)&pwalltile,sizeof(pwalltile),checksum);
    fwrite (&pwallx,sizeof(pwallx),1,file);
    checksum = DoChecksum((byte *)&pwallx,sizeof(pwallx),checksum);
    fwrite (&pwally,sizeof(pwally),1,file);
    checksum = DoChecksum((byte *)&pwally,sizeof(pwally),checksum);
    fwrite (&pwalldir,sizeof(pwalldir),1,file);
    checksum = DoChecksum((byte *)&pwalldir,sizeof(pwalldir),checksum);
    fwrite (&pwallpos,sizeof(pwallpos),1,file);
    checksum = DoChecksum((byte *)&pwallpos,sizeof(pwallpos),checksum);

    //
    // WRITE OUT CHECKSUM
    //
    fwrite (&checksum,sizeof(checksum),1,file);

    fwrite (&lastgamemusicoffset,sizeof(lastgamemusicoffset),1,file);

    return(true);
}

//===========================================================================

/*
==================
=
= LoadTheGame
=
==================
*/

boolean LoadTheGame(FILE *file,int x,int y)
{
    int32_t checksum,oldchecksum;
    objtype nullobj;
    statobj_t nullstat;
    int c;

    checksum = 0;

    DiskFlopAnim(x,y);
    checksum = LWMP_LoadTheGame(file, checksum);

    DiskFlopAnim(x,y);
    fread (&gamestate,sizeof(gamestate),1,file);
    checksum = DoChecksum((byte *)&gamestate,sizeof(gamestate),checksum);

    DiskFlopAnim(x,y);
    fread (&LevelRatios[0],sizeof(LRstruct)*LRpack,1,file);
    checksum = DoChecksum((byte *)&LevelRatios[0],sizeof(LRstruct)*LRpack,checksum);

    DiskFlopAnim(x,y);
    SetupGameLevel ();

    DiskFlopAnim(x,y);
    fread (tilemap,sizeof(tilemap),1,file);
    checksum = DoChecksum((byte *)tilemap,sizeof(tilemap),checksum);

    DiskFlopAnim(x,y);

    int actnum=0, i;
    for(i=0;i<MAPSIZE;i++)
    {
        for(int j=0;j<MAPSIZE;j++)
        {
            fread (&actnum,sizeof(word),1,file);
            checksum = DoChecksum((byte *) &actnum,sizeof(word),checksum);
            if(actnum&0x8000)
                actorat[i][j]=objlist+(actnum&0x7fff);
            else
                actorat[i][j]=(objtype *)(uintptr_t) actnum;
        }
    }

    fread (areaconnect,sizeof(areaconnect),1,file);
    for (LWMP_REPEAT(c))
    {
        fread (areabyplayer_mp,sizeof(areabyplayer_mp),1,file);
    }

    InitActorList ();
    DiskFlopAnim(x,y);
    for (LWMP_REPEAT(c))
    {
        fread (player_mp,sizeof(*player_mp),1,file);
        player_mp->state=(statetype *) ((uintptr_t)player_mp->state+(uintptr_t)&s_player);
    }

    while (1)
    {
        DiskFlopAnim(x,y);
        fread (&nullobj,sizeof(nullobj),1,file);
        if (nullobj.active == ac_badobject)
            break;
        GetNewActor ();
        nullobj.state=(statetype *) ((uintptr_t)nullobj.state+(uintptr_t)&s_grdstand);
        // don't copy over the links
        memcpy (newobj,&nullobj,sizeof(nullobj)-8);
    }

    DiskFlopAnim(x,y);
    word laststatobjnum;
    fread (&laststatobjnum,sizeof(laststatobjnum),1,file);
    laststatobj=statobjlist+laststatobjnum;
    checksum = DoChecksum((byte *)&laststatobjnum,sizeof(laststatobjnum),checksum);

    DiskFlopAnim(x,y);
    for(i=0;i<MAXSTATS;i++)
    {
        fread(&nullstat,sizeof(nullstat),1,file);
        checksum = DoChecksum((byte *)&nullstat,sizeof(nullstat),checksum);
        for (LWMP_REPEAT(c))
        {
            nullstat.visspot_mp=(byte *) ((uintptr_t)nullstat.visspot_mp+(uintptr_t)spotvis_mp);
        }
        memcpy(statobjlist+i,&nullstat,sizeof(nullstat));
    }

    DiskFlopAnim(x,y);
    fread (doorposition,sizeof(doorposition),1,file);
    checksum = DoChecksum((byte *)doorposition,sizeof(doorposition),checksum);
    DiskFlopAnim(x,y);
    fread (doorobjlist,sizeof(doorobjlist),1,file);
    checksum = DoChecksum((byte *)doorobjlist,sizeof(doorobjlist),checksum);

    DiskFlopAnim(x,y);
    fread (&pwallstate,sizeof(pwallstate),1,file);
    checksum = DoChecksum((byte *)&pwallstate,sizeof(pwallstate),checksum);
    fread (&pwalltile,sizeof(pwalltile),1,file);
    checksum = DoChecksum((byte *)&pwalltile,sizeof(pwalltile),checksum);
    fread (&pwallx,sizeof(pwallx),1,file);
    checksum = DoChecksum((byte *)&pwallx,sizeof(pwallx),checksum);
    fread (&pwally,sizeof(pwally),1,file);
    checksum = DoChecksum((byte *)&pwally,sizeof(pwally),checksum);
    fread (&pwalldir,sizeof(pwalldir),1,file);
    checksum = DoChecksum((byte *)&pwalldir,sizeof(pwalldir),checksum);
    fread (&pwallpos,sizeof(pwallpos),1,file);
    checksum = DoChecksum((byte *)&pwallpos,sizeof(pwallpos),checksum);

    if (gamestate.secretcount)      // assign valid floorcodes under moved pushwalls
    {
        word *map, *obj; word tile, sprite;
        map = mapsegs[0]; obj = mapsegs[1];
        for (y=0;y<mapheight;y++)
            for (x=0;x<mapwidth;x++)
            {
                tile = *map++; sprite = *obj++;
                if (sprite == PUSHABLETILE && !tilemap[x][y]
                    && (tile < AREATILE || tile >= (AREATILE+NUMMAPS)))
                {
                    if (*map >= AREATILE)
                        tile = *map;
                    if (*(map-1-mapwidth) >= AREATILE)
                        tile = *(map-1-mapwidth);
                    if (*(map-1+mapwidth) >= AREATILE)
                        tile = *(map-1+mapwidth);
                    if ( *(map-2) >= AREATILE)
                        tile = *(map-2);

                    *(map-1) = tile; *(obj-1) = 0;
                }
            }
    }

    for (LWMP_REPEAT(c))
    {
        Thrust(0,0);    // set player->areanumber to the floortile you're standing on
        player_mp->run_dist = 0;
    }

    fread (&oldchecksum,sizeof(oldchecksum),1,file);

    fread (&lastgamemusicoffset,sizeof(lastgamemusicoffset),1,file);
    if(lastgamemusicoffset<0) lastgamemusicoffset=0;


    if (oldchecksum != checksum)
    {
        Message(STR_SAVECHT1"\n"
                STR_SAVECHT2"\n"
                STR_SAVECHT3"\n"
                STR_SAVECHT4);

        IN_ClearKeysDown();
        IN_Ack();

        gamestate.oldscore = gamestate.score = 0;
        for (LWMP_REPEAT(c))
        {
            gamestate.weapon_mp =
                gamestate.chosenweapon_mp =
                gamestate.bestweapon_mp = wp_pistol;
            gamestate.ammo_mp = 8;
            gamestate.lives_mp = 1;
        }
    }

    return true;
}

//===========================================================================

/*
==========================
=
= ShutdownId
=
= Shuts down all ID_?? managers
=
==========================
*/

void ShutdownId (void)
{
    US_Shutdown ();         // This line is completely useless...
    SD_Shutdown ();
    PM_Shutdown ();
    IN_Shutdown ();
    VW_Shutdown ();
    CA_Shutdown ();
#if defined(GP2X_940)
    GP2X_Shutdown();
#endif
}


//===========================================================================

/*
==================
=
= BuildTables
=
= Calculates:
=
= scale                 projection constant
= sintable/costable     overlapping fractional tables
=
==================
*/

const float radtoint = (float)(FINEANGLES/2/PI);

void BuildTables (void)
{
    //
    // calculate fine tangents
    //

    int i;
    for(i=0;i<FINEANGLES/8;i++)
    {
        double tang=tan((i+0.5)/radtoint);
        finetangent[i]=(int32_t)(tang*GLOBAL1);
        finetangent[FINEANGLES/4-1-i]=(int32_t)((1/tang)*GLOBAL1);
    }

    //
    // costable overlays sintable with a quarter phase shift
    // ANGLES is assumed to be divisable by four
    //

    float angle=0;
    float anglestep=(float)(PI/2/ANGLEQUAD);
    for(i=0; i<ANGLEQUAD; i++)
    {
        fixed value=(int32_t)(GLOBAL1*sin(angle));
        sintable[i]=sintable[i+ANGLES]=sintable[ANGLES/2-i]=value;
        sintable[ANGLES-i]=sintable[ANGLES/2+i]=-value;
        angle+=anglestep;
    }
    sintable[ANGLEQUAD] = 65536;
    sintable[3*ANGLEQUAD] = -65536;

#if defined(USE_STARSKY) || defined(USE_RAIN) || defined(USE_SNOW)
    Init3DPoints();
#endif
}

//===========================================================================


/*
====================
=
= CalcProjection
=
= Uses focallength
=
====================
*/

void CalcProjection (int32_t focal)
{
    int     i;
    int    intang;
    float   angle;
    double  tang;
    int     halfview;
    double  facedist;

    focallength = focal;
    facedist = focal+MINDIST;
    halfview = vw(viewwidth_mp)/2;                                 // half view in pixels

    //
    // calculate scale value for vertical height calculations
    // and sprite x calculations
    //
    viewscale_mp = (fixed) (halfview*facedist/(VIEWGLOBAL/2));

    //
    // divide heightnumerator_mp by a posts distance to get the posts height for
    // the heightbuffer.  The pixel height is height>>2
    //
    heightnumerator_mp = (TILEGLOBAL*viewscale_mp)>>6;

    //
    // calculate the angle offset from view angle of each pixel's ray
    //

    for (i=0;i<halfview;i++)
    {
        // start 1/2 pixel over, so viewangle bisects two middle pixels
        tang = (int32_t)i*VIEWGLOBAL/vw(viewwidth_mp)/facedist;
        angle = (float) atan(tang);
        intang = (int) (angle*radtoint);
        pixelangle_mp[halfview-1-i] = intang;
        pixelangle_mp[halfview+i] = -intang;
    }
}



//===========================================================================

/*
===================
=
= SetupWalls
=
= Map tile values to scaled pics
=
===================
*/

void SetupWalls (void)
{
    int     i;

    horizwall[0]=0;
    vertwall[0]=0;

    for (i=1;i<MAXWALLTILES;i++)
    {
        horizwall[i]=(i-1)*2;
        vertwall[i]=(i-1)*2+1;
    }
}

//===========================================================================

/*
==========================
=
= SignonScreen
=
==========================
*/

void SignonScreen (void)                        // VGA version
{
    int c;
    VL_SetVGAPlaneMode ();

    VL_MungePic (signon,MaxX,MaxY);

    for (LWMP_NONSPLIT(c))
    {
        VL_MemToScreen (signon,MaxX,MaxY,0,0,NUMCHUNKS);
    }
}


/*
==========================
=
= FinishSignon
=
==========================
*/

void FinishSignon (void)
{
    #ifndef SPEAR
    VW_Bar (0,189,300,11,VL_GetPixel(0,0));
    WindowX = 0;
    WindowW = MaxX;
    PrintY = MaxY - 10;

    #ifndef JAPAN
    SETFONTCOLOR(14,4);

    #ifdef SPANISH
    US_CPrint ("Oprima una tecla");
    #else
    US_CPrint ("Press a key");
    #endif

    #endif
    VW_UpdateScreen();

    if (!param_nowait)
        IN_Ack ();

    #ifndef JAPAN
    VW_Bar (0,MaxY - 11,MaxX - 20,11,VL_GetPixel(0,0));

    PrintY = MaxY - 10;
    SETFONTCOLOR(10,4);

    #ifdef SPANISH
    US_CPrint ("pensando...");
    #else
    US_CPrint ("Working...");
    #endif

    VW_UpdateScreen();
    #endif

    SETFONTCOLOR(0,15);
    #else
    VW_UpdateScreen();

    if (!param_nowait)
        VW_WaitVBL(3*70);
    #endif
}

//===========================================================================

/*
=====================
=
= InitDigiMap
=
=====================
*/

// channel mapping:
//  -1: any non reserved channel
//   0: player weapons
//   1: boss weapons

typedef struct DigiMapEntry_s
{
    soundnames snd;
    int digi;
    int channel;
    const char *extFileName;
    bool looped;
    int tileDist;
    int fadeInMs;
} DigiMapEntry_t;

static DigiMapEntry_t wolfdigimap[] =
    {
        // These first sounds are in the upload version
#ifndef SPEAR
        { HALTSND,                HALTDIGISND,  -1, "HALTSND", false, SoundDistance::noLimit },
        { DOGBARKSND,             DOGBARKDIGISND,  -1, "DOGBARKSND", false, SoundDistance::noLimit },
        { CLOSEDOORSND,           CLOSEDOORDIGISND,  -1, "CLOSEDOORSND", false, SoundDistance::noLimit },
        { OPENDOORSND,            OPENDOORDIGISND,  -1, "OPENDOORSND", false, SoundDistance::noLimit },
        { ATKMACHINEGUNSND,       ATKMACHINEGUNDIGISND,   0, "ATKMACHINEGUNSND", false, SoundDistance::noLimit },
        { ATKPISTOLSND,           ATKPISTOLDIGISND,   0, "ATKPISTOLSND", false, SoundDistance::noLimit },
        { ATKGATLINGSND,          ATKGATLINGDIGISND,   0, "ATKGATLINGSND", false, SoundDistance::noLimit },
        { SCHUTZADSND,            SCHUTZADDIGISND,  -1, "SCHUTZADSND", false, SoundDistance::noLimit },
        { GUTENTAGSND,            GUTENTAGDIGISND,  -1, "GUTENTAGSND", false, SoundDistance::noLimit },
        { MUTTISND,               MUTTIDIGISND,  -1, "MUTTISND", false, SoundDistance::noLimit },
        { BOSSFIRESND,            BOSSFIREDIGISND,  1, "BOSSFIRESND", false, SoundDistance::noLimit },
        { SSFIRESND,              SSFIREDIGISND, -1, "SSFIRESND", false, SoundDistance::noLimit },
        { DEATHSCREAM1SND,        DEATHSCREAM1DIGISND, -1, "DEATHSCREAM1SND", false, SoundDistance::noLimit },
        { DEATHSCREAM2SND,        DEATHSCREAM2DIGISND, -1, "DEATHSCREAM2SND", false, SoundDistance::noLimit },
        { DEATHSCREAM3SND,        DEATHSCREAM3DIGISND, -1, "DEATHSCREAM3SND", false, SoundDistance::noLimit },
        { TAKEDAMAGESND,          TAKEDAMAGEDIGISND, -1, "TAKEDAMAGESND", false, SoundDistance::noLimit },
        { PUSHWALLSND,            PUSHWALLDIGISND, -1, "PUSHWALLSND", false, SoundDistance::noLimit },

        { LEBENSND,               LEBENDIGISND, -1, "LEBENSND", false, SoundDistance::noLimit },
        { NAZIFIRESND,            NAZIFIREDIGISND, -1, "NAZIFIRESND", false, SoundDistance::noLimit },
        { SLURPIESND,             SLURPIEDIGISND, -1, "SLURPIESND", false, SoundDistance::noLimit },

        { YEAHSND,                YEAHDIGISND, -1, "YEAHSND", false, SoundDistance::noLimit },

#ifndef UPLOAD
        // These are in all other episodes
        { DOGDEATHSND,            DOGDEATHDIGISND, -1, "DOGDEATHSND", false, SoundDistance::noLimit },
        { AHHHGSND,               AHHHGDIGISND, -1, "AHHHGSND", false, SoundDistance::noLimit },
        { DIESND,                 DIEDIGISND, -1, "DIESND", false, SoundDistance::noLimit },
        { EVASND,                 EVADIGISND, -1, "EVASND", false, SoundDistance::noLimit },

        { TOT_HUNDSND,            TOT_HUNDDIGISND, -1, "TOT_HUNDSND", false, SoundDistance::noLimit },
        { MEINGOTTSND,            MEINGOTTDIGISND, -1, "MEINGOTTSND", false, SoundDistance::noLimit },
        { SCHABBSHASND,           SCHABBSHADIGISND, -1, "SCHABBSHASND", false, SoundDistance::noLimit },
        { HITLERHASND,            HITLERHADIGISND, -1, "HITLERHASND", false, SoundDistance::noLimit },
        { SPIONSND,               SPIONDIGISND, -1, "SPIONSND", false, SoundDistance::noLimit },
        { NEINSOVASSND,           NEINSOVASDIGISND, -1, "NEINSOVASSND", false, SoundDistance::noLimit },
        { DOGATTACKSND,           DOGATTACKDIGISND, -1, "DOGATTACKSND", false, SoundDistance::noLimit },
        { LEVELDONESND,           LEVELDONEDIGISND, -1, "LEVELDONESND", false, SoundDistance::noLimit },
        { MECHSTEPSND,            MECHSTEPDIGISND, -1, "MECHSTEPSND", false, SoundDistance::noLimit },

        { SCHEISTSND,             SCHEISTDIGISND, -1, "SCHEISTSND", false, SoundDistance::noLimit },
        { DEATHSCREAM4SND,        DEATHSCREAM4DIGISND, -1, "DEATHSCREAM4SND", false, SoundDistance::noLimit },         // AIIEEE
        { DEATHSCREAM5SND,        DEATHSCREAM5DIGISND, -1, "DEATHSCREAM5SND", false, SoundDistance::noLimit },         // DEE-DEE
        { DONNERSND,              DONNERDIGISND, -1, "DONNERSND", false, SoundDistance::noLimit },         // EPISODE 4 BOSS DIE
        { EINESND,                EINEDIGISND, -1, "EINESND", false, SoundDistance::noLimit },         // EPISODE 4 BOSS SIGHTING
        { ERLAUBENSND,            ERLAUBENDIGISND, -1, "ERLAUBENSND", false, SoundDistance::noLimit },         // EPISODE 6 BOSS SIGHTING
        { DEATHSCREAM6SND,        DEATHSCREAM6DIGISND, -1, "DEATHSCREAM6SND", false, SoundDistance::noLimit },         // FART
        { DEATHSCREAM7SND,        DEATHSCREAM7DIGISND, -1, "DEATHSCREAM7SND", false, SoundDistance::noLimit },         // GASP
        { DEATHSCREAM8SND,        DEATHSCREAM8DIGISND, -1, "DEATHSCREAM8SND", false, SoundDistance::noLimit },         // GUH-BOY!
        { DEATHSCREAM9SND,        DEATHSCREAM9DIGISND, -1, "DEATHSCREAM9SND", false, SoundDistance::noLimit },         // AH GEEZ!
        { KEINSND,                KEINDIGISND, -1, "KEINSND", false, SoundDistance::noLimit },         // EPISODE 5 BOSS SIGHTING
        { MEINSND,                MEINDIGISND, -1, "MEINSND", false, SoundDistance::noLimit },         // EPISODE 6 BOSS DIE
        { ROSESND,                ROSEDIGISND, -1, "ROSESND", false, SoundDistance::noLimit },         // EPISODE 5 BOSS DIE

#endif
#else
//
// SPEAR OF DESTINY DIGISOUNDS
//
        { HALTSND,                HALTDIGISND,  -1, "HALTSND", false, SoundDistance::noLimit },
        { CLOSEDOORSND,           CLOSEDOORDIGISND,  -1, "CLOSEDOORSND", false, SoundDistance::noLimit },
        { OPENDOORSND,            OPENDOORDIGISND,  -1, "OPENDOORSND", false, SoundDistance::noLimit },
        { ATKMACHINEGUNSND,       ATKMACHINEGUNDIGISND,   0, "ATKMACHINEGUNSND", false, SoundDistance::noLimit },
        { ATKPISTOLSND,           ATKPISTOLDIGISND,   0, "ATKPISTOLSND", false, SoundDistance::noLimit },
        { ATKGATLINGSND,          ATKGATLINGDIGISND,   0, "ATKGATLINGSND", false, SoundDistance::noLimit },
        { SCHUTZADSND,            SCHUTZADDIGISND,  -1, "SCHUTZADSND", false, SoundDistance::noLimit },
        { BOSSFIRESND,            BOSSFIREDIGISND,   1, "BOSSFIRESND", false, SoundDistance::noLimit },
        { SSFIRESND,              SSFIREDIGISND,  -1, "SSFIRESND", false, SoundDistance::noLimit },
        { DEATHSCREAM1SND,        DEATHSCREAM1DIGISND, -1, "DEATHSCREAM1SND", false, SoundDistance::noLimit },
        { DEATHSCREAM2SND,        DEATHSCREAM2DIGISND, -1, "DEATHSCREAM2SND", false, SoundDistance::noLimit },
        { TAKEDAMAGESND,          TAKEDAMAGEDIGISND, -1, "TAKEDAMAGESND", false, SoundDistance::noLimit },
        { PUSHWALLSND,            PUSHWALLDIGISND, -1, "PUSHWALLSND", false, SoundDistance::noLimit },
        { AHHHGSND,               AHHHGDIGISND, -1, "AHHHGSND", false, SoundDistance::noLimit },
        { LEBENSND,               LEBENDIGISND, -1, "LEBENSND", false, SoundDistance::noLimit },
        { NAZIFIRESND,            NAZIFIREDIGISND, -1, "NAZIFIRESND", false, SoundDistance::noLimit },
        { SLURPIESND,             SLURPIEDIGISND, -1, "SLURPIESND", false, SoundDistance::noLimit },
        { LEVELDONESND,           LEVELDONEDIGISND, -1, "LEVELDONESND", false, SoundDistance::noLimit },
        { DEATHSCREAM4SND,        DEATHSCREAM4DIGISND, -1, "DEATHSCREAM4SND", false, SoundDistance::noLimit },         // AIIEEE
        { DEATHSCREAM3SND,        DEATHSCREAM3DIGISND, -1, "DEATHSCREAM3SND", false, SoundDistance::noLimit },         // DOUBLY-MAPPED!!!
        { DEATHSCREAM5SND,        DEATHSCREAM5DIGISND, -1, "DEATHSCREAM5SND", false, SoundDistance::noLimit },         // DEE-DEE
        { DEATHSCREAM6SND,        DEATHSCREAM6DIGISND, -1, "DEATHSCREAM6SND", false, SoundDistance::noLimit },         // FART
        { DEATHSCREAM7SND,        DEATHSCREAM7DIGISND, -1, "DEATHSCREAM7SND", false, SoundDistance::noLimit },         // GASP
        { DEATHSCREAM8SND,        DEATHSCREAM8DIGISND, -1, "DEATHSCREAM8SND", false, SoundDistance::noLimit },         // GUH-BOY!
        { DEATHSCREAM9SND,        DEATHSCREAM9DIGISND, -1, "DEATHSCREAM9SND", false, SoundDistance::noLimit },         // AH GEEZ!
        { GETGATLINGSND,          GETGATLINGDIGISND, -1, "GETGATLINGSND", false, SoundDistance::noLimit },         // Got Gat replacement

#ifndef SPEARDEMO
        { DOGBARKSND,             DOGBARKDIGISND,  -1, "DOGBARKSND", false, SoundDistance::noLimit },
        { DOGDEATHSND,            DOGDEATHDIGISND, -1, "DOGDEATHSND", false, SoundDistance::noLimit },
        { SPIONSND,               SPIONDIGISND, -1, "SPIONSND", false, SoundDistance::noLimit },
        { NEINSOVASSND,           NEINSOVASDIGISND, -1, "NEINSOVASSND", false, SoundDistance::noLimit },
        { DOGATTACKSND,           DOGATTACKDIGISND, -1, "DOGATTACKSND", false, SoundDistance::noLimit },
        { TRANSSIGHTSND,          TRANSSIGHTDIGISND, -1, "TRANSSIGHTSND", false, SoundDistance::noLimit },         // Trans Sight
        { TRANSDEATHSND,          TRANSDEATHDIGISND, -1, "TRANSDEATHSND", false, SoundDistance::noLimit },         // Trans Death
        { WILHELMSIGHTSND,        WILHELMSIGHTDIGISND, -1, "WILHELMSIGHTSND", false, SoundDistance::noLimit },         // Wilhelm Sight
        { WILHELMDEATHSND,        WILHELMDEATHDIGISND, -1, "WILHELMDEATHSND", false, SoundDistance::noLimit },         // Wilhelm Death
        { UBERDEATHSND,           UBERDEATHDIGISND, -1, "UBERDEATHSND", false, SoundDistance::noLimit },         // Uber Death
        { KNIGHTSIGHTSND,         KNIGHTSIGHTDIGISND, -1, "KNIGHTSIGHTSND", false, SoundDistance::noLimit },         // Death Knight Sight
        { KNIGHTDEATHSND,         KNIGHTDEATHDIGISND, -1, "KNIGHTDEATHSND", false, SoundDistance::noLimit },         // Death Knight Death
        { ANGELSIGHTSND,          ANGELSIGHTDIGISND, -1, "ANGELSIGHTSND", false, SoundDistance::noLimit },         // Angel Sight
        { ANGELDEATHSND,          ANGELDEATHDIGISND, -1, "ANGELDEATHSND", false, SoundDistance::noLimit },         // Angel Death
        { GETSPEARSND,            GETSPEARDIGISND, -1, "GETSPEARSND", false, SoundDistance::noLimit },         // Got Spear replacement
#endif
#endif
        { BJDIESND,               BJDIEDIGISND, -1, "BJDIESND", false, SoundDistance::noLimit },
        { GETBACKPACKSND,         GETBACKPACKDIGISND, -1, "GETBACKPACKSND", false, SoundDistance::noLimit },
        { BJFALLSND,              BJFALLDIGISND, -1, "BJFALLSND", false, SoundDistance::noLimit },
        { BJRESPAWNSND,           BJRESPAWNDIGISND, -1, "BJRESPAWNSND", false, SoundDistance::noLimit },
        { WBARRELDEATHSND,        WBARRELDEATHDIGISND, -1, "WBARRELDEATHSND", false, SoundDistance::noLimit },
        { TBARRELDEATHSND,        TBARRELDEATHDIGISND, -1, "TBARRELDEATHSND", false, SoundDistance::noLimit },
        { EBARRELDEATHSND,        EBARRELDEATHDIGISND, -1, "EBARRELDEATHSND", false, SoundDistance::noLimit },
        { TANKDEATHSND,           TANKDEATHDIGISND, -1, "TANKDEATHSND", false, SoundDistance::noLimit },
        { ATKTANKGUNSND,          ATKTANKGUNDIGISND, -1, "ATKTANKGUNSND", false, SoundDistance::noLimit },
        { TANKACCELSND,           TANKACCELDIGISND, -1, "TANKACCELSND", false, SoundDistance::noLimit },
        { TANKDECELSND,           TANKDECELDIGISND, -1, "TANKDECELSND", false, SoundDistance::noLimit },
        { TANKMOVINGSND,          TANKMOVINGDIGISND, -1, "TANKMOVINGSND", true, SoundDistance::noLimit, 500 },
        { ATKMG42SND,             ATKMG42DIGISND, -1, "ATKMG42SND", false, SoundDistance::noLimit },
        { MG42USESND,             MG42USEDIGISND, -1, "MG42USESND", false, SoundDistance::noLimit },
        { TANKUSEWHEELSND,        TANKUSEWHEELDIGISND, -1, "TANKUSEWHEELSND", false, SoundDistance::noLimit },
        { TANKUSEGUNSND,          TANKUSEGUNDIGISND, -1, "TANKUSEGUNSND", false, SoundDistance::noLimit },
        { TANKFASTACCELSND,       TANKFASTACCELDIGISND, -1, "TANKFASTACCELSND", false, SoundDistance::noLimit },
        { TANKFASTDECELSND,       TANKFASTDECELDIGISND, -1, "TANKFASTDECELSND", false, SoundDistance::noLimit },
        { TANKFASTMOVINGSND,      TANKFASTMOVINGDIGISND, -1, "TANKFASTMOVINGSND", true, SoundDistance::noLimit, 500 },
        { GETHEARTSND,            GETHEARTDIGISND, -1, "GETHEARTSND", false, SoundDistance::noLimit },
        { GETFLAGSND,             GETFLAGDIGISND, -1, "GETFLAGSND", false, SoundDistance::noLimit },
        { DROPFLAGSND,            DROPFLAGDIGISND, -1, "DROPFLAGSND", false, SoundDistance::noLimit },
        { ATKFLAGSND,             ATKFLAGDIGISND, -1, "ATKFLAGSND", false, SoundDistance::noLimit },
        { HSKULLSPAWNSND,         HSKULLSPAWNDIGISND, -1, "HSKULLSPAWNSND", false, SoundDistance::noLimit },
        { HSKULLCLEARSND,         HSKULLCLEARDIGISND, -1, "HSKULLCLEARSND", false, SoundDistance::noLimit },
        { GIBEXPLODESND,          GIBEXPLODEDIGISND, -1, "GIBEXPLODESND", false, SoundDistance::noLimit },
        { ATKPUNCHHITSND,         ATKPUNCHHITDIGISND, -1, "ATKPUNCHHITSND", false, SoundDistance::noLimit },
        { ATKPUNCHMISSEDSND,      ATKPUNCHMISSEDDIGISND, -1, "ATKPUNCHMISSEDSND", false, SoundDistance::noLimit },
        { ATKPUNCHCHARGESND,      ATKPUNCHCHARGEDIGISND, -1, "ATKPUNCHCHARGESND", false, SoundDistance::noLimit },
        { ATKFLAGMISSEDSND,       ATKFLAGMISSEDDIGISND, -1, "ATKFLAGMISSEDSND", false, SoundDistance::noLimit },
        { GETRAMPAGEAMMOSND,      GETRAMPAGEAMMODIGISND, -1, "GETRAMPAGEAMMOSND", false, SoundDistance::noLimit },
        { ZOMBIEATTACKSND,        ZOMBIEATTACKDIGISND, -1, "ZOMBIEATTACKSND", false, SoundDistance::noLimit },
        { RTNFLAGSND,             RTNFLAGDIGISND, -1, "RTNFLAGSND", false, SoundDistance::noLimit },
        { TANKIDLESND,            TANKIDLEDIGISND, -1, "TANKIDLESND", true, SoundDistance::noLimit, 500 },
        { TANKIGNITIONSND,        TANKIGNITIONDIGISND, -1, "TANKIGNITIONSND", false, SoundDistance::noLimit },
        { TANKREVSND,             TANKREVDIGISND, -1, "TANKREVSND", false, SoundDistance::noLimit },
        { LASTSOUND,              -1, -1, "", false, SoundDistance::noLimit },
    };


void InitDigiMap (void)
{
    DigiMapEntry_t *map;

    for (map = wolfdigimap; map->snd != LASTSOUND; map++)
    {
        DigiMap[map->snd] = map->digi;
        DigiChannel[map->digi] = map->channel;
        DigiLooped[map->digi] = map->looped;
        DigiTileDist[map->digi] = map->tileDist;
        DigiFadeInMs[map->digi] = map->fadeInMs;
        SD_PrepareSound(map->digi, map->extFileName);
    }
}

#ifndef SPEAR
CP_iteminfo MusicItems={CTL_X,CTL_Y,6,0,32};
CP_itemtype MusicMenu[]=
    {
        {1,"Get Them!",0},
        {1,"Searching",0},
        {1,"P.O.W.",0},
        {1,"Suspense",0},
        {1,"War March",0},
        {1,"Around The Corner!",0},

        {1,"Nazi Anthem",0},
        {1,"Lurking...",0},
        {1,"Going After Hitler",0},
        {1,"Pounding Headache",0},
        {1,"Into the Dungeons",0},
        {1,"Ultimate Conquest",0},

        {1,"Kill the S.O.B.",0},
        {1,"The Nazi Rap",0},
        {1,"Twelfth Hour",0},
        {1,"Zero Hour",0},
        {1,"Ultimate Conquest",0},
        {1,"Wolfpack",0}
    };
#else
CP_iteminfo MusicItems={CTL_X,CTL_Y-20,9,0,32};
CP_itemtype MusicMenu[]=
    {
        {1,"Funky Colonel Bill",0},
        {1,"Death To The Nazis",0},
        {1,"Tiptoeing Around",0},
        {1,"Is This THE END?",0},
        {1,"Evil Incarnate",0},
        {1,"Jazzin' Them Nazis",0},
        {1,"Puttin' It To The Enemy",0},
        {1,"The SS Gonna Get You",0},
        {1,"Towering Above",0}
    };
#endif

#ifndef SPEARDEMO
void DoJukebox(void)
{
    int which,lastsong=-1;
    unsigned start;
    unsigned songs[]=
        {
#ifndef SPEAR
            GETTHEM_MUS,
            SEARCHN_MUS,
            POW_MUS,
            SUSPENSE_MUS,
            WARMARCH_MUS,
            CORNER_MUS,

            NAZI_OMI_MUS,
            PREGNANT_MUS,
            GOINGAFT_MUS,
            HEADACHE_MUS,
            DUNGEON_MUS,
            ULTIMATE_MUS,

            INTROCW3_MUS,
            NAZI_RAP_MUS,
            TWELFTH_MUS,
            ZEROHOUR_MUS,
            ULTIMATE_MUS,
            PACMAN_MUS
#else
            XFUNKIE_MUS,             // 0
            XDEATH_MUS,              // 2
            XTIPTOE_MUS,             // 4
            XTHEEND_MUS,             // 7
            XEVIL_MUS,               // 17
            XJAZNAZI_MUS,            // 18
            XPUTIT_MUS,              // 21
            XGETYOU_MUS,             // 22
            XTOWER2_MUS              // 23
#endif
        };

    IN_ClearKeysDown();
    if (!AdLibPresent && !SoundBlasterPresent)
        return;

    MenuFadeOut();

#ifndef SPEAR
#ifndef UPLOAD
    start = ((SDL_GetTicks()/10)%3)*6;
#else
    start = 0;
#endif
#else
    start = 0;
#endif

    CA_CacheGrChunk (STARTFONT+1);
#ifdef SPEAR
    CacheLump (BACKDROP_LUMP_START,BACKDROP_LUMP_END);
#else
    CacheLump (CONTROLS_LUMP_START,CONTROLS_LUMP_END);
#endif
    CA_LoadAllSounds ();

    fontnumber=1;
    ClearMScreen ();
    VWB_DrawPic(112,184,C_MOUSELBACKPIC);
    DrawStripes (10);
    SETFONTCOLOR (TEXTCOLOR,BKGDCOLOR);

#ifndef SPEAR
    DrawWindow (CTL_X-2,CTL_Y-6,280,13*7,BKGDCOLOR);
#else
    DrawWindow (CTL_X-2,CTL_Y-26,280,13*10,BKGDCOLOR);
#endif

    DrawMenu (&MusicItems,&MusicMenu[start]);

    SETFONTCOLOR (READHCOLOR,BKGDCOLOR);
    PrintY=15;
    WindowX = 0;
    WindowY = MaxX;
    US_CPrint ("Robert's Jukebox");

    SETFONTCOLOR (TEXTCOLOR,BKGDCOLOR);
    VW_UpdateScreen();
    MenuFadeIn();

    do
    {
        which = HandleMenu(&MusicItems,&MusicMenu[start],NULL);
        if (which>=0)
        {
            if (lastsong >= 0)
                MusicMenu[start+lastsong].active = 1;

            StartCPMusic(songs[start + which]);
            MusicMenu[start+which].active = 2;
            DrawMenu (&MusicItems,&MusicMenu[start]);
            VW_UpdateScreen();
            lastsong = which;
        }
    } while(which>=0);

    MenuFadeOut();
    IN_ClearKeysDown();
#ifdef SPEAR
    UnCacheLump (BACKDROP_LUMP_START,BACKDROP_LUMP_END);
#else
    UnCacheLump (CONTROLS_LUMP_START,CONTROLS_LUMP_END);
#endif
}
#endif

/*
==========================
=
= InitGame
=
= Load a few things right away
=
==========================
*/

static void InitGame()
{
    int c;
    Uint32 sdlFlags = SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK;

    if (param_nosound)
    {
        sdlFlags &= ~SDL_INIT_AUDIO;
    }

#ifndef SPEARDEMO
    boolean didjukebox=false;
#endif

    // initialize SDL
#if defined _WIN32
    putenv("SDL_VIDEODRIVER=directx");
#endif
    if(SDL_Init(sdlFlags) < 0)
    {
        printf("Unable to init SDL: %s\n", SDL_GetError());
        exit(1);
    }
    atexit(SDL_Quit);

    int numJoysticks = SDL_NumJoysticks();
    if(param_joystickindex && (param_joystickindex < -1 || param_joystickindex >= numJoysticks))
    {
        if(!numJoysticks)
            printf("No joysticks are available to SDL!\n");
        else
            printf("The joystick index must be between -1 and %i!\n", numJoysticks - 1);
        exit(1);
    }

#if defined(GP2X_940)
    GP2X_MemoryInit();
#endif

    ColorRemap_InitTables();

    SignonScreen ();

#if defined _WIN32
    if(!fullscreen)
    {
        struct SDL_SysWMinfo wmInfo;
        SDL_VERSION(&wmInfo.version);

        if(SDL_GetWMInfo(&wmInfo) != -1)
        {
            HWND hwndSDL = wmInfo.window;
            DWORD style = GetWindowLong(hwndSDL, GWL_STYLE) & ~WS_SYSMENU;
            SetWindowLong(hwndSDL, GWL_STYLE, style);
            SetWindowPos(hwndSDL, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
        }
    }
#endif

    VH_Startup ();
    IN_Startup ();
    PM_Startup ();
    SD_Startup ();
    CA_Startup ();
    US_Startup ();

    // TODO: Will any memory checking be needed someday??
#ifdef NOTYET
#ifndef SPEAR
    if (mminfo.mainmem < 235000L)
#else
    if (mminfo.mainmem < 257000L && !MS_CheckParm("debugmode"))
#endif
    {
        byte *screen;

        CA_CacheGrChunk (ERRORSCREEN);
        screen = grsegs[ERRORSCREEN];
        ShutdownId();
/*        memcpy((byte *)0xb8000,screen+7+7*160,17*160);
        gotoxy (1,23);*/
        exit(1);
    }
#endif


//
// build some tables
//
    InitDigiMap ();

    ReadConfig ();

    SetupSaveGames();

//
// HOLDING DOWN 'M' KEY?
//
#ifndef SPEARDEMO
    if (Keyboard[sc_M])
    {
        DoJukebox();
        didjukebox=true;
    }
    else
#endif

//
// draw intro screen stuff
//
    for (LWMP_NONSPLIT(c))
    {
        IntroScreen ();
    }

#ifdef _arch_dreamcast
    //TODO: VMU Selection Screen
#endif

//
// load in and lock down some basic chunks
//

    CA_CacheGrChunk(STARTFONT);
    CA_CacheGrChunk(STATUSBARPIC);

    LoadLatchMem ();
    BuildTables ();          // trig tables
    SetupWalls ();

    for (LWMP_REPEAT(c))
    {
        NewViewSize (viewsize);
    }

//
// initialize variables
//
    InitRedShifts ();

    for (LWMP_NONSPLIT(c))
    {
#ifndef SPEARDEMO
        if(!didjukebox)
            FinishSignon();
#else
        FinishSignon();
#endif
    }

#ifdef NOTYET
    vdisp = (byte *) (0xa0000+PAGE1START);
    vbuf = (byte *) (0xa0000+PAGE2START);
#endif
}

//===========================================================================

void ShowViewSize (int width)
{
    int oldwidth,oldheight;

    oldwidth = viewwidth_mp;
    oldheight = viewheight_mp;

    if(width == 21)
    {
        viewwidth_mp = MaxX;
        viewheight_mp = MaxY;
        VWB_BarScaledCoord (0, 0, MaxX, MaxY, 0);
    }
    else if(width == 20)
    {
        viewwidth_mp = MaxX;
        viewheight_mp = MaxY - LWMP_StatusLines();
        DrawPlayBorder ();
    }
    else
    {
        viewwidth_mp = width * 16;
        viewheight_mp = (int) (width * 16 * HEIGHTRATIO) + 
            (STATUSLINES - LWMP_StatusLines());
        DrawPlayBorder ();
    }

    viewwidth_mp = oldwidth;
    viewheight_mp = oldheight;
}


void NewViewSize (int width)
{
    viewsize = width;
    if(viewsize == 21)
        LWMP_SetViewSize(MaxX, MaxY);
    else if(viewsize == 20)
        LWMP_SetViewSize(MaxX, MaxY - LWMP_StatusLines());
    else
    {
        LWMP_SetViewSize(width * 16,
            (unsigned)(width * 16 * HEIGHTRATIO) + 
            (STATUSLINES - LWMP_StatusLines()));
    }
}



//===========================================================================

/*
==========================
=
= Quit
=
==========================
*/

void Quit (const char *errorStr, ...)
{
#ifdef NOTYET
    byte *screen;
#endif
    char error[256];
    if(errorStr != NULL)
    {
        va_list vlist;
        va_start(vlist, errorStr);
        vsprintf(error, errorStr, vlist);
        va_end(vlist);
    }
    else error[0] = 0;

    if (!pictable)  // don't try to display the red box before it's loaded
    {
        ShutdownId();
        if (error && *error)
        {
#ifdef NOTYET
            SetTextCursor(0,0);
#endif
            puts(error);
#ifdef NOTYET
            SetTextCursor(0,2);
#endif
            VW_WaitVBL(100);
        }
        exit(1);
    }

    if (!error || !*error)
    {
#ifdef NOTYET
        #ifndef JAPAN
        CA_CacheGrChunk (ORDERSCREEN);
        screen = grsegs[ORDERSCREEN];
        #endif
#endif
        WriteConfig ();
    }
#ifdef NOTYET
    else
    {
        CA_CacheGrChunk (ERRORSCREEN);
        screen = grsegs[ERRORSCREEN];
    }
#endif

    ShutdownId ();

    if (error && *error)
    {
#ifdef NOTYET
        memcpy((byte *)0xb8000,screen+7,7*160);
        SetTextCursor(9,3);
#endif
        puts(error);
#ifdef NOTYET
        SetTextCursor(0,7);
#endif
        VW_WaitVBL(200);
        exit(1);
    }
    else
    if (!error || !(*error))
    {
#ifdef NOTYET
        #ifndef JAPAN
        memcpy((byte *)0xb8000,screen+7,24*160); // 24 for SPEAR/UPLOAD compatibility
        #endif
        SetTextCursor(0,23);
#endif
    }

    exit(0);
}

//===========================================================================


/*
=====================
=
= TitlePage
=
=====================
*/

static bool TitlePage(int picnum, int secs, int &secsTaken)
{
    bool pressed = false;

    unsigned int width, height;
    if (LWMP_GetPic(picnum, &width, &height) != NULL)
    {
        SDL_Color *pal = LWMP_GetPicPal(picnum);

        int c;
        for (LWMP_NONSPLIT(c))
        {
            VWB_DrawPic (0,0,picnum);
        }
        VW_UpdateScreen ();
        if (picnum == SWTITLEPIC)
        {
            VL_FadeIn(0,255,pal,30);
        }

        if (IN_UserInput (TickBase * secs))
        {
            pressed = true;
        }
        if (picnum == SWTITLE2PIC)
        {
            VL_FadeOut(0, 255, 0, 0, 0, 30, NULL);
        }

        secsTaken += secs;
    }

    return pressed;
}


/*
=====================
=
= DemoLoop
=
=====================
*/


static void DemoLoop(void)
{
    int c;
    int LastDemo = 0;

    //
    // check for launch from ted
    //
    if (param_tedlevel != -1)
    {
        param_nowait = true;
        EnableEndGameMenuItem();
        NewGame(param_difficulty,0,param_defaultgamemode);

        #ifndef SPEAR
        gamestate.episode = param_tedlevel/10;
        gamestate.mapon = param_tedlevel%10;
        #else
        gamestate.episode = 0;
        gamestate.mapon = param_tedlevel;
        #endif

        GameLoop();
        Quit (NULL);
    }

    //
    // main game cycle
    //

    #ifndef DEMOTEST
        #ifndef UPLOAD
            #ifndef GOODTIMES
            #ifndef SPEAR
            #ifndef JAPAN
            if (!param_nowait)
                NonShareware();
            #endif
            #else
                #ifndef GOODTIMES
                #ifndef SPEARDEMO
                extern void CopyProtection(void);
                if(!param_goodtimes)
                    CopyProtection();
                #endif
                #endif
            #endif
            #endif
        #endif

        StartCPMusic(INTROSONG);

        #ifndef JAPAN
            if (!param_nowait)
                PG13 ();
        #endif
    #endif

    while (1)
    {
        while (!param_nowait)
        {
            //
            // splitwolf title page
            //
            int secsTaken = 0;
            if (TitlePage(SWTITLEPIC, 2, secsTaken))
            {
                break;
            }
            if (TitlePage(SWTITLE2PIC, 8, secsTaken))
            {
                break;
            }

            //
            // title page
            //
            #ifndef DEMOTEST
            #ifdef SPEAR
            SDL_Color pal[256];
            CA_CacheGrChunk (TITLEPALETTE);
            VL_ConvertPalette(grsegs[TITLEPALETTE], pal, 256);

            CA_CacheGrChunk (TITLE1PIC);
            for (LWMP_NONSPLIT(c))
            {
                VWB_DrawPic (0,0,TITLE1PIC);
            }
            UNCACHEGRCHUNK (TITLE1PIC);

            CA_CacheGrChunk (TITLE2PIC);
            for (LWMP_NONSPLIT(c))
            {
                VWB_DrawPic (0,80,TITLE2PIC);
            }
            UNCACHEGRCHUNK (TITLE2PIC);
            VW_UpdateScreen ();
            VL_FadeIn(0,255,pal,30);

            UNCACHEGRCHUNK (TITLEPALETTE);
            #else
            for (LWMP_NONSPLIT(c))
            {
                CA_CacheScreen (TITLEPIC);
            }
            VW_UpdateScreen ();
            VW_FadeIn();
            #endif
            if (IN_UserInput(TickBase*(15 - secsTaken)))
                break;
            VW_FadeOut();
            //
            // credits page
            //
            for (LWMP_NONSPLIT(c))
            {
                CA_CacheScreen (CREDITSPIC);
            }
            VW_UpdateScreen();
            VW_FadeIn ();
            if (IN_UserInput(TickBase*10))
                break;
            VW_FadeOut ();
            //
            // high scores
            //
            for (LWMP_NONSPLIT(c))
            {
                DrawHighScores ();
            }
            VW_UpdateScreen ();
            VW_FadeIn ();

            if (IN_UserInput(TickBase*10))
                break;
            #endif
            //
            // demo
            //

            #ifndef SPEARDEMO
            PlayDemo (LastDemo++%4);
            #else
            PlayDemo (0);
            #endif

            if (playstate == ex_abort)
                break;
            VW_FadeOut();
            StartCPMusic(INTROSONG);
        }

        VW_FadeOut ();

        #ifdef DEBUGKEYS
        if (Keyboard[sc_Tab] && param_debugmode)
            RecordDemo ();
        else
            US_ControlPanel (0);
        #else
        US_ControlPanel (0);
        #endif

        if (startgame || loadedgame)
        {
            GameLoop ();
            if(!param_nowait)
            {
                VW_FadeOut();
                StartCPMusic(INTROSONG);
            }
        }
    }
}


//===========================================================================

#define IFARG(str) if(!strcmp(arg, (str)))

void CheckParameters(int argc, char *argv[])
{
    bool hasError = false, showHelp = false;
    bool sampleRateGiven = false, audioBufferGiven = false;
    int defaultSampleRate = param_samplerate;
    int numPlayers;
    int width, height;

    for(int i = 1; i < argc; i++)
    {
        char *arg = argv[i];
        IFARG("--goobers")
            param_debugmode = true;
        IFARG("--debugmode")
            param_debugmode = true;
        else IFARG("--baby")
            param_difficulty = 0;
        else IFARG("--easy")
            param_difficulty = 1;
        else IFARG("--normal")
            param_difficulty = 2;
        else IFARG("--hard")
            param_difficulty = 3;
        else IFARG("--nowait")
            param_nowait = true;
        else IFARG("--tedlevel")
        {
            if(++i >= argc)
            {
                printf("The tedlevel option is missing the level argument!\n");
                hasError = true;
            }
            else param_tedlevel = atoi(argv[i]);
        }
        else IFARG("--windowed")
            fullscreen = false;
        else IFARG("--windowed-mouse")
        {
            fullscreen = false;
            forcegrabmouse = true;
        }
        else IFARG("--bits")
        {
            if(++i >= argc)
            {
                printf("The bits option is missing the color depth argument!\n");
                hasError = true;
            }
            else
            {
                screenBits = atoi(argv[i]);
                switch(screenBits)
                {
                    case 8:
                    case 16:
                    case 24:
                    case 32:
                        break;

                    default:
                        printf("Screen color depth must be 8, 16, 24, or 32!\n");
                        hasError = true;
                        break;
                }
            }
        }
        else IFARG("--nodblbuf")
            usedoublebuffering = false;
        else IFARG("--extravbls")
        {
            if(++i >= argc)
            {
                printf("The extravbls option is missing the vbls argument!\n");
                hasError = true;
            }
            else
            {
                extravbls = atoi(argv[i]);
                if(extravbls < 0)
                {
                    printf("Extravbls must be positive!\n");
                    hasError = true;
                }
            }
        }
        else IFARG("--joystick")
        {
            if(++i >= argc)
            {
                printf("The joystick option is missing the index argument!\n");
                hasError = true;
            }
            else param_joystickindex = atoi(argv[i]);   // index is checked in InitGame
        }
        else IFARG("--samplerate")
        {
            if(++i >= argc)
            {
                printf("The samplerate option is missing the rate argument!\n");
                hasError = true;
            }
            else param_samplerate = atoi(argv[i]);
            sampleRateGiven = true;
        }
        else IFARG("--audiobuffer")
        {
            if(++i >= argc)
            {
                printf("The audiobuffer option is missing the size argument!\n");
                hasError = true;
            }
            else param_audiobuffer = atoi(argv[i]);
            audioBufferGiven = true;
        }
        else IFARG("--mission")
        {
            if(++i >= argc)
            {
                printf("The mission option is missing the mission argument!\n");
                hasError = true;
            }
            else
            {
                param_mission = atoi(argv[i]);
                if(param_mission < 0 || param_mission > 3)
                {
                    printf("The mission option must be between 0 and 3!\n");
                    hasError = true;
                }
            }
        }
        else IFARG("--configdir")
        {
            if(++i >= argc)
            {
                printf("The configdir option is missing the dir argument!\n");
                hasError = true;
            }
            else
            {
                size_t len = strlen(argv[i]);
                if(len + 2 > sizeof(configdir))
                {
                    printf("The config directory is too long!\n");
                    hasError = true;
                }
                else
                {
                    strcpy(configdir, argv[i]);
                    if(argv[i][len] != '/' && argv[i][len] != '\\')
                        strcat(configdir, "/");
                }
            }
        }
        else IFARG("--sounddir")
        {
            if(++i >= argc)
            {
                printf("The sounddir option is missing the dir argument!\n");
                hasError = true;
            }
            else
            {
                size_t len = strlen(argv[i]);
                if(len + 2 > sizeof(sounddir))
                {
                    printf("The sound directory is too long!\n");
                    hasError = true;
                }
                else
                {
                    strcpy(sounddir, argv[i]);
                    if(argv[i][len] != '/' && argv[i][len] != '\\')
                        strcat(sounddir, "/");
                }
            }
        }
        else IFARG("--goodtimes")
            param_goodtimes = true;
        else IFARG("--ignorenumchunks")
            param_ignorenumchunks = true;
        else IFARG("--help")
            showHelp = true;
        else IFARG("--nosound")
            param_nosound = true;
        else IFARG("--split")
        {
            if(++i >= argc)
            {
                printf("The split option is missing the players argument!\n");
                hasError = true;
            }
            else
            {
                numPlayers = atoi(argv[i]);
                if (numPlayers <= 0 || numPlayers >= LWMP_MAX_PLAYERS)
                {
                    hasError = true;
                }
                else
                {
                    LWMP_SetNumPlayers(numPlayers);
                }
            }
        }
        else IFARG("--splitlayout")
        {
            if(++i >= argc)
            {
                printf("The splitlayout option is missing the layout argument!\n");
                hasError = true;
            }
            else
            {
                LWMP_SetLayout(argv[i]);
            }
        }
        else IFARG("--splitdatadir")
        {
            if(++i >= argc)
            {
                printf("The splitdatadir option is missing the datadir argument!\n");
                hasError = true;
            }
            else
            {
                LWMP_SetDataDir(argv[i]);
            }
        }
        else IFARG("--res")
        {
            if(i + 2 >= argc)
            {
                printf("The res option needs the width and/or the height argument!\n");
                hasError = true;
            }
            else
            {
                width = atoi(argv[++i]);
                if (width < MaxX)
                {
                    printf("Split screen width must be at least %d!\n", MaxX);
                    hasError = true;
                }

                height = atoi(argv[++i]);
                if (height < MaxY)
                {
                    printf("Split screen height must be at least %d!\n", MaxY);
                    hasError = true;
                }

                if (!hasError)
                {
                    LWMP_SetResolution(width, height);
                }
            }
        }
        else IFARG("--instagibmode")
        {
            param_defaultgamemode = GameMode::instagib;
        }
        else IFARG("--defusemode")
        {
            param_defaultgamemode = GameMode::defuse;
        }
        else IFARG("--vampiremode")
        {
            param_defaultgamemode = GameMode::vampire;
        }
        else IFARG("--ctfmode")
        {
            param_defaultgamemode = GameMode::ctf;
        }
        else IFARG("--harvestermode")
        {
            param_defaultgamemode = GameMode::harvester;
        }
        else IFARG("--bjmutantmode")
        {
            param_defaultgamemode = GameMode::bjmutant;
        }
        else IFARG("--rampagemode")
        {
            param_defaultgamemode = GameMode::rampage;
        }
        else IFARG("--zombiemode")
        {
            param_defaultgamemode = GameMode::zombie;
        }
        else IFARG("--zombieharvestermode")
        {
            param_defaultgamemode = GameMode::zombieharvester;
        }
        else hasError = true;
    }
    if(hasError || showHelp)
    {
        if(hasError) printf("\n");
        printf(
            "SplitWolf " VERSION_DISPLAY " by Team RayCast\n"
            "Based on Wolf4SDL v1.7 ($Revision: 256 $)\n"
            "Ported by Chaos-Software (http://www.chaos-software.de.vu)\n"
            "Original Wolfenstein 3D by id Software\n\n"
            "Usage: Wolf4SDL [options]\n"
            "Options:\n"
            " --help                 This help page\n"
            " --tedlevel <level>     Starts the game in the given level\n"
            " --baby                 Sets the difficulty to baby for tedlevel\n"
            " --easy                 Sets the difficulty to easy for tedlevel\n"
            " --normal               Sets the difficulty to normal for tedlevel\n"
            " --hard                 Sets the difficulty to hard for tedlevel\n"
            " --nowait               Skips intro screens\n"
            " --windowed[-mouse]     Starts the game in a window [and grabs mouse]\n"
            " --bits <b>             Sets the screen color depth\n"
            "                        (use this when you have palette/fading problems\n"
            "                        allowed: 8, 16, 24, 32, default: \"best\" depth)\n"
            " --nodblbuf             Don't use SDL's double buffering\n"
            " --extravbls <vbls>     Sets a delay after each frame, which may help to\n"
            "                        reduce flickering (unit is currently 8 ms, default: 0)\n"
            " --joystick <index>     Use the index-th joystick if available\n"
            "                        (-1 to disable joystick, default: 0)\n"
            " --samplerate <rate>    Sets the sound sample rate (given in Hz, default: %i)\n"
            " --audiobuffer <size>   Sets the size of the audio buffer (-> sound latency)\n"
            "                        (given in bytes, default: 2048 / (44100 / samplerate))\n"
            " --ignorenumchunks      Ignores the number of chunks in VGAHEAD.*\n"
            "                        (may be useful for some broken mods)\n"
            " --configdir <dir>      Directory where config file and save games are stored\n"
#if defined(_arch_dreamcast) || defined(_WIN32)
            "                        (default: current directory)\n"
#else
            "                        (default: $HOME/.wolf4sdl)\n"
#endif
#if defined(SPEAR) && !defined(SPEARDEMO)
            " --mission <mission>    Mission number to play (0-3)\n"
            "                        (default: 0 -> .sod, 1-3 -> .sd*)\n"
            " --goodtimes            Disable copy protection quiz\n"
#endif
            " --nosound              Disable sound\n"
            " --split <players>      Enable split screen for 2-4 players\n"
            " --splitlayout <layout> Choose split screen layout (examples: 2x1, 2x2, 3x1)\n"
            " --splitdatadir <dir>   Choose split screen data directory\n"
            "                        (default: ./lwmp)\n"
            " --res <w> <h>          Sets screen resolution\n"
            " --instagibmode         Enables instagib mode\n"
            " --defusemode           Enables defuse mode\n"
            " --vampiremode          Enables vampire mode\n"
            " --ctfmode              Enables ctf mode\n"
            " --harvestermode        Enables harvester mode\n"
            " --bjmutantmode         Enables bjmutant mode\n"
            " --rampagemode          Enables rampage mode\n"
            " --zombiemode           Enables zombie mode\n"
            " --zombieharvestermode  Enables zombie harvester mode\n"
            , defaultSampleRate
        );
        exit(1);
    }

    if(sampleRateGiven && !audioBufferGiven)
        param_audiobuffer = 2048 / (44100 / param_samplerate);
}

/*
==========================
=
= main
=
==========================
*/

int main (int argc, char *argv[])
{
#if defined(_arch_dreamcast)
    DC_Init();
#else
    CheckParameters(argc, argv);
#endif

    CheckForEpisodes();

    InitGame();

    DemoLoop();

    Quit("Demo loop exited???");
    return 1;
}
