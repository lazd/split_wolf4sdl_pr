// WL_AGENT.C

#include <iostream>
#include "wl_def.h"
#pragma hdrstop

/*
=============================================================================

                                LOCAL CONSTANTS

=============================================================================
*/

#define MAXMOUSETURN    10


#define MOVESCALE       150l
#define BACKMOVESCALE   100l
#define ANGLESCALE      20

/*
=============================================================================

                                GLOBAL VARIABLES

=============================================================================
*/



//
// player state info
//
int32_t         LWMP_DECL(thrustspeed);
vec3fixed_t     LWMP_DECL(thrustvector);

// player coordinates scaled to unsigned
word            LWMP_DECL(plux),LWMP_DECL(pluy);

short           LWMP_DECL(anglefrac);

objtype        *LWMP_DECL(LastAttacker);

/*
=============================================================================

                                                 LOCAL VARIABLES

=============================================================================
*/


void    T_Player (objtype *ob);
void    T_Attack (objtype *ob);

statetype   s_player = {false,SPR_BJ_STAND,0,(statefunc) T_Player,NULL,NULL};
statetype   s_attack = {false,SPR_BJ_STAND,0,(statefunc) T_Attack,NULL,NULL};

statetype   s_playerdie1 = {false,SPR_BJ_DIE_1,15,NULL,(statefunc)A_DeathScream,&s_playerdie2};
statetype   s_playerdie2 = {false,SPR_BJ_DIE_2,15,NULL,NULL,&s_playerdie3};
statetype   s_playerdie3 = {false,SPR_BJ_DIE_3,15,NULL,NULL,&s_playerdie4};
statetype   s_playerdie4 = {false,SPR_BJ_DEAD,120,NULL,(statefunc)RespawnPlayer,&s_playerrespawn1};
statetype   s_playerdead = {false,SPR_BJ_DEAD,0,NULL,NULL,&s_playerdead};
statetype   s_playerrespawn1 = {false,SPR_RESPAWN_1,15,NULL,NULL,&s_playerrespawn2};
statetype   s_playerrespawn2 = {false,SPR_RESPAWN_2,15,NULL,NULL,&s_playerrespawn3};
statetype   s_playerrespawn3 = {false,SPR_RESPAWN_3,15,NULL,NULL,&s_playerrespawn4};
statetype   s_playerrespawn4 = {false,SPR_RESPAWN_4,15,NULL,NULL,&s_playerrespawn5};
statetype   s_playerrespawn5 = {false,SPR_RESPAWN_5,15,NULL,NULL,&s_player};

struct atkinf
{
    int8_t    tics_val, attack, frame;              // attack is 1 for gun, 2 for knife
} attackinfo[NUMWEAPONSEXTRA][14] =
{
    { {6,0,1},{6,2,2},{6,0,3},{6,-1,4} }, // wp_knife
    { {6,0,1},{6,1,2},{6,0,3},{6,-1,4} }, // wp_pistol
    { {6,0,1},{6,1,2},{6,3,3},{6,-1,4} }, // wp_machinegun
    { {6,0,1},{6,1,2},{6,4,3},{6,-1,4} }, // wp_chaingun
    { {6,0,1},{6,1,2},{6,4,3},{6,-1,4} }, // wp_tankgun
    { {6,0,1},{6,2,2},{6,0,3},{6,-1,4} }, // wp_tankwheel
    { {6,0,1},{6,1,2},{6,4,3},{6,-1,4} }, // wp_mg42
    { {6,0,1},{6,2,2},{6,0,3},{6,-1,4} }, // wp_flag
    { {6,0,1},{6,2,2},{6,0,3},{6,-1,4} }, // wp_fists
};

//===========================================================================

//----------

void Attack (void);
void Use (void);
void Search (objtype *ob);
void SelectWeapon (void);
void SelectItem (void);

//----------

boolean TryMove (objtype *ob);
void T_Player (objtype *ob);

void ClipMove (objtype *ob, int32_t xmove, int32_t ymove);

/*
=============================================================================

                                CONTROL STUFF

=============================================================================
*/

/*
======================
=
= CheckWeaponChange
=
= Keys 1-4 change weapons
=
======================
*/

void CheckWeaponChange (void)
{
    Player::checkWeaponChange();
}

/*
======================
=
= InitPlayerColor
=
= Automatically sets player color if unset.
=
======================
*/

void InitPlayerColor(void)
{
    int i;
    int free_shades[COLORREMAP_SHADE_MAX];
    int num_free_shades;

    if (gamestate.player_shade_mp == 0)
    {
        num_free_shades = CalcPlayerShadesFree(free_shades);
        assert(num_free_shades > 0);

        // first player wants grey color if it is available
        if (LWMP_IsFirst())
        {
            for (i = 0; i < num_free_shades; i++)
            {
                if (free_shades[i] == COLORREMAP_SHADE_GREY)
                {
                    break;
                }
            }
            i = (i == num_free_shades ? 0 : i);
            gamestate.player_shade_mp = free_shades[i] + 1;
        }
        else
        {
            gamestate.player_shade_mp = free_shades[0] + 1;
        }
    }
}

/*
======================
=
= CheckColorChange
=
======================
*/

void CheckColorChange (void)
{
    int i;
    int c;
    int shade;

    // init all players colors
    for (LWMP_REPEAT(c))
    {
        InitPlayerColor();
    }

    // each player may cycle to the next color if available
    if(buttonstate_mp[bt_nextcolor] && !buttonheld_mp[bt_nextcolor])
    {
        assert(gamestate.player_shade_mp > 0);

        for (i = 0; i < COLORREMAP_SHADE_MAX; i++)
        {
            shade = (i + (gamestate.player_shade_mp - 1)) % COLORREMAP_SHADE_MAX;

            if (!PlayerShadeIsUsed(shade))
            {
                break;
            }
        }

        if (i != COLORREMAP_SHADE_MAX)
        {
            gamestate.player_shade_mp = shade + 1;
            DrawPlayScreen ();
        }
    }
}


/*
=======================
=
= ControlMovement
=
= Takes controlx,controly, and buttonstate[bt_strafe]
=
= Changes the player's angle and position
=
= There is an angle hack because when going 70 fps, the roundoff becomes
= significant
=
=======================
*/

void ControlMovement (objtype *ob)
{
    Player::controlMovement(ob);
}


/*
=======================
=
= TurnAim
=
=======================
*/

void TurnAim (objtype *ob, int controlx)
{
    anglefrac_mp += controlx;
    const int angleunits = anglefrac_mp/ANGLESCALE;
    anglefrac_mp -= angleunits*ANGLESCALE;
    ob->angle -= angleunits;

    if (ob->angle >= ANGLES)
        ob->angle -= ANGLES;
    if (ob->angle < 0)
        ob->angle += ANGLES;
}


/*
=============================================================================

                            STATUS WINDOW STUFF

=============================================================================
*/


/*
==================
=
= StatusDrawPic
=
==================
*/

void StatusDrawPic (int x, int y, unsigned picnum, int xoff = 0)
{
    const unsigned scy = (y >= 0 ? MaxY - (STATUSLINES - y) : -y - 1);
    LatchDrawPicScaledCoord (x, scy, picnum, xoff);
}

void StatusDrawFace(unsigned picnum)
{
    ColorRemap_SetShade(gamestate.player_shade_mp - 1);
    StatusDrawPic(17, 4, picnum);
    ColorRemap_SetShade(COLORREMAP_SHADE_DEFAULT);

#ifdef _arch_dreamcast
    DC_StatusDrawLCD(picnum);
#endif
}


/*
==================
=
= DrawFace
=
==================
*/

void DrawFace (void)
{
    if(viewsize == 21 && ingame)
    {
        return;
    }

    if (SD_SoundPlaying() == GETGATLINGSND)
    {
        StatusDrawFace(GOTGATLINGPIC);
    }
    else if (gamestate.health_mp)
    {
      #ifdef SPEAR
        if (godmode)
        {
            StatusDrawFace(GODMODEFACE1PIC + gamestate.faceframe_mp);
        }
        else
      #endif
        {
            StatusDrawFace(FACE1APIC + 3 * 
                ((100 - gamestate.health_mp) / 16) + 
                gamestate.faceframe_mp);
        }
    }
    else
    {
      #ifndef SPEAR
        if (LastAttacker_mp && LastAttacker_mp->obclass == needleobj)
        {
            StatusDrawFace(MUTANTBJPIC);
        }
        else
      #endif
        {
            StatusDrawFace(FACE8APIC);
        }
    }
}

/*
===============
=
= UpdateFace
=
= Calls draw face if time to change
=
===============
*/

int LWMP_DECL_VAR(facecount, 0);
int LWMP_DECL_VAR(facetimes, 0);

void UpdateFace (void)
{
    // don't make demo depend on sound playback
    if(demoplayback || demorecord)
    {
        if(facetimes_mp > 0)
        {
            facetimes_mp--;
            return;
        }
    }
    else if(SD_SoundPlaying() == GETGATLINGSND)
        return;

    facecount_mp += tics;
    if (facecount_mp > US_RndT())
    {
        gamestate.faceframe_mp = (US_RndT()>>6);
        if (gamestate.faceframe_mp==3)
            gamestate.faceframe_mp = 1;

        facecount_mp = 0;
        sb(DrawFace ());
    }
}



/*
===============
=
= LatchNumber
=
= right justifies and pads with blanks
=
===============
*/

void LatchNumber (int x, int y, unsigned width, int32_t number)
{
    unsigned length,c;
    char    str[20];

    ltoa (number,str,10);

    length = (unsigned) strlen (str);

    while (length<width)
    {
        StatusDrawPic (x,y,N_BLANKPIC);
        x++;
        width--;
    }

    c = length <= width ? 0 : length-width;

    while (c<length)
    {
        StatusDrawPic (x,y,str[c]-'0'+ N_0PIC);
        x++;
        c++;
    }
}


/*
===============
=
= DrawHealth
=
===============
*/

void DrawHealth (void)
{
    if(viewsize == 21 && ingame)
    {
        return;
    }

    int hp = gamestate.health_mp;
    if (InstagibMode::enabled())
    {
        hp = std::min(hp, 1);
    }

    LatchNumber (21,16,3,hp);
}

/*
===============
=
= TakeDamage
=
===============
*/

void TakeDamage (int points, objtype *attacker)
{
    int i;

    LastAttacker_mp = attacker;

    if (InstagibMode::enabled())
    {
        points = (points > 0 ? 400 : 0);
    }

    if (gamestate.health_mp <= 0 || gamestate.victoryflag)
        return;
    if (gamestate.difficulty==gd_baby)
    {
        points = (points > 0 ? std::max(points >> 2, 1) : 0);
    }

    //if (attacker != NULL && attacker->obclass == bjmutantobj)
    //{
    //    points = (points > 0 ? std::max(points / 2, 1) : 0);
    //}

    if (RampageMode::enabled())
    {
        RampageMode::Player::resistDamage(points);
    }

    if (!godmode)
    {
        gamestate.health_mp -= points;

        SD_PlaySound(TAKEDAMAGESND);

        if (gamestate.pain_frame_mp != 0)
        {
            // switch to another pain frame if already in pain
            gamestate.pain_frame_mp--;
            gamestate.pain_frame_mp = 
                1 - gamestate.pain_frame_mp;
            gamestate.pain_frame_mp++;
        }
        else
        {
            gamestate.damage_tics_mp = 5 + MIN(points, 20);
            gamestate.pain_frame_mp = (US_RndT() & 1) + 1;
        }
    }

    if (gamestate.health_mp<=0)
    {
        gamestate.health_mp = 0;

        if (LWMP_NUM_PLAYERS > 1 && 
            (InfiniteLivesMode::enabled() || gamestate.lives_mp > 0))
        {
            Player::preKill();

            if (!InfiniteLivesMode::enabled())
            {
                gamestate.lives_mp--;
                sb(DrawLives());
            }
            if (attacker != NULL && attacker->obclass == needleobj)
            {
                SpawnBjMutant();
                NewState(player_mp, &s_playerrespawn1);
                RespawnPlayer(player_mp);
            }
            else
            {
                const bool forceRocketGib = InstagibMode::enabled();
                LWMP_GibPreKillActor(player_mp, attacker, forceRocketGib);
                NewState(player_mp, &s_playerdie1);
                player_mp->flags &= ~FL_SHOOTABLE;
            }
        }
        else
        {
            playstate = ex_died;
            killerobj = attacker;
        }
    }

    if (godmode != 2)
        StartDamageFlash (points);

    sb(DrawHealth ());
    sb(DrawFace ());

    //
    // MAKE BJ'S EYES BUG IF MAJOR DAMAGE!
    //
#ifdef SPEAR
    if (points > 30 && gamestate.health_mp!=0 && 
        !godmode && viewsize != 21)
    {
        sb(StatusDrawFace(BJOUCHPIC));
        facecount_mp = 0;
    }
#endif
}

/*
===============
=
= HealSelf
=
===============
*/

void HealSelf (int points)
{
    gamestate.health_mp += points;
    if (gamestate.health_mp>100)
        gamestate.health_mp = 100;

    sb(DrawHealth ());
    sb(DrawFace ());
}


//===========================================================================


/*
===============
=
= DrawLevel
=
===============
*/

void DrawLevel (void)
{
    if(viewsize == 21 && ingame)
    {
        return;
    }
#ifdef SPEAR
    if (gamestate.mapon == 20)
        LatchNumber (2,16,2,18);
    else
#endif
        LatchNumber (2,16,2,gamestate.mapon+1);
}

//===========================================================================


/*
===============
=
= DrawLives
=
===============
*/

void DrawLives (void)
{
    if(viewsize == 21 && ingame)
    {
        return;
    }
    if (InfiniteLivesMode::enabled())
    {
        StatusDrawPic (13,17,N_INFL,3);
        StatusDrawPic (14,17,N_INFR,3);
    }
    else
    {
        LatchNumber (14,16,1,gamestate.lives_mp);
    }
}


/*
===============
=
= GiveExtraMan
=
===============
*/

void GiveExtraMan (void)
{
    if (gamestate.lives_mp<9)
        gamestate.lives_mp++;
    sb(DrawLives ());
    SD_PlaySound (BONUS1UPSND);
}

//===========================================================================

/*
===============
=
= DrawScore
=
===============
*/

void DrawScore (void)
{
    if(viewsize == 21 && ingame)
    {
        return;
    }

    LatchNumber (6,16,6,gamestate.score);
}

/*
===============
=
= GivePoints
=
===============
*/

void GivePoints (int32_t points)
{
    int c;

    gamestate.score += points;
    while (gamestate.score >= gamestate.nextextra)
    {
        gamestate.nextextra += EXTRAPOINTS;
        GiveExtraMan ();
    }
    for (LWMP_REPEAT(c))
    {
        DrawScore ();
    }
}

//===========================================================================

/*
==================
=
= DrawWeapon
=
==================
*/

void DrawWeapon (void)
{
    if(viewsize == 21 && ingame)
    {
        return;
    }

    const weapontype weapon = (player_mp != NULL ?
        Player::weapon() : wp_knife);

    switch (weapon)
    {
    case wp_knife:
    case wp_pistol:
    case wp_machinegun:
    case wp_chaingun:
        StatusDrawPic (32,8,KNIFEPIC + gamestate.weapon_mp);
        break;
    case wp_tankgun:
        StatusDrawPic (32,8,TANKGUNPIC);
        break;
    case wp_tankwheel:
        StatusDrawPic (32,8,TANKWHEELPIC);
        break;
    case wp_mg42:
        StatusDrawPic (32,8,MG42GUNPIC);
        break;
    case wp_flag:
        StatusDrawPic (32,8,FLAGPIC);
        break;
    case wp_fists:
        StatusDrawPic (32,8,FISTSPIC);
        break;
    }
}


/*
==================
=
= DrawKeys
=
==================
*/

void DrawKeys (void)
{
    if(viewsize == 21 && ingame)
    {
        return;
    }

    if (gamestate.keys_mp & 1)
        StatusDrawPic (30,4,GOLDKEYPIC);
    else
        StatusDrawPic (30,4,NOKEYPIC);

    if (gamestate.keys_mp & 2)
        StatusDrawPic (30,20,SILVERKEYPIC);
    else
        StatusDrawPic (30,20,NOKEYPIC);
}

/*
==================
=
= GiveWeapon
=
==================
*/

void GiveWeapon (int weapon_val)
{
    GiveAmmo (6);

    if (weapon_val > gamestate.bestweapon_mp)
    {
        gamestate.bestweapon_mp = gamestate.weapon_mp = 
            gamestate.chosenweapon_mp = (weapontype) weapon_val;
    }

    sb(DrawWeapon ());
}

//===========================================================================

/*
===============
=
= DrawAmmo
=
===============
*/

void DrawAmmo (void)
{
    if(viewsize == 21 && ingame)
    {
        return;
    }

    if (InfiniteAmmoMode::enabled()/* || RampageMode::enabled()*/)
    {
        StatusDrawPic (26,17,N_INFL,3);
        StatusDrawPic (27,17,N_INFR,3);
    }
    else
    {
        LatchNumber (27,16,2,Player::displayAmmo());
    }
}

/*
===============
=
= GiveAmmo
=
===============
*/
void GiveAmmo (int ammo_val)
{
    if (!gamestate.ammo_mp)                            // knife was out
    {
        if (!gamestate.attackframe_mp)
        {
            gamestate.weapon_mp = gamestate.chosenweapon_mp;
            sb(DrawWeapon ());
        }
    }
    gamestate.ammo_mp += ammo_val;
    if (gamestate.ammo_mp > 99)
        gamestate.ammo_mp = 99;
    sb(DrawAmmo ());
}

//===========================================================================

/*
==================
=
= GiveKey
=
==================
*/

void GiveKey (int key)
{
    gamestate.keys_mp |= (1<<key);
    sb(DrawKeys ());

    Autoplay_GotKey();
}

/*
==================
=
= GiveKeys
=
==================
*/

void GiveKeys (int keys)
{
    gamestate.keys_mp |= keys;
    sb(DrawKeys ());
}


/*
=============================================================================

                                MOVEMENT

=============================================================================
*/

/*
===================
=
= SetPlayState
=
===================
*/
void SetPlayState(exit_t newstate)
{
    playstate = newstate;
    if (newstate == ex_completed || newstate == ex_secretlevel)
    {
        levelcompletedby = LWMP_GetInstance();
    }
}


/*
===================
=
= GetBonus
=
===================
*/
void GetBonus (statobj_t *check)
{
    int i;
    int c;
    Backpack_t *backpack;
    weapontype wp;
    short getShape = -1;

    switch (check->itemnumber)
    {
        case    bo_firstaid:
            if (gamestate.health_mp == 100)
                return;

            SD_PlaySound (HEALTH2SND);
            HealSelf (25);
            break;

        case    bo_key1:
        case    bo_key2:
        case    bo_key3:
        case    bo_key4:
            GiveKey (check->itemnumber - bo_key1);
            SD_PlaySound (GETKEYSND);
            break;

        case    bo_cross:
            SD_PlaySound (BONUS1SND);
            sb(GivePoints (100));
            gamestate.treasurecount++;
            break;
        case    bo_chalice:
            SD_PlaySound (BONUS2SND);
            sb(GivePoints (500));
            gamestate.treasurecount++;
            break;
        case    bo_bible:
            SD_PlaySound (BONUS3SND);
            sb(GivePoints (1000));
            gamestate.treasurecount++;
            break;
        case    bo_crown:
            SD_PlaySound (BONUS4SND);
            sb(GivePoints (5000));
            gamestate.treasurecount++;
            break;

        case    bo_clip:
            {
                if (gamestate.ammo_mp == 99)
                    return;

                SD_PlaySound (GETAMMOSND);
                const int ammo = (InstagibMode::enabled() ? 2 : 8);
                GiveAmmo (ammo);
            }
            break;
        case    bo_clip2:
            if (VampireMode::enabled())
            {
                if (!VampireMode::canFeed())
                {
                    return;
                }

                VampireMode::feed();
            }
            else
            {
                if (gamestate.ammo_mp == 99)
                    return;

                SD_PlaySound (GETAMMOSND);
                int ammo = 
                    (
                        InstagibMode::enabled() ? 
                        1 : 4
                    );
                BjMutantMode::scaleAmmo(ammo);
                ZombieMode::scaleAmmo(ammo);
                GiveAmmo (ammo);
            }
            break;

#ifdef SPEAR
        case    bo_25clip:
            if (gamestate.ammo_mp == 99)
                return;

            SD_PlaySound (GETAMMOBOXSND);
            GiveAmmo (25);
            break;
#endif

        case    bo_machinegun:
            SD_PlaySound (GETMACHINESND);
            GiveWeapon (wp_machinegun);
            break;
        case    bo_chaingun:
            SD_PlaySound (GETGATLINGSND);
            facetimes_mp = 38;
            GiveWeapon (wp_chaingun);

            if(viewsize != 21)
            {
                sb(StatusDrawFace (GOTGATLINGPIC));
            }
            facecount_mp = 0;
            break;

        case    bo_fullheal:
            SD_PlaySound (BONUS1UPSND);
            HealSelf (99);
            GiveAmmo (25);
            GiveExtraMan ();
            gamestate.treasurecount++;
            break;

        case    bo_food:
            if (gamestate.health_mp == 100)
                return;

            SD_PlaySound (HEALTH1SND);
            HealSelf (10);
            break;

        case    bo_alpo:
            if (gamestate.health_mp == 100)
                return;

            SD_PlaySound (HEALTH1SND);
            HealSelf (4);
            break;

        case    bo_gibs:
            if (VampireMode::enabled())
            {
                if (!VampireMode::canDrinkBlood())
                    return;

                VampireMode::drinkBlood();
            }
            else
            {
                if (gamestate.health_mp >10)
                    return;

                SD_PlaySound (SLURPIESND);
                HealSelf (1);
            }
            break;

#ifdef SPEAR
        case    bo_spear:
            spearflag = true;
            spearx = player_mp->x;
            speary = player_mp->y;
            spearangle = player_mp->angle;
            SetPlayState(ex_completed);
#endif
        case    bo_backpack:
            for (i = 0; i < MAX_BACKPACKS; i++)
            {
                backpack = &gamestate.backpacks[i];
                if 
                    (
                        backpack->valid &&
                        check->tilex == backpack->tilex &&
                        check->tiley == backpack->tiley
                    )
                {
                    break;
                }
            }
            if (i == MAX_BACKPACKS)
            {
                return;
            }

            for (i = 0; i < wp_max; i++)
            {
                wp = (weapontype)i;
                if (backpack->hasWeapon[i])
                {
                    if (gamestate.bestweapon_mp < wp)
                    {
                        break;
                    }
                }
            }

            if (gamestate.ammo_mp == 99 && i == wp_max && 
                backpack->keys == 0)
            {
                return;
            }

            SD_PlaySound(GETBACKPACKSND);
            if (backpack->ammo != 0)
            {
                GiveAmmo(backpack->ammo);
            }
            if (backpack->keys != 0)
            {
                GiveKeys(backpack->keys);
            }
            for (i = 0; i < wp_max; i++)
            {
                wp = (weapontype)i;
                if (backpack->hasWeapon[i])
                {
                    if (gamestate.bestweapon_mp < wp)
                    {
                        GiveWeapon(wp);
                    }
                }
            }

            memset(backpack, 0, sizeof(Backpack_t));
            break;

        case    bo_puddle:
            if (VampireMode::enabled())
            {
                if (!VampireMode::canDrinkBlood())
                    return;

                VampireMode::drinkBlood();
            }
            else
            {
                return;
            }
            break;

        case    bo_kitchen:
            if (VampireMode::enabled())
            {
                if (!VampireMode::canDrinkBlood())
                    return;

                getShape = SPR_EMPTYKITCHEN;
                VampireMode::drinkBlood(15);
            }
            else
            {
                return;
            }
            break;

        case    bo_rampageclip:
            if (gamestate.ammo_mp == 99)
                return;

            SD_PlaySound (GETRAMPAGEAMMOSND);
            GiveAmmo (4);
            break;
    }

    StartBonusFlash ();
    check->shapenum = getShape;                   // remove from list
    if (getShape != -1)
    {
        check->flags &= ~FL_BONUS;
    }
}

/*
===================
=
= TryMove
=
= returns true if move ok
= debug: use pointers to optimize
===================
*/

boolean TryMove (objtype *ob)
{
    int         xl,yl,xh,yh,x,y;
    objtype    *check;
    int32_t     deltax,deltay;
    int         c;

    xl = (ob->x-PLAYERSIZE) >>TILESHIFT;
    yl = (ob->y-PLAYERSIZE) >>TILESHIFT;

    xh = (ob->x+PLAYERSIZE) >>TILESHIFT;
    yh = (ob->y+PLAYERSIZE) >>TILESHIFT;

#define PUSHWALLMINDIST PLAYERSIZE

    //
    // check for solid walls
    //
    for (y=yl;y<=yh;y++)
    {
        for (x=xl;x<=xh;x++)
        {
            check = actorat[x][y];
            if (check && !ISPOINTER(check))
            {
                if(tilemap[x][y]==64 && x==pwallx && y==pwally)   // back of moving pushwall?
                {
                    switch(pwalldir)
                    {
                        case di_north:
                            if(ob->y-PUSHWALLMINDIST<=(pwally<<TILESHIFT)+((63-pwallpos)<<10))
                                return false;
                            break;
                        case di_west:
                            if(ob->x-PUSHWALLMINDIST<=(pwallx<<TILESHIFT)+((63-pwallpos)<<10))
                                return false;
                            break;
                        case di_east:
                            if(ob->x+PUSHWALLMINDIST>=(pwallx<<TILESHIFT)+(pwallpos<<10))
                                return false;
                            break;
                        case di_south:
                            if(ob->y+PUSHWALLMINDIST>=(pwally<<TILESHIFT)+(pwallpos<<10))
                                return false;
                            break;
                    }
                }
                else return false;
            }
        }
    }

    //
    // check for actors
    //
    if (yl>0)
        yl--;
    if (yh<MAPSIZE-1)
        yh++;
    if (xl>0)
        xl--;
    if (xh<MAPSIZE-1)
        xh++;

    for (y=yl;y<=yh;y++)
    {
        for (x=xl;x<=xh;x++)
        {
            check = actorat[x][y];
            if (ISPOINTER(check) && check != player_mp && 
                (check->flags & FL_SHOOTABLE) )
            {
                const int32_t actorDist = check->actorDist ?
                    check->actorDist : MINACTORDIST;
                deltax = ob->x - check->x;
                if (deltax < -actorDist || deltax > actorDist)
                    continue;
                deltay = ob->y - check->y;
                if (deltay < -actorDist || deltay > actorDist)
                    continue;

                if (Tank::objIsDriver(ob))
                {
                    Tank::addHitObj(check);
                }
                RampageMode::HitObj::next(check);
                return false;
            }
        }
    }
    
    //
    // check for players
    //
    for (LWMP_REPEAT(c))
    {
        if 
            (
                player_mp == ob || 
                (player_mp->flags & FL_SHOOTABLE) == 0
            )
        {
            continue;
        }

        deltax = ob->x - player_mp->x;
        if (deltax < -MINPLAYERDIST || deltax > MINPLAYERDIST)
        {
            continue;
        }

        deltay = ob->y - player_mp->y;
        if (deltay < -MINPLAYERDIST || deltay > MINPLAYERDIST)
        {
            continue;
        }

        LWMP_ENDREPEAT(c);
        return false;
    }

    return true;
}


/*
===================
=
= ClipMove
=
===================
*/

void ClipMove (objtype *ob, int32_t xmove, int32_t ymove)
{
    int32_t    basex,basey;

    basex = ob->x;
    basey = ob->y;

    ob->x = basex+xmove;
    ob->y = basey+ymove;
    if (TryMove (ob))
        return;

#ifndef REMDEBUG
    if (noclip && ob->x > 2*TILEGLOBAL && ob->y > 2*TILEGLOBAL
        && ob->x < (((int32_t)(mapwidth-1))<<TILESHIFT)
        && ob->y < (((int32_t)(mapheight-1))<<TILESHIFT) )
        return;         // walk through walls
#endif

    if (!SD_SoundPlaying())
        SD_PlaySound (HITWALLSND);

    ob->x = basex+xmove;
    ob->y = basey;
    if (TryMove (ob))
        return;

    ob->x = basex;
    ob->y = basey+ymove;
    if (TryMove (ob))
        return;

    ob->x = basex;
    ob->y = basey;
}

//==========================================================================

/*
===================
=
= VictoryTile
=
===================
*/

void VictoryTile (void)
{
#ifndef SPEAR
    SpawnBJVictory ();
#endif

    gamestate.victoryflag = true;
}

/*
===================
=
= Thrust
=
===================
*/

// For player movement in demos exactly as in the original Wolf3D v1.4 source code
static fixed FixedByFracOrig(fixed a, fixed b)
{
    int sign = 0;
    if(b == 65536) b = 65535;
    else if(b == -65536) b = 65535, sign = 1;
    else if(b < 0) b = (-b), sign = 1;

    if(a < 0)
    {
        a = -a;
        sign = !sign;
    }
    fixed res = (fixed)(((int64_t) a * b) >> 16);
    if(sign)
        res = -res;
    return res;
}

void Thrust (int angle, int32_t speed)
{
    int32_t xmove,ymove;
    unsigned offset;

#ifdef WEAPONBOBBLE
   if (gamestate.bobdir_mp != 1) gamestate.bobber_mp++; else gamestate.bobber_mp--;
   if (gamestate.bobber_mp >= 10 || gamestate.bobber_mp <= 0) gamestate.bobdir_mp ^= 1;
   if (gamestate.bobdir2_mp != 1) gamestate.bobber2_mp ++; else gamestate.bobber2_mp --;
   if (gamestate.bobber2_mp >= 20 || gamestate.bobber2_mp <= 0) gamestate.bobdir2_mp ^= 1;
#endif

    //
    // ZERO FUNNY COUNTER IF MOVED!
    //
#ifdef SPEAR
    if (speed)
        funnyticount_mp = 0;
#endif

    thrustspeed_mp += speed;

    //
    // moving bounds speed
    //
    if (speed >= MINDIST*2)
        speed = MINDIST*2-1;

    xmove = DEMOCHOOSE_ORIG_SDL(
                FixedByFracOrig(speed, costable[angle]),
                FixedMul(speed,costable[angle]));
    ymove = DEMOCHOOSE_ORIG_SDL(
                -FixedByFracOrig(speed, sintable[angle]),
                -FixedMul(speed,sintable[angle]));

    thrustvector_mp = vec3fixed_add(thrustvector_mp,
        vec3fixed(xmove, -ymove, 0));

    if (RampageMode::enabled())
    {
        RampageMode::Player::clipMove(xmove, ymove);
    }
    else
    {
        ClipMove(player_mp,xmove,ymove);
    }

    player_mp->tilex = (short)(player_mp->x >> TILESHIFT);                // scale to tile values
    player_mp->tiley = (short)(player_mp->y >> TILESHIFT);

    offset = (player_mp->tiley<<mapshift)+player_mp->tilex;
    player_mp->areanumber = *(mapsegs[0] + offset) -AREATILE;

    if (*(mapsegs[1] + offset) == EXITTILE)
        VictoryTile ();
        
    #ifdef ENDOBJ
    if (*(mapsegs[1] + offset) == EXITLEVEL) 
    {
        SetPlayState(ex_completed);
    }
    #endif 
}

/*
=============================================================================

                                ACTIONS

=============================================================================
*/


/*
===============
=
= Cmd_Fire
=
===============
*/

void Cmd_Fire (void)
{
    buttonheld_mp[bt_attack] = true;

    gamestate.weaponframe_mp = 0;

    player_mp->state = &s_attack;

    const weapontype weapon = Player::weapon();

    gamestate.attackframe_mp = 0;
    gamestate.attackcount_mp =
        attackinfo[weapon][gamestate.attackframe_mp].tics_val;
    gamestate.attacktics_mp = 0;
    gamestate.weaponframe_mp =
        attackinfo[weapon][gamestate.attackframe_mp].frame;
}

//===========================================================================

/*
===============
=
= Cmd_Use
=
===============
*/

void Cmd_Use (void)
{
    int     checkx,checky,doornum,dir;
    boolean elevatorok;
    #ifdef HAVOC
    statobj_t* statptr;    // Added by Havoc for interactive objects
    #endif
    objtype *checkobj;
    statobj_t *statptr;

    const int viewAngle = Player::viewAngle(player_mp);
    //
    // find which cardinal direction the player is facing
    //
    if (viewAngle < ANGLES/8 || viewAngle > 7*ANGLES/8)
    {
        checkx = player_mp->tilex + 1;
        checky = player_mp->tiley;
        dir = di_east;
        elevatorok = true;
    }
    else if (viewAngle < 3*ANGLES/8)
    {
        checkx = player_mp->tilex;
        checky = player_mp->tiley-1;
        dir = di_north;
        elevatorok = false;
    }
    else if (viewAngle < 5*ANGLES/8)
    {
        checkx = player_mp->tilex - 1;
        checky = player_mp->tiley;
        dir = di_west;
        elevatorok = true;
    }
    else
    {
        checkx = player_mp->tilex;
        checky = player_mp->tiley + 1;
        dir = di_south;
        elevatorok = false;
    }

    for (statptr = &statobjlist[0]; statptr != laststatobj; statptr++)
    {
        if (statptr->tilex == checkx && statptr->tiley == checky &&
            !buttonheld_mp[bt_use])
        {
            VampireMode::checkUse(statptr);
        }
    }

    doornum = tilemap[checkx][checky];
    if (*(mapsegs[1]+(checky<<mapshift)+checkx) == PUSHABLETILE)
    {
        //
        // pushable wall
        //

        PushWall (checkx,checky,dir);
        return;
    }
    if (!buttonheld_mp[bt_use] && doornum == ELEVATORTILE && elevatorok)
    {
        //
        // use elevator
        //
        buttonheld_mp[bt_use] = true;

        tilemap[checkx][checky]++;              // flip switch
        if (*(mapsegs[0]+(player_mp->tiley<<mapshift)+player_mp->tilex) == ALTELEVATORTILE)
            SetPlayState(ex_secretlevel);
        else
            SetPlayState(ex_completed);
        SD_PlaySound (LEVELDONESND);
        SD_WaitSoundDone();
    }
    else if (!buttonheld_mp[bt_use] && doornum & 0x80)
    {
        buttonheld_mp[bt_use] = true;
        OperateDoor (doornum & ~0x80);
    }
    else if (!buttonheld_mp[bt_use] && CtfFlag::drop())
    {
        buttonheld_mp[bt_use] = true;
    }
    else if (!buttonheld_mp[bt_use] && Tank::objIsPassenger(player_mp))
    {
        buttonheld_mp[bt_use] = true;
        Tank::exit(false);
    }
    else if (!buttonheld_mp[bt_use] && Tank::checkUse(checkobj))
    {
        buttonheld_mp[bt_use] = true;
        Tank::use(checkobj);
    }
    else if (!buttonheld_mp[bt_use] && Mg42::objIsPassenger(player_mp))
    {
        buttonheld_mp[bt_use] = true;
        Mg42::exit(false);
    }
    else if (!buttonheld_mp[bt_use] && Mg42::checkUse(checkobj))
    {
        buttonheld_mp[bt_use] = true;
        Mg42::use(checkobj);
    }
    else
        SD_PlaySound (DONOTHINGSND);
}


/*
=============================================================================

                                PLAYER CONTROL

=============================================================================
*/

// player spawn tiles
//
// 43 42 41 40 39 38 37
// 44 21 20 19 18 17 36
// 45 22 07 06 05 16 35
// 46 23 08 00 04 15 34 - ring 0; len 0; side 0; max 0
// 47 24 01 02 03 14 33 - ring 1; len 8; side 3; max 8
// 48 09 10 11 12 13 32 - ring 2; len 16; side 5; max 24
// 25 26 27 28 29 30 31 - ring 3; len 24; side 7; max 48
// ...
// xx xx xx xx xx xx xx - ring n > 0; len (n * 8); side (n * 2) + 1; max sum{ k in [0, n] } * 8
//
static bool NextPlayerSpawnTile(int tilenum, int *tilex, int *tiley)
{
    int ring;
    int side;
    int max;
    int min;
    int ring_pos;
    const int max_ring = 8;

    if (tilenum == 0)
    {
        *tilex = *tiley = 0;
        return true;
    }

    // compute ring
    side = 1;
    max = 0;
    for (ring = 0; ring < max_ring; ring++)
    {
        if (tilenum <= max)
        {
            ring_pos = tilenum - min;
            if (ring_pos < side)
            {
                *tilex = -ring + ring_pos;
                *tiley = -ring;
            }
            else if (ring_pos < side * 2 - 1)
            {
                *tilex = ring;
                *tiley = -ring + 1 + ring_pos - side;
            }
            else if (ring_pos < side * 3 - 2)
            {
                *tilex = ring - 1 - (ring_pos - (side * 2 - 1));
                *tiley = ring;
            }
            else
            {
                *tilex = -ring;
                *tiley = ring - 1 - (ring_pos - (side * 3 - 2));
            }
            return true;
        }
        min = max + 1;
        max += (ring + 1) * 8;
        side += 2;
    }

    return false;
}

/*
===============
=
= SpawnPlayer
=
===============
*/

void SpawnPlayer (int tilex, int tiley, int dir, 
    int used_pspawn[LWMP_MAX_PLAYERS])
{
    int i, j;
    int player_tile;
    int player_tilex, player_tiley;
    int floorcode, actorcode;
    bool found_pspawn;

    // attempt to find spawn tile for player
    found_pspawn = false;
    for (i = 0; NextPlayerSpawnTile(i, &player_tilex, &player_tiley); i++)
    {
        player_tilex += tilex;
        player_tiley += tiley;
        if (!TILE_IN_MAP(player_tilex, player_tiley))
        {
            continue;
        }

        player_tile = player_tilex + (player_tiley << mapshift);
        floorcode = (int)*(mapsegs[0] + player_tile);
        actorcode = (int)*(mapsegs[1] + player_tile);

        for (j = 0; j < LWMP_NUM_PLAYERS; j++)
        {
            if (used_pspawn[j] == player_tile + 1)
            {
                break;
            }
        }

        if 
            (
                // first player gets the tile no questions asked
                LWMP_GetInstance() == 0 ||
                (
                    // empty tile
                    floorcode >= AREATILE &&
                    // no actors or player spawn or non-blocking actor
                    (
                        actorcode == 0 || 
                        (actorcode >= 19 && actorcode <= 22) ||
                        (
                            actorcode >= 23 && actorcode <= 74 &&
                            statinfo[actorcode - 23].type != block
                        )
                    ) &&
                    // no other players use this tile
                    j == LWMP_NUM_PLAYERS
                )
            )
        {
            tilex = player_tilex;
            tiley = player_tiley;
            used_pspawn[LWMP_GetInstance()] = player_tile + 1;
            found_pspawn = true;
            break;
        }
    }

    if (!found_pspawn)
    {
        Quit("Cannot find player spawn tile for player %d!", LWMP_GetInstance() + 1);
    }

    player_mp->obclass = playerobj;
    player_mp->active = ac_yes;
    player_mp->tilex = tilex;
    player_mp->tiley = tiley;
    player_mp->areanumber = (byte) *(mapsegs[0]+(player_mp->tiley<<mapshift)+player_mp->tilex);
    player_mp->x = ((int32_t)tilex<<TILESHIFT)+TILEGLOBAL/2;
    player_mp->y = ((int32_t)tiley<<TILESHIFT)+TILEGLOBAL/2;
    player_mp->state = &s_player;
    player_mp->angle = (1-dir)*90;
    if (player_mp->angle<0)
        player_mp->angle += ANGLES;
    player_mp->flags = FL_SHOOTABLE|FL_NEVERMARK;
    Thrust (0,0);                           // set some variables
    player_mp->run_dist = 0;
    gamestate.player_spawnx_mp = tilex;
    gamestate.player_spawny_mp = tiley;
    gamestate.player_spawndir_mp = dir;

    InitAreas ();
}

/*
===============
=
= FindRespawnTile
=
===============
*/
static void FindRespawnTile(int tilex, int tiley, int *spawnx,
    int *spawny)
{
    int i, c;
    int player_tile;
    int player_tilex, player_tiley;
    int floorcode, actorcode;
    bool found_pspawn;
    bool found_player;

    // attempt to find spawn tile for player
    found_pspawn = false;
    for (i = 0; NextPlayerSpawnTile(i, &player_tilex, &player_tiley); i++)
    {
        player_tilex += tilex;
        player_tiley += tiley;
        if (!TILE_IN_MAP(player_tilex, player_tiley))
        {
            continue;
        }

        player_tile = player_tilex + (player_tiley << mapshift);
        floorcode = (int)*(mapsegs[0] + player_tile);
        actorcode = (int)*(mapsegs[1] + player_tile);

        found_player = false;
        for (LWMP_REPEAT(c))
        {
            if (c == 0)
            {
                continue;
            }

            if (player_mp->tilex == player_tilex &&
                player_mp->tiley == player_tiley)
            {
                found_player = true;
                LWMP_ENDREPEAT(c);
                break;
            }
        }

        if 
            (
                // empty tile
                floorcode >= AREATILE &&
                // no actors or non-blocking actor
                (
                    actorcode == 0 || 
                    (
                        actorcode >= 23 && actorcode <= 74 &&
                        statinfo[actorcode - 23].type != block
                    )
                ) &&
                // no other players use this tile
                !found_player
            )
        {
            tilex = player_tilex;
            tiley = player_tiley;
            found_pspawn = true;
            break;
        }
    }

    if (!found_pspawn)
    {
        Quit("Cannot find player respawn tile for player %d!",
            LWMP_GetInstance() + 1);
    }

    *spawnx = tilex;
    *spawny = tiley;
}

/*
===============
=
= RespawnPlayer
=
===============
*/

void RespawnPlayer (objtype *ob)
{
    int i;
    int tilex, tiley, dir;
    bool keepKeys;
    Backpack_t *backpack;
    Backpack_t filledBackpack;
    objtype *oldob;

    tilex = player_mp->tilex;
    tiley = player_mp->tiley;

    // spawn dead body
    oldob = actorat[tilex][tiley];
    SpawnNewObj(tilex, tiley, &s_playerdead);
    newobj->x = player_mp->x;
    newobj->y = player_mp->y;
    newobj->flags |= FL_NONMARK | FL_NEVERMARK;
    newobj->obclass = bjobj;
    newobj->player_index = LWMP_GetInstance();
    newobj->gibbed = ob->gibbed;
    newobj->gibActorId = ob->gibActorId;
    newobj->gibMethod = ob->gibMethod;
    actorat[tilex][tiley] = oldob;

    // reset fall animation
    PL_ResetFallAnim();

    if (LastAttacker_mp == NULL || 
        LastAttacker_mp->obclass != needleobj)
    {
        PL_FillBackpack(&filledBackpack);
        SpawnBackpack(player_mp->tilex, player_mp->tiley, 
            filledBackpack);
        keepKeys = true;
    }
    else
    {
        keepKeys = true;
    }

    // respawn player
    PlaySoundLocActor(BJRESPAWNSND,ob);

    FindRespawnTile(gamestate.player_spawnx_mp, 
        gamestate.player_spawny_mp, &tilex, &tiley);
    dir = gamestate.player_spawndir_mp;

    player_mp->obclass = playerobj;
    player_mp->active = ac_yes;
    player_mp->tilex = tilex;
    player_mp->tiley = tiley;
    player_mp->areanumber = (byte) *(mapsegs[0]+(player_mp->tiley<<mapshift)+player_mp->tilex);
    player_mp->x = ((int32_t)tilex<<TILESHIFT)+TILEGLOBAL/2;
    player_mp->y = ((int32_t)tiley<<TILESHIFT)+TILEGLOBAL/2;
    plux_mp = (word)(player_mp->x >> UNSIGNEDSHIFT);
    pluy_mp = (word)(player_mp->y >> UNSIGNEDSHIFT);
    player_mp->angle = (1-dir)*90;
    if (player_mp->angle<0)
        player_mp->angle += ANGLES;
    player_mp->flags = FL_SHOOTABLE|FL_NEVERMARK;
    Thrust (0,0);                           // set some variables
    player_mp->run_dist = 0;
    player_mp->gibbed = false;

    // lose the weapons and ammo
    gamestate.health_mp = 100;
    gamestate.weapon_mp = gamestate.bestweapon_mp
        = gamestate.chosenweapon_mp = wp_pistol;
    int ammo = STARTAMMO;
    ZombieMode::scaleAmmo(ammo);
    gamestate.ammo_mp = ammo;
    if (AllWeaponsMode::enabled())
    {
        gamestate.weapon_mp = gamestate.bestweapon_mp
            = gamestate.chosenweapon_mp = wp_chaingun;
    }
    if (InfiniteAmmoMode::enabled())
    {
        gamestate.ammo_mp = 99;
    }
    if (!keepKeys)
    {
        gamestate.keys_mp = 0;
    }
    gamestate.attackframe_mp = gamestate.attackcount_mp =
        gamestate.attacktics_mp = gamestate.weaponframe_mp = 0;

    PL_ResetHurtAnim();
    thrustspeed_mp = 0;
    thrustvector_mp = vec3fixed_zero();

    sb(DrawHealth ());
    sb(DrawKeys ());
    sb(DrawWeapon ());
    sb(DrawAmmo ());
    sb(DrawFace ());
}


//===========================================================================

/*
===============
=
= T_KnifeAttack
=
= Update player hands, and try to do damage when the proper frame is reached
=
===============
*/

void    KnifeAttack (objtype *ob)
{
    objtype *check,*closest;
    int32_t  dist;
    int      c;
    int      damage;
    fixed    reach;
    int      atkshootdelta = shootdelta_mp;
    soundnames missedsnd = (soundnames)0;
    soundnames hitsnd = (soundnames)0;

    switch (Player::weapon())
    {
    case wp_flag:
        damage = (US_RndT() >> 3) + 50;
        reach = 0x20000l;
        missedsnd = ATKFLAGMISSEDSND;
        hitsnd = ATKFLAGSND;
        break;
    case wp_fists:
        if (gamestate.ammo_mp > 0)
        {
            damage = Gib::Constants::instantLimit;
        }
        else
        {
            damage = (US_RndT() >> 3) + 50;
        }
        reach = 0x20000l;
        atkshootdelta *= 2;
        missedsnd = ATKPUNCHMISSEDSND;
        hitsnd = ATKPUNCHHITSND;
        break;
    default:
        SD_PlaySound (ATKKNIFESND);
        damage = US_RndT() >> 4;
        reach = 0x18000l;
        break;
    }

    // actually fire
    dist = 0x7fffffff;
    closest = NULL;
    for (check = objlist; check; check = check->next)
    {
        if (check == player_mp)
        {
            continue;
        }

        if ( (check->flags & FL_SHOOTABLE) && (check->visflags_mp & FL_VISABLE)
            && abs(check->viewx_mp - centerx_mp) < atkshootdelta)
        {
            if (check->transx_mp < dist)
            {
                dist = check->transx_mp;
                closest = check;
            }
        }
    }

    if (!closest || dist > reach)
    {
        // missed
        if (missedsnd != 0)
        {
            SD_PlaySound (missedsnd);
        }
        return;
    }

    if (ObjectIsPlayer(closest))
    {
        for (LWMP_REPEAT_ONCE(c, ObjectPlayerIndex(closest)))
        {
            TakeDamage (damage, ob);
        }
    }
    else
    {
        // hit something
#ifndef KNIFEKILL 
        DamageActor (closest,damage);
#else 
        if (closest->flags & FL_ATTACKMODE) // hit something 
            DamageActor (closest,damage); 
        else // backstabbed him; he's outta there 
            KillActor (closest); 
#endif 
        if (gamestate.ammo_mp > 0)
        {
            RampageMode::launch(closest);
        }
    }

    if (hitsnd)
    {
        SD_PlaySound (hitsnd);
    }
}



void    GunAttack (objtype *ob)
{
    objtype *check,*closest,*oldclosest;
    int      damage;
    int      dx,dy,dist;
    int32_t  viewdist, minviewdist;
    int      c;
    vec3fixed_t playeranglevec, lineofsightvec;
    bool     firstHit;

    switch (Player::weapon())
    {
        case wp_pistol:
            SD_PlaySound (ATKPISTOLSND);
            break;
        case wp_machinegun:
            SD_PlaySound (ATKMACHINEGUNSND);
            break;
        case wp_chaingun:
            SD_PlaySound (ATKGATLINGSND);
            break;
        case wp_tankgun:
            SD_PlaySound (ATKTANKGUNSND);
            break;
        case wp_mg42:
            SD_PlaySound (ATKMG42SND);
            break;
    }

    madenoise_mp = true;
    playeranglevec = vec3fixed_anglevec(
        (int32_t)Player::viewAngle(player_mp));
    minviewdist = 0;
    firstHit = true;

nexthit:
    //
    // find potential targets
    //
    viewdist = 0x7fffffffl;
    closest = NULL;

    for (check = objlist; check; check = check->next)
    {
        if (check == player_mp)
        {
            continue;
        }

        if ((check->flags & FL_SHOOTABLE) && 
            (check->visflags_mp & FL_VISABLE) &&
            abs(check->viewx_mp - centerx_mp) < shootdelta_mp)
        {
            if (check->transx_mp < viewdist &&
                check->transx_mp > minviewdist)
            {
                lineofsightvec = vec3fixed(check->x - 
                    player_mp->x, player_mp->y - check->y, 0);
                if (vec3fixed_dot(lineofsightvec, 
                    playeranglevec) > 0 && 
                    CheckLine(check))
                {
                    viewdist = check->transx_mp;
                    closest = check;
                }
            }
        }
    }

    if (closest == NULL)
        return;

    //
    // hit something
    //
    dx = ABS(closest->tilex - player_mp->tilex);
    dy = ABS(closest->tiley - player_mp->tiley);
    dist = dx>dy ? dx:dy;
    if (dist<2)
        damage = US_RndT() / 4;
    else if (dist<4)
        damage = US_RndT() / 6;
    else
    {
        if ( (US_RndT() / 12) < dist)           // missed
            return;
        damage = US_RndT() / 6;
    }

    if (!firstHit)
    {
        // 50% damage reduction on further hits
        damage >>= 1;
    }
    firstHit = false;

    if (ObjectIsPlayer(closest))
    {
        for (LWMP_REPEAT_ONCE(c, ObjectPlayerIndex(closest)))
        {
            TakeDamage (damage, ob);
        }
    }
    else
    {
        const weapontype wp = Player::weapon();
        const bool allowNextPass = (wp == wp_chaingun || wp == wp_tankgun ||
            wp == wp_mg42);
        if (DamageActor (closest,damage) && allowNextPass)
        {
            minviewdist = viewdist;
            goto nexthit;
        }
    }
}

//===========================================================================

/*
===============
=
= VictorySpin
=
===============
*/

void VictorySpin (void)
{
    int32_t    desty;

    if (!player_mp->victoryBjSpawn)
    {
        return;
    }

    if (player_mp->angle > 270)
    {
        player_mp->angle -= (short)(tics * 3);
        if (player_mp->angle < 270)
            player_mp->angle = 270;
    }
    else if (player_mp->angle < 270)
    {
        player_mp->angle += (short)(tics * 3);
        if (player_mp->angle > 270)
            player_mp->angle = 270;
    }

    desty = (((int32_t)player_mp->tiley-5)<<TILESHIFT)-0x3000;

    if (player_mp->y > desty)
    {
        player_mp->y -= tics*4096;
        if (player_mp->y < desty)
            player_mp->y = desty;
    }
}


//===========================================================================

/*
===============
=
= T_Attack
=
===============
*/

void    T_Attack (objtype *ob)
{
    struct  atkinf  *cur;

    UpdateFace ();

    if (gamestate.victoryflag)              // watching the BJ actor
    {
        VictorySpin ();
        return;
    }

    if ( buttonstate_mp[bt_use] && !buttonheld_mp[bt_use] )
        buttonstate_mp[bt_use] = false;

    if ( buttonstate_mp[bt_attack] && !buttonheld_mp[bt_attack])
        buttonstate_mp[bt_attack] = false;

    ControlMovement (ob);
    if (gamestate.victoryflag)              // watching the BJ actor
        return;

    if (RampageMode::enabled())
    {
        RampageMode::Player::drainAmmo();
    }

    plux_mp = (word) (player_mp->x >> UNSIGNEDSHIFT);                     // scale to fit in unsigned
    pluy_mp = (word) (player_mp->y >> UNSIGNEDSHIFT);
    player_mp->tilex = (short)(player_mp->x >> TILESHIFT);                // scale to tile values
    player_mp->tiley = (short)(player_mp->y >> TILESHIFT);

    //
    // change frame and fire
    //
    gamestate.attackcount_mp -= (short) tics;
    gamestate.attacktics_mp += (short) tics;
    while (gamestate.attackcount_mp <= 0)
    {
        cur = &attackinfo[Player::weapon()][gamestate.attackframe_mp];
        switch (cur->attack)
        {
            case -1:
                ob->state = &s_player;
                gamestate.attackframe_mp = gamestate.weaponframe_mp = 0;
                if (Player::noWeaponSwitch())
                {
                    return;
                }
                if (!Player::ammo())
                {
                    gamestate.weapon_mp = wp_knife;
                    sb(DrawWeapon ());
                }
                else
                {
                    if (gamestate.weapon_mp != gamestate.chosenweapon_mp)
                    {
                        gamestate.weapon_mp = gamestate.chosenweapon_mp;
                        sb(DrawWeapon ());
                    }
                }
                return;

            case 4:
                if (!Player::ammo())
                    break;
                if (buttonstate_mp[bt_attack])
                    gamestate.attackframe_mp -= 2;
            case 1:
                if (!Player::ammo())
                {       // can only happen with chain gun
                    gamestate.attackframe_mp++;
                    break;
                }
                GunAttack (ob);
                if (!ammocheat && !InfiniteAmmoMode::enabled())
                    Player::ammo()--;
                sb(DrawAmmo ());
                break;

            case 2:
                KnifeAttack (ob);
                break;

            case 3:
                if (Player::ammo() && buttonstate_mp[bt_attack])
                    gamestate.attackframe_mp -= 2;
                break;
        }

        gamestate.attackcount_mp += cur->tics_val;
        gamestate.attackframe_mp++;
        gamestate.weaponframe_mp =
            attackinfo[Player::weapon()][gamestate.attackframe_mp].frame;
    }
}



//===========================================================================

/*
===============
=
= T_Player
=
===============
*/

void    T_Player (objtype *ob)
{
    if (gamestate.victoryflag)              // watching the BJ actor
    {
        VictorySpin ();
        return;
    }

    UpdateFace ();
    CheckWeaponChange ();
    CheckColorChange ();
    LWMP_CheckMinimapKeys();

    if (gamestate.health_mp <= 0)
    {
        return;
    }

    if ( buttonstate_mp[bt_use] )
        Cmd_Use ();

    if ( buttonstate_mp[bt_attack] && !buttonheld_mp[bt_attack] &&
        !Tank::objIsDriver(ob) &&
        (!Player::weaponRequiresAmmo() || Player::ammo() > 0))
    {
        Cmd_Fire ();
    }

    ControlMovement (ob);
    if (gamestate.victoryflag)              // watching the BJ actor
        return;

    if (RampageMode::enabled())
    {
        RampageMode::Player::drainAmmo();
    }

    plux_mp = (word) (player_mp->x >> UNSIGNEDSHIFT);                     // scale to fit in unsigned
    pluy_mp = (word) (player_mp->y >> UNSIGNEDSHIFT);
    player_mp->tilex = (short)(player_mp->x >> TILESHIFT);                // scale to tile values
    player_mp->tiley = (short)(player_mp->y >> TILESHIFT);
}

//===========================================================================

namespace Player
{
    short &ammo(void)
    {
        if (Tank::objIsGunner(player_mp))
        {
            return Tank::ammo();
        }
        else if (Mg42::objIsPassenger(player_mp))
        {
            return Mg42::ammo();
        }
        
        return gamestate.ammo_mp;
    }
    
    short displayAmmo(void)
    {
        if (player_mp != NULL && Tank::objIsGunner(player_mp))
        {
            return Tank::displayAmmo();
        }
        if (player_mp != NULL && Mg42::objIsPassenger(player_mp))
        {
            return Mg42::displayAmmo();
        }
        
        return gamestate.ammo_mp;
    }

    weapontype weapon(void)
    {
        if (RampageMode::enabled())
        {
            return wp_fists;
        }

        if (player_mp == NULL)
        {
            return wp_pistol;
        }

        if (Tank::objIsPassenger(player_mp))
        {
            return Tank::weapon();
        }
        if (Mg42::objIsPassenger(player_mp))
        {
            return Mg42::weapon();
        }
        if (CtfFlag::objIsCarrier(player_mp))
        {
            return wp_flag;
        }

        return gamestate.weapon_mp;
    }

    bool weaponRequiresAmmo(void)
    {
        const weapontype wp = weapon();
        return wp != wp_knife && wp != wp_flag && wp != wp_fists;
    }

    bool noWeaponSwitch(void)
    {
        return Tank::objIsPassenger(player_mp) || 
            Mg42::objIsPassenger(player_mp) ||
            CtfFlag::objIsCarrier(player_mp);
    }

    void checkWeaponChange(void)
    {
        int newWeapon = -1;

        if (Tank::objIsPassenger(player_mp))
        {
            Tank::checkWeaponChange();
            return;
        }
        else if (Mg42::objIsPassenger(player_mp))
        {
            return;
        }
        else if (CtfFlag::objIsCarrier(player_mp))
        {
            return;
        }

        if (!gamestate.ammo_mp)
            return;

        if(buttonstate_mp[bt_nextweapon] && !buttonheld_mp[bt_nextweapon])
        {
            newWeapon = gamestate.weapon_mp + 1;
            if(newWeapon > gamestate.bestweapon_mp) newWeapon = 0;
        }
        else if(buttonstate_mp[bt_prevweapon] && !buttonheld_mp[bt_prevweapon])
        {
            newWeapon = gamestate.weapon_mp - 1;
            if(newWeapon < 0) newWeapon = gamestate.bestweapon_mp;
        }
        else
        {
            for(int i = wp_knife; i <= gamestate.bestweapon_mp; i++)
            {
                if (buttonstate_mp[bt_readyknife + i - wp_knife])
                {
                    newWeapon = i;
                    break;
                }
            }
        }

        if(newWeapon != -1)
        {
            gamestate.weapon_mp = gamestate.chosenweapon_mp = 
                (weapontype) newWeapon;
            sb(DrawWeapon());
        }
    }

    fixed camZ(void)
    {
        if (Tank::objIsPassenger(player_mp))
        {
            return Tank::camZ();
        }
        else if (Mg42::objIsPassenger(player_mp))
        {
            return Mg42::camZ();
        }

        return gamestate.player_camz_mp;
    }

    void preKill(void)
    {
        if (Tank::objIsPassenger(player_mp))
        {
            Tank::exit(true);
        }
        else if (Mg42::objIsPassenger(player_mp))
        {
            Mg42::exit(true);
        }
        else if (CtfFlag::objIsCarrier(player_mp))
        {
            CtfFlag::drop();
        }
    }

    void controlMovement(objtype *ob)
    {
        int32_t oldx,oldy;
        int     angle;
        int     angleunits;
        vec3fixed_t playeranglevec;

        if (Tank::objIsPassenger(ob))
        {
            Tank::controlMovement(ob);
            return;
        }
        if (Mg42::objIsPassenger(ob))
        {
            Mg42::controlMovement(ob);
            return;
        }

        thrustspeed_mp = 0;
        thrustvector_mp = vec3fixed_zero();

        oldx = player_mp->x;
        oldy = player_mp->y;

        int joyx, joyy;
        int32_t runspeed = RUNMOVE * MOVESCALE * tics;
        int32_t walkspeed = BASEMOVE * MOVESCALE * tics;
        bool joystrafeleft = false;
        bool joystraferight = false;
        if (IN_JoyPresent(joyIndex_mp))
        {
            IN_GetJoyDelta (&joyx, &joyy, joyIndex_mp, SDL_CONTROLLER_AXIS_LEFTX, SDL_CONTROLLER_AXIS_RIGHTY, false);
            float joyfactor = abs(joyx) / 127.0;

            joystrafeleft = joyx < -JOYDEADZONE;
            joystraferight = joyx > JOYDEADZONE;

            // Only adjust run speed if we're controlling with the joystick
            // Otherewise, keyboard strafe won't work at the same time as joystick
            if (joystrafeleft || joystraferight)
            {
                runspeed = runspeed * joyfactor;
                walkspeed = walkspeed * joyfactor;
            }
        }

        bool strafeleft = buttonstate_mp[bt_strafeleft] || joystrafeleft;
        bool straferight = buttonstate_mp[bt_straferight] || joystraferight;

        if(strafeleft)
        {
            angle = ob->angle + ANGLES/4;
            if(angle >= ANGLES)
                angle -= ANGLES;
        }

        if(straferight)
        {
            angle = ob->angle - ANGLES/4;
            if(angle < 0)
                angle += ANGLES;
        }

        if (strafeleft || straferight)
        {
            if(buttonstate_mp[bt_run])
                Thrust(angle, runspeed);
            else
                Thrust(angle, walkspeed);
        }

        //
        // side to side move
        //
        if (buttonstate_mp[bt_strafe])
        {
            //
            // strafing
            //
            //
            if (controlx_mp > 0)
            {
                angle = ob->angle - ANGLES/4;
                if (angle < 0)
                    angle += ANGLES;
                Thrust (angle,controlx_mp*MOVESCALE);      // move to left
            }
            else if (controlx_mp < 0)
            {
                angle = ob->angle + ANGLES/4;
                if (angle >= ANGLES)
                    angle -= ANGLES;
                Thrust (angle,-controlx_mp*MOVESCALE);     // move to right
            }
        }
        else
        {
            //
            // not strafing
            //
            anglefrac_mp += controlx_mp;
            angleunits = anglefrac_mp/ANGLESCALE;
            anglefrac_mp -= angleunits*ANGLESCALE;
            ob->angle -= angleunits;

            if (ob->angle >= ANGLES)
                ob->angle -= ANGLES;
            if (ob->angle < 0)
                ob->angle += ANGLES;
        }

        //
        // forward/backwards move
        //
        if (controly_mp < 0)
        {
            Thrust (ob->angle,-controly_mp*MOVESCALE); // move forwards
        }
        else if (controly_mp > 0)
        {
            angle = ob->angle + ANGLES/2;
            if (angle >= ANGLES)
                angle -= ANGLES;
            Thrust (angle,controly_mp*BACKMOVESCALE);          // move backwards
        }

        playeranglevec = vec3fixed_anglevec((int32_t)player_mp->angle);
        if (vec3fixed_dot(thrustvector_mp, playeranglevec) < 0)
        {
            player_mp->run_dist -= thrustspeed_mp;
            while (player_mp->run_dist < 0)
            {
                player_mp->run_dist += FP(256);
            }
        }
        else
        {
            player_mp->run_dist += thrustspeed_mp;
            while (player_mp->run_dist >= FP(256))
            {
                player_mp->run_dist -= FP(256);
            }
        }

        if (thrustspeed_mp == 0)
        {
            if (gamestate.moveanimtics_mp == 0)
            {
                // reset running distance when BJ stops moving
                ob->run_dist = 0;
            }
        }
        else
        {
            gamestate.moveanimtics_mp = 15;
        }

        if (gamestate.moveanimtics_mp != 0)
        {
            gamestate.moveanimtics_mp = std::max(
                gamestate.moveanimtics_mp - (int32_t)tics, 0);
        }

        if (gamestate.victoryflag)              // watching the BJ actor
            return;
    }

    short viewAngle(objtype *ob)
    {
        if (Tank::objIsPassenger(ob))
        {
            return Tank::viewAngle(ob);
        }

        return ob->angle;
    }

    int weaponShape(void)
    {
        int shapenum = -1;

        if (Tank::objIsDriver(player_mp))
        {
            return Tank::weaponShape();
        }

        if (gamestate.health_mp > 0)
        {
            shapenum = weaponscale[weapon()] + gamestate.weaponframe_mp;
        }
        else
        {
            if (gamestate.player_fall_mp.bounce == 2)
            {
                shapenum = SPR_BJ_FALL;
            }
        }

        return shapenum;
    }

    const lwlib::Vector2f viewDir(void)
    {
        const short ang = viewAngle(player_mp);
        return mat_xform(lwlib::mat2f_rot(-ang), lwlib::vec2f(1.0, 0.0));
    }

    const lwlib::Point2f viewPos(void)
    {
        lwlib::Point2f pos = Object::getPos(player_mp);

        if (reachEnabled())
        {
            pos = reachPos();
        }

        return pos;
    }

    bool reachEnabled(void)
    {
        return (weapon() == wp_flag || weapon() == wp_fists);
    }

    double reach(void)
    {
        const int periodX2 = 6 * 4;
        const int period = periodX2 / 2;
        const int atkTics = gamestate.attacktics_mp;
        const double dist = 0.4;
        return (atkTics < period ? 
            (double)atkTics / period :
            (double)(periodX2 - atkTics) / period) * dist;
    }

    const lwlib::Point2f reachPos(void)
    {
        lwlib::Point2f pos = Object::getPos(player_mp);

        const double r = reach();
        if (r != 0)
        {
            const lwlib::Point2fp dir = vec2f_fixed(viewDir() * r);
            ClipMove(player_mp, dir.v[0], dir.v[1]);

            const lwlib::Point2f oldPos = pos;
            pos = Object::getPos(player_mp);
            Object::setPos(player_mp, oldPos);
        }

        return pos;
    }
}

//===========================================================================

namespace GameMode
{
    namespace Lock
    {
        struct UnbeatenInfo state[GameMode::max];

        void victory(void)
        {
#ifdef SPEAR
            state[gamestate.mode].modeState = false;
#else
            UnbeatenInfo &unbeatenInfo = state[gamestate.mode];
            bool *episodeStates = 
                &unbeatenInfo.episodeStates[0];
            episodeStates[gamestate.episode] = false;

            const int maxEpisodes = lwlib_CountArray(
                unbeatenInfo.episodeStates);
            bool *episodeStatesEnd = episodeStates + maxEpisodes;
            if (std::find(episodeStates, episodeStatesEnd, true) ==
                episodeStatesEnd)
            {
                unbeatenInfo.modeState = false;
            }
#endif
        }

        bool isSet(GameMode::e mode)
        {
            return state[mode].modeState;
        }

        void setEpisodeStates(void)
        {
            for (int i = 0; i < 6; i++)
            {
            }
        }

        void setAll(void)
        {
            for (int i = 0; i < GameMode::max; i++)
            {
                UnbeatenInfo &unbeatenInfo = state[i];
                unbeatenInfo.modeState = true;

                const int maxEpisodes = lwlib_CountArray(
                    unbeatenInfo.episodeStates);
                std::fill(unbeatenInfo.episodeStates,
                    unbeatenInfo.episodeStates + maxEpisodes, true);
            }
        }
    }
}
