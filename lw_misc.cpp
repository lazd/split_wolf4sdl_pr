/*************************************************************************
** Wolf3D Legacy
** Copyright (C) 2012 by LinuxWolf - Team RayCAST
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**************************************************************************
** LinuxWolf Library for Wolf3D Legacy
*************************************************************************/

#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "lw_misc.h"

namespace lwlib
{
	char *Va(const char *format, ...)
	{
		static char string[lwlib_StringSize];
		va_list argptr;

		va_start(argptr, format);
		vsnprintf(string, lwlib_StringSize - 1, format, argptr);
		string[lwlib_StringSize - 1] = '\0';
		va_end(argptr);

		return string;
	}

	char *VaString(lwlib_String_t string, const char *format, ...)
	{
		va_list argptr;

		va_start(argptr, format);
		vsnprintf(string, lwlib_StringSize - 1, format, argptr);
		string[lwlib_StringSize - 1] = '\0';
		va_end(argptr);

		return string;
	}
}

void *lwlib_Calloc(size_t size)
{
	void *p;

	p = malloc(size);
	if (p != NULL)
	{
		memset(p, 0, size);
	}

	return p;
}

void *lwlib_CallocCopy(void *data, size_t size)
{
	void *copy;
	copy = lwlib_Calloc(size);
	memcpy(copy, data, size);
	return copy;
}

int lwlib_strncasecmp(const char *s1, const char *s2, int n)
{
	int c1, c2;

	do 
	{
		c1 = *s1++;
		c2 = *s2++;

		if (!n--)
			return 0;			// strings are equal until end point
		if (c1 != c2) 
		{
			if (c1 >= 'a' && c1 <= 'z')
				c1 -= ('a' - 'A');
			if (c2 >= 'a' && c2 <= 'z')
				c2 -= ('a' - 'A');
			if (c1 != c2)
				return -1;		// strings not equal
		}
	} while (c1);

	return 0;					// strings are equal
}

int lwlib_strcasecmp(const char *s1, const char *s2)
{
	return lwlib_strncasecmp(s1, s2, 0xFFFFFF);
}

int lwlib_IndexOf(int val, const int *values, int numValues)
{
	int i;

	if (values != NULL)
	{
		for (i = 0; i < numValues; i++)
		{
			if (values[i] == val)
			{
				break;
			}
		}
		if (i == numValues)
		{
			i = -1;
		}
	}
	else
	{
		i = -1;
	}

	return i;
}

char *lwlib_StringToLower(lwlib_String_t string)
{
	int i;

	for (i = 0; i < lwlib_StringSize && string[i] != '\0'; i++)
	{
		string[i] = tolower(string[i]);
	}

	return string;
}

char *lwlib_StringToUpper(lwlib_String_t string)
{
	int i;

	for (i = 0; i < lwlib_StringSize && string[i] != '\0'; i++)
	{
		string[i] = toupper(string[i]);
	}

	return string;
}

lwlib_Str_t lwlib_Str_ToLower(lwlib_Str_t a)
{
	lwlib_StringToLower(a.v);
	return a;
}

lwlib_Str_t lwlib_Str_ToUpper(lwlib_Str_t a)
{
	lwlib_StringToUpper(a.v);
	return a;
}
